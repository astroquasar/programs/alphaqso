__version__ = '4.1.0'
from .fitting import batch_run,fit_system
from .simulation import create_simulated_model
from .curveplot import curveplot
from .metals import mgfe,qlist
from .utils import *
from .vpfit import *
