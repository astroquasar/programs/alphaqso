.. title:: Docs

.. image:: _images/logo_alphaqso.png
   :target: index.html
   :width: 25%

.. raw:: html

   <br>

.. image:: https://img.shields.io/badge/License-MIT-blue.svg
   :target: https://gitlab.com/astroquasar/programs/alphaqso/-/raw/master/LICENSE
.. image:: https://badge.fury.io/py/alphaqso.svg
   :target: https://pypi.python.org/pypi/alphaqso/
.. image:: https://img.shields.io/pypi/dm/alphaqso
.. image:: https://zenodo.org/badge/DOI/10.5281/zenodo.598393.svg
   :target: https://doi.org/10.5281/zenodo.598393
	    
This documentation aims to provide detailed information regarding the use of the ALPHAQSO program. This program was built by `Vincent Dumont <https://vincentdumont.gitlab.io/>`_ and is part of the `AstroQuasar <https://astroquasar.gitlab.io/>`_ group's toolset available in our `GitLab group <https://gitlab.com/astroquasar/programs/alphaqso>`_.

Installation
------------

The package can be easily installed via the ``pip`` Python package manager as follows::

  sudo pip install alphaqso

Once installed, the software can be executed simply by typing ``alphaqso`` on the terminal. If the path to a readable spectrum is correct, this will open an interactive matplotlib window displaying all the metal transitions regions (from either the default or user-defined list).

License Agreement
-----------------

.. literalinclude:: ../LICENSE
   :language: html

Reporting issues
----------------

.. raw:: html

   <br>
   <a href="https://gitlab.com/astroquasar/programs/alphaqso/-/issues/new?issue%5Bmilestone_id%5D=" class="button3">
   <font color="#ffffff">Submit a ticket</font>
   </a>

If you find any bugs when running this program, please make sure to report them by clicking on the above link and submit a ticket on the software's official Gitlab repository.
	      
.. toctree::
   :hidden:

   alphadist/index

	    
