Distortion calculations
-----------------------

One of the main features implemented in this program is the ability to include long-range wavelength-scale distortions to quasar spectra and look at the impact on the absolute chi-square and varying alpha fits through VPFIT. Since this particular set of tools might be useful for many other research projects, we made a standalone Python script that can be executed without the need to import the whole ``alphaqso`` library. The download link below will allow you to download directly the script without having to clone the entire gitlab repository or install the whole library on pip (do right click and select `Save Link As...` to download the file):

:download:`Download script <https://gitlab.com/astroquasar/programs/alphaqso/-/raw/master/bin/alphaDist.py>`

Below we list all the functions and classes present in the ``alphaDist.py`` script. The links below will take you to more detailed descriptions of the functions and classes where all input parameters, returned variables and methods are listed along with links to the source code and some examples on how to execute the operation.

Common functions
''''''''''''''''

.. currentmodule:: alphaDist

.. autosummary::
   :toctree: generated/

   isfloat
   getshift

Main classes
''''''''''''

The 3 main operations (i.e. building the model, do the calculations, and plot the results) will be carried out by 3 different classes.

.. currentmodule:: alphaDist

.. autosummary::
	     
   DistModel
   AlphaDist
   PlotCurve
