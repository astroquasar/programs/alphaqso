:orphan:
   
AlphaDist
=========

.. currentmodule:: alphaDist

.. autoclass:: AlphaDist
   :no-inherited-members:

     None
     
   .. rubric:: Methods Summary

   .. autosummary::

      ~atominfo
      ~convert26to13
      ~create_model
      ~fit_system 
      ~makeatomlist
      ~write_model

   .. rubric:: Methods Documentation
   
   .. automethod:: atominfo
   .. automethod:: convert26to13
   .. automethod:: create_model
   .. automethod:: fit_system
   .. automethod:: makeatomlist
   .. automethod:: write_model
