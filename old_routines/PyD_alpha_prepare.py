#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def prepare(model):

    self.qvar  = np.empty((0,2))
    self.qini  = []
    self.qend  = []
    self.qred  = []
    self.qblue = []
    self.olap  = []
    self.ovlp  = '-'
    self.regtotal = self.regboth = self.regblue = self.regred = 0

    ''' Read fort.13 and store header information '''

    fort_header_old  = np.empty((0,8))
    fort_content_old = np.empty((0,8))

    if sys.argv[1]=='run' and '--previous' in sys.argv and self.slope!=self.distmid and self.distsep!=0:
        i         = self.slope - self.distsep if self.slope > self.distmid else self.slope+self.distsep
        path_dist = '0.000' if round(i,3)==0 else str('%.3f'%i).replace('-','m') if i<0 else 'p'+str('%.3f'%i)
        path_head = 'sims/'+self.simtest+'/model' if '--simulation' in sys.argv else 'model'
        path_head = self.fitdir+self.selection+'/'+self.model+'/'+path_head+'/header.dat'
        read_head = np.loadtxt(path_head,dtype='str',delimiter='\n',ndmin=1)
        path_fort = 'sims/'+self.simtest if '--simulation' in sys.argv else 'runs'
        path_fort = self.fitdir+self.selection+'/'+self.model+'/'+path_fort+'/'+self.test
        path_fort = path_fort if '--raijin' in sys.argv else path_fort+'/'+path_dist+'/'+model+'/'
        read_fort = open(path_fort+'/'+model+'_fit.13','r')
    else:
        path_head = 'sims/'+self.simtest+'/model' if '--simulation' in sys.argv else 'model'
        path_head = self.fitdir+self.selection+'/'+self.model+'/'+path_head+'/'
        read_head = np.loadtxt(path_head+'header.dat',dtype='str',delimiter='\n',ndmin=1)
        read_fort = open(path_head+model+'.13','r')
        
    read_fort = [line.strip() for line in read_fort]

    ''' Check vp_setup.dat if the alpha column is present '''

    novars = 4
    vpset = self.fitdir+self.selection+'/'+self.model+'/'+path_head+'/vp_setup.dat'
    if os.path.exists(vpset):
        for line in np.loadtxt(vpset,dtype=str,delimiter='\n',comments='!'):
            if 'novars' in line.lower() and line.split()[1]==3:
                novars = 3

    i = flag = comp = twoq = 0
    
    while i < len(read_fort):

        if flag==2 and (read_fort[i]=='' or (read_fort[i].split()[0]=='>>' and read_fort[i].split()[-3]=='1')):
            break
        
        if read_fort[i]=='*':
            flag = flag+1
            i = offset = i + 1
        
        if flag==1 and read_fort[i][0]!='!':
            vals = read_fort[i].replace('!',' ').split()
            val0 = 'data/'+vals[0].replace('data/','').replace('../','')
            val1 = int(vals[1])
            val2 = '%.2f'%float(vals[2])
            val3 = '%.2f'%float(vals[3])
            val4 = vals[4].split('=')[0]+'='+str('%5.8f'%float(vals[4].split('=')[1]))
            val5 = read_head[i-offset].split()[0]
            val6 = '' if len(read_head[i-offset].split())==1 else read_head[i-offset].split()[1]
            val7 = '' if len(read_head[i-offset].split())==1 else read_head[i-offset].split()[2]
            fort_header_old = np.vstack((fort_header_old,[val0,val1,val2,val3,val4,val5,val6,val7]))
                
        if flag==2 and read_fort[i][0]!='!':

            vals = read_fort[i].split()
            val0 = vals[0]+' '+vals[1] if len(vals[0])==1 else vals[0]
            k = 1 if len(vals[0])==1 else 0
            val1 = '%.5f'%float(vals[k+1][:-2]+re.compile(r'[^\d.-]+').sub('',vals[k+1][-2:]))
            if '--colfix' in sys.argv:
                val1 = val1+'FF'
            else:
                val1 = val1+" ".join(re.findall("[a-zA-Z]+",vals[k+1][-2:]))
            zabs = float(vals[k+2][:-2]+re.compile(r'[^\d.-]+').sub('',vals[k+2][-2:]))
            val2 = '%.7f'%zabs
            val2 = val2+" ".join(re.findall("[a-zA-Z]+",vals[k+2][-2:]))
            val3 = '%.4f'%float(vals[k+3][:-2]+re.compile(r'[^\d.-]+').sub('',vals[k+3][-2:]))
            val3 = val3+" ".join(re.findall("[a-zA-Z]+",vals[k+3][-2:]))

            div  = 10**(-6) if 'E-0' in vals[k+4] else 1
            val4 = '0.000' if '*' in vals[k+4] else '%.3f'%(float(vals[k+4][:-2]+re.compile(r'[^\d.-]+').sub('',vals[k+4][-2:]))/div)
            val4 = val4+" ".join(re.findall("[a-zA-Z]+",vals[k+4][-2:]))
            twoq = 1 if vals[k+4][-2]=='q' and twoq==0 else twoq
                
            val5 = '%.2f'%float(vals[k+5][:-2]+re.compile(r'[^\d.-]+').sub('',vals[k+5][-2:]))
            val5 = val5+" ".join(re.findall("[a-zA-Z]+",vals[k+5][-2:]))
            val6 = '%.2E'%float(vals[k+6][:-2]+re.compile(r'[^\d.-]+').sub('',vals[k+6][-2:]))
            val6 = val6+" ".join(re.findall("[a-zA-Z]+",vals[k+6][-2:]))
            val7 = str(int(float(vals[k+7][:-2]+re.compile(r'[^\d.-]+').sub('',vals[k+7][-2:]))))
            val7 = val7+" ".join(re.findall("[a-zA-Z]+",vals[k+7][-2:]))
            fort_content_old = np.vstack((fort_content_old,[val0,val1,val2,val3,val4,val5,val6,val7]))
            comp = comp+1 if vals[0] not in ['H','>>','<<','<>','__','??'] else comp
            
        i = i + 1

    self.ncomp = comp
    self.twoq  = twoq
        
    ''' Store selected fort.13 header, and shifts '''

    self.mgflag     = 0
    store_shift     = []
    fort_header_new = np.empty((0,8))
    
    for i in range (len(fort_header_old)):

        self.armflag = 'null'
        trans = fort_header_old[i,-3]
        wrest = float(self.atominfo(trans)[1])
        shift = self.getshift(float(fort_header_old[i,2]),\
                              float(fort_header_old[i,3]),\
                              self.slope)

        if self.instrument=='uves':

            cond1 = self.arm=='all'
            cond2 = self.armflag==self.arm
            cond3 = self.armflag in self.arm
            if cond1 or cond2 or cond3 or cond4:
                fort_header_new = np.vstack((fort_header_new,fort_header_old[i]))
                store_shift.append(shift)
                if '--mgisotope' in sys.argv and 'Mg' in fort_header_old[i,-3]:
                    self.mgflag = 1
                if fort_header_old[i,-2]!='external':
                    self.qvar = np.vstack((self.qvar,[trans,wrest]))
                if fort_header_old[i,-2]=='overlap':
                    self.ovlp  = 'o'

            self.qini.append(wrest)
            if self.armflag=='blue':
                self.qblue.append(wrest)
                self.qend.append(wrest)
            if self.armflag=='red':
                self.qred.append(wrest)
                self.qend.append(wrest)
            if self.armflag=='overlap':
                self.olap.append(fort_header_old[i,-3])

        else:

            fort_header_new = np.vstack((fort_header_new,fort_header_old[i]))
            store_shift.append(shift)
            if '--mgisotope' in sys.argv and 'Mg' in fort_header_old[i,-3]:
                self.mgflag = 1
            if fort_header_old[i,-2]!='external':
                self.qvar = np.vstack((self.qvar,[fort_header_old[i,-3],wrest]))
            if fort_header_old[i,-2]=='overlap':
                self.ovlp = 'o'

    if len(self.qvar)==0:
        self.qmin = self.qmax = self.qvar
    else:
        qlist     = np.array(self.qvar[:,1],dtype='float')
        qlist_min = np.where(qlist==min(qlist))[0][0]
        qlist_max = np.where(qlist==max(qlist))[0][0]
        self.qmin = self.qvar[qlist_min,0].replace('_','')
        self.qmax = self.qvar[qlist_max,0].replace('_','')
        self.qvar = str(int(abs(max(qlist)-min(qlist))))

    ''' Store selected fort.13 content, and shift components '''
    
    fort_content_new = np.empty((0,8))
    mgcomps = np.empty((0,8))
    
    for i in range (len(fort_content_old)):
        
        if '--mgisotope' in sys.argv and 'Mg' in fort_content_old[i,0]:
            mgcomps = np.vstack((mgcomps,fort_content_old[i]))
        else:
            fort_content_new = np.vstack((fort_content_new,fort_content_old[i]))        

    ''' Implement Mg and Aw components '''

    if '--mgisotope' in sys.argv and self.mgflag==1:
        for p in range (len(mgcomps)):
            fort_content_new = np.vstack((fort_content_new,mgcomps[p]))        
            fort_content_new[-1,1] = re.compile(r'[^\d.-]+').sub('',fort_content_new[-1,1])+'x'
        for p in range (len(mgcomps)):
            fort_content_new = np.vstack((fort_content_new,mgcomps[p]))
            fort_content_new[-1,0] = fort_content_new[-1,0].replace('Mg','Aw')
            if p==0 and len(mgcomps)>1:
                fort_content_new[-1,1] = re.compile(r'[^\d.-]+').sub('',fort_content_new[-1,1])+'%'
            else:
                fort_content_new[-1,1] = re.compile(r'[^\d.-]+').sub('',fort_content_new[-1,1])+'X'
            fort_content_new[-1,2] = fort_content_new[-1,2].upper()
            fort_content_new[-1,3] = fort_content_new[-1,3].upper()
            fort_content_new[-1,4] = fort_content_new[-1,4].upper()
            fort_content_new[-1,5] = fort_content_new[-1,5].upper()

    ''' Implement fix shift values in fort arrays '''
    
    if self.distortion not in ['','0.000']:
        for p in range (len(store_shift)):
            shift = ['>>','1.00000FF','0.0000000FF',store_shift[p]+'FF','0.000FF','0.00','0.00E+00',p+1]
            fort_content_new = np.vstack((fort_content_new,shift))

    self.fort_header_new = fort_header_new
    self.fort_content_new = fort_content_new

    ''' Output action '''

    skip1 = ( self.arm=='red' and len(self.qred)==0 )
    skip2 = ( self.arm=='blue' and len(self.blue)==0 )
    skip3 = ( self.arm=='redblue' and len(self.qred)==0 and len(self.blue)==0 )
    skip4 = ( sys.argv[1] in ['run','check'] and '--mgisotope' in sys.argv and self.mgflag==0 )
    skip5 = ( sys.argv[1]=='run' and '--catchup' in sys.argv and os.path.exists(self.distpath+'/thermal'+self.modflag+'/fort.26')==True )
#    skip6 = ( sys.argv[1]=='results' and float(self.qvar)<1000 )
    skip7 = ( sys.argv[1]=='results' and self.selection in self.outliers )

    if skip1 or skip2 or skip3 or skip4 or skip5 or skip7:
        return 'skip'
    else:
        return 'proceed'
    
#==================================================================================================================
