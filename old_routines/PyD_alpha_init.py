#!/usr/bin/env python

#==================================================================================================================

import pylab,matplotlib,os,sys,re,math,datetime,operator,random,ctypes,binascii
import numpy                         as np
import matplotlib                    as mpl
import matplotlib.dates              as mdates
import matplotlib.pyplot             as plt
from matplotlib.pyplot               import *
from pylab                           import *
from matplotlib                      import rc
from matplotlib._png                 import read_png
from matplotlib.offsetbox            import AnnotationBbox, OffsetImage
from mpl_toolkits.axes_grid1         import make_axes_locatable
from scipy                           import optimize,stats
from scipy.optimize                  import curve_fit
from matplotlib.backends.backend_pdf import PdfPages
from time                            import clock
from scipy.ndimage                   import gaussian_filter1d
if os.getenv('HOME')=='/home/z3399255':
    import pandas                    as pd
if os.getenv('HOME')=='/Users/vincent':
    import pandas                    as pd
    import theano.tensor             as t
    from astropy.io                  import fits
    from mpl_toolkits.basemap        import Basemap
    
#==================================================================================================================

from PyD_alpha_calc                  import *     # Perform spherical distances calculations
from PyD_alpha_checkchisq            import *     # Check alpha measurements among all distortion models
from PyD_alpha_checkuvesarm          import *     # Tabulate q varibility in UVES systems based on arm selection
from PyD_alpha_checkuveshead         import *     # List wavelength settings used over all UVES science exposures
from PyD_alpha_checkuvesreg          import *     # Check exposure coverage for each fitting region
from PyD_alpha_commits               import *     # List all the commits and command examples made in this code
from PyD_alpha_createfit13           import *     # Create fort.13 of final results based on fort.18
from PyD_alpha_createlist            import *     # Create list of all the systems to fit
from PyD_alpha_createsimsimple       import *     # Create simple simulated spectra
from PyD_alpha_createsimcomplex      import *     # Create complex simulated spectra
from PyD_alpha_fit                   import *     # Run VPFIT to perform Voigt profile fitting
from PyD_alpha_fitdipole             import *     # Run alphafit
from PyD_alpha_fitgaussian           import *     # Gaussian fit of the distortion results
from PyD_alpha_fitlinear             import *     # Linear fit of the distortion results
from PyD_alpha_fitparabola           import *     # Parabolic fit of the distortion results
from PyD_alpha_fitwmean              import *     # Calculate and plot weighted and unweighted mean slopes
from PyD_alpha_getatominfo           import *     # Store atomic data from atom.dat
from PyD_alpha_getchisquare          import *     # Get chi-square from each da/a measurement
from PyD_alpha_getdipolechisq        import *     # Get normalised chi square from dipole alpha results
from PyD_alpha_getmag                import *     # Get dipole amplitude from alphafit results
from PyD_alpha_getmetals             import *     # Get dipole amplitude from alphafit results
from PyD_alpha_getndf                import *     # Get number degree of freedoms from all da/a measurements
from PyD_alpha_getresults            import *     # Save results of all da/a measurements in array
from PyD_alpha_getshift              import *     # Get distortion correction value to apply
from PyD_alpha_getsigmarand          import *     # Get sigma random from alphafit results
from PyD_alpha_getdistresults        import *     # Get reults from all distortion slope
from PyD_alpha_help                  import *     # Display list of commands
from PyD_alpha_ltscommon             import *     # Common code from Julian's code
from PyD_alpha_ltsdipole             import *     # Calculate LTS for dipole results
from PyD_alpha_ltslinear             import *     # Calculate LTS for slope results
from PyD_alpha_plot4papers           import *     # Create specific plots for publicationsf
from PyD_alpha_plot4talks            import *     # Plot figure for talk
from PyD_alpha_plotalpha             import *     # Plot da/a vs. distance to dipole and sky map
from PyD_alpha_plotcurves            import *     # Plot distortion curve for all absorption systems
from PyD_alpha_plotchitest           import *     # Plot curves using different chi-square jump discarding criteria
from PyD_alpha_plotcompare           import *     # gif plots of all alpha/chisq/ndf values over all distortions
from PyD_alpha_plotcompking          import *     # Compare values between Murphy and King
from PyD_alpha_plotfunction          import *     # Plot distortion function for any absorption system
from PyD_alpha_plotdiscard           import *     # Plot best global chi square fit versus number of systems discarded
from PyD_alpha_plothiresamplitude    import *     # Plot HIRES amplitude-type distortions by redshift samples
from PyD_alpha_plothiresslope        import *     # Plot HIRES slope-type distortions by redshift samples
from PyD_alpha_plotmetals            import *     # Plot zabs vs. transition rest-wavelength for all systems
from PyD_alpha_plotexample           import *     # Plot figure with both Whitmore and our model
from PyD_alpha_plotmodels            import *     # Plots all the distortion models for UVES and HIRES
from PyD_alpha_plotsimcurve          import *     # Plot turbulent chi-square curves from simulated spectra
from PyD_alpha_plotslopes            import *     # Analytic plot of distortion slopes
from PyD_alpha_plotuvesarms          import *     # Plot UVES distortions for combined, blue and red arms
from PyD_alpha_plotwidth             import *     # Plot average equivalent width against number of components
from PyD_alpha_prepare               import *     # Check header and prepare new fort.13 and header files
from PyD_alpha_rcode                 import *     # Run Julian King's R code and plot results
from PyD_alpha_readfort18            import *     # Read fort.26 and store chisq, alpha, chisqnu, and ndf
from PyD_alpha_run                   import *     # Setup to run VPFIT on all the systems
from PyD_alpha_supercomputer         import *     # Setup and process data in NCI Raijin cluster
from PyD_alpha_various               import *     # Various test and cleaning routines
from PyD_alpha_voigt                 import *     # Voigt profile
from PyD_alpha_whitedwarf            import *     # Distortion analysis on White Dwarf spectrum

#==================================================================================================================

if len(sys.argv)==1:
    help()

#==================================================================================================================

rc('font', size=2, family='sans-serif')
rc('axes', labelsize=8, linewidth=0.2)
rc('legend', fontsize=2, handlelength=10)
rc('xtick', labelsize=10)
rc('ytick', labelsize=10)
rc('lines', lw=0.2, mew=0.2)
rc('grid', linewidth=0.2)

#==================================================================================================================

home        = os.getenv('HOME')+'/ASTRO/analysis/alpha'
home        = os.getenv('HOME')+'/Volumes/ASTRO_HOME/ASTRO/analysis/alpha' if '--server'   in sys.argv else home
home        = '/media/removable/ASTRO/analysis/alpha'                      if '--external' in sys.argv else home
home        = '/short/gd5/ASTRO/analysis/alpha'                            if '--raijin'   in sys.argv else home

alphapath   = '/home/561/vxd561/ASTRO/code/PyD_alpha/' if '--raijin'   in sys.argv else ''
vpfitpath   = '/home/561/vxd561/ASTRO/code/vpfit10/'   if '--raijin'   in sys.argv else ''

fitdir      = home+'/systems/'
wdrep       = home+'/systems/G191-B2B/'
Crep        = home+'/dipole/'
Rrep        = home+'/alpha_analysis/'
rundir      = home+'/runs/'
uveshead    = home+'/headers/'
compdir     = home+'/multiplots/compare/'
here        = os.getenv('PWD')
pathdata    = os.path.abspath(__file__).rsplit('/',1)[0] + '/data/'

skipabs     = []
discard     = []
keck        = ['HIRES_churchill','HIRES_sargent','HIRES_prochaska','HIRES_outram']
vlt         = ['UVES_squader']
murphy      = ['HIRES_murphy_A','HIRES_murphy_B1','HIRES_murphy_B2','HIRES_murphy_C']
outliers    = ['J194454+770552/2.84330/HIRES_sargent',
               'J220852-194400/1.01720/HIRES_sargent',
               'J220852-194400/2.07620/HIRES_sargent',
               'J000448-415728/1.54190/UVES_squader']
degensys    = ['J111113-080402/3.60770/UVES_squader',
               'J034943-381031/3.02470/HIRES_prochaska']
badwhit     = ['J200324-325145/2.03290/UVES_squader']
clippeduves = ['J104032-272749/1.38610/UVES_squader',
               'J010311+131617/2.30920/UVES_squader',
               'J234628+124900/2.17130/UVES_squader',
               'J010311+131617/1.79750/UVES_squader',
               'J044017-433308/2.04820/UVES_squader',
               'J055246-363727/1.74750/UVES_squader',
               'J040718-441014/2.59480/UVES_squader']

if os.getenv('HOME')=='/home/561/vxd561':

    curlist = np.genfromtxt(pathdata+'allsys.dat',names=True,dtype=object,comments='!')
    distsim = np.genfromtxt(pathdata+'sims.dat',names=True,dtype=object,comments='!')

else:
    
    coddam   = '/Users/vincent/ASTRO/cloud/Servers/UNSW/database/coddam/data/'
    coddam   = pathdata if os.getenv('HOME')=='/home/z3399255' else coddam
    coddam   = pd.ExcelFile(coddam+'coddam.xlsx')
    sheets   = coddam.sheet_names
    qsolist  = coddam.parse('QSO',header=0)
    dipole   = coddam.parse('dipole',header=0)
    curlist  = coddam.parse('Acur',header=0)
    publist  = coddam.parse('Ahist',header=0,skiprows=1)
    distres  = coddam.parse('distortions',header=0,skiprows=1)
    distsim  = coddam.parse('simulations',header=0)
    
atom        = makeatomlist(pathdata+'atom.dat')
uvesexp     = np.genfromtxt(pathdata+'uvesexp.dat',names=True,dtype=object,comments='!')
hiresexp    = np.genfromtxt(pathdata+'hiresexp.dat',names=True,dtype=object,comments='!')
uvesset     = np.genfromtxt(pathdata+'settings.dat',names=True,dtype=object,comments='!',skip_header=4)

done        = False
c           = 299792.458
alphara     = 17.3
alphadec    = -61
uvesblue    = 3900.
uvesred     = 5800.
zsample     = 'all'
zcut        = 1.6
wd_slope    = np.arange(-15,15.1,0.1)
hires_amp   = np.arange(0,400,50)

#==================================================================================================================

argument   = np.array(sys.argv, dtype='str')
distmid    = 0 if '--distmid' not in sys.argv else float(argument[np.where(argument=='--distmid')[0][0]+1])
distmin    = 0 if '--distmin' not in sys.argv else float(argument[np.where(argument=='--distmin')[0][0]+1])
distmax    = 0 if '--distmax' not in sys.argv else float(argument[np.where(argument=='--distmax')[0][0]+1])
distsep    = 0 if '--distsep' not in sys.argv else float(argument[np.where(argument=='--distsep')[0][0]+1])

if distmin < distmax <= distmid:
    distlist = np.arange(distmax,distmin-0.001,-distsep)
    distplot = sorted(distlist)
elif distmax > distmin >= distmid:
    distlist = np.arange(distmin,distmax+0.001,distsep)
    distplot = sorted(distlist)
elif distmin < distmid < distmax:
    distlist = np.hstack((np.arange(distmid,distmax+0.001,distsep),np.arange(distmid,distmin-0.001,-distsep)))
    distplot = sorted(np.delete(distlist,0))

binning    = 12          if '--bin'        not in sys.argv else   int(argument[np.where(argument=='--bin'       )[0][0]+1])
order      = 3           if '--order'      not in sys.argv else   int(argument[np.where(argument=='--order'     )[0][0]+1])
totrep     = 0           if '--repeat'     not in sys.argv else   int(argument[np.where(argument=='--repeat'    )[0][0]+1])
stdev      = 0           if '--stdev'      not in sys.argv else float(argument[np.where(argument=='--stdev'     )[0][0]+1])
slope      = 0           if '--distortion' not in sys.argv else float(argument[np.where(argument=='--distortion')[0][0]+1])
chisq      = 1.E-6       if '--chisq'      not in sys.argv else float(argument[np.where(argument=='--chisq'     )[0][0]+1])
snr        = None        if '--snr'        not in sys.argv else float(argument[np.where(argument=='--snr'       )[0][0]+1])
threshold  = None        if '--threshold'  not in sys.argv else float(argument[np.where(argument=='--threshold' )[0][0]+1])
wmin       = None        if '--wmin'       not in sys.argv else float(argument[np.where(argument=='--wmin'      )[0][0]+1])
wmax       = None        if '--wmax'       not in sys.argv else float(argument[np.where(argument=='--wmax'      )[0][0]+1])
xmin       = None        if '--xmin'       not in sys.argv else float(argument[np.where(argument=='--xmin'      )[0][0]+1])
xmax       = None        if '--xmax'       not in sys.argv else float(argument[np.where(argument=='--xmax'      )[0][0]+1])
ymin       = None        if '--ymin'       not in sys.argv else float(argument[np.where(argument=='--ymin'      )[0][0]+1])
ymax       = None        if '--ymax'       not in sys.argv else float(argument[np.where(argument=='--ymax'      )[0][0]+1])
mode       = None        if '--mode'       not in sys.argv else float(argument[np.where(argument=='--mode'      )[0][0]+1])
sample     = None        if '--sample'     not in sys.argv else   str(argument[np.where(argument=='--sample'    )[0][0]+1])
instrument = None        if '--instrument' not in sys.argv else   str(argument[np.where(argument=='--instrument')[0][0]+1])
simulation = None        if '--simulation' not in sys.argv else   str(argument[np.where(argument=='--simulation')[0][0]+1])
plot       = None        if '--plot'       not in sys.argv else   str(argument[np.where(argument=='--plot'      )[0][0]+1])
model      = 'original'  if '--model'      not in sys.argv else   str(argument[np.where(argument=='--model'     )[0][0]+1])
selection  = 'published' if '--selection'  not in sys.argv else   str(argument[np.where(argument=='--selection' )[0][0]+1])
vpversion  = '10'        if '--vpfit'      not in sys.argv else   str(argument[np.where(argument=='--vpfit'     )[0][0]+1])
arm        = 'all'       if '--arm'        not in sys.argv else   str(argument[np.where(argument=='--arm'       )[0][0]+1])
shape      = 'slope'     if '--shape'      not in sys.argv else   str(argument[np.where(argument=='--shape'     )[0][0]+1])

instrument = 'uves'        if 'UVES' in selection    else 'hires'              if 'HIRES' in selection else instrument
tilt       = '0.000'       if round(distmid,3)==0    else 'm%.3f'%abs(distmid) if distmid<0            else 'p%.3f'%abs(distmid)
distortion = '0.000'       if round(slope,3)==0      else 'm%.3f'%abs(slope)   if slope<0              else 'p%.3f'%abs(slope)
model      = 'original'    if model=='original'      else 'latest'             if model=='latest'      else 'model%02i'%float(model)
deviation  = '0.00'        if round(stdev,2)==0      else '%.2f'%stdev
deviation  = deviation+'T' if '--expind' in sys.argv else deviation

vpcommand  = vpfitpath+'vpfit'+vpversion
atomdir    = pathdata+'atom.dat'
atomdir    = pathdata+'atom_mgisotope.dat' if '--mgisotope' in sys.argv else atomdir
atomdir    = pathdata+'atom_murphy.dat'    if '--murphy'    in sys.argv else atomdir
vpfsetup   = 'vp_setup_king.dat' if vpversion=='9.5-king' else 'vp_setup_hires.dat' if instrument=='hires' else 'vp_setup_uves.dat'
vpfsetup   = np.loadtxt(pathdata+vpfsetup,dtype=str,delimiter='\n',comments='!')
vpfsetup   = np.hstack((vpfsetup,['chisqthres %.E 2 %.E'%(chisq,chisq)])) if vpversion!='9.5-king' else vpfsetup

test       = 'v'+vpversion+'_chisq%.E'%chisq
test       = test+'_'+arm            if instrument=='uves'  and '--simulation' not in sys.argv     else test
test       = test+'_'+shape          if instrument=='hires' and (slope!=0 or distsep!=0)           else test
test       = test+'_step%.E'%distsep if distsep!=0                                                 else test
test       = test+'_prev'            if '--previous'   in sys.argv and (slope!=0 or distsep!=0)    else test
test       = test+'_jw'              if '--whitmore'   in sys.argv and (slope!=0 or distsep!=0)    else test
test       = test+'_murphy'          if '--murphy'     in sys.argv                                 else test
test       = test+'_null'            if '--simulation' not in sys.argv and '--noalpha' in sys.argv else test

if '--simulation' in sys.argv:
    simtest = simulation+'_'+tilt     
    simtest = simtest+'_'+deviation if '--expind' in sys.argv else simtest
    simtest = simtest+'_snr%.E'%snr if '--snr'    in sys.argv else simtest

#test = test+'_mg'              if '--mgisotope'   in sys.argv    else test
#test = test+'_noalpha'         if '--noalpha'     in sys.argv    else test
#test = test+'_colfix'          if '--colfix'      in sys.argv    else test
#test = test+'_fddhires'        if '--fdd'         in sys.argv    else test
#test = test+'_fddhires2'       if '--fdd2'        in sys.argv    else test
#test = test+'_final'           if '--finalmodel'  in sys.argv    else test

if '--test' in sys.argv:
    k = np.where(argument=='--test')[0][0]
    test = argument[k+1]

modflag = ''
#modflag = modflag+'_'+str(totrep)+'-0'            if totrep!=0 and sys.argv[1]=='run' else modflag
#modflag = modflag+'_'+str(totrep)+'-'+str(totrep) if totrep!=0 and sys.argv[1]!='run' else modflag

#==================================================================================================================
