#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def various():

    if self.mode==1:
    
        ''' Print out the original and latest symlink target for each system '''
        
        for i in range(len(self.curlist)):
            system   = self.curlist['system'][i]
            original = os.path.realpath(self.fitdir+system+'/original/')[-2:]
            original = '-' if original=='al' else original
            latest   = os.path.realpath(self.fitdir+system+'/latest/')[-2:]
            latest   = '-' if latest=='st' else latest
            print original,'\t',latest

    if self.mode==2:

        ''' Check which of the completed fits contain both Zn and Cr '''
        
        zncr = 0
        for i in range(len(self.curlist)):
            system = self.curlist['system'][i]
            if self.curlist['status'][i]=='completed':
                if os.path.exists(self.fitdir+system+'/model00/model/header.dat'):
                    header = np.loadtxt(self.fitdir+system+'/model00/model/header.dat',dtype=str,delimiter='\n')
                else:
                    header = np.loadtxt(self.fitdir+system+'/model00/original/header.dat',dtype=str,delimiter='\n')
                zn,cr  = 0,0
                for j in range(len(header)):
                    zn = 1 if 'external' not in header[j] and 'Zn' in header[j].split()[0] else zn
                    cr = 1 if 'external' not in header[j] and 'Cr' in header[j].split()[0] else cr
                if zn==cr==1:
                    print system
                    zncr += 1
        print zncr

    if self.mode==3:
    
        ''' Check which systems have nearby companions within 0.001 in redshift space '''
        
        n=1
        syslist = np.empty((0,3))
        for i in range(1,len(self.curlist)):
            qso1  = str(self.curlist['system'][i-1]).split('/')[0]
            zabs1 = float(str(self.curlist['system'][i-1]).split('/')[1])
            qso2  = str(self.curlist['system'][i]).split('/')[0]
            zabs2 = float(str(self.curlist['system'][i]).split('/')[1])
            if qso1==qso2 and abs(zabs1-zabs2)<=0.001 and self.curlist['status'][i]=='recorded' and self.curlist['dataset'][i]!='multiple':
                print n,'-',self.curlist['system'][i]
                n+=1

    if self.mode==4:
            
        ''' Create model 05, remove da/a columns for each latest model and print anchor ion component '''
        
        for i in range(1,len(self.curlist)):
            system = str(self.curlist['system'][i])
            if str(self.curlist['status'][i])!='recorded':
                print '|- Processing:',system
                os.system('mkdir -p '+self.fitdir+system+'/model05/model/')
                if os.path.exists(self.fitdir+system+'/model05/model/data')==True:
                    os.system('rm '+self.fitdir+system+'/model05/model/data')
                os.system('ln -s ../../data/ '+self.fitdir+system+'/model05/model/')
                #os.system('cp /Users/vincent/Downloads/atom.dat '+self.fitdir+system+'/model05/model/atom.dat')
                #if str(self.curlist['instrument'][i])=='UVES':
                #    os.system('cp /Users/vincent/Downloads/uves.dat '+self.fitdir+system+'/model05/model/vp_setup.dat')
                #if str(self.curlist['instrument'][i])=='HIRES':
                #    os.system('cp /Users/vincent/Downloads/hires.dat '+self.fitdir+system+'/model05/model/vp_setup.dat')
                header = np.loadtxt(self.fitdir+system+'/latest/model/header.dat',delimiter='\n',dtype=str,ndmin=1)
                outhead = open(self.fitdir+system+'/model05/model/header.dat','w')
                for head in header:
                    outhead.write(head+'\n')
                outhead.close()
                for model in ['thermal','turbulent']:
                    infort = open(self.fitdir+system+'/latest/model/'+model+'.13','r')
                    outfort = open(self.fitdir+system+'/model05/model/'+model+'.13','w')
                    tie = '-'
                    star = 0
                    for line in infort:
                        if len(line.split())==0:
                            break
                        elif '*' in line:
                            if line.split()[0][0]!='!':
                                outfort.write(line)
                            star += 1
                        elif star==1 and line.split()[0][0]!='!':
                            outfort.write(line)
                        elif star==2 and line.split()[0][0]!='!':
                            m = ' 0.00   0.00E+00  0\n' if model=='thermal' else ' 0.00   1.00E+00  0\n'
                            if len(line.split()[0])==1:
                                a = line.index(line.split()[5])
                                b = line.index(' 0.00 ') if ' 0.00 ' in line else line.index(' 1.00 ')
                                outfort.write(line[:a-1]+m)#line[b-1:])
                                if 'q' in line.split()[5] and tie=='-':
                                    tie = line.split('!')[-1].strip()
                                elif 'q' in line.split()[5] and tie!='-':
                                    tie = tie+' / '+line.split('!')[-1].strip()
                            else:
                                a = line.index(line.split()[4])
                                b = line.index(' 0.00 ') if ' 0.00 ' in line else line.index(' 1.00 ')
                                outfort.write(line[:a-1]+m)#line[b-1:])
                                if 'q' in line.split()[4] and tie=='-':
                                    tie = line.split('!')[-1].strip()
                                elif 'q' in line.split()[4] and tie!='-':
                                    tie = tie+' / '+line.split('!')[-1].strip()
                    infort.close()
                    outfort.close()
                    os.chdir(self.fitdir+system+'/model05/model/')
                    #print self.fitdir+system+'/model05/model/'
                    #model = 'thermal' if model=='thermal' else 'turbulent'
                    #os.system('PyD_velplot '+model+'.13 --header header.dat --details --getwave --version vpfit10 --fit --output '+model)

    if self.mode==5:

        ''' Copy all latest version of published King+Murphy sample '''
        
        os.chdir('/Users/vincent/Documents/')
        os.system('mkdir spectra/UVES_squader')
        os.system('cp '+self.fitdir+'/spectra/UVES_squader/* spectra/UVES_squader/')
        for i in range(len(self.publist)):
            destin = 'systems/'+self.publist['system'][i]
            print destin
            os.system('mkdir -p '+destin)
            os.system('cp '+self.fitdir+self.publist['system'][i]+'/latest/model/thermal.13   '+destin)
            os.system('cp '+self.fitdir+self.publist['system'][i]+'/latest/model/turbulent.13 '+destin)
            os.system('cp '+self.fitdir+self.publist['system'][i]+'/latest/model/vp_setup.dat '+destin)
            os.system('cp '+self.fitdir+self.publist['system'][i]+'/latest/model/header.dat   '+destin)
            os.system('cp '+self.fitdir+self.publist['system'][i]+'/latest/model/atom.dat     '+destin)
            oldpath = os.path.realpath(self.fitdir+self.publist['system'][i]+'/data/')
            newpath = oldpath.split('alpha/systems/')[-1]
            if os.path.exists(newpath)==False:
                os.system('mkdir -p '+newpath)
                os.system('cp '+oldpath+'/* '+newpath)
            os.system('ln -s ../../../../'+newpath+' systems/'+self.publist['system'][i]+'/data')

    if self.mode==6:

        ''' Copy latest model for each system '''
        
        path = '/Users/vincent/Documents/systems/'
        os.system('mkdir -p '+path)
        os.chdir(path)
        
        for i in range(len(self.curlist)):
            system = str(self.curlist['system'][i])
            if str(self.curlist['status'][i])!='recorded':
                print '|- Processing:',system
                model = 'model%02i'%float(self.curlist['latest'][i])
                os.system('mkdir -p '+path+system+'/'+model+'/model')
                datapath = os.path.realpath(self.fitdir+system+'/data').replace('/Users/vincent/ASTRO/analysis/alpha/systems/','../../../')
                os.system('ln -s '+datapath+' '+path+system+'/data')
                os.system('cp '+self.fitdir+system+'/'+model+'/model/header.dat   '+path+system+'/'+model+'/model')
                os.system('cp '+self.fitdir+system+'/'+model+'/model/thermal.13   '+path+system+'/'+model+'/model')
                os.system('cp '+self.fitdir+system+'/'+model+'/model/turbulent.13 '+path+system+'/'+model+'/model')
                
    if self.mode==7:
        
        ''' Copy model for each system used for simulations '''
        
        path = '/Users/vincent/Documents/systems/'
        os.system('mkdir -p '+path)
        os.chdir(path)
        
        for i in range(len(self.distsim)):
            system = str(self.distsim['system'][i])
            print '|- Processing:',system
            model  = 'model%02i'%float(self.distsim['model'][i])
            os.system('mkdir -p '+path+system+'/'+model+'/model')
            datapath = os.path.realpath(self.fitdir+system+'/data').replace('/Users/vincent/ASTRO/analysis/alpha/systems/','../../../')
            os.system('ln -s '+datapath+' '+path+system+'/data')
            os.system('cp '+self.fitdir+system+'/'+model+'/model/header.dat   '+path+system+'/'+model+'/model')
            os.system('cp '+self.fitdir+system+'/'+model+'/model/thermal.13   '+path+system+'/'+model+'/model')
            os.system('cp '+self.fitdir+system+'/'+model+'/model/turbulent.13 '+path+system+'/'+model+'/model')
            os.system('cp '+self.fitdir+system+'/'+model+'/model/thermal.18   '+path+system+'/'+model+'/model')
            os.system('cp '+self.fitdir+system+'/'+model+'/model/turbulent.18 '+path+system+'/'+model+'/model')
            os.system('cp '+self.fitdir+system+'/'+model+'/model/thermal.26   '+path+system+'/'+model+'/model')
            os.system('cp '+self.fitdir+system+'/'+model+'/model/turbulent.26 '+path+system+'/'+model+'/model')

    if self.mode==8:

        ''' Create simlinks to original CJ systems '''
        
        path = '/Users/vincent/Documents/checking/'
        os.system('mkdir -p '+path)
        for i in range(len(self.curlist)):
            system = str(self.curlist['system'][i])
            if str(self.curlist['status'][i])!='recorded' and self.curlist['00'][i]=='CJ':
                print '|- Processing:',system
                os.system('ls '+self.fitdir+system+'/model* > list')
                for line in np.loadtxt('list',dtype=str,delimiter='\n'):
                    if system in line and 'model05' not in line:
                        syspath = path+'/'+system.replace('/','_')+'/'
                        os.system('mkdir -p '+syspath)
                        os.system('ln -s '+line.replace(':','/model')+' '+syspath+'/'+line.split('/')[-1][:-1])
                        #os.system('mkdir -p '+folder)
                        #os.chdir(line.replace(':','/model'))
                        #os.system('PyD_velplot thermal.13   --header header.dat --details --getwave --version vpfit10 --output thermal')
                        #os.system('PyD_velplot turbulent.13 --header header.dat --details --getwave --version vpfit10 --output turbulent')
                        #os.system('cp header.dat    '+path)
                        #os.system('cp thermal.13    '+path)
                        #os.system('cp thermal.18    '+path)
                        #os.system('cp thermal.26    '+path)
                        #os.system('cp thermal.pdf   '+path)
                        #os.system('cp turbulent.13  '+path)
                        #os.system('cp turbulent.18  '+path)
                        #os.system('cp turbulent.26  '+path)
                        #os.system('cp turbulent.pdf '+path)
            
    if self.mode==9:

        ''' Create folder of published King+Murphy systems with chunks and velocity plots '''
        
        for i in range(len(self.publist)):
            
            system = str(self.publist['system'][i])
            print '|- Processing:',system
            
            #target = self.fitdir+'/'+system+'/model05/model/'
            #destin = '/Users/vincent/Documents/pubsys/'+system+'/'
            #os.system('mkdir -p '+destin)
            #os.system('mkdir -p '+destin+'/chunks')
            #if os.path.exists(target+'atom.dat')==False:
            #    os.system('cp /Users/vincent/Documents/atom.dat '+target)
            #if os.path.exists(target+'vp_setup.dat')==False:
            #    os.system('cp /Users/vincent/Documents/vp_setup.dat '+target)
            #os.chdir(target)
            #os.system('cp '+target+'header.dat    '+destin)
            #os.system('cp '+target+'thermal.13    '+destin)
            #os.system('cp '+target+'turbulent.13  '+destin)
            #os.system('cp '+target+'chunks/*      '+destin+'/chunks/')
            
            target = self.fitdir+'/'+system+'/'
            os.system('find '+target+' -name turbulent_fit.pdf -type f > list')
            plots = np.loadtxt('list',ndmin=1,dtype=str,delimiter='\n')
            destin = '/Users/vincent/Documents/velplots/'
            os.system('mkdir -p '+destin)
            if len(plots)>0:
                os.system('cp '+plots[0]+' '+destin+system.replace('/','--')+'.pdf')
            else:
                print system
            #os.system('PyD_velplot thermal.13 --header header.dat --details --getwave --version vpfit10 --output thermal')

    if self.mode==10:

        ''' Check if all systems present in the repository are tabulated in CoDDAM and vice-versa '''
        
        systems = [str(self.curlist['system'][i]) for i in range(len(self.curlist))]
        os.system('find '+self.fitdir+' -mindepth 3 -maxdepth 3 | sort > list')
        syslist = np.loadtxt('list',dtype=str)
        for line in syslist:
            quasar = line.split('/')[-3]
            zabs   = line.split('/')[-2]
            sample = line.split('/')[-1]
            if 'UVES_' in sample or 'HIRES_' in sample:
                system = quasar+'/'+zabs+'/'+sample
                if system not in systems:
                    print system
                    
        for i in range(len(self.curlist)):
            system = str(self.curlist['system'][i])
            if str(self.curlist['status'][i])!='recorded' and os.path.exists(self.fitdir+system)==False:
                print str(self.curlist['system'][i])
            
#==================================================================================================================
