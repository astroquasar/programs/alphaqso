#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def plotcompgif():

    if self.compflag=='chisq':
        plot1_limit = 0.5
        plot1_ticks = [-0.4,-0.3,-0.2,-0.1,0,0.1,0.2,0.3,0.4]
        plot2_limit = 1
        plot2_ticks = [-0.8,-0.6,-0.4,-0.2,0,0.2,0.4,0.6,0.8]
    else:
        plot1_limit = 10
        plot1_ticks = [-8.0,-6.0,-4.0,-2.0,0,2.0,4.0,6.0,8.0]
        plot2_limit = 5
        plot2_ticks = [-4.0,-3.0,-2.0,-1.0,0,1.0,2.0,3.0,4.0]

    rc('xtick', labelsize=7)
    rc('ytick', labelsize=7)

    gifcreate = 'cd ./ && convert -delay 40 -density 170 -loop 0 '
    nullres   = self.rundir+'/'+self.instrument+'/'+self.selection+'/'+self.test+'/0.000/results.dat'
    nullres   = np.genfromtxt(nullres,names=True,dtype=object,comments='!')
            
    idx = 0
    for i in self.loop:

        strslope = '0.000' if round(i,3)==0 else str('%.3f'%i).replace('-','m') if '-' in str(i) else 'p'+str('%.3f'%i)
        pathdir  = self.rundir+'/'+self.instrument+'/'+self.selection+'/'+self.test+'/'+strslope+'/results.dat'
        if os.path.exists(pathdir)==True:

            print self.instrument+' | '+self.test+' | '+strslope

            fig = figure(figsize=(11,8))
            plt.subplots_adjust(left=0.07, right=0.95, bottom=0.05, top=0.94, hspace=0, wspace=0)
            
            ax1 = plt.subplot(311,xlim=[0,len(nullres)+1],ylim=[-plot1_limit,plot1_limit])
            ax1.get_xaxis().set_visible(False)
            ax1.yaxis.set_major_locator(plt.FixedLocator(plot1_ticks))
            ax1.axhline(y=0,ls='dotted',color='black')
            text(len(nullres)+1,1.1*plot1_limit,'$k =$'+'{:>8}'.format('%.3f'%float(round(i,3))),ha='right',fontsize=10)
            ylabel(r'$'+self.complabel+'_{\mathrm{ther},0} - '+self.complabel+'_{\mathrm{ther},k}$')
            ax2 = plt.subplot(312,xlim=[0,len(nullres)+1],ylim=[-plot1_limit,plot1_limit])
            ax2.get_xaxis().set_visible(False)
            ax2.yaxis.set_major_locator(plt.FixedLocator(plot1_ticks))
            ax2.axhline(y=0,ls='dotted',color='black')
            ylabel(r'$'+self.complabel+'_{\mathrm{turb},0} - '+self.complabel+'_{\mathrm{turb},k}$')
            ax3 = plt.subplot(313,xlim=[0,len(nullres)+1],ylim=[-plot2_limit,plot2_limit])
            ax3.xaxis.set_major_locator(plt.NullLocator())
            ax3.yaxis.set_major_locator(plt.FixedLocator(plot2_ticks))
            ax3.axhline(y=0,ls='dotted',color='black')
            ylabel(r'$'+self.complabel+'_{\mathrm{ther},k} - '+self.complabel+'_{\mathrm{turb},k}$')
            xlabel('Systems')
            
            output = np.genfromtxt(pathdir,names=True,dtype=object,comments='!')

            for k in range (len(output)):

                p = np.where((nullres['sample']==output['sample'][k]) \
                                 & (nullres['qso']==output['qso'][k]) \
                                 & (nullres['z_em']==output['z_em'][k]) \
                                 & (nullres['z_abs']==output['z_abs'][k]))[0][0]

                if (output['ther_alpha'][k] not in ['0.000','-'] or output['ther_error'][k] not in ['0.000','-']) \
                        and abs(float(nullres['ther_'+self.compflag][p])-float(output['ther_'+self.compflag][k]))<plot1_limit:
                    ax1.scatter(k+1,float(nullres['ther_'+self.compflag][p])-float(output['ther_'+self.compflag][k]),s=10,edgecolors='none')
                    ax1.text(k+1,float(nullres['ther_'+self.compflag][p])-float(output['ther_'+self.compflag][k]),\
                                 '  '+str(int(k+1)),rotation='90',ha='center',va='bottom',fontsize=4)
                elif output['ther_alpha'][k] not in ['0.000','-'] or output['ther_error'][k] not in ['0.000','-']:
                    ax1.scatter(k+1,0,s=10,edgecolors='none',color='orange')
                    ax1.text(k+1,0,'  '+str(int(k+1)),rotation='90',ha='center',va='bottom',fontsize=4)
                else:
                    ax1.scatter(k+1,0,s=10,edgecolors='none',color='red')
                    ax1.text(k+1,0,'  '+str(int(k+1)),rotation='90',ha='center',va='bottom',fontsize=4)

                if (output['turb_alpha'][k] not in ['0.000','-'] or output['turb_error'][k] not in ['0.000','-']) \
                        and abs(float(nullres['turb_'+self.compflag][p])-float(output['turb_'+self.compflag][k]))<plot1_limit:
                    ax2.scatter(k+1,float(nullres['turb_'+self.compflag][p])-float(output['turb_'+self.compflag][k]),s=10,edgecolors='none')
                    ax2.text(k+1,float(nullres['turb_'+self.compflag][p])-float(output['turb_'+self.compflag][k]),\
                                 '  '+str(int(k+1)),rotation='90',ha='center',va='bottom',fontsize=4)
                elif output['turb_alpha'][k] not in ['0.000','-'] or output['turb_error'][k] not in ['0.000','-']:
                    ax2.scatter(k+1,0,s=10,edgecolors='none',color='orange')
                    ax2.text(k+1,0,'  '+str(int(k+1)),rotation='90',ha='center',va='bottom',fontsize=4)
                else:
                    ax2.scatter(k+1,0,s=10,edgecolors='none',color='red')
                    ax2.text(k+1,0,'  '+str(int(k+1)),rotation='90',ha='center',va='bottom',fontsize=4)

                if (output['ther_alpha'][k] not in ['0.000','-'] or output['ther_error'][k] not in ['0.000','-']) \
                        and (output['turb_alpha'][k] not in ['0.000','-'] or output['turb_error'][k] not in ['0.000','-']) \
                        and abs(float(output['ther_'+self.compflag][p])-float(output['turb_'+self.compflag][k]))<plot2_limit:
                    ax3.scatter(k+1,float(output['ther_'+self.compflag][p])-float(output['turb_'+self.compflag][k]),s=10,edgecolors='none')
                    ax3.text(k+1,float(output['ther_'+self.compflag][p])-float(output['turb_'+self.compflag][k]),\
                                 '  '+str(int(k+1)),rotation='90',ha='center',va='bottom',fontsize=4)
                elif (output['ther_alpha'][k] not in ['0.000','-'] or output['ther_error'][k] not in ['0.000','-']) \
                        and (output['turb_alpha'][k] not in ['0.000','-'] or output['turb_error'][k] not in ['0.000','-']):
                    ax3.scatter(k+1,0,s=10,edgecolors='none',color='orange')
                    ax3.text(k+1,0,'  '+str(int(k+1)),rotation='90',ha='center',va='bottom',fontsize=4)                    
                else:
                    ax3.scatter(k+1,0,s=10,edgecolors='none',color='red')
                    ax3.text(k+1,0,'  '+str(int(k+1)),rotation='90',ha='center',va='bottom',fontsize=4)
        
            savefig('compres_'+strslope+'.pdf')
            close(fig)

            gifcreate = gifcreate+' compres_'+strslope+'.pdf'

    gifcreate = gifcreate+' results.gif'
    os.system(gifcreate)
            
#==================================================================================================================

def plotcompalpha():

    def alphares(data):
        results = []
        for i in range(len(data)):
            if data['sample'][i]+'/'+data['qso'][i]+'/'+data['z_abs'][i] not in self.outliers:
                results.append(data['MoM_alpha'][i])
        return results

    rc('xtick', labelsize=7)
    rc('ytick', labelsize=7)

    uves,hires = [],[]
    for i in range (len(self.publist)):
        if 'UVES_king' in self.publist['path'][i] and self.publist['path'][i] not in self.outliers:
            uves.append(self.publist['alpha'][i])
        if 'HIRES_murphy' in self.publist['path'][i] and self.publist['path'][i] not in self.outliers:
            hires.append(self.publist['alpha_pub'][i])

    ''' UVES plots '''

    xmin,xmax = -20,25
    ymin,ymax = -20,30

    y01 = alphares(np.genfromtxt(self.rundir+'/uves/published/v9.5-king/results.dat',names=True,dtype=object,comments='!'))
    y02 = alphares(np.genfromtxt(self.rundir+'/uves/published/v9.5/results.dat',names=True,dtype=object,comments='!'))
    y03 = alphares(np.genfromtxt(self.rundir+'/uves/published/v10/results.dat',names=True,dtype=object,comments='!'))
    y04 = alphares(np.genfromtxt(self.rundir+'/uves/published/v10_1/results.dat',names=True,dtype=object,comments='!'))
    y05 = alphares(np.genfromtxt(self.rundir+'/uves/published/v10_2/results.dat',names=True,dtype=object,comments='!'))
    y06 = alphares(np.genfromtxt(self.rundir+'/uves/published/v10_chisq/results.dat',names=True,dtype=object,comments='!'))
    y07 = alphares(np.genfromtxt(self.rundir+'/uves/published/v10_final/results.dat',names=True,dtype=object,comments='!'))
    y08 = alphares(np.genfromtxt(self.rundir+'/uves/published/v10_final_1/results.dat',names=True,dtype=object,comments='!'))
    y09 = alphares(np.genfromtxt(self.rundir+'/uves/published/v10_final_2/results.dat',names=True,dtype=object,comments='!'))
    y10 = alphares(np.genfromtxt(self.rundir+'/uves/published/v10_final_chisq/results.dat',names=True,dtype=object,comments='!'))
    
    fig = figure(figsize=(8,11))
    plt.subplots_adjust(left=0.07, right=0.95, bottom=0.07, top=0.94, hspace=0, wspace=0)

    ax = plt.subplot(611,xlim=[xmin,xmax],ylim=[ymin,ymax])
    plot(uves,uves,ls='dotted',color='black',alpha=0.5)
    scatter(uves,uves,s=20,edgecolors='none',color='blue',alpha=0.5)
    ax.yaxis.set_major_locator(plt.FixedLocator([-10,0,10,20]))
    plt.setp(ax.get_xticklabels(), visible=False)
    ylabel('original')

    ax = plt.subplot(612,xlim=[xmin,xmax],ylim=[ymin,ymax])
    plot(uves,uves,ls='dotted',color='black',alpha=0.5)
    scatter(uves,y01,s=20,edgecolors='none',color='red',alpha=0.5)
#    ax.xaxis.set_major_locator(plt.NullLocator())
    plt.setp(ax.get_xticklabels(), visible=False)
    ax.yaxis.set_major_locator(plt.FixedLocator([-10,0,10,20]))
    ylabel('v9.5-king')

    ax = plt.subplot(613,xlim=[xmin,xmax],ylim=[ymin,ymax])
    plot(uves,uves,ls='dotted',color='black',alpha=0.5)
    scatter(uves,y02,s=20,edgecolors='none',color='green',alpha=0.5)
#    ax.xaxis.set_major_locator(plt.NullLocator())
    plt.setp(ax.get_xticklabels(), visible=False)
    ax.yaxis.set_major_locator(plt.FixedLocator([-10,0,10,20]))
    ylabel('v9.5')

    ax = plt.subplot(614,xlim=[xmin,xmax],ylim=[ymin,ymax])
    plot(uves,uves,ls='dotted',color='black',alpha=0.5)
    scatter(uves,y03,s=20,edgecolors='none',color='brown',alpha=0.5)
#    ax.xaxis.set_major_locator(plt.NullLocator())
    plt.setp(ax.get_xticklabels(), visible=False)
    ax.yaxis.set_major_locator(plt.FixedLocator([-10,0,10,20]))
    ylabel('v10')

    ax = plt.subplot(615,xlim=[xmin,xmax],ylim=[ymin,ymax])
    plot(uves,uves,ls='dotted',color='black',alpha=0.5)
    scatter(uves,y06,s=20,edgecolors='none',color='purple',alpha=0.5)
#    ax.xaxis.set_major_locator(plt.NullLocator())
    plt.setp(ax.get_xticklabels(), visible=False)
    ax.yaxis.set_major_locator(plt.FixedLocator([-10,0,10,20]))
    ylabel('v10_chisq')

    ax = plt.subplot(616,xlim=[xmin,xmax],ylim=[ymin,ymax])
    plot(uves,uves,ls='dotted',color='black',alpha=0.5)
    scatter(uves,y07,s=20,edgecolors='none',color='orange',alpha=0.5)
    ax.yaxis.set_major_locator(plt.FixedLocator([-10,0,10,20]))
    ylabel('v10_final')

    xlabel('Original da/a measurements')
    savefig('compalpha_uves.pdf')
    close(fig)

    ''' HIRES plots '''
            
    y01 = alphares(np.genfromtxt(self.rundir+'/hires/published/v9.5-king/results.dat',names=True,dtype=object,comments='!'))
    y02 = alphares(np.genfromtxt(self.rundir+'/hires/published/v9.5/results.dat',names=True,dtype=object,comments='!'))
    y03 = alphares(np.genfromtxt(self.rundir+'/hires/published/v10/results.dat',names=True,dtype=object,comments='!'))
    
    fig = figure(figsize=(8,11))
    plt.subplots_adjust(left=0.07, right=0.95, bottom=0.07, top=0.94, hspace=0, wspace=0)

    ax = plt.subplot(411,xlim=[-8,8],ylim=[-15,20])
    plot(hires,hires,ls='dotted',color='black',alpha=0.5)
    scatter(hires,hires,s=20,edgecolors='none',color='blue',alpha=0.5)
#    ax.xaxis.set_major_locator(plt.NullLocator())
    plt.setp(ax.get_xticklabels(), visible=False)
    ax.yaxis.set_major_locator(plt.FixedLocator([-10,-5,0,5,10,15]))
    ylabel('original')

    ax = plt.subplot(412,xlim=[-8,8],ylim=[-15,20])
    plot(hires[0:14]+hires[15:len(hires)],hires[0:14]+hires[15:len(hires)],ls='dotted',color='black',alpha=0.5)
    scatter(hires[0:14]+hires[15:len(hires)],y01[0:14]+y01[15:len(hires)],s=20,edgecolors='none',color='red',alpha=0.5)
#    ax.xaxis.set_major_locator(plt.NullLocator())
    plt.setp(ax.get_xticklabels(), visible=False)
    ax.yaxis.set_major_locator(plt.FixedLocator([-10,-5,0,5,10,15]))
    ylabel('v9.5-king')

    ax = plt.subplot(413,xlim=[-8,8],ylim=[-15,20])
    plot(hires,hires,ls='dotted',color='black',alpha=0.5)
    scatter(hires,y02,s=20,edgecolors='none',color='green',alpha=0.5)
#    ax.xaxis.set_major_locator(plt.NullLocator())
    plt.setp(ax.get_xticklabels(), visible=False)
    ax.yaxis.set_major_locator(plt.FixedLocator([-10,-5,0,5,10,15]))
    ylabel('v9.5')

    ax = plt.subplot(414,xlim=[-8,8],ylim=[-15,20])
    plot(hires,hires,ls='dotted',color='black',alpha=0.5)
    scatter(hires,y03,s=20,edgecolors='none',color='orange',alpha=0.5)
    ax.yaxis.set_major_locator(plt.FixedLocator([-10,-5,0,5,10,15]))
    ylabel('v10')

    xlabel('Original da/a measurements')
    savefig('compalpha_hires.pdf')
    close(fig)
            
#==================================================================================================================
