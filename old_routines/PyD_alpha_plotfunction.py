#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def plotfunctions():
    
    os.chdir(self.home+'/results/models/')
    
    slope = 0.2#0.117
    xmin,xmax = 2800,11000
    ymin,ymax = -400,400
    xlist = [3000,4000,5000,6000,7000,8000,9000,10000]
    ylist = [-300,-200,-100,0,100,200,300]

    publist   = [str(i) for i in self.dipole['system']]
    
    for i in range(len(self.curlist)):

        system = self.curlist['system'][i]
        quasar = system.split('/')[0]
        sample = system.split('/')[2]

        if sample=='UVES_squader' and (self.selection=='published' or system==self.selection):
            
            print system
            ions  = np.loadtxt(self.fitdir+system+'/model00/model/turbulent.13',delimiter='\n',dtype=str)
            trans = []
            flag  = 0
            for line in ions:
                if '*' in line and flag==1:
                    break
                elif '*' in line:
                    flag = 1
                else:
                    trans.append((float(line.split()[2])+float(line.split()[3]))/2)

            fig = figure(figsize=(7,4))
            plt.subplots_adjust(left=0.12, right=0.95, bottom=0.12, top=0.95, hspace=0, wspace=0.1)
            ax = plt.subplot(211,xlim=[xmin,xmax],ylim=[ymin,ymax])
    
            ''' Distortion models and fitting regions '''    
    
            exp1 = np.empty((0,4))
            exp2 = np.empty((0,4))

            pos  = np.where(self.uvesexp['name']==quasar)[0][0]
            for l in range (pos,len(self.uvesexp)):
                if self.uvesexp['name'][l]!=quasar:
                    break
                for k in range(len(self.uvesset)):
                    cond1 = self.uvesset['cent'][k]==self.uvesexp['cent'][l]
                    cond2 = self.uvesset['arm'][k]==self.uvesexp['arm'][l]
                    cond3 = self.uvesset['mode'][k]==self.uvesexp['mode'][l]
                    if cond1==cond2==cond3==True:
                        wbeg = 10*float(self.uvesset['TS_min'][k])
                        cent = 10*float(self.uvesset['cent'][k])
                        wend = 10*float(self.uvesset['TS_max'][k])
                        time = float(self.uvesexp['exptime'][l])
                        if cent not in [float(j) for j in exp1[:,2]]:
                            color = 'blue' if self.uvesexp['arm'][l]=='BLUE' else 'red'
                            exp1 = np.vstack((exp1,[color,wbeg,cent,wend]))
                        exp2 = np.vstack((exp2,[time,wbeg,cent,wend]))
                        break
            
            for k in range (len(exp1)):
                color   = exp1[k,0]
                wastart = float(exp1[k,1])
                wacent  = float(exp1[k,2])
                waend   = float(exp1[k,3])
                x       = np.arange(wastart,waend,1)
                y       = slope*(x-wacent)
                ax.plot(x,y,color=color,lw=1.5,zorder=3)
                ax.axvline(x=wastart,color='black',zorder=2,lw=1,ls='dotted')
                ax.axvspan(wastart,waend,facecolor='yellow',alpha=0.1,zorder=1)
                ax.axvline(x=waend,color='black',zorder=2,lw=1,ls='dotted')
                ax.scatter(wacent,0,color='black',marker='d',edgecolors='none',s=25,zorder=4)
                
            ax.axhline(y=0,ls='dotted',color='black')
            ax.xaxis.set_major_locator(plt.FixedLocator(xlist))
            ax.yaxis.set_major_locator(plt.FixedLocator(ylist))
            plt.setp(ax.get_xticklabels(), visible=False)
            ylabel('Velocity shift (m/s)',size=10)
        
            for i in range(len(trans)):
                ax.axvline(x=trans[i],ymin=0.85,ymax=0.9,color='red',lw=1)
        
            ''' Correction function '''
        
            ax = plt.subplot(212,xlim=[xmin,xmax],ylim=[ymin,ymax])
        
            for k in range (len(exp1)):
                wastart = float(exp1[k,1])
                wacent  = float(exp1[k,2])
                waend   = float(exp1[k,3])
                ax.axvline(x=wastart,color='black',zorder=2,lw=1,ls='dotted')
                ax.axvspan(wastart,waend,facecolor='yellow',alpha=0.1)
                ax.axvline(x=waend,color='black',zorder=2,lw=1,ls='dotted')
        
            x,y = np.arange(3000,10500,1),[]
            for wa in x:
                vdist,texp = [],[]
                for i in range (len(exp2)):
                    if exp2[i,1] < wa < exp2[i,3]:
                        vdist.append(slope*(wa-exp2[i,2]))
                        texp.append(np.sqrt(exp2[i,0]))
                if vdist!=[]:
                    y.append(sum(np.array(texp)*np.array(vdist))/sum(np.array(texp)))
                else:
                    y.append(None)
            ax.plot(x,y,color='black',lw=1.5,zorder=1)
            ax.axhline(y=0,ls='dotted',color='black')
            ax.xaxis.set_major_locator(plt.FixedLocator(xlist))
            ax.yaxis.set_major_locator(plt.FixedLocator(ylist))
        
            for i in range(len(trans)):
                ax.axvline(x=trans[i],ymin=0.85,ymax=0.9,color='red',lw=1)
        
            xlabel('Wavelength ($\AA$)',size=10)
            ylabel('Velocity shift (m/s)',size=10)
        
            savefig('systems/'+system.replace('/','_').replace('.','z')+'.pdf')
            plt.close(fig)

#==================================================================================================================

def plotfunctions_zncr():

    systems = np.array([['J005824+004113',1.072],
                        ['J010311+131617',2.309],
                        ['J010826-003724',1.371],
                        ['J022620-285750',1.023],
                        ['J084106+031206',1.342],
                        #['J102904+103901',1.622],
                        #['J123724+010615',1.305],
                        #['J175603+574848',1.971],
                        ['J220852-194400',1.921]])
    
    exposures = np.array([#[1.072,336,810, 3300],
                          #[1.072,379,829,37200],
                          [1.072,390,564,17300],
                          [1.072,470,760,12900],
                          #[2.309,424,869,31620],
                          [2.309,390,860, 7200],
                          [2.309,390,590,15400],
                          [1.371,390,580,14800],
                          [1.371,346,580,  860],
                          [1.371,437,860,  860],
                          [1.023,390,564,12800],
                          [1.023,390,580, 9000],
                          [1.023,437,760,13560],
                          #[1.342,414,848,26100],
                          [1.342,470,760,11580],
                          #[1.622,435,869,21600],
                          #[1.305,400,848,23600],
                          #[1.305,435,869, 1600],
                          #[1.971,435,869,31200],
                          #[1.971,318,605,23200],
                          [1.921,346,580, 9000],
                          [1.921,390,564,20700],
                          [1.921,437,860, 4200],
                          [1.921,455,850,19600]])

    slope = 0.12
    xmin,xmax = 3000,10000
    ymin,ymax = -250,250
    for i in range(len(systems)):

        zabs   = float(systems[i,1])
        system = systems[i,0]+'_'+str(zabs).replace('.','z')
        
        fig = figure(figsize=(7,4))
        plt.subplots_adjust(left=0.12, right=0.95, bottom=0.12, top=0.95, hspace=0, wspace=0.1)
        ax = plt.subplot(211,xlim=[xmin,xmax],ylim=[ymin,ymax])
    
        ''' Distortion models and fitting regions '''    
        
        exp1 = np.empty((0,4))
        exp2 = np.empty((0,4))
        
        pos  = np.where(exposures[:,0]==zabs)[0]
        for l in pos:
            time = float(exposures[l,3])
            blue = '%i'%exposures[l,1]
            red  = '%i'%exposures[l,2]
            for wmid in [blue,red]:
                k    = np.where(self.uvesset['cent']==wmid)[0][0]
                wbeg = 10*float(self.uvesset['TS_min'][k])
                cent = 10*float(self.uvesset['cent'][k])
                wend = 10*float(self.uvesset['TS_max'][k])
                if cent not in [float(j) for j in exp1[:,2]]:
                    color = 'blue' if wmid==blue else 'red'
                    exp1 = np.vstack((exp1,[color,wbeg,cent,wend]))
                exp2 = np.vstack((exp2,[time,wbeg,cent,wend]))
        
        for k in range (len(exp1)):
            color   = exp1[k,0]
            wastart = float(exp1[k,1])
            wacent  = float(exp1[k,2])
            waend   = float(exp1[k,3])
            x       = np.arange(wastart,waend,1)
            y       = slope*(x-wacent)
            ax.plot(x,y,color=color,lw=1.5,zorder=3)
            ax.axvline(x=wastart,color='black',zorder=2,lw=1,ls='dotted')
            ax.axvspan(wastart,waend,facecolor='yellow',alpha=0.1,zorder=1)
            ax.axvline(x=waend,color='black',zorder=2,lw=1,ls='dotted')
            ax.scatter(wacent,0,color='black',marker='d',edgecolors='none',s=25,zorder=4)
            
        ax.axhline(y=0,ls='dotted',color='black')
        ax.yaxis.set_major_locator(plt.FixedLocator(ylist))
        plt.setp(ax.get_xticklabels(), visible=False)
        ylabel('Velocity shift (m/s)',size=10)
        
#        for i in range(len(trans)):
#            ax.axvline(x=trans[i],ymin=0.85,ymax=0.9,color='red',lw=1)
        
        ''' Correction function '''
        
        ax = plt.subplot(212,xlim=[xmin,xmax],ylim=[ymin,ymax])
        
        for k in range (len(exp1)):
            wastart = float(exp1[k,1])
            wacent  = float(exp1[k,2])
            waend   = float(exp1[k,3])
            ax.axvline(x=wastart,color='black',zorder=2,lw=1,ls='dotted')
            ax.axvspan(wastart,waend,facecolor='yellow',alpha=0.1)
            ax.axvline(x=waend,color='black',zorder=2,lw=1,ls='dotted')
        
        x,y = np.arange(3000,10500,1),[]
        for wa in x:
            vdist,texp = [],[]
            for i in range (len(exp2)):
                if exp2[i,1] < wa < exp2[i,3]:
                    vdist.append(slope*(wa-exp2[i,2]))
                    texp.append(np.sqrt(exp2[i,0]))
            if vdist!=[]:
                y.append(sum(np.array(texp)*np.array(vdist))/sum(np.array(texp)))
            else:
                y.append(None)
        ax.plot(x,y,color='black',lw=1.5,zorder=1)
        ax.axhline(y=0,ls='dotted',color='black')
        ax.yaxis.set_major_locator(plt.FixedLocator(ylist))
        
        ax.axvline(x=2026.1376450*(zabs+1),ymin=0.85,ymax=0.9,color='red',lw=1)
        ax.axvline(x=2062.6610100*(zabs+1),ymin=0.85,ymax=0.9,color='red',lw=1)
        ax.axvline(x=2056.2568010*(zabs+1),ymin=0.85,ymax=0.9,color='red',lw=1)
        ax.axvline(x=2062.2359290*(zabs+1),ymin=0.85,ymax=0.9,color='red',lw=1)
        ax.axvline(x=2066.1638990*(zabs+1),ymin=0.85,ymax=0.9,color='red',lw=1)
        
        xlabel('Wavelength ($\AA$)',size=10)
        ylabel('Velocity shift (m/s)',size=10)
        
        savefig('chromiumzinc/'+system+'.pdf')
        plt.clf()

#==================================================================================================================
