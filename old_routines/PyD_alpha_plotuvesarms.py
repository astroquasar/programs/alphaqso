#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def plotuvesarms(instrument,distlist):

    os.chdir(self.home+'/results/first/')
    
    self.instrument = instrument
    self.distlist   = distlist
        
    fig = figure(figsize=(12,12))
    plt.subplots_adjust(left=0.08, right=0.95, bottom=0.08, top=0.93, hspace=0.2, wspace=0.2)

    print '\nUVES all systems combined\n'
    
    x,y,dy = getmag('all','')
    if '--reset' in sys.argv:
        xmin,xmax,ymin,ymax = min(x),max(x),min(y),max(y)
    else:
        xmin,xmax,ymin,ymax = -0.4,1,-0.5,2.5
    ax = plt.subplot(5,3,1,xlim=[xmin,xmax],ylim=[ymin,ymax])
    title('UVES all arms combined $\mathrm{(151}$ $\mathrm{systems)}$\n',fontsize=10)
    ax.errorbar(x,y,yerr=dy,fmt='o',ms=5,markeredgecolor='none',ecolor='grey',alpha=0.8)
    ax.axhline(y=0,ls='dotted',color='black')
    ylabel('Amplitude\n',fontsize=8)
#    fitparabola(x,y,ymin,ymax,xmin,xmax,0.2,0.6)

    x,y = getchisq('all','')
    if '--reset' in sys.argv:
        xmin,xmax,ymin,ymax = min(x),max(x),min(y),max(y)
    else:
        xmin,xmax,ymin,ymax = -0.1,0.3,300,500
    ax = plt.subplot(5,3,4,xlim=[xmin,xmax],ylim=[ymin,ymax])
    scatter(x,y,s=25,edgecolors='none',alpha=0.8)
    ax.axhline(y=0,ls='dotted',color='black')
    ylabel('Dipole fitting Chi-square\n',fontsize=8)
#    fitparabola(x,y,ymin,ymax,xmin,xmax,0,0.2,flag='saveonesig')

    x,y = getsigmarand('all','')
    if '--reset' in sys.argv:
        xmin,xmax,ymin,ymax = min(x),max(x),min(y),max(y)
    else:
        xmin,xmax,ymin,ymax = -0.1,0.3,0.5,2
    ax = plt.subplot(5,3,7,xlim=[xmin,xmax],ylim=[ymin,ymax])
    scatter(x,y,s=25,edgecolors='none',alpha=0.8)
    ax.axhline(y=0,ls='dotted',color='black')
    ylabel('Sigma-rand factor\n',fontsize=8)
#    fitparabola(x,y,ymin,ymax,xmin,xmax,-0.1,0.3,flag='plotonesig')

    x,y,dy = getglobchisq('all','')
    if '--reset' in sys.argv:
        xmin,xmax,ymin,ymax = min(x),max(x),min(y),max(y)
    else:
        xmin,xmax,ymin,ymax = -0.1,0.3,79000,83000
    ax = plt.subplot(5,3,10,xlim=[xmin,xmax],ylim=[ymin,ymax])
    scatter(x,y,s=25,edgecolors='none',alpha=0.8)
    ax.axhline(y=0,ls='dotted',color='black')
    ylabel('Global system fitting chi square\n',fontsize=8)
    a = x.index(0.00)
    b = x.index(0.07)
    c = x.index(0.08)
    d = x.index(0.14)
    e = x.index(0.15)
    bad = [a,b,c,d,e]
    x = np.delete(x,bad,0)
    y = np.delete(y,bad,0)
#    fitparabola(x,y,ymin,ymax,xmin,xmax,-0.2,0.4,flag='saveonesig')

    x,y,dy = getndf('all','')
    if '--reset' in sys.argv:
        xmin,xmax,ymin,ymax = min(x),max(x),min(y),max(y)
    else:
        xmin,xmax,ymin,ymax = -0.1,0.3,510,550
    ax = plt.subplot(5,3,13,xlim=[xmin,xmax],ylim=[ymin,ymax])
    scatter(x,y,s=25,edgecolors='none',alpha=0.8)
    ax.get_yaxis().get_major_formatter().set_useOffset(False)
    ax.axhline(y=0,ls='dotted',color='black')
    xlabel('\nApplied Distortion Slope',fontsize=8)
    ylabel('Average number of degrees of freedom\n',fontsize=8)

    #==============================================================================================================
    
    print '\nUVES blue arm transitions only\n'
    
    x,y,dy = getmag('blue','')
    ax = plt.subplot(5,3,2)
    title('UVES blue arm only $\mathrm{(23}$ $\mathrm{systems)}$\n',fontsize=10)
    ax.errorbar(x,y,yerr=dy,fmt='o',ms=5,markeredgecolor='none',ecolor='grey',alpha=0.8)
    ax.axhline(y=0,ls='dotted',color='black')

    x,y = getchisq('blue','')
    ax = plt.subplot(5,3,5)
    scatter(x,y,s=25,edgecolors='none',alpha=0.8)
    ax.axhline(y=0,ls='dotted',color='black')
    
    x,y = getsigmarand('blue','')
    ax = plt.subplot(5,3,8)
    scatter(x,y,s=25,edgecolors='none',alpha=0.8)
    ax.axhline(y=0,ls='dotted',color='blacsk')
    
    x,y,dy = getglobchisq('blue','')
    ax = plt.subplot(5,3,11)
    scatter(x,y,s=25,edgecolors='none',alpha=0.8)
    ax.axhline(y=0,ls='dotted',color='black')
    
    x,y,dy = getndf('blue','')
    xmin,xmax,ymin,ymax = min(x),max(x),min(y),max(y)
    ax = plt.subplot(5,3,14)
    scatter(x,y,s=25,edgecolors='none',alpha=0.8)
    ax.axhline(y=0,ls='dotted',color='black')
    xlabel('\nApplied Distortion Slope',fontsize=8)

    #==============================================================================================================
    
    print '\nUVES red arm transitions only\n'
    
    x,y,dy = getmag('red','')
    if '--reset' in sys.argv:
        xmin,xmax,ymin,ymax = min(x),max(x),min(y),max(y)
    else:
        xmin,xmax,ymin,ymax = -0.1,0.7,-1,5
    ax = plt.subplot(5,3,3,xlim=[xmin,xmax],ylim=[ymin,ymax])
    title('UVES red arm only $\mathrm{(122}$ $\mathrm{systems)}$\n',fontsize=10)
    ax.errorbar(x,y,yerr=dy,fmt='o',ms=5,markeredgecolor='none',ecolor='grey',alpha=0.8)
    ax.axhline(y=0,ls='dotted',color='black')
#    fitparabola(x,y,ymin,ymax,xmin,xmax,0.1,0.5)
    
    x,y = getchisq('red','')
    if '--reset' in sys.argv:
        xmin,xmax,ymin,ymax = min(x),max(x),min(y),max(y)
    else:
        xmin,xmax,ymin,ymax = -0.05,0.25,200,300
    ax = plt.subplot(5,3,6,xlim=[xmin,xmax],ylim=[ymin,ymax])
    scatter(x,y,s=25,edgecolors='none',alpha=0.8)
    ax.axhline(y=0,ls='dotted',color='black')
#    fitparabola(x,y,ymin,ymax,xmin,xmax,0,0.2,flag='saveonesig')

    x,y = getsigmarand('red','')
    if '--reset' in sys.argv:
        xmin,xmax,ymin,ymax = min(x),max(x),min(y),max(y)
    else:
        xmin,xmax,ymin,ymax = -0.05,0.25,0.85,1.15
    ax = plt.subplot(5,3,9,xlim=[xmin,xmax],ylim=[ymin,ymax])
    scatter(x,y,s=25,edgecolors='none',alpha=0.8)
    ax.axhline(y=0,ls='dotted',color='black')
#    fitparabola(x,y,ymin,ymax,xmin,xmax,0,0.2,flag='plotonesig')

    x,y,dy = getglobchisq('red','')
    ax = plt.subplot(5,3,12)
    scatter(x,y,s=25,edgecolors='none',alpha=0.8)
    ax.axhline(y=0,ls='dotted',color='black')
    
    x,y,dy = getndf('red','')
    xmin,xmax,ymin,ymax = min(x),max(x),min(y),max(y)
    ax = plt.subplot(5,3,15)
    scatter(x,y,s=25,edgecolors='none',alpha=0.8)
    ax.axhline(y=0,ls='dotted',color='black')
    xlabel('\nApplied Distortion Slope',fontsize=8)
    
    savefig('uves_arms.pdf')
    clf()

#==================================================================================================================
