#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def getsloperesults():

    qsos    = []
    results = np.empty((0,9))    
    data    = np.empty((0,18))
    for i in range(len(self.distres)):

        qso    = self.distres['system'][i].split('/')[0]
        zabs   = self.distres['system'][i].split('/')[1]
        model  = 'model%02i'%float(self.distres['model weight'][i])
        cond0  = self.distres['system'][i] not in self.degensys
        cond1  = self.distres['MoM slope'][i] not in ['-','degenerate','badwhit']# and abs(float(self.distres['MoM slope'][i]))<5
        cond2  = self.distres['instrument'][i].lower()==self.instrument
        cond3a = self.distres['whitmore'][i]=='yes' and '--whitmore' in sys.argv and self.distres['system'][i] not in self.badwhit
        cond3b = self.distres['whitmore'][i]=='no'  and '--whitmore' not in sys.argv
        
        if cond0 and cond1 and cond2 and (cond3a or cond3b):
            print qso,zabs
            iqso = np.where(np.logical_and(self.distres['name']==qso,self.distres['Thermal slope']!='-'))[0]
            flag = 0
            disp = []
            cent = []
            fort = np.loadtxt(self.fitdir+self.distres['system'][i]+'/'+model+'/model/thermal.13',dtype=str,delimiter='\n')
            for line in fort:
                if '*' in line:
                    flag += 1
                elif flag==1:
                    wmin = float(line.split()[2])
                    wmax = float(line.split()[3])
                    cent.append((wmin+wmax)/2.)
                    disp.append(2*(wmax-wmin)/(wmin+wmax)*self.c)

            # Estimating average observing date among the exposures
            
            if '--whitmore' in sys.argv:
                middate = float('nan')
            else:                
                date  = []
                idx   = np.where(self.uvesexp['name']==qso)[0]
                for j in idx:
                    for k in range(len(self.uvesset)):
                        cond1 = self.uvesset['cent'][k]==self.uvesexp['cent'][j]
                        cond2 = self.uvesset['arm'][k]==self.uvesexp['arm'][j]
                        cond3 = self.uvesset['mode'][k]==self.uvesexp['mode'][j]
                        if cond1==cond2==cond3==True:
                            wmin = 10*float(self.uvesset['TS_min'][k])
                            wmax = 10*float(self.uvesset['TS_max'][k])
                            break
                    for wmid in cent:
                        if wmin < wmid < wmax:
                            obsdate = self.uvesexp['dataset'][j].replace('UVES.','').replace('T',' ')
                            date.append(datetime.datetime.strptime(obsdate,'%Y-%m-%d %H:%M:%S.%f'))
                            break                    
                deltas  = [(date[k]-date[0]).days for k in range(len(date))]
                middate = (date[0]+datetime.timedelta(days=sum(deltas)/len(deltas))).date()
            
            therchisq  = float(self.distres['ther chisq individual'][i])
            therslope  = float(self.distres['Thermal slope'][i])
            thererror  = float(self.distres['Thermal error'][i])
            turbchisq  = float(self.distres['turb chisq individual'][i])
            turbslope  = float(self.distres['Turbulent slope'][i])
            turberror  = float(self.distres['Turbulent error'][i])
            momchisq   = float(self.distres['MoM chisq individual'][i])
            momslope   = float(self.distres['MoM slope'][i])
            momerror   = float(self.distres['MoM error'][i])
            ntrans     = float(self.distres['ntrans'][i])
            ntied      = float(self.distres['tied'][i])
            vdisp      = np.average(disp)
            z,ra,dec,d = calc_dist2dip(qso)
            data       = np.vstack((data,[middate,d,float(zabs),vdisp,ra,dec,therchisq,therslope,thererror,turbchisq,turbslope,turberror,momchisq,momslope,momerror,ntrans,ntied,i]))
            results    = np.vstack((results,[therslope,therslope/thererror**2,1/thererror**2,
                                             turbslope,turbslope/turberror**2,1/turberror**2,
                                             momslope ,momslope/momerror**2  ,1/momerror**2]))
            
    return data
            
#==================================================================================================================

def slopebin(data,isort):

    k = 1
    data = np.array(sorted(data,key=lambda col: col[isort]))
    bindata = np.empty((0,15))
    for i in range(0,len(data),self.binning):
        ilim      = i+self.binning if i+self.binning<=len(data) else len(data)
        if '--show' in sys.argv:
            print '\n\nbin',k,'\n'
            print 'Date        |  Distance    |  Redshift    |  vdisp     |  RA          |  DEC         |  Chisq     |  Thermal Slope              |  Chisq     |  Turbulent Slope            |  Chisq     |  MoM Slope                  |  Quasar'
            print '----------  |  ----------  |  ----------  |  --------  |  ----------  |  ----------  |  --------  |  -------------------------  |  --------  |  -------------------------  |  --------  |  -------------------------  |  --------------'
            for j in range(i,ilim):
                print '{:<10}  | '.format(str(data[j,0])),
                print '{:>10}  | '.format('%.6f'%data[j,1]),
                print '{:>10}  | '.format('%.6f'%data[j,2]),
                print  '{:>8}  | '.format('%.2f'%data[j,3]),
                print '{:>10}  | '.format('%.6f'%data[j,4]),
                print '{:>10}  | '.format( '%.6f'%data[j,5]),
                print  '{:>8}  |  {:>10} +/- {:>10}  | '.format('%.2f'%data[j,6],'%.6f'%data[j,7],'%.6f'%data[j,8]),
                print  '{:>8}  |  {:>10} +/- {:>10}  | '.format('%.2f'%data[j,9],'%.6f'%data[j,10],'%.6f'%data[j,11]),
                print  '{:>8}  |  {:>10} +/- {:>10}  | '.format('%.2f'%data[j,12],'%.6f'%data[j,13],'%.6f'%data[j,14]),
                print self.distres['name'][data[j,15]]
        middate   = min(data[i:ilim,0]) + (max(data[i:ilim,0]) - min(data[i:ilim,0]))/2
        distance  = np.average([data[j,1] for j in range(i,ilim)])
        redshift  = np.average([data[j,2] for j in range(i,ilim)])
        vdisp     = np.average([data[j,3] for j in range(i,ilim)])
        ra        = np.average([data[j,4] for j in range(i,ilim)])
        dec       = np.average([data[j,5] for j in range(i,ilim)]) 
        therchisq = sum([data[j,6] for j in range(i,ilim)])
        therslope = sum([data[j,7]/data[j,8]**2 for j in range(i,ilim)]) / sum([1/data[j,8]**2 for j in range(i,ilim)])
        thererror = 1 / np.sqrt(sum(1/data[j,8]**2 for j in range(i,ilim)))
        turbchisq = sum([data[j,9] for j in range(i,ilim)])
        turbslope = sum([data[j,10]/data[j,11]**2 for j in range(i,ilim)]) / sum([1/data[j,11]**2 for j in range(i,ilim)])
        turberror = 1 / np.sqrt(sum(1/data[j,11]**2 for j in range(i,ilim)))
        momchisq  = sum([data[j,12] for j in range(i,ilim)])
        momslope  = sum([data[j,13]/data[j,14]**2 for j in range(i,ilim)]) / sum([1/data[j,14]**2 for j in range(i,ilim)])
        momerror  = 1 / np.sqrt(sum(1/data[j,14]**2 for j in range(i,ilim)))
        if '--show' in sys.argv:
            print '----------  |  ----------  |  ----------  |  --------  |  ----------  |  ----------  |  --------  |  -------------------------  |  --------  |  -------------------------  |  --------  |  -------------------------  |  --------------'
            print '{:<10}  | '.format(str(middate)),
            print '{:>10}  | '.format('%.6f'%distance),
            print '{:>10}  | '.format('%.6f'%redshift),
            print  '{:>8}  | '.format('%.2f'%vdisp),
            print '{:>10}  | '.format('%.6f'%ra),
            print '{:>10}  | '.format('%.6f'%dec),
            print  '{:>8}  |  {:>10} +/- {:>10}  | '.format('%.2f'%therchisq,'%.6f'%therslope,'%.6f'%thererror),
            print  '{:>8}  |  {:>10} +/- {:>10}  | '.format('%.2f'%turbchisq,'%.6f'%turbslope,'%.6f'%turberror),
            print  '{:>8}  |  {:>10} +/- {:>10}  | '.format('%.2f'%momchisq ,'%.6f'%momslope ,'%.6f'%momerror ),
        bindata   = np.vstack((bindata,[middate,distance,redshift,vdisp,ra,dec,therchisq,therslope,thererror,turbchisq,turbslope,turberror,momchisq,momslope,momerror]))
        k += 1
    return bindata

#==================================================================================================================
 
def plotslopes():
    
    self.path = self.home+'/results/slopes/'
    os.chdir(self.path)
    
    folder = self.instrument+'_jw' if '--whitmore' in sys.argv else self.instrument
    data = getsloperesults()
        
    print '\n\nSorting by chi-squares\n'

    fig = figure(figsize=(12,6))
    plt.subplots_adjust(left=0.06, right=0.97, bottom=0.07, top=0.92, hspace=0, wspace=0)
    for i in [0,1,2]:
        xmin,xmax = -5,5
        title = 'Thermal' if i==0 else 'Turbulent' if i==1 else 'Method-of-Moment'
        ax = plt.subplot(1,3,i+1,xlim=[xmin,xmax],ylim=[0,3500])
        ax.errorbar(data[:,7+3*i],data[:,6+3*i],fmt='o',ms=5,markeredgecolor='none',ecolor='grey',alpha=0.4,color='black')
        ax.axvline(x=0,ls='dotted',color='black')
        xlabel('Best distortion slope',fontsize=10)
        if i!=0: plt.setp(ax.get_yticklabels(), visible=False)
        if i==0: ylabel('Absolute chi-square',fontsize=10)
        divider = make_axes_locatable(ax)
        ax2 = divider.append_axes("top",1.2,xlim=[xmin,xmax],ylim=[0,30])
        ax2.hist(data[:,7+3*i],bins=200,histtype='stepfilled',color='b',alpha=0.4,lw=0.2)
        setp(ax2.get_xticklabels(), visible=False)
        ax2.yaxis.set_major_locator(plt.FixedLocator(np.arange(0,31,10)))
        ax2.set_title(title,color='red',fontsize=10)
        if i!=0: plt.setp(ax2.get_yticklabels(), visible=False)       
    savefig(folder+'/slopes_chisq.pdf')
    clf()

    print '\n\nSorting by redshift\n'

    bindata = slopebin(data,2)
    fig = figure(figsize=(8,10))
    plt.subplots_adjust(left=0.1, right=0.95, bottom=0.05, top=0.95, hspace=0, wspace=0)
    for i in [0,1,2]:
        title = 'Thermal' if i==0 else 'Turbulent' if i==1 else 'Method-of-Moment'
        ax = plt.subplot(3,1,i+1,ylim=[-1,1])
        ax.errorbar(data[:,2],data[:,7+3*i],fmt='o',ms=5,markeredgecolor='none',ecolor='grey',alpha=0.4,color='black')
        ax.errorbar(bindata[:,2],bindata[:,7+3*i],fmt='o',ms=10,markeredgecolor='none',alpha=0.6,color='blue',elinewidth=3)
        t = text(3.4,0.8,title,color='red',weight='bold',fontsize=10,ha='right')
        t.set_bbox(dict(color='white', alpha=0.5, edgecolor=None))
        ax.axhline(y=0,ls='dotted',color='black')
        ax.axhline(y=-0.04,ls='dashed',color='red',lw=1)
        xlabel('Average absorption redshift',fontsize=10)
        ylabel('Best distortion slope',fontsize=10)
        t = text(3.4,0.8,title,color='red',weight='bold',fontsize=10,ha='right')
        t.set_bbox(dict(color='white', alpha=0.5, edgecolor=None))
        ax.yaxis.set_major_locator(plt.FixedLocator(np.arange(-0.8,0.9,0.2)))
        if i!=2: plt.setp(ax.get_xticklabels(), visible=False)
    savefig(folder+'/slopes_redshift.pdf')
    clf()

    print '\n\nSorting by distance to best fitting model\n'

    bindata = slopebin(data,1)
    fig = figure(figsize=(8,10))
    plt.subplots_adjust(left=0.1, right=0.95, bottom=0.05, top=0.95, hspace=0, wspace=0)
    for i in [0,1,2]:
        title = 'Thermal' if i==0 else 'Turbulent' if i==1 else 'Method-of-Moment'
        ax = plt.subplot(3,1,i+1,xlim=[0,180],ylim=[-1,1])
        ax.errorbar(data[:,1],data[:,7+3*i],fmt='o',ms=5,markeredgecolor='none',ecolor='grey',alpha=0.4,color='black')
        ax.errorbar(bindata[:,1],bindata[:,7+3*i],fmt='o',ms=10,markeredgecolor='none',alpha=0.6,color='blue',elinewidth=3)
        ax.axhline(y=0,ls='dotted',color='black')
        xlabel(r'$\Theta$, angle from dipole ('+str(self.alphara)+','+str(self.alphadec)+')',fontsize=10)
        ylabel('Best distortion slope',fontsize=10)
        t = text(170,0.8,title,color='red',weight='bold',fontsize=10,ha='right')
        t.set_bbox(dict(color='white', alpha=0.5, edgecolor=None))
        ax.yaxis.set_major_locator(plt.FixedLocator(np.arange(-0.8,0.9,0.2)))
        if i!=2: plt.setp(ax.get_xticklabels(), visible=False)
    savefig(folder+'/slopes_distance.pdf')
    clf()

    print '\n\nSorting by RA\n'

    bindata = slopebin(data,4)
    fig = figure(figsize=(8,10))
    plt.subplots_adjust(left=0.1, right=0.95, bottom=0.05, top=0.95, hspace=0, wspace=0)
    for i in [0,1,2]:
        title = 'Thermal' if i==0 else 'Turbulent' if i==1 else 'Method-of-Moment'
        ax = plt.subplot(3,1,i+1,xlim=[0,24],ylim=[-1,1])
        ax.errorbar(data[:,4],data[:,7+3*i],fmt='o',ms=5,markeredgecolor='none',ecolor='grey',alpha=0.4,color='black')
        ax.errorbar(bindata[:,4],bindata[:,7+3*i],fmt='o',ms=10,markeredgecolor='none',alpha=0.6,color='blue',elinewidth=3)
        ax.axhline(y=0,ls='dotted',color='black')
        xlabel('Average Right Ascension',fontsize=10)
        ylabel('Best distortion slope',fontsize=10)
        t = text(23,0.8,title,color='red',weight='bold',fontsize=10,ha='right')
        t.set_bbox(dict(color='white', alpha=0.5, edgecolor=None))
        ax.yaxis.set_major_locator(plt.FixedLocator(np.arange(-0.8,0.9,0.2)))
        if i!=2: plt.setp(ax.get_xticklabels(), visible=False)
    savefig(folder+'/slopes_ra.pdf')
    clf()

    print '\n\nSorting by DEC\n'

    bindata = slopebin(data,5)
    fig = figure(figsize=(8,10))
    plt.subplots_adjust(left=0.1, right=0.95, bottom=0.05, top=0.95, hspace=0, wspace=0)
    for i in [0,1,2]:
        title = 'Thermal' if i==0 else 'Turbulent' if i==1 else 'Method-of-Moment'
        ax = plt.subplot(3,1,i+1,xlim=[-90,90],ylim=[-1,1])
        ax.errorbar(data[:,5],data[:,7+3*i],fmt='o',ms=5,markeredgecolor='none',ecolor='grey',alpha=0.4,color='black')
        ax.errorbar(bindata[:,5],bindata[:,7+3*i],fmt='o',ms=10,markeredgecolor='none',alpha=0.6,color='blue',elinewidth=3)
        ax.axhline(y=0,ls='dotted',color='black')
        xlabel('Average Declinaison',fontsize=10)
        ylabel('Best distortion slope',fontsize=10)
        t = text(85,0.8,title,color='red',weight='bold',fontsize=10,ha='right')
        t.set_bbox(dict(color='white', alpha=0.5, edgecolor=None))
        ax.yaxis.set_major_locator(plt.FixedLocator(np.arange(-0.8,0.9,0.2)))
        if i!=2: plt.setp(ax.get_xticklabels(), visible=False)
    savefig(folder+'/slopes_dec.pdf')
    clf()

    print '\n\nSorting by dispersion\n'

    bindata = slopebin(data,4)
    fig = figure(figsize=(12,6))
    plt.subplots_adjust(left=0.06, right=0.95, bottom=0.07, top=0.95, hspace=0.25, wspace=0.25)
    for i in [0,1,2]:
        title = 'Thermal' if i==0 else 'Turbulent' if i==1 else 'Method-of-Moment'
        ax = plt.subplot(1,3,i+1,xlim=[-5,5],ylim=[0,450])
        ax.errorbar(data[:,7+3*i],data[:,3],fmt='o',ms=5,markeredgecolor='none',ecolor='grey',alpha=0.5,color='black')
        ax.set_title(title,color='red',fontsize=12)
        ax.axhline(y=0,ls='dotted',color='black')
        xlabel('Best distortion slope',fontsize=10)
        ylabel('Velocity dispersion',fontsize=10)
    savefig(folder+'/slopes_dispersion.pdf')
    clf()

    if '--whitmore' not in sys.argv:
    
        print '\n\nSorting by average date\n'
    
        bindata = slopebin(data,0)
        fig = figure(figsize=(8,10))
        plt.subplots_adjust(left=0.1, right=0.95, bottom=0.05, top=0.95, hspace=0, wspace=0)
        for i in [0,1,2]:
            title = 'Thermal' if i==0 else 'Turbulent' if i==1 else 'Method-of-Moment'
            ax = plt.subplot(3,1,1+i,xlim=[datetime.date(2000,1,1),datetime.date(2008,1,1)],ylim=[-1,1])
            ax.errorbar(data[:,0],data[:,7+3*i],fmt='o',ms=5,markeredgecolor='none',alpha=0.4,color='black')
            ax.errorbar(bindata[:,0],bindata[:,7+3*i],fmt='o',ms=10,markeredgecolor='none',alpha=0.6,color='blue',elinewidth=3)
            ax.axhline(y=0,ls='dotted',color='black')
            xlabel('Average observation date',fontsize=10)
            ylabel('Best distortion slope',fontsize=10)
            t = text(datetime.date(2007,8,1),0.8,title,color='red',weight='bold',fontsize=10,ha='right')
            t.set_bbox(dict(color='white', alpha=0.5, edgecolor=None))
            ax.yaxis.set_major_locator(plt.FixedLocator(np.arange(-0.8,0.9,0.2)))
            if i!=2: plt.setp(ax.get_xticklabels(), visible=False)
        savefig(folder+'/slopes_dates.pdf')
        clf()

#==================================================================================================================

def getdates():

    qsos = []
    data = np.empty((0,8))
    for i in range(len(self.distres)):
        
        qso    = self.distres['system'][i].split('/')[0]
        zabs   = self.distres['system'][i].split('/')[1]
        cond1  = self.distres['MoM slope'][i]!='-'
        cond2  = self.distres['instrument'][i].lower()==self.instrument
        cond3a = self.distres['whitmore'][i]=='yes' and '--whitmore' in sys.argv
        cond3b = self.distres['whitmore'][i]=='no'  and '--whitmore' not in sys.argv
        cond3  = qso not in qsos
        
        if cond1 and cond2 and (cond3a or cond3b):
            
            iqso = np.where(np.logical_and(self.distres['name']==qso,self.distres['Thermal slope']!='-'))[0]
            
            flag = 0
            cent = []
            fort = np.loadtxt(self.fitdir+self.distres['system'][i]+'/model00/model/thermal.13',dtype=str,delimiter='\n')
            for line in fort:
                if '*' in line:
                    flag += 1
                elif flag==1:
                    cent.append((float(line.split()[2])+float(line.split()[3]))/2.)

            date = []
            idx  = np.where(self.uvesexp['name']==qso)[0]
            for j in idx:
                for k in range(len(self.uvesset)):
                    cond1 = self.uvesset['cent'][k]==self.uvesexp['cent'][j]
                    cond2 = self.uvesset['arm'][k]==self.uvesexp['arm'][j]
                    cond3 = self.uvesset['mode'][k]==self.uvesexp['mode'][j]
                    if cond1==cond2==cond3==True:
                        wmin = 10*float(self.uvesset['TS_min'][k])
                        wmax = 10*float(self.uvesset['TS_max'][k])
                        break
                for wmid in cent:
                    if wmin < wmid < wmax:
                        obsdate = self.uvesexp['dataset'][j].replace('UVES.','').replace('T',' ')
                        date.append(datetime.datetime.strptime(obsdate,'%Y-%m-%d %H:%M:%S.%f'))
                        break
                    
            therslope  = float(self.distres['Thermal slope'][i])
            thererror  = float(self.distres['Thermal error'][i])
            turbslope  = float(self.distres['Turbulent slope'][i])
            turberror  = float(self.distres['Turbulent error'][i])
            momslope   = float(self.distres['MoM slope'][i])
            momerror   = float(self.distres['MoM error'][i])
            
            for obsdate in date:
                data   = np.vstack((data,[obsdate,therslope,thererror,turbslope,turberror,momslope,momerror,i]))

    return data
            
#==================================================================================================================

def bindates(data,isort):

    k = 1
    data = np.array(sorted(data,key=lambda col: col[isort]))
    bindata = np.empty((0,7))
    for i in range(0,len(data),self.binning):
        ilim      = i+self.binning if i+self.binning<=len(data) else len(data)
        middate   = min(data[i:ilim,0]) + (max(data[i:ilim,0]) - min(data[i:ilim,0]))/2
        therslope = sum([data[j,1]/data[j,2]**2 for j in range(i,ilim)]) / sum([1/data[j,2]**2 for j in range(i,ilim)])
        thererror = 1 / np.sqrt(sum(1/data[j,2]**2 for j in range(i,ilim)))
        turbslope = sum([data[j,3]/data[j,4]**2 for j in range(i,ilim)]) / sum([1/data[j,4]**2 for j in range(i,ilim)])
        turberror = 1 / np.sqrt(sum(1/data[j,4]**2 for j in range(i,ilim)))
        momslope  = sum([data[j,5]/data[j,6]**2 for j in range(i,ilim)]) / sum([1/data[j,6]**2 for j in range(i,ilim)])
        momerror  = 1 / np.sqrt(sum(1/data[j,6]**2 for j in range(i,ilim)))
        bindata   = np.vstack((bindata,[middate,therslope,thererror,turbslope,turberror,momslope,momerror]))
        k += 1
    return bindata


#==================================================================================================================

def plotdates():

    self.path = self.home+'/results/slopes/'
    os.chdir(self.path)
    
    data = getdates()
    bindata = bindates(data,0)
    fig = figure(figsize=(8,12))
    plt.subplots_adjust(left=0.1, right=0.9, bottom=0.05, top=0.95, hspace=0.25, wspace=0)
    for i in [0,1,2]:
        title = 'Thermal' if i==0 else 'Turbulent' if i==1 else 'Method-of-Moment'
        ax = plt.subplot(3,1,1+i,xlim=[datetime.date(2000,6,1),datetime.date(2009,8,1)],ylim=[-1,1])
        ax.errorbar(data[:,0],data[:,1+2*i],fmt='o',ms=3,markeredgecolor='none',alpha=0.2,color='black')
        ax.errorbar(bindata[:,0],bindata[:,1+2*i],yerr=bindata[:,2+2*i],fmt='o',ms=8,markeredgecolor='none',alpha=0.6,color='red',elinewidth=3)
        ax.set_title(title,color='red',fontsize=12)
        ax.axhline(y=0,ls='dotted',color='black')
        xlabel('Average observation date',fontsize=10)
        ylabel('Best distortion slope',fontsize=10)
    savefig(self.instrument+'/expdates.pdf')
    clf()

#==================================================================================================================
