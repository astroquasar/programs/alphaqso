#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def getdistresults(mode=None,fit=False,loop=False):

    self.path = self.home+'/results/curves/'
    os.chdir(self.path)
    
    target = self.selection
    if target=='published':
        self.out = open('temp.dat','w')
        
    systot = 0
    
    self.name = self.instrument + '-'
    self.name = self.name + 'sim_'       if '--simulation' in sys.argv   else self.name
    self.name = self.name + 'v10_slope'  if self.instrument=='hires'     else self.name
    self.name = self.name + 'v10_all'    if self.instrument=='uves'      else self.name
    self.name = self.name + '_jw'        if '--whitmore' in sys.argv     else self.name
    self.name = self.name + '_prev'#      if '--previous' in sys.argv    else self.name
    self.name = self.name + '-' + mode   if mode in ['global','model']   else self.name
    self.name = self.name + '-' + self.sample if self.sample!=None       else self.name
    
    sysidx = []
    for i in range(len(self.distres)):
        cond0 = self.selection in [self.distres['system'][i],'published']
        cond1 = self.distres['instrument'][i].lower()==self.instrument
        cond2 = self.distres['whitmore'][i]=='yes' and '--whitmore' in sys.argv
        cond3 = self.distres['whitmore'][i]=='no'  and '--whitmore' not in sys.argv
        cond4 = self.sample==None or self.sample in self.distres['system'][i]
        if cond0 and cond1 and (cond2 or cond3) and cond4:
            sysidx.append(i)
            
    print '\nGet',mode,'distortion results for',len(sysidx),'systems\n'
    
    data  = np.empty((0,4))
    chisq = np.empty((0,7))
    model = np.empty((0,4))
    
    for i in sysidx:
        
        curvemode = 'weight' if mode=='global' else 'individual'
        
        self.selection  = self.distres['system'][i]
        self.instrument = self.distres['instrument'][i].lower()
        self.wminther   = float(self.distres['thermal min_fit'][i])
        self.wmaxther   = float(self.distres['thermal max_fit'][i])
        self.wminturb   = float(self.distres['turbulent min_fit'][i])
        self.wmaxturb   = float(self.distres['turbulent max_fit'][i])
        self.wminmom    = float(self.distres['MoM min_fit'][i])
        self.wmaxmom    = float(self.distres['MoM max_fit'][i])
        self.distmin    = float(self.distres['min '+curvemode][i])
        self.distmax    = float(self.distres['max '+curvemode][i])
        self.distsep    = float(self.distres['step '+curvemode][i])
        self.model      = 'model%02i'%float(self.distres['model '+curvemode][i])
        self.test       = 'v10_chisq%.E'%float(self.distres['threshold '+curvemode][i])
        self.test       = self.test + '_all' if self.instrument=='uves' else self.test + '_slope'
        self.test       = self.test + '_step%.E'%self.distsep
        self.test       = self.test + '_prev'# if '--previous' in sys.argv else self.test
        self.test       = self.test + '_jw' if '--whitmore' in sys.argv else self.test

        if self.distmin < self.distmax <= 0:
            self.distlist = np.arange(self.distmax,self.distmin-0.001,-self.distsep)
            self.distplot = sorted(self.distlist)
        elif self.distmax > self.distmin >= 0:
            self.distlist = np.arange(self.distmin,self.distmax+0.001,self.distsep)
            self.distplot = sorted(self.distlist)
        elif self.distmin < 0 < self.distmax:
            self.distlist = np.hstack((np.arange(0,self.distmax+0.001,self.distsep),np.arange(0,self.distmin-0.001,-self.distsep)))
            self.distplot = sorted(np.delete(self.distlist,0))
            
        self.getchisquare()
    
        if mode=='curve':
            
            print '\n{0:<10}\n{1:<7} | {2:<8}\n'.format(self.selection,self.model,self.test)
            if loop==True and len(self.fitres)!=len(self.distplot):
                self.out.write('{0:<40}\t'.format(self.selection)+'{:>10}\t'.format('-')*15+'\n')
            elif loop==True:
                self.out.write('{:<40}\t'.format(self.selection))
            if len(self.fitres)==len(self.distplot):
                self.plotcurves(fit=fit,loop=loop)
                
        if mode=='global':
            
            cond1  = len(self.fitres)==len(self.distplot)
            cond2  = self.distres['parabola weight'][i]=='good'
            
            if cond1 and cond2:
                systot += 1
                if len(chisq)==0:
                    chisq = self.fitres[:,[0,1,2,5,6,9,10]]
                else:
                    chisq[:,1:] += self.fitres[:,[1,2,5,6,9,10]]
                    
            therslope = '-' if len(self.fitres[:,2])==0  else '%.2f'%np.mean(self.fitres[:,2])
            turbslope = '-' if len(self.fitres[:,6])==0  else '%.2f'%np.mean(self.fitres[:,6])
            momslope  = '-' if len(self.fitres[:,10])==0 else '%.2f'%np.mean(self.fitres[:,10])
            print '{0:>10}\t{1:>10}\t{2:>10}'.format(therslope,turbslope,momslope)
            #print '{0:>10}\t{1:>10}\t{2:>10}\t{3:<10}'.format(therslope,turbslope,momslope,self.selection)
            self.plotcurves(fit=fit,loop=loop)

        if mode=='model' and len(self.fitres)==len(self.distplot):
            
            for j in range(3):
                x    = self.fitres[:,0]
                y    = self.fitres[:,2+4*j]
                xfit = np.arange(-5,5,0.0001)
                yfit = self.fitparabola2(x,y,fitrange=xfit)
                if len(model)==0:
                    model = np.zeros((len(xfit),4),dtype=float)
                    model[:,0] = xfit
                model[:,j+1] += yfit
                
        if mode=='alpha' and len(self.fitres)>0:
            
            data = np.vstack((data,[self.distres['redshift'][i],self.distres['MoM alpha'][i],self.distres['MoM error'][i],self.distres['name'][i]]))
            
            if '--show' in sys.argv:
                print '{:>15}'.format(self.distres['redshift'][i]),
                print '{:>15}'.format(self.distres['MoM alpha'][i]),
                print '{:>15}'.format(self.distres['MoM alpha'][i]),
                print '{:>15}'.format(self.distres['name'][i])

    if target=='published':
        self.out.close()
        os.system('mv temp.dat '+self.home+'/results/curves/'+self.name+'.dat')

                
    if mode in ['global','model']:
        print 'Total number of systems used:',systot
        self.results = chisq if mode=='global' else model
        self.plotweighted(mode)
        
    if mode=='alpha':
        return data
    
#==================================================================================================================
