#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def fitdipole_reproduce():

    # Fitting the UVES results

    systems = []
    dipolepath = self.Crep+'/uves/reproduce/'
    dipolepath = self.Crep+'/uves/reproduce/'
    os.system('mkdir -p '+dipolepath)
    os.chdir(dipolepath)
    save = open('results.dat','w')
    save.write('##ALPHAFIT##\n')
    for i in range (len(self.dipole)):
        zabs   = self.dipole['z_abs'][i]
        if self.dipole['analyser'][i]=='JAK':
            val = self.dipole['RA King\n(hh:mm:ss)'][i].split(':')
            ra  = float(val[0])+float(val[1])/60+float(val[2])/3600
            val = self.dipole['DEC King\n(dd:mm:ss)'][i].split(':')
            if (val[0][0]=='-'):
                dec = float(val[0])-float(val[1])/60-float(val[2])/3600
            else:
                dec = float(val[0])+float(val[1])/60+float(val[2])/3600
            save.write('{:<10}'.format('%.6f'%float(zabs)))
            save.write('{:>12}'.format('%.6f'%float(ra)))
            save.write('{:>12}'.format('%.6f'%float(dec)))
            save.write('{:>12}'.format('%.6f'%round(float(self.dipole['alpha'][i]),int(float(self.dipole['dec_alpha'][i])))))
            save.write('{:>12}'.format('%.6f'%round(float(self.dipole['error'][i]),int(float(self.dipole['dec_error'][i])))))
            save.write('\n')
            systems.append(self.dipole['name'][i])
    save.write('##END##\n')
    save.close()
    dipolefit(systems)

    # Fitting the HIRES high-contrast measurements

    systems = []
    dipolepath = self.Crep+'/hires/reproduce/high_contrast/'
    os.system('mkdir -p '+dipolepath)
    os.chdir(dipolepath)
    save = open('results.dat','w')
    save.write('##ALPHAFIT##\n')
    for i in range (len(self.dipole)):
        zabs   = self.dipole['z_abs'][i]
        if self.dipole['analyser'][i]=='MTM' and self.dipole['high-contrast'][i]=='x':
            val = self.dipole['RA King\n(hh:mm:ss)'][i].split(':')
            ra  = float(val[0])+float(val[1])/60+float(val[2])/3600
            val = self.dipole['DEC King\n(dd:mm:ss)'][i].split(':')
            if (val[0][0]=='-'):
                dec = float(val[0])-float(val[1])/60-float(val[2])/3600
            else:
                dec = float(val[0])+float(val[1])/60+float(val[2])/3600
            save.write('{:<10}'.format('%.6f'%float(zabs)))
            save.write('{:>12}'.format('%.6f'%float(ra)))
            save.write('{:>12}'.format('%.6f'%float(dec)))
            save.write('{:>12}'.format('%.6f'%round(float(self.dipole['alpha'][i]),int(float(self.dipole['dec_alpha'][i])))))
            save.write('{:>12}'.format('%.6f'%round(float(self.dipole['error'][i]),int(float(self.dipole['dec_error'][i])))))
            save.write('\n')
            systems.append(self.dipole['name'][i])
    save.write('##END##\n')
    save.close()
    dipolefit(systems)
  
    # Fitting combined high-contrast and rest of HIRES measurements

    systems = []
    dipolepath = self.Crep+'/hires/reproduce/all_contrast/'
    os.system('mkdir -p '+dipolepath)
    os.chdir(dipolepath)
    save = open('results.dat','w')
    save.write('##ALPHAFIT##\n')
    for line in np.loadtxt(self.Crep+'/hires/reproduce/high_contrast/alphafit_errmod.dat',dtype=str,delimiter='\n'):
        save.write(line.replace(line.split()[-1],'')+'\n')
        systems.append(line.split()[-1])
    for i in range (len(self.dipole)):
        zabs   = self.dipole['z_abs'][i]
        if self.dipole['analyser'][i]=='MTM' and self.dipole['high-contrast'][i]!='x':
            val = self.dipole['RA King\n(hh:mm:ss)'][i].split(':')
            ra  = float(val[0])+float(val[1])/60+float(val[2])/3600
            val = self.dipole['DEC King\n(dd:mm:ss)'][i].split(':')
            if (val[0][0]=='-'):
                dec = float(val[0])-float(val[1])/60-float(val[2])/3600
            else:
                dec = float(val[0])+float(val[1])/60+float(val[2])/3600
            save.write('{:<10}'.format('%.6f'%float(zabs)))
            save.write('{:>12}'.format('%.6f'%float(ra)))
            save.write('{:>12}'.format('%.6f'%float(dec)))
            save.write('{:>12}'.format('%.6f'%round(float(self.dipole['alpha'][i]),int(float(self.dipole['dec_alpha'][i])))))
            save.write('{:>12}'.format('%.6f'%round(float(self.dipole['error'][i]),int(float(self.dipole['dec_error'][i])))))
            save.write('\n')
            systems.append(self.dipole['name'][i])
    save.write('##END##\n')
    save.close()
    dipolefit(systems)

    comb  = self.Crep+'/comb/reproduce/'
    hires = self.Crep+'/hires/reproduce/all_contrast/alphafit_errmod.dat'
    uves  = self.Crep+'/uves/reproduce/alphafit_errmod.dat'

    systems = []
    hires   = np.loadtxt(hires,dtype=str,delimiter='\n')
    uves    = np.loadtxt(uves,dtype=str,delimiter='\n')
    data    = np.append(hires,uves)
    
    print 'Location:',comb
    os.system('mkdir -p '+comb)
    os.chdir(comb)
    
    save = open('results.dat','w')
    save.write('##ALPHAFIT##\n')
    for line in data:
        save.write(line.replace(line.split()[-1],'')+'\n')
        systems.append(line.split()[-1])
    save.write('##END##\n')
    save.close()
    dipolefit(systems)
    
#==================================================================================================================

def fitdipole_reproduce_sample():

    systems = []        
    path = self.Crep+'hires/reproduce/'
    print 'Location:',path
    os.system('mkdir -p '+path)
    os.chdir(path)
    
    save = open('results.dat','w')
    save.write('##ALPHAFIT##\n')
    for i in range (len(self.dipole)):
        zabs   = self.dipole['z_abs'][i]
        cond1  = self.dipole['analyser'][i]=='MTM'
        cond2a = self.sample!=None and self.sample in self.dipole['system'][i]
        cond2b = '--stable' in sys.argv and self.dipole['system'][i] not in self.badhires
        if cond1 and (cond2a or cond2b):
            val = self.dipole['RA King\n(hh:mm:ss)'][i].split(':')
            ra  = float(val[0])+float(val[1])/60+float(val[2])/3600
            val = self.dipole['DEC King\n(dd:mm:ss)'][i].split(':')
            if (val[0][0]=='-'):
                dec = float(val[0])-float(val[1])/60-float(val[2])/3600
            else:
                dec = float(val[0])+float(val[1])/60+float(val[2])/3600
            save.write('{:<10}'.format('%.6f'%float(zabs)))
            save.write('{:>12}'.format('%.6f'%float(ra)))
            save.write('{:>12}'.format('%.6f'%float(dec)))
            save.write('{:>12}'.format('%.6f'%round(float(self.dipole['alpha'][i]),int(float(self.dipole['dec_alpha'][i])))))
            save.write('{:>12}'.format('%.6f'%round(float(self.dipole['error'][i]),int(float(self.dipole['dec_error'][i])))))
            save.write('\n')
            systems.append(self.dipole['name'][i])
    save.write('##END##\n')
    save.close()
    dipolefit(systems)
                
#==================================================================================================================

def fitdipole_combined():
        
    comb  = self.Crep+'comb/all/hires_all-uves_all/'
    list1 = self.Crep+'hires/all/unclipped/v10_chisq1E-04/0.000/alphafit_errmod.dat'
    list2 = self.Crep+'uves/all/unclipped/v10_chisq1E-04_all/0.000/alphafit_errmod.dat'

    systems = []
    list1   = np.loadtxt(list1,dtype=str,delimiter='\n')
    list2   = np.loadtxt(list2,dtype=str,delimiter='\n')
    data    = np.append(list1,list2)
    
    print 'Location:',comb
    os.system('mkdir -p '+comb)
    os.chdir(comb)
    
    save = open('results.dat','w')
    save.write('##ALPHAFIT##\n')
    for line in data:
        save.write(line.replace(line.split()[-1],'')+'\n')
        systems.append(line.split()[-1])
    save.write('##END##\n')
    save.close()
    dipolefit(systems)

#==================================================================================================================

def fitdipole_sims():

    fortlist = self.fortlist()
    filelist = np.loadtxt(fortlist,dtype='str',comments='!',ndmin=1)
    os.system('rm '+fortlist)
    
    data  = np.empty((0,4))
    model = 'thermal' if self.simulation=='simple' else 'turbulent'
    path  = self.Crep+self.instrument+'/simulation/'+self.simtest+'/'+self.test+'/'+self.distortion+'/'
    for self.distpath in filelist:
        self.qso  = self.distpath.split('/')[-8]
        self.zabs = self.distpath.split('/')[-7]
        if os.path.exists(self.distpath+'/'+model+'/'+model+'.18'):
            self.readfort18(self.distpath+'/'+model+'/'+model+'.18')
            if self.simulation=='simple' and self.ther_chisq!='-':
                data = np.vstack((data,[self.zabs,self.ther_alpha,self.ther_error,self.qso]))
            elif self.simulation=='complex' and self.turb_chisq!='-':
                data = np.vstack((data,[self.zabs,self.turb_alpha,self.turb_error,self.qso]))

    self.path = path
    self.data = data
    self.prepresults()
    
#==================================================================================================================

def fitdipole():
    
    fortlist = self.fortlist()
    filelist = np.loadtxt(fortlist,dtype='str',comments='!',ndmin=1)
    os.system('rm '+fortlist)
    
    data = np.empty((0,4))
    path = 'distortion' if '--distortion' in sys.argv else self.sample if self.sample!=None else self.selection
    path = path+'/original'  if '--original'  in sys.argv else \
           path+'/bestchisq' if '--bestchisq' in sys.argv else \
           path+'/thermal'   if '--thermal'   in sys.argv else \
           path+'/turbulent' if '--turbulent' in sys.argv else \
           path+'/clipped'   if '--clip'      in sys.argv else \
           path+'/unclipped'
    path = self.Crep+self.instrument+'/'+path+'/'+self.test+'/'+self.distortion+'/'

    for self.distpath in filelist:
        self.qso    = self.distpath.split('/')[-7]
        self.zabs   = self.distpath.split('/')[-6]
        self.sample = self.distpath.split('/')[-5]
        self.getresults()
        system = self.qso+'/'+self.zabs+'/'+self.sample
        cond1  = self.mom_chisq!='-'
        cond2  = self.turb_chisq!='-'
        cond3  = self.ther_chisq!='-'
        conda  = '--original'  in sys.argv
        condb  = '--turbulent' in sys.argv
        condc  = '--thermal'   in sys.argv
        condd  = '--bestchisq' in sys.argv
        condA  = self.sample in self.murphy
        condB  = os.path.exists(self.fitdir+system+'/model00/model/turbulent.13')
        condC  = os.path.exists(self.fitdir+system+'/model00/model/thermal.13')
        if cond1 and condb==condc==condd==False and (conda==False or condA==False):
            data = np.vstack((data,[self.zabs,self.mom_alpha,self.mom_error,self.qso]))
        elif cond2 and condB and (conda or condb or (condd and float(self.turb_chisq)<=float(self.ther_chisq))):
            data = np.vstack((data,[self.zabs,self.turb_alpha,self.turb_error,self.qso]))
        elif cond3 and condC and (conda or condc or (condd and float(self.ther_chisq)<=float(self.turb_chisq))):
            data = np.vstack((data,[self.zabs,self.ther_alpha,self.ther_error,self.qso]))

    self.path = path
    self.data = data
    self.prepresults()
    
#==================================================================================================================

def fitdipole_all():
    
    fortlist = self.fortlist()
    filelist = np.loadtxt(fortlist,dtype='str',comments='!',ndmin=1)
    os.system('rm '+fortlist)
    
    data = np.empty((0,4))
    path = self.Crep+'all/temp/'

    for self.distpath in filelist:
        self.qso    = self.distpath.split('/')[-7]
        self.zabs   = self.distpath.split('/')[-6]
        self.sample = self.distpath.split('/')[-5]
        self.getresults()
        system      = self.qso+'/'+self.zabs+'/'+self.sample
        syslist     = np.array([str(i) for i in self.curlist['system']])
        idx         = np.where(syslist==system)[0][0]
        if self.curlist['00'][idx]=='CJ' and self.mom_alpha!='-' and self.mom_error!='-' \
           and len(self.curlist['ion list'][idx].split())>1 \
           and self.turb_chisq_nu<1.5 and self.ther_chisq_nu<1.5 \
           :
            data = np.vstack((data,[self.zabs,self.mom_alpha,self.mom_error,self.qso]))
            
    self.path = path
    self.data = data
    self.prepresults()
    
#==================================================================================================================

def prepresults():

    print 'Location:',self.path
    os.system('mkdir -p '+self.path)
    os.chdir(self.path)
    
    save = open('results.dat','w')
    save.write('##ALPHAFIT##\n')
    for i in range(len(self.data)):
        zem,ra,dec,dist = self.calc_dist2dip(self.data[i,-1][:14])
        zabs  = float(self.data[i,0])
        alpha = float(self.data[i,1])
        error = float(self.data[i,2])
        save.write('{:<10}'.format('%.6f'%zabs))    # z_abs
        save.write('{:>12}'.format('%.6f'%ra))      # ra
        save.write('{:>12}'.format('%.6f'%dec))     # dec
        save.write('{:>12}'.format('%.6f'%alpha))   # MoM_alpha
        save.write('{:>12}'.format('%.6f'%error))   # MoM_da
        save.write('\n')
    save.write('##END##\n')
    save.close()
    dipolefit(self.data[:,-1])
    
#==================================================================================================================

def createinput(fullpath):

    output = np.genfromtxt(self.here+'/results.dat',names=True,dtype=object,comments='!')

    save = open('results.dat','w')
    save.write('##ALPHAFIT##\n')

    for i in range (len(output)):

        alpha = None
        zabs = float(output['z_abs'][i])
        zem,ra,dec,dist = self.calc_dist2dip(output['qso'][i])

        condition = output['sample'][i]+'/'+output['qso'][i]+'/'+output['z_abs'][i] not in self.skipabs

        if condition and (('highz' not in fullpath and 'lowz' not in fullpath) \
                or ('highz' in fullpath and zabs>=self.zcut) \
                or ('lowz' in fullpath and zabs<self.zcut)):

            condturb1 = output['turb_alpha'][i] not in ['0.000','-','nan'] \
                        and output['turb_error'][i] not in ['0.000','-','nan']
            condther1 = output['ther_alpha'][i] not in ['0.000','-','nan'] \
                        and output['ther_error'][i] not in ['0.000','-','nan']
            condturb2 = output['turb_alpha'][i] in ['0.000','-','nan'] \
                        or output['turb_error'][i] in ['0.000','-','nan']
            condther2 = output['ther_alpha'][i] in ['0.000','-','nan'] \
                        or output['ther_error'][i] in ['0.000','-','nan']
            
            if condturb1 and condther2:
                alpha = output['turb_alpha'][i]
                error = output['turb_error'][i]
            if condturb2 and condther1:
                alpha = output['ther_alpha'][i]
                error = output['ther_error'][i]
            if condturb1 and condther1:
                alpha = output['MoM_alpha'][i]
                error = output['MoM_error'][i]
        
        if alpha != None:
            save.write('{:<10}'.format('%.6f'%float(zabs)))
            save.write('{:>12}'.format('%.6f'%float(ra*24./360.)))
            save.write('{:>12}'.format('%.6f'%float(dec)))
            save.write('{:>12}'.format('%.6f'%float(alpha)))
            save.write('{:>12}'.format('%.6f'%float(error)))
            save.write('\n')
        
    save.write('##END##\n')
    save.close()

#==================================================================================================================

def dipolefit(systems):

    print 'Perform Dipole fitting...'

    os.system('touch command')
    os.system('alphafit results.dat < command > termout.dat')
    os.system("for x in $(ls *.ps); do echo $x; ps2pdf $x $(echo $x|sed -e 's/ps$/pdf/');done")
    os.system('rm command *.ps')

    errmod = np.loadtxt('alphafit_errmod.dat')
    save = open('alphafit_errmod.dat','w')
    save.write('##ALPHAFIT##\n')
    for i in range (len(errmod)):
        save.write('{:<10}'.format('%.6f'%errmod[i,0]))   # zabs
        save.write('{:>12}'.format('%.6f'%errmod[i,1]))   # ra
        save.write('{:>12}'.format('%.6f'%errmod[i,2]))   # dec
        save.write('{:>12}'.format('%.6f'%errmod[i,3]))   # MoM_alpha
        save.write('{:>12}'.format('%.6f'%errmod[i,4]))   # MoM_da
        save.write('{:<12}'.format('     '+systems[i]))   # system
        save.write('\n')
    save.write('##END##\n')
    save.close()

    os.chdir(self.here)
    
#==================================================================================================================
