import alpha

def clean_repository():
    '''
    Perform various cleaning function.
    
    **Required arguments**
    
    --mode    Cleaning function to run
    
    **Available modes**
    
      1.   Print out the original and latest symlink target for each system
      2.   Check which of the completed fits contain both Zn and Cr
      3.   Check which systems have nearby companions within 0.001 in redshift space
      4.   Create model 05, remove da/a columns for each latest model and print anchor ion component
      5.   Copy all latest version of published King+Murphy sample
      6.   Copy latest model for each system
      7.   Copy model for each system used for simulations
      8.   Create simlinks to original CJ systems
      9.   Create folder of published King+Murphy systems with chunks and velocity plots
      10.  Check if all systems present in the repository are tabulated in CoDDAM and vice-versa 
      11.  Change model numbering
    '''
    
    if setup.mode==1:
        
        for i in range(len(setup.curlist)):
            system   = setup.curlist['system'][i]
            original = os.path.realpath(setup.fitdir+system+'/original/')[-2:]
            original = '-' if original=='al' else original
            latest   = os.path.realpath(setup.fitdir+system+'/latest/')[-2:]
            latest   = '-' if latest=='st' else latest
            print original,'\t',latest

    if setup.mode==2:

        zncr = 0
        for i in range(len(setup.curlist)):
            system = setup.curlist['system'][i]
            if setup.curlist['status'][i]=='completed':
                if os.path.exists(setup.fitdir+system+'/model00/model/header.dat'):
                    header = np.loadtxt(setup.fitdir+system+'/model00/model/header.dat',dtype=str,delimiter='\n')
                else:
                    header = np.loadtxt(setup.fitdir+system+'/model00/original/header.dat',dtype=str,delimiter='\n')
                zn,cr  = 0,0
                for j in range(len(header)):
                    zn = 1 if 'external' not in header[j] and 'Zn' in header[j].split()[0] else zn
                    cr = 1 if 'external' not in header[j] and 'Cr' in header[j].split()[0] else cr
                if zn==cr==1:
                    print system
                    zncr += 1
        print zncr

    if setup.mode==3:

        n=1
        syslist = np.empty((0,3))
        for i in range(1,len(setup.curlist)):
            qso1  = str(setup.curlist['system'][i-1]).split('/')[0]
            zabs1 = float(str(setup.curlist['system'][i-1]).split('/')[1])
            qso2  = str(setup.curlist['system'][i]).split('/')[0]
            zabs2 = float(str(setup.curlist['system'][i]).split('/')[1])
            if qso1==qso2 and abs(zabs1-zabs2)<=0.001 and setup.curlist['status'][i]=='recorded' and setup.curlist['dataset'][i]!='multiple':
                print n,'-',setup.curlist['system'][i]
                n+=1

    if setup.mode==4:

        for i in range(1,len(setup.curlist)):
            system = str(setup.curlist['system'][i])
            if str(setup.curlist['status'][i])!='recorded':
                print '|- Processing:',system
                os.system('mkdir -p '+setup.fitdir+system+'/model05/model/')
                if os.path.exists(setup.fitdir+system+'/model05/model/data')==True:
                    os.system('rm '+setup.fitdir+system+'/model05/model/data')
                os.system('ln -s ../../data/ '+setup.fitdir+system+'/model05/model/')
                #os.system('cp /Users/vincent/Downloads/atom.dat '+setup.fitdir+system+'/model05/model/atom.dat')
                #if str(setup.curlist['instrument'][i])=='UVES':
                #    os.system('cp /Users/vincent/Downloads/uves.dat '+setup.fitdir+system+'/model05/model/vp_setup.dat')
                #if str(setup.curlist['instrument'][i])=='HIRES':
                #    os.system('cp /Users/vincent/Downloads/hires.dat '+setup.fitdir+system+'/model05/model/vp_setup.dat')
                header = np.loadtxt(setup.fitdir+system+'/latest/model/header.dat',delimiter='\n',dtype=str,ndmin=1)
                outhead = open(setup.fitdir+system+'/model05/model/header.dat','w')
                for head in header:
                    outhead.write(head+'\n')
                outhead.close()
                for model in ['thermal','turbulent']:
                    infort = open(setup.fitdir+system+'/latest/model/'+model+'.13','r')
                    outfort = open(setup.fitdir+system+'/model05/model/'+model+'.13','w')
                    tie = '-'
                    star = 0
                    for line in infort:
                        if len(line.split())==0:
                            break
                        elif '*' in line:
                            if line.split()[0][0]!='!':
                                outfort.write(line)
                            star += 1
                        elif star==1 and line.split()[0][0]!='!':
                            outfort.write(line)
                        elif star==2 and line.split()[0][0]!='!':
                            m = ' 0.00   0.00E+00  0\n' if model=='thermal' else ' 0.00   1.00E+00  0\n'
                            if len(line.split()[0])==1:
                                a = line.index(line.split()[5])
                                b = line.index(' 0.00 ') if ' 0.00 ' in line else line.index(' 1.00 ')
                                outfort.write(line[:a-1]+m)#line[b-1:])
                                if 'q' in line.split()[5] and tie=='-':
                                    tie = line.split('!')[-1].strip()
                                elif 'q' in line.split()[5] and tie!='-':
                                    tie = tie+' / '+line.split('!')[-1].strip()
                            else:
                                a = line.index(line.split()[4])
                                b = line.index(' 0.00 ') if ' 0.00 ' in line else line.index(' 1.00 ')
                                outfort.write(line[:a-1]+m)#line[b-1:])
                                if 'q' in line.split()[4] and tie=='-':
                                    tie = line.split('!')[-1].strip()
                                elif 'q' in line.split()[4] and tie!='-':
                                    tie = tie+' / '+line.split('!')[-1].strip()
                    infort.close()
                    outfort.close()
                    os.chdir(setup.fitdir+system+'/model05/model/')
                    #print setup.fitdir+system+'/model05/model/'
                    #model = 'thermal' if model=='thermal' else 'turbulent'
                    #os.system('PyD_velplot '+model+'.13 --header header.dat --details --getwave --version vpfit10 --fit --output '+model)

    if setup.mode==5:

        os.chdir('/Users/vincent/Documents/')
        os.system('mkdir spectra/UVES_squader')
        os.system('cp '+setup.fitdir+'/spectra/UVES_squader/* spectra/UVES_squader/')
        for i in range(len(setup.publist)):
            destin = 'systems/'+setup.publist['system'][i]
            print destin
            os.system('mkdir -p '+destin)
            os.system('cp '+setup.fitdir+setup.publist['system'][i]+'/latest/model/thermal.13   '+destin)
            os.system('cp '+setup.fitdir+setup.publist['system'][i]+'/latest/model/turbulent.13 '+destin)
            os.system('cp '+setup.fitdir+setup.publist['system'][i]+'/latest/model/vp_setup.dat '+destin)
            os.system('cp '+setup.fitdir+setup.publist['system'][i]+'/latest/model/header.dat   '+destin)
            os.system('cp '+setup.fitdir+setup.publist['system'][i]+'/latest/model/atom.dat     '+destin)
            oldpath = os.path.realpath(setup.fitdir+setup.publist['system'][i]+'/data/')
            newpath = oldpath.split('alpha/systems/')[-1]
            if os.path.exists(newpath)==False:
                os.system('mkdir -p '+newpath)
                os.system('cp '+oldpath+'/* '+newpath)
            os.system('ln -s ../../../../'+newpath+' systems/'+setup.publist['system'][i]+'/data')

    if setup.mode==6:

        path = '/Users/vincent/Desktop/systems/'
        os.system('mkdir -p '+path)
        os.chdir(path)
        for i in range(len(setup.curlist)):
            system = str(setup.curlist['system'][i])
            if str(setup.curlist['vd17a'][i])!='-' and str(setup.curlist['instrument'][i])=='HIRES':
                print '|- Processing:',system
                model = 'model-%.1f'%float(setup.curlist['vd17a'][i])
                os.system('rm '+path+'/'+system.replace('/','-'))
                os.system('ln -s '+setup.fitdir+system+'/'+model+'/model '+path+'/'+system.replace('/','-'))
                os.system('rm '+path+'/'+system.replace('/','-')+'/data')
                os.system('ln -s ../../data '+path+'/'+system.replace('/','-')+'/data')
                datapath = os.path.realpath(setup.fitdir+system+'/data').replace('/Users/vincent/ASTRO/analysis/alpha/systems/','../../../')
                os.chdir(path+'/'+system.replace('/','-'))
                #os.system('velplot thermal.13 --output thermal --getwave --details --version vpfit10 --header header.dat')
                #os.system('velplot turbulent.13 --output turbulent --getwave --details --version vpfit10 --header header.dat')
                
    if setup.mode==7:

        path = '/Users/vincent/Documents/systems/'
        os.system('mkdir -p '+path)
        os.chdir(path)
        
        for i in range(len(setup.distres)):
            system = str(setup.distres['system'][i])
            print '|- Processing:',system
            model  = 'model-%.1f'%float(setup.distres['model'][i])
            os.system('mkdir -p '+path+system+'/'+model+'/model')
            datapath = os.path.realpath(setup.fitdir+system+'/data').replace('/Users/vincent/ASTRO/analysis/alpha/systems/','../../../')
            os.system('ln -s '+datapath+' '+path+system+'/data')
            os.system('cp '+setup.fitdir+system+'/'+model+'/model/header.dat   '+path+system+'/'+model+'/model')
            os.system('cp '+setup.fitdir+system+'/'+model+'/model/thermal.13   '+path+system+'/'+model+'/model')
            os.system('cp '+setup.fitdir+system+'/'+model+'/model/turbulent.13 '+path+system+'/'+model+'/model')
            os.system('cp '+setup.fitdir+system+'/'+model+'/model/thermal.18   '+path+system+'/'+model+'/model')
            os.system('cp '+setup.fitdir+system+'/'+model+'/model/turbulent.18 '+path+system+'/'+model+'/model')
            os.system('cp '+setup.fitdir+system+'/'+model+'/model/thermal.26   '+path+system+'/'+model+'/model')
            os.system('cp '+setup.fitdir+system+'/'+model+'/model/turbulent.26 '+path+system+'/'+model+'/model')

    if setup.mode==8:

        path = '/Users/vincent/Documents/checking/'
        os.system('mkdir -p '+path)
        for i in range(len(setup.curlist)):
            system = str(setup.curlist['system'][i])
            if str(setup.curlist['status'][i])!='recorded' and setup.curlist['00'][i]=='CJ':
                print '|- Processing:',system
                os.system('ls '+setup.fitdir+system+'/model* > list')
                for line in np.loadtxt('list',dtype=str,delimiter='\n'):
                    if system in line and 'model05' not in line:
                        syspath = path+'/'+system.replace('/','_')+'/'
                        os.system('mkdir -p '+syspath)
                        os.system('ln -s '+line.replace(':','/model')+' '+syspath+'/'+line.split('/')[-1][:-1])
                        #os.system('mkdir -p '+folder)
                        #os.chdir(line.replace(':','/model'))
                        #os.system('PyD_velplot thermal.13   --header header.dat --details --getwave --version vpfit10 --output thermal')
                        #os.system('PyD_velplot turbulent.13 --header header.dat --details --getwave --version vpfit10 --output turbulent')
                        #os.system('cp header.dat    '+path)
                        #os.system('cp thermal.13    '+path)
                        #os.system('cp thermal.18    '+path)
                        #os.system('cp thermal.26    '+path)
                        #os.system('cp thermal.pdf   '+path)
                        #os.system('cp turbulent.13  '+path)
                        #os.system('cp turbulent.18  '+path)
                        #os.system('cp turbulent.26  '+path)
                        #os.system('cp turbulent.pdf '+path)
            
    if setup.mode==9:

        for i in range(len(setup.publist)):
            system = str(setup.publist['system'][i])
            print '|- Processing:',system
            #target = setup.fitdir+'/'+system+'/model05/model/'
            #destin = '/Users/vincent/Documents/pubsys/'+system+'/'
            #os.system('mkdir -p '+destin)
            #os.system('mkdir -p '+destin+'/chunks')
            #if os.path.exists(target+'atom.dat')==False:
            #    os.system('cp /Users/vincent/Documents/atom.dat '+target)
            #if os.path.exists(target+'vp_setup.dat')==False:
            #    os.system('cp /Users/vincent/Documents/vp_setup.dat '+target)
            #os.chdir(target)
            #os.system('cp '+target+'header.dat    '+destin)
            #os.system('cp '+target+'thermal.13    '+destin)
            #os.system('cp '+target+'turbulent.13  '+destin)
            #os.system('cp '+target+'chunks/*      '+destin+'/chunks/')
            target = setup.fitdir+'/'+system+'/'
            os.system('find '+target+' -name turbulent_fit.pdf -type f > list')
            plots = np.loadtxt('list',ndmin=1,dtype=str,delimiter='\n')
            destin = '/Users/vincent/Documents/velplots/'
            os.system('mkdir -p '+destin)
            if len(plots)>0:
                os.system('cp '+plots[0]+' '+destin+system.replace('/','--')+'.pdf')
            else:
                print system
            #os.system('PyD_velplot thermal.13 --header header.dat --details --getwave --version vpfit10 --output thermal')

    if setup.mode==10:

        systems = [str(setup.curlist['system'][i]) for i in range(len(setup.curlist))]
        os.system('find '+setup.fitdir+' -mindepth 3 -maxdepth 3 | sort > list')
        syslist = np.loadtxt('list',dtype=str)
        for line in syslist:
            quasar = line.split('/')[-3]
            zabs   = line.split('/')[-2]
            sample = line.split('/')[-1]
            if 'UVES_' in sample or 'HIRES_' in sample:
                system = quasar+'/'+zabs+'/'+sample
                if system not in systems:
                    print system
                    
        for i in range(len(setup.curlist)):
            system = str(setup.curlist['system'][i])
            if str(setup.curlist['status'][i])!='recorded' and os.path.exists(setup.fitdir+system)==False:
                print str(setup.curlist['system'][i])
