#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *
  
#==================================================================================================================

def getmag(pole='Dipole'):

    print '\n\t'+pole+' amplitude\n'
    
    x,y,dy = [],[],[]
    
    for i in self.distlist:
        
        strslope = '0.000' if round(i,3)==0 \
                   else str('%.3f'%i).replace('-','m') if '-' in str(i) \
                   else 'p'+str('%.3f'%i)
        
        pathdir  = self.Crep+self.instrument+'/'+self.selection+'/'+self.test+'/'\
                   +strslope+'/'+self.zsample+'/termout.dat'

        if os.path.exists(pathdir)==True and round(i,2) not in self.discard:
            
            output = np.loadtxt(pathdir,dtype='str',delimiter='\n')
            
            for k in range (len(output)):
                
                if pole=='Dipole' and 'Generalised linear fit with new errors' in output[k]:

                    mag_value = float(output[k+23].split('(')[1].split('+/-')[0])
                    mag_error = float(output[k+23].split(')')[0].split('+/-')[-1])
                    print '\t\t',\
                        '{:>10}'.format('%.3f'%i),\
                        '{:>10}'.format('%.3f'%mag_value),\
                        '{:>10}'.format('%.3f'%mag_error)
                    x.append(round(i,3))
                    y.append(mag_value)
                    dy.append(mag_error)
                    break

                if pole=='Monopole' and 'Generalised linear fit with new errors' in output[k]:
                    
                    mag_value = float(output[k+15].split('(')[1].split('+/-')[0])
                    mag_error = float(output[k+15].split(')')[0].split('+/-')[-1])
                    print '\t\t',\
                      '{:>10}'.format('%.3f'%i),\
                      '{:>10}'.format('%.3f'%mag_value),\
                      '{:>10}'.format('%.3f'%mag_error)
                    x.append(round(i,3))
                    y.append(mag_value)
                    dy.append(mag_error)
                    break
                
    return x,y,dy
    
#==================================================================================================================

def getprobability():

    print '\n\tDipole Probability\n'
    
    x,y = [],[]
    
    for i in self.distlist:
        
        strslope = '0.000' if round(i,3)==0 \
                   else str('%.3f'%i).replace('-','m') if '-' in str(i) \
                   else 'p'+str('%.3f'%i)
        
        pathdir  = self.Crep+self.instrument+'/'+self.selection+'/'+self.test+'/'\
                   +strslope+'/'+self.zsample+'/termout.dat'

        if os.path.exists(pathdir)==True and round(i,2) not in self.discard:
            
            output = np.loadtxt(pathdir,dtype='str',delimiter='\n')
            
            for k in range (len(output)):
                
                if 'bootstrap()' in output[k]:
                    
                    prob = float(output[k].split()[4].replace(',',''))
                    print '\t\t',\
                      '{:>10}'.format('%.3f'%i),\
                      '{:>10}'.format('%.3f'%prob)
                    x.append(round(i,3))
                    y.append(prob)
                    break
                
    return x,y
    
#==================================================================================================================
