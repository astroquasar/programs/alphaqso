#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def check_uves_arm():

    print '\nScanning the systems...'

    save = open('uvesarm.dat','w')
    
    save.write('{:<20}'.format('sample'))
    save.write('{:<15}'.format('qso'))
    save.write('{:>10}'.format('z_em'))
    save.write('{:>10}'.format('z_abs'))
    save.write('{:>10}'.format('ra'))
    save.write('{:>10}'.format('dec'))

    save.write('{:>10}'.format('nb_ini'))
    save.write('{:>10}'.format('qmin_ini'))
    save.write('{:>10}'.format('qmax_ini'))
    save.write('{:>10}'.format('qvar_ini'))
    
    save.write('{:>10}'.format('nb_blue'))
    save.write('{:>10}'.format('qmin_blue'))
    save.write('{:>10}'.format('qmax_blue'))
    save.write('{:>10}'.format('qvar_blue'))
    
    save.write('{:>10}'.format('nb_red'))
    save.write('{:>10}'.format('qmin_red'))
    save.write('{:>10}'.format('qmax_red'))
    save.write('{:>10}'.format('qvar_red'))
    
    save.write('{:>10}'.format('nb_end'))
    save.write('{:>10}'.format('qmin_end'))
    save.write('{:>10}'.format('qmax_end'))
    save.write('{:>10}'.format('qvar_end'))
    
    save.write('\n')
    
    k = overlap = armblue = armred = armboth = nochange = 0
    self.sumregtotal = self.sumregboth = self.sumregblue = self.sumregred = 0
        
    for i in range (len(self.publist)):

        self.distpath  = self.fitdir+'/'+self.publist['path'][i]
        self.sample    = self.publist['path'][i].split('/')[0]
        self.qso       = self.publist['path'][i].split('/')[1]
        self.zabs      = self.publist['path'][i].split('/')[2]
        self.selection = self.sample+'/'+self.qso+'/'+self.zabs

        if self.sample=='UVES_king': 
            
            prepare()
            self.sumregtotal = self.sumregtotal + self.regtotal
            self.sumregboth  = self.sumregboth  + self.regboth
            self.sumregblue  = self.sumregblue  + self.regblue
            self.sumregred   = self.sumregred   + self.regred
                    
            qminini  = '-' if len(self.qini)==0 else int(min(self.qini))
            qmaxini  = '-' if len(self.qini)==0 else int(max(self.qini))
            qvarini  = '-' if len(self.qini)==0 else int(max(self.qini)-min(self.qini))
            
            qminblue = '-' if len(self.qblue)==0 else int(min(self.qblue))
            qmaxblue = '-' if len(self.qblue)==0 else int(max(self.qblue))
            qvarblue = '-' if len(self.qblue)==0 else int(max(self.qblue)-min(self.qblue))
            
            qminred  = '-' if len(self.qred)==0 else int(min(self.qred))
            qmaxred  = '-' if len(self.qred)==0 else int(max(self.qred))
            qvarred  = '-' if len(self.qred)==0 else int(max(self.qred)-min(self.qred))
            
            qminend  = '-' if len(self.qend)==0 else int(min(self.qend))
            qmaxend  = '-' if len(self.qend)==0 else int(max(self.qend))
            qvarend  = '-' if len(self.qend)==0 else int(max(self.qend)-min(self.qend))
    
            if len(self.olap)!=0:
                overlap = overlap + 1
            if (len(self.qblue)!=0 and len(self.qred)==0) and len(self.olap)==0:
                armblue = armblue + 1
            if (len(self.qblue)==0 and len(self.qred)!=0) and len(self.olap)==0:
                armred = armred + 1
            if (len(self.qblue)!=0 and len(self.qred)!=0) or len(self.olap)!=0:
                armboth = armboth + 1
            if qvarini==qvarend:
                nochange = nochange + 1

            zem,ra,dec,dist = calc_dist2dip(self.qso)
            
            save.write('{:<20}'.format(self.sample))
            save.write('{:<15}'.format(self.qso))
            save.write('{:>10}'.format('%.5f'%zem))
            save.write('{:>10}'.format(self.zabs))
            save.write('{:>10}'.format('%.2f'%ra))
            save.write('{:>10}'.format('%.2f'%dec))
    
            save.write('{:>10}'.format(len(self.qini)))
            save.write('{:>10}'.format(qminini))
            save.write('{:>10}'.format(qmaxini))
            save.write('{:>10}'.format(qvarini))
            
            save.write('{:>10}'.format(len(self.qblue)))
            save.write('{:>10}'.format(qminblue))
            save.write('{:>10}'.format(qmaxblue))
            save.write('{:>10}'.format(qvarblue))
            
            save.write('{:>10}'.format(len(self.qred)))
            save.write('{:>10}'.format(qminred))
            save.write('{:>10}'.format(qmaxred))
            save.write('{:>10}'.format(qvarred))
    
            save.write('{:>10}'.format(len(self.qend)))
            save.write('{:>10}'.format(qminend))
            save.write('{:>10}'.format(qmaxend))
            save.write('{:>10}'.format(qvarend))
            
            save.write('\n')

            k = k + 1

    print ''
    print 'Total number of systems .. :',k
    print '  Blue arm only .......... :',armblue
    print '  Red arm only ........... :',armred
    print '  Both arms .............. :',armboth,'from which',overlap,'have fitting regions overlapping both arms.'
    print ''
    print 'Total number of fitting regions:',self.sumregtotal
    print '  Blue arm only .......... :',self.sumregblue
    print '  Red arm only ........... :',self.sumregred
    print '  Both arms .............. :',self.sumregboth
    print ''
    print 'Discarding the',self.sumregboth,'fitting regions overlapping both arms:'
    print '  q contrast unchanged ... :',nochange
    print '  q contrast changed ..... :',k-nochange
    print ''

#==================================================================================================================
