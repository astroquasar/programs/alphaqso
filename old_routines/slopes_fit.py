import alpha

class slopes_fit(object):

    """
    Plot chi-square curves for simulations.

    This operation will produce chi-square and da/a curves from simulated
    absorption systems. This is similar than plot_curve_real with the
    difference that we are not doing at different chi-square curve mode
    and we only look at turbulent fit.
    """ 

    def showhelp(self):
        
        print ""
        print "-------------------------------------------------------------------------"
        print ""
        print "required arguments:"
        print ""
        print "   --instrument  High-resolution spectrograph used"
        print "                   [uves]  VLT spectrograph"
        print "                   [hires] Keck spectrograph"
        print ""
        print "optional arguments:"
        print ""
        print "   --expind      Whether we use individual slope per exposure or not"
        print "   --mode        Specify which transitions to consider"
        print "   --show        Print out the results for every slope in the terminal"
        print "   --simulation  Type of simulated model to be used"
        print "                   [simple]  One component per system"
        print "                   [complex] Realistic kinematic structure"
        print "   --stdev       Standard deviation for random slope per exposure"
        print "   --whitmore    Look at results from simplistic distortion model"
        print ""
        print "example:"
        print ""
        print "   alpha slopes_fit --instrument uves --simulation complex \ "
        print "                    --expind --stdev 0.05"
        print ""
        print "-------------------------------------------------------------------------"
        print ""
        quit()
        
    def __init__(self):
        
        # Display help message or check if required arguments are given
        if '--help' in sys.argv or '-h' in sys.argv:
            self.showhelp()
        elif setup.instrument==None:
            print 'ERROR: --instrument is missing...'
            quit()
        if '--hist' in sys.argv:
            self.makehist()
            quit()
        folder = setup.instrument
        folder = folder+'_jw' if '--whitmore' in sys.argv else folder
        folder = folder+'_sim_%.2f'%setup.stdev  if setup.simulation!=None and setup.expind==False else folder
        folder = folder+'_sim_%.2fT'%setup.stdev if setup.simulation!=None and setup.expind==True  else folder
        os.system('mkdir -p '+setup.home+'/results/slopes/'+folder)
        os.chdir(setup.home+'/results/slopes/'+folder)
        reflist = np.empty((0,2))
        systems = np.empty((0,3))
        outfile = open('slopes.dat','w')
        sim     = 'sim2' if '--expind' in sys.argv or setup.simulation==None else 'sim'
        chsqlim = 1
        syslist = []
        for i in range(len(setup.distres)):
            slope   = setup.distres['%s slope%02i'%(sim,100*setup.stdev)][i] if setup.simulation!=None else setup.distres['MoM slope'][i]
            error   = setup.distres['%s error%02i'%(sim,100*setup.stdev)][i] if setup.simulation!=None else setup.distres['MoM error'][i]
            errstat = setup.distres['%s alpha_err%02i'%(sim,100*setup.stdev)][i]   if setup.simulation!=None else setup.distres['MoM alpha_err'][i]
            errsys = setup.distres['%s sys%02i'%(sim,100*setup.stdev)][i]   if setup.simulation!=None else setup.distres['MoM alpha_sys'][i]
            cond1  = slope!='-' and error!='-'
            cond2  = setup.distres['instrument'][i].lower()==setup.instrument
            cond3  = setup.distres['system'][i] not in setup.degensys+setup.badwhit
            cond4a = setup.whitmore==True  and setup.distres['whitmore'][i]=='yes'
            cond4b = setup.whitmore==False and setup.distres['whitmore'][i]=='no'
            cond5a = setup.simulation==None and setup.mode==None
            cond5b = setup.distres['%s res00'%sim][i]<chsqlim and \
                     setup.distres['%s res05'%sim][i]<chsqlim and \
                     setup.distres['%s res10'%sim][i]<chsqlim and \
                     setup.distres['%s res15'%sim][i]<chsqlim and \
                     setup.distres['%s res20'%sim][i]<chsqlim
            if cond1 and cond2 and cond3 and (cond4a or cond4b) and (cond5a or cond5b):
                reflist = np.vstack((reflist,['%.4f'%slope,'%.4f'%error]))
                systems = np.vstack((systems,['%.4f'%slope,'%.4f'%error,setup.distres['system'][i]]))
                print '{:>10} {:>10}'.format('%.4f'%slope,'%.4f'%error)
                outfile.write('{0:>9}{1:>9}\n'.format('%.4f'%slope,'%.4f'%error))
                if errsys/errstat>14:
                    print setup.distres['system'][i]
                syslist.append(errsys/errstat)
        print np.mean(syslist)
        outfile.close()
        # Plot histogram of systematic/statistical values
        xmin,xmax = 0,8
	fig = figure(figsize=(10,7))
        plt.subplots_adjust(left=0.05, right=0.97, bottom=0.07, top=0.95, hspace=0.1, wspace=0)
        ax = plt.subplot(111,xlim=[xmin,xmax])
        ax.hist(syslist,bins=25,stacked=True,fill=True,alpha=0.4,edgecolor="black",range=[xmin,xmax])
        xlabel(r'$\sigma_\mathrm{sys}/\sigma_\mathrm{stat}$',fontsize=10)
        ylabel('Frequency',fontsize=10)
        savefig('hist_systematic.pdf')
        clf()        
        print ''
        # Calculate weighted mean of slope values
        open('command','w').write('slopes.dat\n')
        os.system('wmclipnr2 < command > wmclipnr.dat')
        i,flag = 0,0
        rejects = np.empty((0,2))
        results = np.loadtxt('wmclipnr.dat',dtype=str,delimiter='\n')
        vals = np.empty((0,4))
        while i<len(results):
            line = results[i].split()
            if 'Chi-squared, sample size, weighted mean, error:' in results[i]:
                flag  = 0
                chisq = float(line[6])
                size  = float(line[7])
                mean  = float(line[8])
                error = float(line[9])
                vals  = np.vstack((vals,[chisq,size,mean,error]))
                for j in range(len(reflist)):
                    if len(np.where(np.logical_and(newlist[:,0]==reflist[j,0],newlist[:,1]==reflist[j,1]))[0])==0:
                        rejects = np.vstack((rejects,['%.4f'%float(reflist[j,0]),'%.4f'%float(reflist[j,1])]))
                        reflist = newlist
                        break
            if flag==1:
                newlist = np.vstack((newlist,['%.4f'%(float(line[0])),'%.4f'%(float(line[1]))]))
            if 'SAMPLE AFTER CLIPPING:' in results[i]:
                flag = 1
                newlist = np.empty((0,2))
            i += 1
        for i in range(len(rejects)):
            j = np.where(np.logical_and(systems[:,0]==rejects[i,0],systems[:,1]==rejects[i,1]))[0][0]
            print systems[j,2],rejects[i,0],rejects[i,1]
        os.system('wmclipnr < command > wmclipnr.dat')
        os.system('wmean    < command > wmean.dat')
        os.system('rm command')
        
        fig = figure(figsize=(12,10))
        plt.subplots_adjust(left=0.1, right=0.9, bottom=0.05, top=0.95, hspace=0.05, wspace=0)
        ax = plt.subplot(2,1,1,xlim=[min(vals[:,1]),max(vals[:,1])])#,ylim=[100,600])
        ax.scatter(vals[:,1],vals[:,0],alpha=0.6,color='black',s=20)
        #ax.yaxis.set_major_locator(plt.FixedLocator([200,300,400,500]))
        setp(ax.get_xticklabels(), visible=False)
        plt.gca().invert_xaxis()
        ylabel('Chi-squared',fontsize=10)
        ax = plt.subplot(2,1,2,xlim=[min(vals[:,1]),max(vals[:,1])])#,ylim=[-0.18,-0.02])
        ax.errorbar(vals[:,1],vals[:,2],yerr=vals[:,3],fmt='o',ms=8,markeredgecolor='none',alpha=0.6,color='red',elinewidth=3)
        #ax.yaxis.set_major_locator(plt.FixedLocator([-0.16,-0.14,-0.12,-0.10,-0.08,-0.06,-0.04]))
        plt.gca().invert_xaxis()
        ylabel('Weighted mean',fontsize=10)
        xlabel('Sample size',fontsize=10)
        savefig('wmclipnr.pdf')
        clf()

    def makehist(self):

        fig = figure(figsize=(12,10))
        plt.subplots_adjust(left=0.1, right=0.9, bottom=0.05, top=0.95, hspace=0.1, wspace=0)
        xmin,xmax = -10,10
        stdlist = [0,0.05,0.1,0.15,0.2]
        for i in range(len(stdlist)):
            ax = plt.subplot(len(stdlist),1,i+1,xlim=[xmin,xmax],ylim=[0,30])
            stdev = stdlist[i]
            slopes = np.loadtxt('%s/results/slopes/uves_sim_%.2fT/slopes.dat'%(setup.home,stdev))
            ax.hist(slopes[:,0],bins=50,stacked=True,fill=True,alpha=0.4,edgecolor="black",range=[xmin,xmax])
            ax.yaxis.set_major_locator(MultipleLocator(5))
            ylabel('Frequency',fontsize=10)
            ax.text(7,25,r'$\sigma_\mathrm{intrinsic}=$%0.2f'%stdev)
            if i<len(stdlist)-1: plt.setp(ax.get_xticklabels(),visible=False)
        xlabel('Best distortion slope (m/s/\AA)',fontsize=10)
        savefig('histslopes.pdf')
        clf()
        
