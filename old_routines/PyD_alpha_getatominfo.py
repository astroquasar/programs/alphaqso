#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def isfloat(value):
    try:
      float(value)
      return True
    except ValueError:
      return False

#==================================================================================================================

def makeatomlist(atompath):     # Store data from atom.dat

    atom   = np.empty((0,6))
    atomdat     = np.loadtxt(atompath,dtype='str',delimiter='\n')
    for element in atomdat:
        l       = element.split()
        i       = 0      if len(l[0])>1 else 1
        species = l[0]   if len(l[0])>1 else l[0]+l[1]
        wave    = 0 if len(l)<i+2 else 0 if isfloat(l[i+1])==False else l[i+1]
        f       = 0 if len(l)<i+3 else 0 if isfloat(l[i+2])==False else l[i+2]
        gamma   = 0 if len(l)<i+4 else 0 if isfloat(l[i+3])==False else l[i+3]
        mass    = 0 if len(l)<i+5 else 0 if isfloat(l[i+4])==False else l[i+4]
        alpha   = 0 if len(l)<i+6 else 0 if isfloat(l[i+5])==False else l[i+5]
        if species not in ['>>','<<','<>','__']:
            atom = np.vstack((atom,[species,wave,f,gamma,mass,alpha]))
    return atom
            
#==================================================================================================================

def atominfo(atomID):     # Get atomic data from selected atomID

    target = [0,0,0,0,0]
    atomID = atomID.split('_')
    for i in range(len(self.atom)):
        element     = self.atom[i,0]
        wavelength  = self.atom[i,1]
        oscillator  = self.atom[i,2]
        gammavalue  = self.atom[i,3]
        qcoeff      = self.atom[i,5]
        if (len(atomID)>1 and element==atomID[0] and abs(float(wavelength)-float(atomID[1]))<abs(float(target[1])-float(atomID[1]))) \
           or (len(atomID)==1 and element==atomID[0]):
           target = [element,wavelength,oscillator,gammavalue,qcoeff] 
    if target==[0,0,0,0,0]:
        print atomID,'not identifiable...'
        quit()
    return target
    
#==================================================================================================================
