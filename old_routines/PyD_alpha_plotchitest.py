#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def plotchiglobal():

    ''' Plot parameters '''

    rc('font', size=2, family='sans-serif')
    rc('axes', labelsize=10, linewidth=0.2)
    rc('legend', fontsize=10, handlelength=10)
    rc('xtick', labelsize=7)
    rc('ytick', labelsize=7)
    rc('lines', lw=0.2, mew=0.2)
    rc('grid', linewidth=0.2)

    ''' Define parameters and target sample '''

    xmin,xmax = -0.5,0.5
    threshold = np.array([[ None,   10,1.000,0.900,0.800,0.700],
                          [0.600,0.500,0.400,0.300,0.200,0.100],
                          [0.090,0.080,0.070,0.060,0.050,0.040],
                          [0.030,0.020,0.010,0.009,0.008,0.007],
                          [0.006,0.005,0.004,0.003,0.002,0.001]])

    self.instrument = 'uves'
    pdfname   = 'uves-globchisq-comb' if '--combined' in sys.argv else 'uves-globchisq'
    pdf_pages = PdfPages(pdfname+'.pdf')
                    
    for chisq in threshold:
        
        fig = figure(figsize=(8.27,11.69))
        plt.subplots_adjust(left=0.05, right=0.95, bottom=0.05, top=0.95, hspace=0.15, wspace=0.2)
        i = 1
        
        for criteria in chisq:

            self.selection = 'published'
            if '--combined' in sys.argv:
                self.skipabs = np.empty((0,1))        
                self.test = 'v10_all'
                check_chisq(skipabs=self.skipabs) if criteria==None else check_chisq('chisq',chisq=criteria,skipabs=self.skipabs)
                self.test = 'v10_all_jw'
                check_chisq(skipabs=self.skipabs) if criteria==None else check_chisq('chisq',chisq=criteria,skipabs=self.skipabs)

            for self.test in ['v10_all','v10_all_jw']:

                self.selection = 'published'
                if '--combined' not in sys.argv:
                    check_chisq() if criteria==None else check_chisq('chisq',chisq=criteria)
                    
                print '\n> Test:{0:>13} | Criteria:{1:>8} | Discarded:{2:>4}'.format(self.test,criteria,len(self.skipabs))

                x,yabs,y = getglobchisq()
                xfit,yfit,xmid,xm1sig,xp1sig = fitparabola(x,yabs)

                ymin = 0 if len(yabs)==0 else min(yabs)
                ymax = 0 if len(yabs)==0 else max(yabs)
                ax = plt.subplot(6,2,i,xlim=[xmin,xmax],ylim=[ymin,ymax])
                ax.errorbar(x,yabs,fmt='o',ms=3,markeredgecolor='none',ecolor='grey',alpha=0.7,color='black')
                ax.axhline(y=0,ls='dotted',color='black')
                ax.axvline(x=0,color='black',lw=0.1)
                plot(xfit,yfit,c='red',lw=1) 

                y_formatter = matplotlib.ticker.ScalarFormatter(useOffset=False)
                ax.yaxis.set_major_formatter(y_formatter)
                
                ax.plot(xfit,yfit,color='red')
                ax.axvline(x=xm1sig,color='red',ls='dotted',lw=1)
                ax.axvline(x=xmid,color='red',ls='dashed',lw=1)
                ax.axvline(x=xp1sig,color='red',ls='dotted',lw=1)

                ax.set_title('Global MoM $\chi^2$ - Our model',fontsize=10) if i==1 else None
                ax.set_title('Global MoM $\chi^2$ - Whitmore model',fontsize=10) if i==2 else None
                
                jump = 'no criteria' if criteria==None else '$\chi^2$ jump $<$ '+str(criteria)
                t1 = ax.text(0,ymax-0.10*(ymax-ymin),jump,color='grey',ha='center',fontsize=6)
                t2 = ax.text(0,ymax-0.17*(ymax-ymin),str(len(self.skipabs))+' discarded',color='grey',ha='center',fontsize=6)
                t3 = ax.text(0,ymax-0.24*(ymax-ymin),'$\pm1\sigma = %.3f\pm%.3f$'%(xmid,abs(max(xm1sig,xp1sig)-xmid)),color='red',fontsize=6,ha='center')
                t1.set_bbox(dict(color='white', alpha=0.7, edgecolor=None))
                t2.set_bbox(dict(color='white', alpha=0.7, edgecolor=None))
                t3.set_bbox(dict(color='white', alpha=0.7, edgecolor=None))
                    
                i = i + 1
                
        pdf_pages.savefig(fig)
    pdf_pages.close()

#==================================================================================================================

def plotchidipole():

    ''' Plot parameters '''

    rc('font', size=2, family='sans-serif')
    rc('axes', labelsize=10, linewidth=0.2)
    rc('legend', fontsize=10, handlelength=10)
    rc('xtick', labelsize=7)
    rc('ytick', labelsize=7)
    rc('lines', lw=0.2, mew=0.2)
    rc('grid', linewidth=0.2)

    ''' Define parameters and target sample '''

    xmin,xmax = -0.5,0.5
    threshold = np.array([[ None,   10,1.000,0.900,0.800,0.700],
                          [0.600,0.500,0.400,0.300,0.200,0.100],
                          [0.090,0.080,0.070,0.060,0.050,0.040],
                          [0.030,0.020,0.010,0.009,0.008,0.007],
                          [0.006,0.005,0.004,0.003,0.002,0.001]])

    self.instrument = 'uves'
    pdfname   = 'uves-dipole-comb' if '--combined' in sys.argv else 'uves-dipole'
    pdf_pages = PdfPages(pdfname+'.pdf')
                    
    for chisq in threshold:
        
        fig = figure(figsize=(8.27,11.69))
        plt.subplots_adjust(left=0.05, right=0.95, bottom=0.05, top=0.95, hspace=0.15, wspace=0.2)
        i = 1
        
        for criteria in chisq:

            self.selection = 'published'
            if '--combined' in sys.argv:
                self.skipabs = np.empty((0,1))        
                self.test = 'v10_all'
                check_chisq(skipabs=self.skipabs) if criteria==None else check_chisq('chisq',chisq=criteria,skipabs=self.skipabs)
                self.test = 'v10_all_jw'
                check_chisq(skipabs=self.skipabs) if criteria==None else check_chisq('chisq',chisq=criteria,skipabs=self.skipabs)

            for self.test in ['v10_all','v10_all_jw']:

                self.selection = 'published'
                if '--combined' not in sys.argv:
                    check_chisq() if criteria==None else check_chisq('chisq',chisq=criteria)
                self.selection = self.selection if criteria==None else self.selection+'/threshold/'+'%.3f'%criteria
                if os.path.exists(self.Crep+self.instrument+'/'+self.selection+'/'+self.test+'/')==False:
                    break
                
                print '\n> Test:{0:>13} | Criteria:{1:>8} | Discarded:{2:>4}'.format(self.test,criteria,len(self.skipabs))

                x,yabs,y = getdipolechisq()
                xfit,yfit,xmid,xm1sig,xp1sig = fitparabola(x,yabs)

                ymin = 0 if len(yabs)==0 else min(yabs)
                ymax = 0 if len(yabs)==0 else max(yabs)
                ax = plt.subplot(6,2,i,xlim=[xmin,xmax],ylim=[ymin,ymax])
                ax.errorbar(x,yabs,fmt='o',ms=3,markeredgecolor='none',ecolor='grey',alpha=0.7,color='black')
                ax.axhline(y=0,ls='dotted',color='black')
                ax.axvline(x=0,color='black',lw=0.1)
                plot(xfit,yfit,c='red',lw=1) 

                y_formatter = matplotlib.ticker.ScalarFormatter(useOffset=False)
                ax.yaxis.set_major_formatter(y_formatter)
                
                ax.plot(xfit,yfit,color='red')
                ax.axvline(x=xm1sig,color='red',ls='dotted',lw=1)
                ax.axvline(x=xmid,color='red',ls='dashed',lw=1)
                ax.axvline(x=xp1sig,color='red',ls='dotted',lw=1)

                ax.set_title('Dipole $\chi^2$ - Our model',fontsize=10) if i==1 else None
                ax.set_title('Dipole $\chi^2$ - Whitmore model',fontsize=10) if i==2 else None
                
                jump = 'no criteria' if criteria==None else '$\chi^2$ jump $<$ '+str(criteria)
                t1 = ax.text(0,ymax-0.10*(ymax-ymin),jump,color='grey',ha='center',fontsize=6)
                t2 = ax.text(0,ymax-0.17*(ymax-ymin),str(len(self.skipabs))+' discarded',color='grey',ha='center',fontsize=6)
                t3 = ax.text(0,ymax-0.24*(ymax-ymin),'$\pm1\sigma = %.3f\pm%.3f$'%(xmid,abs(max(xm1sig,xp1sig)-xmid)),color='red',fontsize=6,ha='center')
                t1.set_bbox(dict(color='white', alpha=0.7, edgecolor=None))
                t2.set_bbox(dict(color='white', alpha=0.7, edgecolor=None))
                t3.set_bbox(dict(color='white', alpha=0.7, edgecolor=None))
                    
                i = i + 1
                
        pdf_pages.savefig(fig)
    pdf_pages.close()

#==================================================================================================================

def plotchisigmarand():

    ''' Plot parameters '''

    rc('font', size=2, family='sans-serif')
    rc('axes', labelsize=10, linewidth=0.2)
    rc('legend', fontsize=10, handlelength=10)
    rc('xtick', labelsize=7)
    rc('ytick', labelsize=7)
    rc('lines', lw=0.2, mew=0.2)
    rc('grid', linewidth=0.2)

    ''' Define parameters and target sample '''

    xmin,xmax = -0.5,0.5
    threshold = np.array([[ None,   10,1.000,0.900,0.800,0.700],
                          [0.600,0.500,0.400,0.300,0.200,0.100],
                          [0.090,0.080,0.070,0.060,0.050,0.040],
                          [0.030,0.020,0.010,0.009,0.008,0.007],
                          [0.006,0.005,0.004,0.003,0.002,0.001]])

    self.instrument = 'uves'
    pdfname   = 'uves-sigmarand-comb' if '--combined' in sys.argv else 'uves-sigmarand'
    pdf_pages = PdfPages(pdfname+'.pdf')
                    
    for chisq in threshold:
        
        fig = figure(figsize=(8.27,11.69))
        plt.subplots_adjust(left=0.05, right=0.95, bottom=0.05, top=0.95, hspace=0.15, wspace=0.2)
        i = 1
        
        for criteria in chisq:

            self.selection = 'published'
            if '--combined' in sys.argv:
                self.skipabs = np.empty((0,1))        
                self.test = 'v10_all'
                check_chisq(skipabs=self.skipabs) if criteria==None else check_chisq('chisq',chisq=criteria,skipabs=self.skipabs)
                self.test = 'v10_all_jw'
                check_chisq(skipabs=self.skipabs) if criteria==None else check_chisq('chisq',chisq=criteria,skipabs=self.skipabs)

            for self.test in ['v10_all','v10_all_jw']:

                self.selection = 'published'
                if '--combined' not in sys.argv:
                    check_chisq() if criteria==None else check_chisq('chisq',chisq=criteria)
                self.selection = self.selection if criteria==None else self.selection+'/threshold/'+'%.3f'%criteria
                if os.path.exists(self.Crep+self.instrument+'/'+self.selection+'/'+self.test+'/')==False:
                    break
                
                print '\n> Test:{0:>13} | Criteria:{1:>8} | Discarded:{2:>4}'.format(self.test,criteria,len(self.skipabs))

                x,y = getsigmarand()
                xfit,yfit,xmid,xm1sig,xp1sig = fitparabola(x,y)

                ymin = 0 if len(y)==0 else min(y)
                ymax = 0 if len(y)==0 else max(y)
                ax = plt.subplot(6,2,i,xlim=[xmin,xmax],ylim=[ymin,ymax])
                ax.errorbar(x,y,fmt='o',ms=3,markeredgecolor='none',ecolor='grey',alpha=0.7,color='black')
                ax.axhline(y=0,ls='dotted',color='black')
                ax.axvline(x=0,color='black',lw=0.1)
                plot(xfit,yfit,c='red',lw=1) 

                y_formatter = matplotlib.ticker.ScalarFormatter(useOffset=False)
                ax.yaxis.set_major_formatter(y_formatter)
                
                ax.plot(xfit,yfit,color='red')
                ax.axvline(x=xmid,color='red',ls='dashed',lw=1)

                ax.set_title('$\sigma_\mathrm{rand}$ - Our model',fontsize=10) if i==1 else None
                ax.set_title('$\sigma_\mathrm{rand}$ - Whitmore model',fontsize=10) if i==2 else None
                
                jump = 'no criteria' if criteria==None else '$\chi^2$ jump $<$ '+str(criteria)
                t1 = ax.text(0,ymax-0.10*(ymax-ymin),jump,color='grey',ha='center',fontsize=6)
                t2 = ax.text(0,ymax-0.17*(ymax-ymin),str(len(self.skipabs))+' discarded',color='grey',ha='center',fontsize=6)
                t3 = ax.text(0,ymax-0.24*(ymax-ymin),'Minimum at %.3f'%xmid,color='red',fontsize=6,ha='center')
                t1.set_bbox(dict(color='white', alpha=0.7, edgecolor=None))
                t2.set_bbox(dict(color='white', alpha=0.7, edgecolor=None))
                t3.set_bbox(dict(color='white', alpha=0.7, edgecolor=None))
                    
                i = i + 1
                
        pdf_pages.savefig(fig)
    pdf_pages.close()

#==================================================================================================================

def plotchimodel():

    ''' Plot parameters '''

    rc('font', size=2, family='sans-serif')
    rc('axes', labelsize=10, linewidth=0.2)
    rc('legend', fontsize=10, handlelength=10)
    rc('xtick', labelsize=7)
    rc('ytick', labelsize=7)
    rc('lines', lw=0.2, mew=0.2)
    rc('grid', linewidth=0.2)

    ''' Define parameters and target sample '''

    xmin,xmax = -0.5,0.5
    threshold = np.array([[ None,   10,1.000,0.900,0.800,0.700],
                          [0.600,0.500,0.400,0.300,0.200,0.100],
                          [0.090,0.080,0.070,0.060,0.050,0.040],
                          [0.030,0.020,0.010,0.009,0.008,0.007],
                          [0.006,0.005,0.004,0.003,0.002,0.001]])

    self.test = 'v10_slope_fddhires' if self.instrument=='hires' else None
    self.test = 'v10_all' if self.instrument=='uves' else self.test
    self.test = 'v10_all_jw' if self.instrument=='uves' and '--whitmore' in sys.argv else self.test
    pdfname   = self.instrument+'-whitmore' if self.instrument=='uves' and '--whitmore' in sys.argv else self.instrument
    pdf_pages = PdfPages(pdfname+'.pdf')
    
    for chisq in threshold:
        
        fig = figure(figsize=(8.27,11.69))
        plt.subplots_adjust(left=0.05, right=0.95, bottom=0.05, top=0.95, hspace=0.15, wspace=0.2)
        i = 1
        
        for criteria in chisq:
            
            for model in ['turb','ther',None]:

                check_chisq() if criteria==None else check_chisq('chisq',chisq=criteria,model=model)

                print '\n> Model:{0:>5} | Criteria:{1:>8} | Discarded:{2:>4}'.format(model,criteria,len(self.skipabs))

                x,yabs,yred = getglobchisq(model=model)
                xfit,yfit,xmid,xm1sig,xp1sig = fitparabola(x,yabs)

                ymin = 0 if len(yabs)==0 else min(yabs)
                ymax = 0 if len(yabs)==0 else max(yabs)
                ax = plt.subplot(6,3,i,xlim=[xmin,xmax],ylim=[ymin,ymax])
                ax.errorbar(x,yabs,fmt='o',ms=3,markeredgecolor='none',ecolor='grey',alpha=0.7,color='black')
                ax.axhline(y=0,ls='dotted',color='black')
                ax.axvline(x=0,color='black',lw=0.1)
                plot(xfit,yfit,c='red',lw=1) 

                y_formatter = matplotlib.ticker.ScalarFormatter(useOffset=False)
                ax.yaxis.set_major_formatter(y_formatter)
                
                ax.plot(xfit,yfit,color='red')
                ax.axvline(x=xm1sig,color='red',ls='dotted',lw=1)
                ax.axvline(x=xmid,color='red',ls='dashed',lw=1)
                ax.axvline(x=xp1sig,color='red',ls='dotted',lw=1)
                
                mod = 'Turbulent' if i==1 else 'Thermal' if i==2 else 'MoM'
                ax.set_title(mod+' $\chi^2$',fontsize=10) if i in [1,2,3] else None

                jump = 'no criteria' if criteria==None else '$\chi^2$ jump $<$ '+str(criteria)
                t1 = ax.text(0,ymax-0.10*(ymax-ymin),jump,color='grey',ha='center',fontsize=6)
                t2 = ax.text(0,ymax-0.17*(ymax-ymin),str(len(self.skipabs))+' discarded',color='grey',ha='center',fontsize=6)
                t3 = ax.text(0,ymax-0.24*(ymax-ymin),'$\pm1\sigma = %.3f\pm%.3f$'%(xmid,abs(max(xm1sig,xp1sig)-xmid)),color='red',fontsize=6,ha='center')
                t1.set_bbox(dict(color='white', alpha=0.7, edgecolor=None))
                t2.set_bbox(dict(color='white', alpha=0.7, edgecolor=None))
                t3.set_bbox(dict(color='white', alpha=0.7, edgecolor=None))
                    
                i = i + 1
                
        pdf_pages.savefig(fig)
    pdf_pages.close()

#==================================================================================================================

def plotchitelescope():

    ''' Plot parameters '''

    rc('font', size=2, family='sans-serif')
    rc('axes', labelsize=10, linewidth=0.2)
    rc('legend', fontsize=10, handlelength=10)
    rc('xtick', labelsize=7)
    rc('ytick', labelsize=7)
    rc('lines', lw=0.2, mew=0.2)
    rc('grid', linewidth=0.2)

    ''' Define parameters and target sample '''

    xmin,xmax = -0.5,0.5
    threshold = np.array([[ None,   10,1.000,0.900,0.800,0.700],
                          [0.600,0.500,0.400,0.300,0.200,0.100],
                          [0.090,0.080,0.070,0.060,0.050,0.040],
                          [0.030,0.020,0.010,0.009,0.008,0.007],
                          [0.006,0.005,0.004,0.003,0.002,0.001]])

    pdf_pages = PdfPages('instrument_noalpha.pdf') if '--noalpha' in sys.argv else PdfPages('instrument.pdf')
    
    for chisq in threshold:
        
        fig = figure(figsize=(8.27,11.69))
        plt.subplots_adjust(left=0.05, right=0.95, bottom=0.05, top=0.95, hspace=0.15, wspace=0.2)
        i = 1
        
        for criteria in chisq:
            
            testuves  = 'v10_all_noalpha' if '--noalpha' in sys.argv else 'v10_all'
            testhires = 'v10_slope_fddhires_noalpha' if '--noalpha' in sys.argv else 'v10_slope_fddhires'

            for self.test in [testuves,testhires]:

                self.instrument = 'uves' if self.test==testuves else 'hires'
                check_chisq() if criteria==None else check_chisq('chisq',chisq=criteria)
                print '\n> Instrument:{0:>6} | Criteria:{1:>8} | Discarded:{2:>4}'.format(self.instrument,criteria,len(self.skipabs))
                
                x,yabs,y = getglobchisq()
                xfit,yfit,xmid,xm1sig,xp1sig = fitparabola(x,yabs)

                ymin = 0 if len(yabs)==0 else min(yabs)
                ymax = 0 if len(yabs)==0 else max(yabs)
                ax = plt.subplot(6,2,i,xlim=[xmin,xmax],ylim=[ymin,ymax])
                ax.errorbar(x,yabs,fmt='o',ms=3,markeredgecolor='none',ecolor='grey',alpha=0.7,color='black')
                ax.axhline(y=0,ls='dotted',color='black')
                ax.axvline(x=0,color='black',lw=0.1)
                plot(xfit,yfit,c='red',lw=1) 

                y_formatter = matplotlib.ticker.ScalarFormatter(useOffset=False)
                ax.yaxis.set_major_formatter(y_formatter)
                
                ax.plot(xfit,yfit,color='red')
                ax.axvline(x=xm1sig,color='red',ls='dotted',lw=1)
                ax.axvline(x=xmid,color='red',ls='dashed',lw=1)
                ax.axvline(x=xp1sig,color='red',ls='dotted',lw=1)
                
                ax.set_title('Global MoM $\chi^2$ - UVES',fontsize=10) if i==1 else None                    
                ax.set_title('Global MoM $\chi^2$ - HIRES',fontsize=10) if i==2 else None

                jump = 'no criteria' if criteria==None else '$\chi^2$ jump $<$ '+str(criteria)
                t1 = ax.text(0,ymax-0.10*(ymax-ymin),jump,color='grey',ha='center',fontsize=6)
                t2 = ax.text(0,ymax-0.17*(ymax-ymin),str(len(self.skipabs))+' discarded',color='grey',ha='center',fontsize=6)
                t3 = ax.text(0,ymax-0.24*(ymax-ymin),'$\pm1\sigma = %.3f\pm%.3f$'%(xmid,abs(max(xm1sig,xp1sig))-xmid),color='red',fontsize=6,ha='center')
                t1.set_bbox(dict(color='white', alpha=0.7, edgecolor=None))
                t2.set_bbox(dict(color='white', alpha=0.7, edgecolor=None))
                t3.set_bbox(dict(color='white', alpha=0.7, edgecolor=None))

                i = i + 1                
                
        pdf_pages.savefig(fig)
    pdf_pages.close()

#==================================================================================================================
