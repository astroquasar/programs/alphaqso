#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *
        
#==================================================================================================================

def run():

    selection = self.selection
    fortlist  = self.fortlist()
    filelist  = np.loadtxt(fortlist,dtype='str',comments='!',ndmin=1)
    for i in range(len(filelist)):
        self.distpath  = filelist[i]
        self.qso       = filelist[i].split('/')[-7]
        self.zabs      = filelist[i].split('/')[-6]
        self.sample    = filelist[i].split('/')[-5]
        self.selection = self.qso+'/'+self.zabs+'/'+self.sample
        targetmodel    =  os.path.realpath(self.fitdir+self.selection+'/'+self.model).split('/')[-1]
        print '|- System:',self.selection
        print '|  |- Model-Test:',targetmodel,'-',self.test
        print '|  |  |- Distortion slope:',self.distortion
        for nrep in range(0,self.totrep+1,1):
            self.modflag = self.modflag if nrep==0 else self.modflag.replace('-'+str(nrep-1),'-'+str(nrep))
            print '|  |  |  |- VPFIT iteration:',nrep+1
            if '--nofit' not in sys.argv:
                if self.prepare('turbulent')=='proceed':
                    print '|  |  |  |  |- Processing Turbulent fitting...'
                    self.fit('turbulent')
                if self.prepare('thermal')=='proceed':
                    print '|  |  |  |  |- Processing Thermal fitting...'
                    self.fit('thermal')
            elif os.path.exists(self.distpath+'/thermal'+self.modflag+'/thermal_fit.13')==True:
                os.chdir(self.distpath+'/turbulent'+self.modflag+'/')
                os.system('PyD_velplot turbulent_ini.13 --output turbulent_ini --version vpfit10 --header header.dat --vpfsetup ./vp_setup.dat --getwave --atomdir ./atom.dat --details')
                os.system('PyD_velplot turbulent_fit.13 --output turbulent_fit --version vpfit10 --header header.dat --vpfsetup ./vp_setup.dat --getwave --atomdir ./atom.dat --details')
                os.chdir(self.distpath+'/thermal'+self.modflag+'/')
                os.system('PyD_velplot thermal_ini.13 --output thermal_ini --version vpfit10 --header header.dat --vpfsetup ./vp_setup.dat --getwave --atomdir ./atom.dat --details')
                os.system('PyD_velplot thermal_fit.13 --output thermal_fit --version vpfit10 --header header.dat --vpfsetup ./vp_setup.dat --getwave --atomdir ./atom.dat --details')
            else:
                print '|  |  |  |- Path not found for plotting...'

        print '|'

        if '--compress' in sys.argv:

            destination = self.here.replace('list','execute')+'/'
            os.chdir(self.fitdir)
            target   = self.selection+'/'+self.model+'/runs/'+self.test+'/'+self.distortion
            filename = target.replace('/','--')
            os.system('tar -zcvf '+filename+'.tar.gz '+target+'/')
            os.system('rm -rf '+target+'/')
            os.system('mkdir -p '+destination)
            os.system('mv '+filename+'.tar.gz '+destination)
            
        os.chdir(self.here)

    os.system('rm '+fortlist)
    self.selection = selection
            
#==================================================================================================================
