import alpha

def curve_sim():

    """
    Plot chi-square and da/a curves from simulated
    absorption systems. This is similar than plot_curve_real with the
    difference that we are not doing at different chi-square curve mode
    and we only look at turbulent fit.
    """ 

    def showhelp():
        
        print ""
        print "-------------------------------------------------------------------------"
        print ""
        print "required arguments:"
        print ""
        print "   --instrument  High-resolution spectrograph used"
        print "                   [uves]  VLT spectrograph"
        print "                   [hires] Keck spectrograph"
        print ""
        print "   --simulation  Type of simulated model to be used"
        print "                   [simple]  One component per system"
        print "                   [complex] Realistic kinematic structure"
        print ""
        print "optional arguments:"
        print ""
        print "   --expind      Whether we use individual slope per exposure or not"
        print "   --noplot      Do not make the figures"
        print "   --selection   Individual or list of absorption systems to study"
        print "   --show        Print out the results for every slope in the terminal"
        print "   --stdev       Standard deviation for random slope per exposure"
        print "   --whitmore    Look at results from simplistic distortion model"
        print ""
        print "example:"
        print ""
        print "   alpha curve_sim --instrument uves --simulation complex \ "
        print "                   --expind --stdev 0.05"
        print ""
        print "-------------------------------------------------------------------------"
        print ""
        quit()
        
    # Display help message or check if required arguments are given
    if '--help' in sys.argv or '-h' in sys.argv: showhelp()
    elif setup.instrument==None or setup.simulation==None:
        print 'ERROR: Either --instrument or --simulation is missing...'
        quit()
    # Initializing the figure
    rc('font', size=2, family='sans-serif')
    rc('axes', labelsize=10, linewidth=0.2)
    rc('legend', fontsize=10, handlelength=10)
    rc('xtick', labelsize=7)
    rc('ytick', labelsize=7)
    rc('lines', lw=0.2, mew=0.2)
    rc('grid', linewidth=0.4)
    # Define path and filename of output figure
    path       = 'sim1/' if setup.expind==False else 'sim2/'
    setup.name = setup.instrument+'-sim_%.2f'%setup.stdev
    setup.name = setup.name + '_v10_slope' if setup.instrument=='hires'    else setup.name
    setup.name = setup.name + '_v10_all'   if setup.instrument=='uves'     else setup.name
    setup.name = setup.name + '_jw'        if '--whitmore' in sys.argv     else setup.name
    setup.name = setup.name + '_prev'      #if '--previous' in sys.argv    else setup.name
    # Go to specific curve directory where all the figures will be stored
    os.chdir(setup.home+'/results/curves/'+path)
    # If list of published system selected, write results on ASCII file
    writefile  = True if setup.selection=='published' else False
    if writefile==True:
        output = open('temp.dat','w')
    # Store indexes of selected systems in a list
    idxs = []
    for i in range(len(setup.distres)):
        cond1 = setup.selection in [setup.distres['system'][i],'published']
        cond2 = setup.distres['instrument'][i].lower()==setup.instrument
        cond3 = setup.sample==None or setup.sample in setup.distres['system'][i]
        if cond1 and cond2 and cond3:
            idxs.append(i)
    # Loop over all selected systems
    for i in idxs:
        # Extract tabulating distortion information
        setup.selection  = setup.distres['system'][i]
        setup.instrument = setup.distres['instrument'][i].lower() 
        suffix           = '%02i'%(100*setup.stdev)
        setup.distmid    = float(setup.distres['mid'+suffix][i])
        setup.distmin    = float(setup.distres['min'+suffix][i])
        setup.distmax    = float(setup.distres['max'+suffix][i])
        setup.distsep    = float(setup.distres['step'+suffix][i])
        setup.wminturb   = float(setup.distres['minfit'+suffix][i])
        setup.wmaxturb   = float(setup.distres['maxfit'+suffix][i])
        setup.chisq      = float(setup.distres['threshold'][i])
        setup.model      = 'model-%.1f'%float(setup.distres['model'][i])
        setup.test       = 'v10_chisq%.E_step%.E_prev'%(setup.chisq,setup.distsep)
        tilt             = '0.000' if round(setup.distmid,3)==0 else 'm%.3f'%abs(setup.distmid) if setup.distmid<0 else 'p%.3f'%abs(setup.distmid)
        deviation        = '%.2f'%setup.stdev if setup.expind else '0.00'
        simtest          = setup.simulation+'_'+tilt+'_'+deviation
        simtest          = simtest+'_snr%.E'%setup.snr if '--snr' in sys.argv else simtest
        print '\n',setup.selection
        if setup.distmin < setup.distmax <= setup.distmid:
            setup.distlist = numpy.arange(setup.distmax,setup.distmin-0.001,-setup.distsep)
            setup.distplot = sorted(setup.distlist)
        elif setup.distmax > setup.distmin >= setup.distmid:
            setup.distlist = numpy.arange(setup.distmin,setup.distmax+0.001,setup.distsep)
            setup.distplot = sorted(setup.distlist)
        elif setup.distmin < setup.distmid < setup.distmax:
            setup.distlist = numpy.hstack((numpy.arange(setup.distmid,setup.distmax+0.001,setup.distsep),numpy.arange(setup.distmid,setup.distmin-0.001,-setup.distsep)))
            setup.distplot = sorted(numpy.delete(setup.distlist,0))
        # Extract turbulent results
        fitres   = numpy.empty((0,5))
        for j in setup.distplot:
            strslope      = '0.000' if round(j,3)==0 else str('%.3f'%j).replace('-','m') if '-' in str(j) else 'p'+str('%.3f'%j)
            setup.distpath = setup.fitdir+'/'+setup.selection+'/'+setup.model+'/sims/'+simtest+'/'+setup.test+'/'+strslope+'/'
            setup.quasar   = setup.selection.split('/')[0]
            setup.zabs     = setup.selection.split('/')[1]
            setup.sample   = setup.selection.split('/')[2]
            if os.path.exists(setup.distpath+'/turbulent/turbulent.18')==True:
                getresults()
                dist          = round(j,3)
                fitres        = numpy.vstack((fitres,[None]*5))
                fitres[-1,0]  = dist
                fitres[-1,1]  = setup.turb_df
                fitres[-1,2]  = setup.turb_chisq
                fitres[-1,3]  = setup.turb_alpha
                fitres[-1,4]  = setup.turb_error
                if '-' not in fitres[-1]:
                    if '--show' in sys.argv:
                        print '{:>7} '.format('%.3f'%dist),
                        print '{:>11}'.format('%i'%setup.turb_df),
                        print '{:>11}'.format('%.4f'%setup.turb_chisq),
                        print '{:>11}'.format('%.4f'%setup.turb_alpha),
                        print '{:>11}'.format('%.4f'%setup.turb_error)
                else:
                    fitres = numpy.delete(fitres,-1,0)
        # Plot turbulent chi-square curve
        fig = figure(figsize=(6,7))
        plt.subplots_adjust(left=0.1, right=0.97, bottom=0.07, top=0.98, hspace=0.03, wspace=0)
        x,y = fitres[:,0],fitres[:,2]
        ymin = 0 if len(y)==0 else min(y)-1 if min(y)==max(y) else min(y)
        ymax = 1 if len(y)==0 else max(y)+1 if min(y)==max(y) else max(y)
        ax   = plt.subplot(211,xlim=[setup.distmin,setup.distmax])
        ax.errorbar(x,y,fmt='o',ms=6,markeredgecolor='none',ecolor='grey',alpha=0.7,color='black')
        if setup.selection not in setup.degensys+setup.badwhit and len(fitres)>0:
            fitparabola2(x,y,model='Turbulent')
        ax.yaxis.set_major_formatter(matplotlib.ticker.ScalarFormatter(useOffset=False))
        ax.set_ylabel('$\chi_\mathrm{abs}^2$',fontsize=10)
        plt.setp(ax.get_xticklabels(),visible=False)
        ylim(ymin,ymax)
        t1 = ax.text((setup.distmin+setup.distmax)/2,ymax-0.1*(ymax-ymin),setup.selection,color='grey',ha='center',fontsize=6)
        t1.set_bbox(dict(color='white', alpha=0.7, edgecolor=None))
        x,y,yerr = fitres[:,0],fitres[:,3]*10,fitres[:,4]*10
        ymin = 0 if len(y)==0 else min(y)-1 if min(y)==max(y) else min(y-yerr)
        ymax = 1 if len(y)==0 else max(y)+1 if min(y)==max(y) else max(y+yerr)
        ax   = plt.subplot(212,xlim=[setup.distmin,setup.distmax])
        ax.errorbar(x,y,fmt='o',ms=6,markeredgecolor='none',ecolor='grey',alpha=0.7,color='black')
        if setup.selection not in setup.degensys+setup.badwhit and len(fitres)>0:
            fitlinear(x,y,yerr=yerr,model='Turbulent')
        ax.yaxis.set_major_formatter(matplotlib.ticker.ScalarFormatter(useOffset=False))
        ax.set_xlabel(r'Distortion slope (m/s/$\mathrm{\AA}$)',fontsize=10)
        ax.set_ylabel(r'$\Delta\alpha/\alpha$ (ppm)',fontsize=10)
        ylim(ymin,ymax)
        t1 = ax.text((setup.distmin+setup.distmax)/2,ymax-0.1*(ymax-ymin),setup.selection,color='grey',ha='center',fontsize=6)
        t1.set_bbox(dict(color='white', alpha=0.7, edgecolor=None))
        os.system('mkdir -p '+setup.name)
        savefig(setup.name + '/' + setup.selection.replace('/','_').replace('.','z')+'.pdf')
        plt.close(fig)
        if writefile==True:
            output.write('{:<40}\t'.format(setup.selection))
            output.write('{0:>10}\t'.format('%.2f'%numpy.mean(fitres[:,2])))
            slope = 'badwhit' if setup.selection in setup.badwhit and '--whitmore' in sys.argv else 'degenerate' if setup.selection in setup.degensys else '%.4f'%setup.slope
            error = 'badwhit' if setup.selection in setup.badwhit and '--whitmore' in sys.argv else 'degenerate' if setup.selection in setup.degensys else '%.4f'%setup.slope_error
            output.write('{0:>10}\t{1:>10}\t'.format(slope,error))
            output.write('{0:>10}\t\n'.format(setup.residuals))
    if writefile==True:
        output.close()
        os.system('mv temp.dat '+setup.name+'.dat')
        
