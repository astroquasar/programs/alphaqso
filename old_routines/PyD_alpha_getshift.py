#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def getshift(left,right,slope,mode=None):

    def uves_slope(cent,x,slope):
        return slope*(x-cent)
    def uves_whitmore(x,slope):
        if x < 3259.1:
            return slope*(3259.1-3900)
        if 3259.1 < x < 4518.8:
            return slope*(x-3900)
        if 4518.8 < x < 4726.9:
            y1 = -slope*3900 + slope*4518.8
            y2 = -slope*5800 + slope*4726.9
            a  = (y2-y1)/(4726.9-4518.8)
            b  = 4518.8 - y1/a
            return a*(x-b)
        if 4726.9 < x < 6834.9:
            return slope*(x-5800)
        if x > 6834.9:
            return slope*(6834.9-5800)
    def hires_amplitude(left,right,amplitude):
        middle = (left+right)/2
        x0,x1  = 3800.,5300.
        if middle<x0:
            return amplitude
        if x0<middle<x1:
            return amplitude * (np.cos((middle-x0)*np.pi/(x1-x0)))**2
        if x1<middle:
            return amplitude
    def hires_slope(x,slope):
        return slope*(x-5500.)

    middle = (left+right)/2
    shift = blflag = rdflag = 0
    if self.sample in self.vlt:                                                                                           # If data sample of the selected system is a UVES system
        if '--whitmore' in sys.argv and slope!=0:                                                                         # If Whitmore distortion model and non-zero slope
            shift = uves_whitmore(middle,slope)                                                                           # Calculate the shift based on the Whitmore model
        elif 'UVES_squader' in self.selection:                                                                            # If selected system is using UVES SQUADER data
            pos = np.where(self.uvesexp['name']==self.qso)[0][0]                                                          # Search position of the quasar in the list of UVES exposures (self.uvesexp)
            sumshift = sumcount = 0                                                                                       # Initialize total shift and exposure time parameter
            for l in range (pos,len(self.uvesexp)):                                                                       # Loop in the list of UVES exposures until break is called
                if self.uvesexp['name'][l]!=self.qso:                                                                     # If quasar name in list different from the selected system
                    break                                                                                                 # Break the loop
                for k in range(len(self.uvesset)):                                                                        # For each exposure, loop over all the existing settings
                    cond1 = self.uvesset['cent'][k]==self.uvesexp['cent'][l]                                              # Condition 1: Central wavelength of setting same than the exposure
                    cond2 = self.uvesset['arm'][k]==self.uvesexp['arm'][l]                                                # Condition 2: Optical arm of setting same than the exposure
                    cond3 = self.uvesset['mode'][k]==self.uvesexp['mode'][l]                                              # Condition 3: Optical mode of setting same than the exposure
                    if cond1==cond2==cond3==True:                                                                         # If the 3 conditions are satisfied
                        wbeg  = 10*float(self.uvesset['TS_min'][k])                                                       # Define starting wavelength (in Angstrom) from value in the list of settings (in nm)
                        cent  = 10*float(self.uvesset['cent'][k])                                                         # Define central wavelength (in Angstrom) from value in the list of settings (in nm)
                        wend  = 10*float(self.uvesset['TS_max'][k])                                                       # Define ending wavelength (in Angstrom) from value in the list of settings (in nm)
                        break                                                                                             # Break the loop in the list of settings as the correct setting has been matched
                if mode=='create' and '--expind' in sys.argv:                                                             # If creating spectrum and expind option used, define a random slope based on exposure settings
                    if len(self.shifts)==0 or str(self.uvesexp['dataset'][l]) not in self.shifts[:,0]:                    # If no slope already attributed or exposure not yet tabulated in self.shifts
                        slope = self.distmid if self.stdev==0 else np.random.normal(self.distmid,self.stdev)              # Determine slope for this exposure using gaussian distribution
                        self.shifts = np.vstack((self.shifts,[str(self.uvesexp['dataset'][l]),str(slope)]))               # Store the exposure name and associated slope value calculated in self.shifts
                    if len(self.shifts)>0 and str(self.uvesexp['dataset'][l]) in self.shifts[:,0]:                        # If exposure already tabulated in self.shifts
                        idx   = np.where(self.shifts[:,0]==self.uvesexp['dataset'][l])[0][0]                              # Find index position of this exposure in self.shifts
                        slope = float(self.shifts[idx,1])                                                                 # Extract previously calculated and stored slope value for this exposure
                if self.uvesexp['arm'][l]=='BLUE' and wbeg < left and right < wend:
                    sumshift = sumshift + np.sqrt(float(self.uvesexp['exptime'][l])) * uves_slope(cent,middle,slope)
                    sumcount = sumcount + np.sqrt(float(self.uvesexp['exptime'][l]))
                    blflag = 1
                if self.uvesexp['arm'][l]=='RED' and wbeg < left and right < wend:
                    sumshift = sumshift + np.sqrt(float(self.uvesexp['exptime'][l])) * uves_slope(cent,middle,slope)
                    sumcount = sumcount + np.sqrt(float(self.uvesexp['exptime'][l]))
                    rdflag = 1
            shift = sumshift / sumcount
            self.regtotal = self.regtotal + 1
            if blflag==1 and rdflag==0:
                self.armflag = 'blue'
                self.regblue = self.regblue + 1
            if blflag==0 and rdflag==1:
                self.armflag = 'red'
                self.regred = self.regred + 1
            if blflag==rdflag==1:
                self.armflag = 'overlap'
                self.regboth = self.regboth + 1
    if self.sample in self.keck:
        if self.slope==0:
            shift = 0
        elif '--whitmore' in sys.argv and self.shape=='amplitude':
            shift = shift + hires_amplitude(left,right,slope)
        elif '--whitmore' in sys.argv and self.shape=='slope':
            shift = shift + hires_slope(middle,slope)
        elif 'HIRES_sargent' in self.selection:
            pos = np.where(np.logical_and(self.hiresexp['name']==self.qso,self.hiresexp['sample']==self.sample))[0][0]
            sumshift = sumcount = 0
            for l in range (pos,len(self.hiresexp)):
                if self.hiresexp['name'][l]!=self.qso:
                    break
                wbeg = float(self.hiresexp['blue'][l])
                cent = float(self.hiresexp['cent'][l])
                wend = float(self.hiresexp['red'][l])
                if wbeg < left and right < wend:
                    sumshift = sumshift + np.sqrt(float(self.hiresexp['exptime'][l])) * uves_slope(cent,middle,slope)
                    sumcount = sumcount + np.sqrt(float(self.hiresexp['exptime'][l]))
            shift = sumshift / sumcount
            
    return '{0:.4f}'.format(float(shift)/1000.)

#==================================================================================================================
