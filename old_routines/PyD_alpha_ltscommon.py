#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def genrand_real2():

    # Period parameters
    N = 624
    M = 397
    MATRIX_A = 0x09908b0df   # constant vector a
    UPPER_MASK = 0x080000000 # most significant w-r bits
    LOWER_MASK = 0x07fffffff # least significant r bits
    
    # Tempering parameters
    TEMPERING_MASK_B = 0x9d2c5680
    TEMPERING_MASK_C = 0xefc60000
    
    def TEMPERING_SHIFT_U(y):
        return (y >> 11)
    
    def TEMPERING_SHIFT_S(y):
        return (y << 7)
    
    def TEMPERING_SHIFT_T(y):
        return (y << 15)
    
    def TEMPERING_SHIFT_L(y):
        return (y >> 18)

    mt = []   # the array for the state vector
    mti = N+1 # mti==N+1 means mt[N] is not initialized
    mag01 = [0x0, MATRIX_A]
    y = 0
    if mti >= N: # generate N words at one time
        if mti == N+1:   # if init_genrand() has not been called,
            seed = 5489  # a default initial seed is used
            mt = [None] * N
            mt[0] = ctypes.c_uint32(seed).value
            mti = 1
            while (mti < N):
                mt[mti] = ctypes.c_uint32(1812433253 * (mt[mti - 1] ^ (mt[mti - 1] >> 30)) + mti).value
                mti += 1
        kk = 0
        while (kk < N-M):
            y = (mt[kk]&UPPER_MASK)|(mt[kk+1]&LOWER_MASK)
            mt[kk] = mt[kk+M] ^ (y >> 1) ^ mag01[y & 0x1]
            kk += 1
        while (kk < N-1):
            y = (mt[kk]&UPPER_MASK)|(mt[kk+1]&LOWER_MASK)
            mt[kk] = mt[kk+(M-N)] ^ (y >> 1) ^ mag01[y & 0x1]
            kk += 1
        y = (mt[N-1]&UPPER_MASK)|(mt[0]&LOWER_MASK)
        mt[N-1] = mt[M-1] ^ (y >> 1) ^ mag01[y & 0x1]
        mti = 0
    y = mt[mti+1]

    y ^= TEMPERING_SHIFT_U(y)
    y ^= TEMPERING_SHIFT_S(y) & TEMPERING_MASK_B
    y ^= TEMPERING_SHIFT_T(y) & TEMPERING_MASK_C
    y ^= TEMPERING_SHIFT_L(y)

    return y*(1.0/4294967296.0)

#==================================================================================================================

def probit(x):

    # Return the probit function, the inverse normal cumulative distribution function, via a series expansion
    maxiters  = 1000000
    toltarget = 1e-6
    rpot      = np.sqrt(math.pi/2)
    d         = [1]+maxiters*[0]
    xterm     = maxiters*[0]
    tol       = 0
    ans       = 0
    k         = -1
    
    while k==-1 or tol>toltarget:

        k += 1
        if k>=maxiters-1:
            print 'Error: k = %ld too small in probit(). Increase maxiters\n'%k
            print '    Tolerance achieved = %lf which is less than target %lf\n'%(tol,toltarget)
            
        dk = k
        if k==0: # First term
            xterm[k] = (2.0*x-1.0)
            term     = d[k]/(2.0*dk+1)*xterm[k]
            ans      = ans + rpot*term
            prevans  = 1e-5
        else: # Series expansion - generate next coefficient
            prevans  = ans
            xterm[k] = (2.0*x-1.0)*(2.0*x-1.0)*xterm[k-1]
            term     = d[k]/(2.0*dk+1)*xterm[k]
            ans      = ans + rpot*term
          
        # Calculate apparent accuracy
        tol = abs((ans-prevans)/prevans)
        if tol>toltarget:
            # Generate next series coefficient for next iteration
            d[k+1] = 0.0
            for j in range(0,k+1):
                dj     = j
                d[k+1] = d[k+1] + (math.pi/4)*d[j]*d[k-j]/((dj+1)*(2*dj+1))

    return ans

#==================================================================================================================

def dindexx(n,arr,indx):

    def swap(indx,a,b):
        itemp   = indx[a]
        indx[a] = indx[b]
        indx[b] = itemp
        return indx

    # Indexes an array arr[1..n], i.e., outputs the array indx[1..n] such
    # that arr[indx[j]] is in ascending order for j = 1, 2, . . . ,N. The
    # input quantities n and arr are not changed.

    M,ir,l = 7,n,1
    jstack = 0
    NSTACK = 50
    istack = [0]*NSTACK
    indx   = [j for j in range(1,n+1)]
    flag1  = 0
    flag2  = 0
    
    while flag==0:
	if ir-l<M:
            for j in range(l+1,ir+1):
		indxt = indx[j]
		a = arr[indxt]
                for i in range(j-1,l-1,-1):
		    if arr[indx[i]]<=a:
                        break
		    indx[i+1] = indx[i]
		indx[i+1]=indxt
	    if jstack==0:
                flag = 1
                break
	    ir = istack[jstack-1]
	    l = istack[jstack-1]
	else:
	    k = (l+ir) >> 1
	    indx = swap(indx,k,l+1)
	    if arr[indx[l]]>arr[indx[ir]]:
		indx = swap(indx,l,ir)
	    if arr[indx[l+1]]>arr[indx[ir]]:
		indx = swap(indx,l+1,ir)
	    if arr[indx[l]]>arr[indx[l+1]]:
		indx = swap(indx,l,l+1)
	    i = l + 1
	    j = ir
	    indxt = indx[l+1]
	    a = arr[indxt]
            while flag2==0:
                while arr[indx[i]]<a:
		    i+=1
		while arr[indx[j]]>a:
                    j-=1
		if j<i:
                    break
		swap(indx,i,j)
	    indx[l+1] = indx[j]
	    indx[j] = indxt
	    jstack += 2
	    if jstack>NSTACK:
	      print 'ERROR: NSTACK too small in indexx\n'
              quit()
	    if ir-i+1 >= j-l:
		istack[jstack] = ir
		istack[jstack-1] = i
		ir = j-1
	    else:
		istack[jstack] = j-1
		istack[jstack-1] = l
		l = i

#==================================================================================================================
