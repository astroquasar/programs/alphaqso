#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def getresults(original=False):

    self.ther_chisq = self.turb_chisq = self.mom_chisq = self.mom_df = self.mom_alpha = self.mom_error = '-'

    distpath = self.distpath.replace('_mg','') if '--mgisotope' in sys.argv and self.mgflag==0 else self.distpath
    therfort = distpath+'/thermal'+self.modflag+'/thermal.18'     if original==False else distpath+'/thermal.18'
    turbfort = distpath+'/turbulent'+self.modflag+'/turbulent.18' if original==False else distpath+'/turbulent.18'
    # norepeat = fortloc.replace('_'+str(self.totrep),'')

    ''' Store thermal and turbulent chi square, alpha, and error '''
    
    if os.path.exists(therfort)==True:
        self.readfort18(therfort)
    if os.path.exists(turbfort)==True:
        self.readfort18(turbfort)

    ''' Use a weighting method, weighted as the likelihoods '''
    
    if self.turb_chisq!='-' and self.ther_chisq!='-':
        
        k          = self.ther_n - self.ther_df
        ther_AICc  = self.ther_chisq + 2*k + 2*k*(k+1)/(self.ther_n-k-1)
        
        k          = self.turb_n - self.turb_df
        turb_AICc  = self.turb_chisq + 2*k + 2*k*(k+1)/(self.turb_n-k-1)
        
        csmin      = min([ther_AICc,turb_AICc])
        k1         = math.exp(-(ther_AICc-csmin)/2)
        k2         = math.exp(-(turb_AICc-csmin)/2)
        k          = k1 + k2
        k1         = k1/k
        k2         = k2/k
        
        self.mom_df    = k1 * self.ther_df    + k2 * self.turb_df
        self.mom_chisq = k1 * self.ther_chisq + k2 * self.turb_chisq
        self.mom_alpha = k1 * self.ther_alpha + k2 * self.turb_alpha
        self.mom_error = np.sqrt(k1*self.ther_error**2 + \
                                 k2*self.turb_error**2 + \
                                 k1*self.ther_alpha**2 + \
                                 k2*self.turb_alpha**2 - \
                                 self.mom_alpha**2)

#==================================================================================================================

def getoriginal():

    print '\n{0:<50} {1:>10}{2:>15}{3:>15}{4:>15}{5:>15}{6:>15}{7:>15}{8:>15}{9:>15}{10:>15}{11:>15}{12:>15}{13:>5}{14:>5}\n'.format('system','ther_alpha','ther_error','ther_chisq','ther_df','ther_n','turb_alpha','turb_error','turb_chisq','turb_df','turb_n','mom_alpha','mom_error','regs','tied')
    info = np.empty((0,15))
    for i in range(len(self.publist)):
        system    = self.publist['system'][i]
        self.zabs = self.publist['z_abs'][i]
        info      = np.vstack((info,[system]+['-']*14))
        if 'murphy' in system:
            flag = 0 
            for model in ['thermal','turbulent']:
                idx  = 1 if model=='thermal' else 6
                path = self.fitdir+system+'/model00/model/'+model+'.18'
                if os.path.exists(path)==True:
                    fort18 = np.loadtxt(path,dtype=str,delimiter='\n')
                    for i in range(len(fort18)-1,0,-1):
                        if 'statistics for whole fit:' in fort18[i] and flag==0:
                            info[-1,idx+3] = fort18[i+2].split()[4] #ndf
                            info[-1,idx+4] = fort18[i+2].split()[3] #npix
                            flag       += 1
                        if 'dfsc/fsc' in fort18[i] and flag==1:
                            info[-1,idx+0] = '%.5f'%float(fort18[i].split()[2]) #alpha
                            info[-1,idx+1] = '%.5f'%float(fort18[i].split()[4]) #error
                            flag       += 1
                        if 'chi-squared :' in fort18[i] and flag==2:
                            info[-1,idx+2] = '%.5f'%float(fort18[i].split()[-3].replace(',','')) #chisq
                            flag       += 1
                            
        if 'king' in system:

            self.distpath = self.fitdir+system+'/model00/model/'
            self.getresults(original=True)

            info[-1,1]  = '%.5f'%self.ther_alpha
            info[-1,2]  = '%.5f'%self.ther_error
            info[-1,3]  = '%.5f'%self.ther_chisq
            info[-1,4]  = '%.0f'%self.ther_df
            info[-1,5]  = '%.0f'%self.ther_n
            info[-1,6]  = '%.5f'%self.turb_alpha
            info[-1,7]  = '%.5f'%self.turb_error
            info[-1,8]  = '%.5f'%self.turb_chisq
            info[-1,9]  = '%.0f'%self.turb_df
            info[-1,10] = '%.0f'%self.turb_n
            info[-1,11] = '%.6f'%self.mom_alpha
            info[-1,12] = '%.6f'%self.mom_error
            
        reg,tied=0,0
        for model in ['thermal','turbulent']:
            path = self.fitdir+system+'/model00/model/'+model+'.13'
            if os.path.exists(path)==True:
                fort13 = np.loadtxt(path,dtype=str,delimiter='\n')
                i,reg,tied,flag = 0,0,0,0
                while i < len(fort13):
                    if fort13[i].split()[0]=='*':
                        flag += 1
                        i += 1
                    if flag==1:
                        reg += 1
                    if flag==2:
                        if ((len(fort13[i].split()[0])==1 and fort13[i].split()[4][-1].islower()==True) or (len(fort13[i].split()[0])>1 and fort13[i].split()[3][-1].islower()==True)):
                            tied +=1
                    i += 1
        print '{0:<50} {1:>10}{2:>15}{3:>15}{4:>15}{5:>15}{6:>15}{7:>15}{8:>15}{9:>15}{10:>15}{11:>15}{12:>15}{13:>5}{14:>5}'.format(info[-1,0],info[-1,1],info[-1,2],info[-1,3],info[-1,4],info[-1,5],info[-1,6],info[-1,7],info[-1,8],info[-1,9],info[-1,10],info[-1,11],info[-1,12],str(int(reg)),str(int(tied)))
        
#==================================================================================================================

def getallres():
    
    self.model = 'latest'
    results = open('temp.dat','w')
    info = np.empty((0,15))
    for i in range(1,len(self.curlist)):
        if self.selection in ['published',str(self.curlist['system'][i])]:
            system = str(self.curlist['system'][i])
            self.zabs = self.curlist['z_abs'][i]
            print '|- Processing',system
            if str(self.curlist['status'][i])=='recorded':
                results.write('-\t'*16+'-\n')
            else:
                if os.path.exists(self.fitdir+system+'/original')==True:
                    os.system('rm '+self.fitdir+system+'/original')
                if self.curlist['symlinks'][i]!='-':
                    os.system('ln -s model'+self.curlist['symlinks'][i]+' '+self.fitdir+system+'/original')
                if os.path.exists(self.fitdir+system+'/latest')==True:
                    os.system('rm '+self.fitdir+system+'/latest')
                if self.curlist['Unnamed: 17'][i]!='-':
                    os.system('ln -s model'+self.curlist['Unnamed: 17'][i]+' '+self.fitdir+system+'/latest')
                self.distpath = self.fitdir+system+'/'+self.model+'/model/'
                self.getresults(original=True)
                for model in ['thermal','turbulent']:
                    ext,reg,ions,qlist='no',0,'',[]
                    head = self.fitdir+system+'/'+self.model+'/model/header.dat'
                    path = self.fitdir+system+'/'+self.model+'/model/'+model+'.13'
                    if os.path.exists(path)==True:
                        header = np.loadtxt(head,dtype=str,delimiter='\n',ndmin=1,comments='!')
                        for i in range(len(header)):
                            if 'external' in header[i]:
                                ext = 'yes'
                            else:
                                reg += 1
                                if header[i].split()[0].split('_')[0] not in ions:
                                    ions += header[i].split()[0].split('_')[0]+' '
                                    qlist.append(float(self.atominfo(header[i].split()[0])[-1]))
                        qvar = '-' if len(qlist)<2 else abs(max(qlist)-min(qlist))
                        fort13 = np.loadtxt(path,dtype=str,delimiter='\n',comments='!')
                        i,tied,flag,mult = 0,0,0,0
                        while i < len(fort13):
                            if fort13[i].strip()=='':
                                break
                            if fort13[i].split()[0]=='*':
                                flag += 1
                            if flag==2 and fort13[i].split()[0]!='*':
                                if ((len(fort13[i].split()[0])==1 and fort13[i].split()[4][-1].islower()==True) \
                                    or (len(fort13[i].split()[0])>1 and fort13[i].split()[3][-1].islower()==True)):
                                    if ((len(fort13[i].split()[0])==1 and 'q' in fort13[i].split()[5]) \
                                        or (len(fort13[i].split()[0])>1 and 'q' in fort13[i].split()[4])):
                                        mult += 1
                                    tied +=1
                            i += 1
                
                ions = '-'   if ions==''  else ions[:-1]
                qvar = '-'   if qvar=='-' else str(int(qvar))
                mult = 'yes' if mult>1    else 'no'

                ther_alpha    = '-' if self.ther_alpha=='-'    else '%.5f'%self.ther_alpha
                ther_error    = '-' if self.ther_error=='-'    else '%.5f'%self.ther_error
                ther_df       = '-' if self.ther_df=='-'       else '%.5f'%self.ther_df
                ther_n        = '-' if self.ther_n=='-'        else '%.5f'%self.ther_n
                ther_chisq    = '-' if self.ther_chisq=='-'    else '%.5f'%self.ther_chisq
                ther_chisq_nu = '-' if self.ther_chisq_nu=='-' else '%.5f'%self.ther_chisq_nu
                 
                turb_alpha    = '-' if self.turb_alpha=='-'    else '%.5f'%self.turb_alpha
                turb_error    = '-' if self.turb_error=='-'    else '%.5f'%self.turb_error
                turb_df       = '-' if self.turb_df=='-'       else '%.5f'%self.turb_df
                turb_n        = '-' if self.turb_n=='-'        else '%.5f'%self.turb_n
                turb_chisq    = '-' if self.turb_chisq=='-'    else '%.5f'%self.turb_chisq
                turb_chisq_nu = '-' if self.turb_chisq_nu=='-' else '%.5f'%self.turb_chisq_nu

                mom_alpha     = '-' if self.mom_alpha=='-'     else '%.5f'%self.mom_alpha
                mom_error     = '-' if self.mom_error=='-'     else '%.5f'%self.mom_error

                line = str(len(ions.split()))+'\t'+ions+'\t'+str(int(reg))+'\t'+qvar+'\t'+str(int(tied))+'\t'+ext+'\t'+mult+'\t'+\
                       ther_alpha+'\t'+ther_error+'\t'+ther_df+'\t'+ther_n+'\t'+ther_chisq+'\t'+ther_chisq_nu+'\t'+\
                       turb_alpha+'\t'+turb_error+'\t'+turb_df+'\t'+turb_n+'\t'+turb_chisq+'\t'+turb_chisq_nu+'\t'+\
                       mom_alpha +'\t'+mom_error+'\n'
                
                results.write(line)
                if self.selection!='published':
                    print line

    results.close()
        
#==================================================================================================================
