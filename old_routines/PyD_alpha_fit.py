#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *
        
#==================================================================================================================

def fit(model):

    # Create model folder

    modpath = self.distpath+'/'+model+self.modflag
    if os.path.exists(modpath)==False:
        os.system('mkdir -p '+modpath)
    os.chdir(modpath)

    if '--finalmodel' in sys.argv:

        fort2fit = model+'_ini.26'

        ''' Create new fort.13 and header files '''
        
        write_header = open(modpath+'/header.dat','w')
        write_fort   = open(modpath+'/'+fort2fit,'w')
        os.system('rm '+modpath+'/'+model+'_ini.13')
    
        for i in range (len(self.fort_header_new)):
            datalength = [len(self.fort_header_new[k,0]) for k in range (len(self.fort_header_new))]
            datalength = "{0:<"+str(max(datalength))+"}"
            write_fort.write('%% ')
            write_fort.write(datalength.format(self.fort_header_new[i,0])+' ')
            write_fort.write('{0:>5}'.format(self.fort_header_new[i,1])+' ')
            write_fort.write('{0:>10}'.format('%.2f'%float(self.fort_header_new[i,2]))+' ')
            write_fort.write('{0:>10}'.format('%.2f'%float(self.fort_header_new[i,3]))+' ')
            write_fort.write('{0:<17}'.format(self.fort_header_new[i,4])+' ! ')
            write_fort.write('{0:<15}'.format(self.fort_header_new[i,5])+' ')
            write_header.write('{0:<15}'.format(self.fort_header_new[i,5])+' ')
            if self.fort_header_new[i,6]!='':
                write_fort.write('{0:<15}'.format(self.fort_header_new[i,6])+' ')
                write_header.write('{0:<15}'.format(self.fort_header_new[i,6])+' ')
                write_fort.write('{0:<15}'.format(self.fort_header_new[i,7]))
                write_header.write('{0:<15}'.format(self.fort_header_new[i,7]))
            write_header.write('\n')
            write_fort.write('\n')
        
        line26 = np.loadtxt(self.fitdir+self.selection+'/original/'+model+'.26',dtype='str',delimiter='\n')
        for i in range (len(line26)):
            if line26[i][0:2]!='%%':
                write_fort.write(line26[i]+'\n')
            
        write_header.close()
        write_fort.close()
        
    else:

        fort2fit = model+'_ini.13'

        ''' Create new fort.13 and header files '''
        
        write_header = open(modpath+'/header.dat','w')
        write_fort   = open(modpath+'/'+fort2fit,'w')

        write_fort.write('   *\n')
        for i in range (len(self.fort_header_new)):
            datalength = [len(self.fort_header_new[k,0]) for k in range (len(self.fort_header_new))]
            datalength = "{0:<"+str(max(datalength))+"}"
            write_fort.write(datalength.format(self.fort_header_new[i,0])+' ')
            write_fort.write('{0:>5}'.format(self.fort_header_new[i,1])+' ')
            write_fort.write('{0:>10}'.format('%.2f'%float(self.fort_header_new[i,2]))+' ')
            write_fort.write('{0:>10}'.format('%.2f'%float(self.fort_header_new[i,3]))+' ')
            write_fort.write('{0:<17}'.format(self.fort_header_new[i,4])+' ! ')
            write_fort.write('{0:<15}'.format(self.fort_header_new[i,5])+' ')
            write_header.write('{0:<15}'.format(self.fort_header_new[i,5])+' ')
            if self.fort_header_new[i,6]!='':
                write_fort.write('{0:<15}'.format(self.fort_header_new[i,6])+' ')
                write_header.write('{0:<15}'.format(self.fort_header_new[i,6])+' ')
                write_fort.write('{0:<15}'.format(self.fort_header_new[i,7]))
                write_header.write('{0:<15}'.format(self.fort_header_new[i,7]))
            write_header.write('\n')
            write_fort.write('\n')
        write_fort.write('  *\n')
        
        for i in range (len(self.fort_content_new)):
            val = self.fort_content_new[i]
            write_fort.write('   '+'{0:<5}'.format(val[0])+' ')
            write_fort.write('{0:>5}'.format(val[1].split('.')[0])+'.'+'{0:<7}'.format(val[1].split('.')[1])+' ')
            write_fort.write('{0:>3}'.format(val[2].split('.')[0])+'.'+'{0:<9}'.format(val[2].split('.')[1])+' ')
            write_fort.write('{0:>4}'.format(val[3].split('.')[0])+'.'+'{0:<6}'.format(val[3].split('.')[1])+' ')
            if '--simulation' not in sys.argv and '--noalpha' in sys.argv:
                write_fort.write('    0.000FF  ')
            else:
                write_fort.write('{0:>5}'.format(val[4].split('.')[0])+'.'+'{0:<6}'.format(val[4].split('.')[1])+' ')
            write_fort.write('{0:>5}'.format(val[5].split('.')[0])+'.'+'{0:<2}'.format(val[5].split('.')[1])+' ')
            if model=='turbulent' and val[2][-1].islower():
                write_fort.write('  1.00E+00 ')
            else:
                write_fort.write('  0.00E+00 ')
            write_fort.write('{0:>2}'.format(val[7])+' ! ')
            write_fort.write('{0:>4}'.format(i+1))
            write_fort.write('\n')
            
        write_header.close()
        write_fort.close()
        
    # Remove and re-create link to data folder

    if os.path.exists(modpath+'/data')==True:
        os.system('rm '+modpath+'/data')
    if '--simulation' in sys.argv:
        os.system('ln -s ../../../model/data '+modpath)
    else:
        os.system('ln -s ../../../../../data '+modpath)

    # Prepare model folders and run VPFIT
    
    if os.path.exists('atom.dat')==True:
        os.system('rm atom.dat vp_setup.dat')
    os.system('cp '+self.atomdir+' atom.dat')
    opfile = open('vp_setup.dat','w')
    for row in self.vpfsetup:
        opfile.write(row+'\n')
    opfile.close()
    os.environ['ATOMDIR']='./atom.dat'
    os.environ['VPFSETUP']='./vp_setup.dat'

    if '--iterone' in sys.argv:            
        open('fitcommands','w').write('e\n\n\n'+fort2fit+'\nn\nn\n')
    elif '--illcond' in sys.argv:
        open('fitcommands','w').write('f\nil\n\n\n'+fort2fit+'\nn\nn\n')
    else:
        open('fitcommands','w').write('f\n\n\n'+fort2fit+'\nn\nn\n')
    os.system(self.vpcommand+' < fitcommands > termout')
    if os.path.exists('fort.26')==True:
        createfit13(model)
        
#==================================================================================================================
