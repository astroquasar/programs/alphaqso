import alpha

class dipole_fitting:
    '''
    Perform dipole fitting.
    
    **Optional operations**
    
       --combined    Do dipole fitting from combined UVES+HIRES results
       --custom      Call specific da/a results list
       --king12      Reproduce results published in King et al. 2012
       --simulation  Dipole fitting from simulation results 
    
    **Required arguments**
    
       --instrument  High-resolution spectrograph used
                       [uves]  VLT spectrograph
                       [hires] Keck spectrograph
    
    **Optional arguments**
    
       --chisq       Change chi-square of targetted system
       --mgisotope   Fit for non-terrestrial Mg abundances
       --model       Model version of the system to use for analysis
       --original    Calculate dipole only on mode previously used
       --previous    Use model from previous slope as first guess for next
       --selection   Individual or list of absorption systems to study
    
    Examples
    --------
    
      >>> alpha dipole --instrument uves --mgisotope \ 
      >>>              --selection J001602-001225/1.14680/UVES_squader
    '''
        
    def __init__(self):
        if '--king12' in sys.argv:
            self.king12()  
        elif '--combined' in sys.argv:
            self.combination()
        elif '--simulation' in sys.argv:
            self.simulation()
        elif '--custom' in sys.argv:
            self.custom()
        elif '--standard' in sys.argv:
            self.standard()
            
    def king12(self):
        # Fitting the UVES results
        systems = []
        dipolepath = setup.Crep+'/uves/reproduce/'
        os.system('mkdir -p '+dipolepath)
        os.chdir(dipolepath)
        save = open('results.dat','w')
        save.write('##ALPHAFIT##\n')
        for i in range (len(setup.dipole)):
            zabs   = setup.dipole['z_abs'][i]
            if setup.dipole['analyser'][i]=='JAK':
                val = setup.dipole['RA King\n(hh:mm:ss)'][i].split(':')
                ra  = float(val[0])+float(val[1])/60+float(val[2])/3600
                val = setup.dipole['DEC King\n(dd:mm:ss)'][i].split(':')
                if (val[0][0]=='-'):
                    dec = float(val[0])-float(val[1])/60-float(val[2])/3600
                else:
                    dec = float(val[0])+float(val[1])/60+float(val[2])/3600
                save.write('{:<10}'.format('%.6f'%float(zabs)))
                save.write('{:>12}'.format('%.6f'%float(ra)))
                save.write('{:>12}'.format('%.6f'%float(dec)))
                save.write('{:>12}'.format('%.6f'%round(float(setup.dipole['alpha'][i]),int(float(setup.dipole['dec_alpha'][i])))))
                save.write('{:>12}'.format('%.6f'%round(float(setup.dipole['error'][i]),int(float(setup.dipole['dec_error'][i])))))
                save.write('\n')
                systems.append(setup.dipole['name'][i])
        save.write('##END##\n')
        save.close()
        self.do_fitting(systems)
        # Fitting the HIRES high-contrast measurements
        systems = []
        dipolepath = setup.Crep+'/hires/reproduce/high_contrast/'
        os.system('mkdir -p '+dipolepath)
        os.chdir(dipolepath)
        save = open('results.dat','w')
        save.write('##ALPHAFIT##\n')
        for i in range (len(setup.dipole)):
            zabs   = setup.dipole['z_abs'][i]
            if setup.dipole['analyser'][i]=='MTM' and setup.dipole['high-contrast'][i]=='x':
                val = setup.dipole['RA King\n(hh:mm:ss)'][i].split(':')
                ra  = float(val[0])+float(val[1])/60+float(val[2])/3600
                val = setup.dipole['DEC King\n(dd:mm:ss)'][i].split(':')
                if (val[0][0]=='-'):
                    dec = float(val[0])-float(val[1])/60-float(val[2])/3600
                else:
                    dec = float(val[0])+float(val[1])/60+float(val[2])/3600
                save.write('{:<10}'.format('%.6f'%float(zabs)))
                save.write('{:>12}'.format('%.6f'%float(ra)))
                save.write('{:>12}'.format('%.6f'%float(dec)))
                save.write('{:>12}'.format('%.6f'%round(float(setup.dipole['alpha'][i]),int(float(setup.dipole['dec_alpha'][i])))))
                save.write('{:>12}'.format('%.6f'%round(float(setup.dipole['error'][i]),int(float(setup.dipole['dec_error'][i])))))
                save.write('\n')
                systems.append(setup.dipole['name'][i])
        save.write('##END##\n')
        save.close()
        self.do_fitting(systems)
        # Fitting combined high-contrast and rest of HIRES measurements
        systems = []
        dipolepath = setup.Crep+'/hires/reproduce/all_contrast/'
        os.system('mkdir -p '+dipolepath)
        os.chdir(dipolepath)
        save = open('results.dat','w')
        save.write('##ALPHAFIT##\n')
        for line in np.loadtxt(setup.Crep+'/hires/reproduce/high_contrast/alphafit_errmod.dat',dtype=str,delimiter='\n'):
            save.write(line.replace(line.split()[-1],'')+'\n')
            systems.append(line.split()[-1])
        for i in range (len(setup.dipole)):
            zabs   = setup.dipole['z_abs'][i]
            if setup.dipole['analyser'][i]=='MTM' and setup.dipole['high-contrast'][i]!='x':
                val = setup.dipole['RA King\n(hh:mm:ss)'][i].split(':')
                ra  = float(val[0])+float(val[1])/60+float(val[2])/3600
                val = setup.dipole['DEC King\n(dd:mm:ss)'][i].split(':')
                if (val[0][0]=='-'):
                    dec = float(val[0])-float(val[1])/60-float(val[2])/3600
                else:
                    dec = float(val[0])+float(val[1])/60+float(val[2])/3600
                save.write('{:<10}'.format('%.6f'%float(zabs)))
                save.write('{:>12}'.format('%.6f'%float(ra)))
                save.write('{:>12}'.format('%.6f'%float(dec)))
                save.write('{:>12}'.format('%.6f'%round(float(setup.dipole['alpha'][i]),int(float(setup.dipole['dec_alpha'][i])))))
                save.write('{:>12}'.format('%.6f'%round(float(setup.dipole['error'][i]),int(float(setup.dipole['dec_error'][i])))))
                save.write('\n')
                systems.append(setup.dipole['name'][i])
        save.write('##END##\n')
        save.close()
        self.do_fitting(systems)
        comb  = setup.Crep+'/comb/reproduce/'
        hires = setup.Crep+'/hires/reproduce/all_contrast/alphafit_errmod.dat'
        uves  = setup.Crep+'/uves/reproduce/alphafit_errmod.dat'
        systems = []
        hires   = np.loadtxt(hires,dtype=str,delimiter='\n')
        uves    = np.loadtxt(uves,dtype=str,delimiter='\n')
        data    = np.append(hires,uves)
        print 'Location:',comb
        os.system('mkdir -p '+comb)
        os.chdir(comb)
        save = open('results.dat','w')
        save.write('##ALPHAFIT##\n')
        for line in data:
            save.write(line.replace(line.split()[-1],'')+'\n')
            systems.append(line.split()[-1])
        save.write('##END##\n')
        save.close()
        self.do_fitting(systems)
        
    def sample(self):
        systems = []        
        path = setup.Crep+'hires/reproduce/'
        print 'Location:',path
        os.system('mkdir -p '+path)
        os.chdir(path)
        save = open('results.dat','w')
        save.write('##ALPHAFIT##\n')
        for i in range (len(setup.dipole)):
            zabs   = setup.dipole['z_abs'][i]
            cond1  = setup.dipole['analyser'][i]=='MTM'
            cond2a = setup.sample!=None and setup.sample in setup.dipole['system'][i]
            cond2b = '--stable' in sys.argv and setup.dipole['system'][i] not in setup.badhires
            if cond1 and (cond2a or cond2b):
                val = setup.dipole['RA King\n(hh:mm:ss)'][i].split(':')
                ra  = float(val[0])+float(val[1])/60+float(val[2])/3600
                val = setup.dipole['DEC King\n(dd:mm:ss)'][i].split(':')
                if (val[0][0]=='-'):
                    dec = float(val[0])-float(val[1])/60-float(val[2])/3600
                else:
                    dec = float(val[0])+float(val[1])/60+float(val[2])/3600
                save.write('{:<10}'.format('%.6f'%float(zabs)))
                save.write('{:>12}'.format('%.6f'%float(ra)))
                save.write('{:>12}'.format('%.6f'%float(dec)))
                save.write('{:>12}'.format('%.6f'%round(float(setup.dipole['alpha'][i]),int(float(setup.dipole['dec_alpha'][i])))))
                save.write('{:>12}'.format('%.6f'%round(float(setup.dipole['error'][i]),int(float(setup.dipole['dec_error'][i])))))
                save.write('\n')
                systems.append(setup.dipole['name'][i])
        save.write('##END##\n')
        save.close()
        self.do_fitting(systems)
                    
    def combination(self):
        comb  = setup.Crep+'comb/vd17d/hires_0.000-uves_0.000/'
        list1 = setup.Crep+'hires/vd17d/normal/v10_chisq1E-06/0.000/alphafit_errmod.dat'
        list2 = setup.Crep+'uves/vd17d/normal/v10_chisq1E-06_all/0.000/alphafit_errmod.dat'
        systems = []
        list1   = np.loadtxt(list1,dtype=str,delimiter='\n')
        list2   = np.loadtxt(list2,dtype=str,delimiter='\n')
        data    = np.append(list1,list2)
        print 'Location:',comb
        os.system('mkdir -p '+comb)
        os.chdir(comb)
        save = open('results.dat','w')
        save.write('##ALPHAFIT##\n')
        for line in data:
            save.write(line.replace(line.split()[-1],'')+'\n')
            systems.append(line.split()[-1])
        save.write('##END##\n')
        save.close()
        self.do_fitting(systems)
    
    def simulation(self):
        fortlist = system_list()
        filelist = np.loadtxt(fortlist,dtype='str',comments='!',ndmin=1)
        os.system('rm '+fortlist)
        data  = np.empty((0,4))
        model = 'thermal' if setup.simulation=='simple' else 'turbulent'
        path  = setup.Crep+setup.instrument+'/simulation/'+setup.simtest+'/'+setup.test+'/'+setup.distortion+'/'
        for setup.distpath in filelist:
            setup.qso  = setup.distpath.split('/')[-8]
            setup.zabs = setup.distpath.split('/')[-7]
            if os.path.exists(setup.distpath+'/'+model+'/'+model+'.18'):
                setup.readfort18(setup.distpath+'/'+model+'/'+model+'.18')
                if setup.simulation=='simple' and setup.ther_chisq!='-':
                    data = np.vstack((data,[setup.zabs,setup.ther_alpha,setup.ther_error,setup.qso]))
                elif setup.simulation=='complex' and setup.turb_chisq!='-':
                    data = np.vstack((data,[setup.zabs,setup.turb_alpha,setup.turb_error,setup.qso]))
    
        setup.path = path
        setup.data = data
        self.make_input()
        
    def standard(self):
        fortlist = system_list()
        filelist = np.loadtxt(fortlist,dtype='str',comments='!',ndmin=1)
        os.system('rm '+fortlist)
        data = np.empty((0,4))
        path = setup.sample      if setup.sample!=None         else setup.selection
        path = path+'/original'  if '--original'   in sys.argv else \
               path+'/bestchisq' if '--bestchisq'  in sys.argv else \
               path+'/thermal'   if '--thermal'    in sys.argv else \
               path+'/turbulent' if '--turbulent'  in sys.argv else \
               path+'/normal'
        path = setup.Crep+setup.instrument+'/'+path+'/'+setup.test+'/'+setup.distortion+'/'
        for setup.distpath in filelist:
            setup.qso    = setup.distpath.split('/')[-7]
            setup.zabs   = setup.distpath.split('/')[-6]
            setup.sample = setup.distpath.split('/')[-5]
            getresults()
            system = setup.qso+'/'+setup.zabs+'/'+setup.sample
            cond1  = setup.mom_chisq!='-'
            cond2  = setup.turb_chisq!='-'
            cond3  = setup.ther_chisq!='-'
            conda  = '--original'  in sys.argv
            condb  = '--turbulent' in sys.argv
            condc  = '--thermal'   in sys.argv
            condd  = '--bestchisq' in sys.argv
            condA  = setup.sample  in setup.murphy
            if cond1 and condb==condc==condd==False and (conda==False or condA==False):
                data = np.vstack((data,[setup.zabs,setup.mom_alpha,setup.mom_error,setup.qso]))
            elif cond2 and (conda or condb or (condd and float(setup.turb_chisq)<=float(setup.ther_chisq))):
                data = np.vstack((data,[setup.zabs,setup.turb_alpha,setup.turb_error,setup.qso]))
            elif cond3 and (conda or condc or (condd and float(setup.ther_chisq)<=float(setup.turb_chisq))):
                data = np.vstack((data,[setup.zabs,setup.ther_alpha,setup.ther_error,setup.qso]))
        setup.path = path
        setup.data = data
        self.make_input()

    def custom(self):
        setup.data = np.loadtxt(setup.custom,dtype=str)
        save = open('results.dat','w')
        save.write('##ALPHAFIT##\n')
        for i in range(len(setup.data)):
            print setup.data[i,0]
            zem,ra,dec,dist = dist2dip(setup.data[i,0])
            zabs  = float(setup.data[i,1])
            alpha = float(setup.data[i,2])
            error = float(setup.data[i,3])
            save.write('{:<10}'.format('%.6f'%zabs))    # z_abs
            save.write('{:>12}'.format('%.6f'%ra))      # ra
            save.write('{:>12}'.format('%.6f'%dec))     # dec
            save.write('{:>12}'.format('%.6f'%alpha))   # MoM_alpha
            save.write('{:>12}'.format('%.6f'%error))   # MoM_da
            save.write('\n')
        save.write('##END##\n')
        save.close()
        self.do_fitting(setup.data[:,0])
        
    def make_input(self):
        print 'Location:',setup.path
        os.system('mkdir -p '+setup.path)
        os.chdir(setup.path)
        save = open('results.dat','w')
        save.write('##ALPHAFIT##\n')
        for i in range(len(setup.data)):
            zem,ra,dec,dist = dist2dip(setup.data[i,-1][:14])
            zabs  = float(setup.data[i,0])
            alpha = float(setup.data[i,1])
            error = float(setup.data[i,2])
            save.write('{:<10}'.format('%.6f'%zabs))    # z_abs
            save.write('{:>12}'.format('%.6f'%ra))      # ra
            save.write('{:>12}'.format('%.6f'%dec))     # dec
            save.write('{:>12}'.format('%.6f'%alpha))   # MoM_alpha
            save.write('{:>12}'.format('%.6f'%error))   # MoM_da
            save.write('\n')
        save.write('##END##\n')
        save.close()
        self.do_fitting(setup.data[:,-1])
        
    def make_input2(self,fullpath):
        output = np.genfromtxt(setup.here+'/results.dat',names=True,dtype=object,comments='!')
        save = open('results.dat','w')
        save.write('##ALPHAFIT##\n')
        for i in range (len(output)):
            alpha = None
            zabs = float(output['z_abs'][i])
            zem,ra,dec,dist = dist2dip(output['qso'][i])
            condition = output['sample'][i]+'/'+output['qso'][i]+'/'+output['z_abs'][i] not in setup.skipabs
            if condition and (('highz' not in fullpath and 'lowz' not in fullpath) \
                    or ('highz' in fullpath and zabs>=setup.zcut) \
                    or ('lowz' in fullpath and zabs<setup.zcut)):
                condturb1 = output['turb_alpha'][i] not in ['0.000','-','nan'] \
                            and output['turb_error'][i] not in ['0.000','-','nan']
                condther1 = output['ther_alpha'][i] not in ['0.000','-','nan'] \
                            and output['ther_error'][i] not in ['0.000','-','nan']
                condturb2 = output['turb_alpha'][i] in ['0.000','-','nan'] \
                            or output['turb_error'][i] in ['0.000','-','nan']
                condther2 = output['ther_alpha'][i] in ['0.000','-','nan'] \
                            or output['ther_error'][i] in ['0.000','-','nan']
                if condturb1 and condther2:
                    alpha = output['turb_alpha'][i]
                    error = output['turb_error'][i]
                if condturb2 and condther1:
                    alpha = output['ther_alpha'][i]
                    error = output['ther_error'][i]
                if condturb1 and condther1:
                    alpha = output['MoM_alpha'][i]
                    error = output['MoM_error'][i]
            if alpha != None:
                save.write('{:<10}'.format('%.6f'%float(zabs)))
                save.write('{:>12}'.format('%.6f'%float(ra*24./360.)))
                save.write('{:>12}'.format('%.6f'%float(dec)))
                save.write('{:>12}'.format('%.6f'%float(alpha)))
                save.write('{:>12}'.format('%.6f'%float(error)))
                save.write('\n')
        save.write('##END##\n')
        save.close()
        
    def do_fitting(self,systems):
        print 'Perform Dipole fitting...'
        os.system('touch command')
        os.system('alphafit results.dat < command > termout.dat')
        os.system("for x in $(ls *.ps); do echo $x; ps2pdf $x $(echo $x|sed -e 's/ps$/pdf/');done")
        os.system('rm command *.ps')
        errmod = np.loadtxt('alphafit_errmod.dat')
        save = open('alphafit_errmod.dat','w')
        save.write('##ALPHAFIT##\n')
        for i in range (len(errmod)):
            save.write('{:<10}'.format('%.6f'%errmod[i,0]))   # zabs
            save.write('{:>12}'.format('%.6f'%errmod[i,1]))   # ra
            save.write('{:>12}'.format('%.6f'%errmod[i,2]))   # dec
            save.write('{:>12}'.format('%.6f'%errmod[i,3]))   # MoM_alpha
            save.write('{:>12}'.format('%.6f'%errmod[i,4]))   # MoM_da
            save.write('{:<12}'.format('     '+systems[i]))   # system
            save.write('\n')
        save.write('##END##\n')
        save.close()
        os.chdir(setup.here)
        
