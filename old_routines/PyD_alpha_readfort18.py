#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def readfort18(fortpath):

    daoaun = 1.# if '--published' in sys.argv else 1e-6 
    if '--published' not in sys.argv:
        if os.path.exists(fortpath.replace(fortpath.split('/')[-1],'vp_setup.dat')):
            for line in np.loadtxt(fortpath.replace(fortpath.split('/')[-1],'vp_setup.dat'),dtype=str,delimiter='\n',comments='!'):
                if 'daoaun'in line:
                    daoaun = float(line.split()[1])

    flag,n,df,chisq,chisq_nu,alpha,error,n,df = 0,'-','-','-','-','-','-','-','-'
    fort18 = np.loadtxt(fortpath,dtype='str',delimiter='\n')

    for i in range(len(fort18)-1,0,-1):

        if 'maxdev' in fort18[i] and flag==0:
            reg = 0
            for j in range(i+1,len(fort18)):
                line = fort18[j]
                s    = 1 if len(line.split()[0])==1 else 0
                zabs = float(re.compile(r'[^\d.-]+').sub('',line.split()[1+s]))
                if 'q' in str(line.split()[7+s]) and abs(2*(zabs-float(self.zabs))/(zabs+float(self.zabs))) < 0.02:
                    alpha = float(line.split()[7+s].split('q')[0])*daoaun/1e-5
                    error = float(line.split()[8+s].split('q')[0])*daoaun/1e-5
                    flag  = 1
                    break
                reg += 1
            if flag==0:
                flag = -1

        if 'statistics for whole fit:' in fort18[i] and flag==1:
            n     = float(fort18[i+2].split()[3])
            df    = float(fort18[i+2].split()[4])
            flag  = 2

        if 'arameter errors:' in fort18[i] and flag==2:
            line  = fort18[i+1+reg]
            s     = 1 if len(line.split()[0])==1 else 0
            if 'q' in str(line.split()[4+s]) and '*' not in str(line.split()[4+s]):
                error = float(line.split()[4+s].split('q')[0])*daoaun/1e-5
            flag = 3

        if 'chi-squared :' in fort18[i] and flag==3:
            chisq_nu = float(fort18[i].split()[2])
            chisq = float(fort18[i].split()[-3].replace(',',''))
            line  = fort18[i+2+reg]
            s     = 1 if len(line.split()[0])==1 else 0
            if 'q' in str(line.split()[4+s]) and '*' not in str(line.split()[4+s]):
                alpha = float(line.split()[4+s].split('q')[0])*daoaun/1e-5
            break
        
    if fortpath.split('/')[-1]=='thermal.18':
        self.ther_chisq_nu = chisq_nu
        self.ther_chisq = chisq
        self.ther_alpha = alpha
        self.ther_error = error
        self.ther_df    = df
        self.ther_n     = n

    if fortpath.split('/')[-1]=='turbulent.18':
        self.turb_chisq_nu = chisq_nu
        self.turb_chisq = chisq
        self.turb_alpha = alpha
        self.turb_error = error
        self.turb_df    = df
        self.turb_n     = n

#==================================================================================================================

def readfort13(fortpath):
    
    flag = 0
    fort13 = np.loadtxt(fortpath,dtype='str',delimiter='\n')
    for line in fort13:
        flag = flag+1 if '*' in line else flag
        if flag==1 and '*' not in line:
            wmin = float(line.split()[2])
            wmax = float(line.split()[3])
            self.alltrans += 1
            if wmax < 3259.1 or (4518.8 < wmin and wmax < 4726.9) or wmin > 6834.9:
                self.outside += 1

#==================================================================================================================
