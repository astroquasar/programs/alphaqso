#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def supercomputer_bashlist():
    
    results = self.distsim
    joblist = open('runjobs.sh','w')
    joblist.write('#!/bin/bash\n')
    
    for i in range(len(results)):

        distsep = float(results['distsep'][i])
        options = ' --raijin --previous --compress --simulation complex --expind'
        options = options + ' --selection ' + results['system'][i]
        options = options + ' --model '     + results['model'][i]
        options = options + ' --stdev %.2f'%self.stdev
        options = options + ' --distmid -0.123'
        options = options + ' --distmin %.3f'%(-0.123-10*distsep)
        options = options + ' --distmax %.3f'%(-0.123+10*distsep)
        options = options + ' --distsep %.3f'%distsep
        options = options + ' --chisq 1E-5'
        
        name    = results['system'][i].replace('/','--')
        outfile = open(name+'.sh','w')
        outfile.write('#!/bin/bash\n')
        outfile.write('module load python/2.7.5\n')
        outfile.write('module load python/2.7.5-matplotlib\n')
        outfile.write(self.alphapath+'PyD_alpha run'+options+'\n')
        outfile.close()
        
        joblist.write('qsub -P gd5 -q normal -l walltime=40:00:00,mem=300MB -l wd '+self.here+'/'+name+'.sh\n')
            
    joblist.close()
    
#==================================================================================================================

def supercomputer_include(tarfile):

    os.system('tar -zxvf '+tarfile)
    os.chdir(tarfile.replace('.tar.gz',''))
    #os.system('ls *.tar.gz > listabsorbers')
    os.system('find . -name "*.tar.gz" -type f > listabsorbers')
    abslist = np.loadtxt('listabsorbers',dtype='str',ndmin=1)
    for absorber in abslist:
        os.system('tar -zxvf '+absorber)
        target = absorber.replace('.tar.gz','').replace('--','/')
        destin = self.fitdir+'/'+target
        if os.path.exists(target+'/turbulent/'):
            os.system('mkdir -p '+destin+'/turbulent/')
            os.system('rsync -avzlr --delete '+target+'/turbulent/ '+destin+'/turbulent/')
        if os.path.exists(target+'/thermal/'):
            os.system('mkdir -p '+destin+'/thermal/')
            os.system('rsync -avzlr --delete '+target+'/thermal/ '+destin+'/thermal/')
        if os.path.exists(target+'/turbulent/')==False and os.path.exists(target+'/thermal/')==False:
            os.system('mkdir -p '+destin)
            os.system('rsync -avzlr --delete '+target+'/ '+destin+'/')
        os.system('rm -rf '+tarfile.split('--')[0])
        
#==================================================================================================================

def supercomputer_prepare():

    for system in self.publist['system']:
        
        os.system('ls '+self.fitdir+'/'+system+'/ > list')
        filelist = np.loadtxt('list',dtype=str)
        for line in filelist:
            if 'model' in line:
                pathhead = self.fitdir+'/'+system+'/'+line+'/model/header.dat'
                pathther = self.fitdir+'/'+system+'/'+line+'/model/thermal.13'
                pathturb = self.fitdir+'/'+system+'/'+line+'/model/turbulent.13'
                os.system('mkdir -p ./prepare/'+system+'/'+line+'/model/')
                if os.path.exists(pathhead): os.system('cp '+pathhead+' ./prepare/'+system+'/'+line+'/model/')
                if os.path.exists(pathther): os.system('cp '+pathther+' ./prepare/'+system+'/'+line+'/model/')
                if os.path.exists(pathturb): os.system('cp '+pathturb+' ./prepare/'+system+'/'+line+'/model/')
            
        if 'UVES_king' in system:
            os.system('ln -s ../../../spectra/UVES_king ./prepare/'+system+'/data')
        else:
            os.system('mkdir -p ./prepare/'+system+'/data/')
            os.system('cp '+self.fitdir+'/'+system+'/data/* ./prepare/'+system+'/data/')

#==================================================================================================================

def supercomputer_clean():
    
    os.system('ls ./execute/*.tar.gz  > lista')
    os.system('ls ./compress/*.tar.gz > listb')
    os.system('ls ./list/*.sh         > listc')

    lista = [] if os.stat('lista').st_size==0 else np.loadtxt('lista',dtype=str)
    listb = [] if os.stat('listb').st_size==0 else np.loadtxt('listb',dtype=str)
    listc = [] if os.stat('listc').st_size==0 else np.loadtxt('listc',dtype=str)[:-1]

    done  = []
    todo  = []
    
    for tarfile in lista:

        path   = tarfile.split('/')[-1].split('--')
        name   = path[0] + '--' + path[1] + '--' + path[2]
        system = path[0] + '/'  + path[1] + '/'  + path[2]
        
        i      = np.where(self.distres['system']==system)[0][0]
        step   = float(self.distres['step'][i]) if '/jobs2/' in self.here else 0.01
        min1   = float(self.distres['min'][i])  if '/jobs2/' in self.here else -0.2
        max1   = float(self.distres['max'][i])  if '/jobs2/' in self.here else 0
        min2   = '0.000' if round(min1,3)==0 else str('%.3f'%min1).replace('-','m') if '-' in str(min1) else 'p'+str('%.3f'%min1)
        max2   = '0.000' if round(max1,3)==0 else str('%.3f'%max1).replace('-','m') if '-' in str(max1) else 'p'+str('%.3f'%max1)
        
        cond1  = '0.000' in tarfile
        cond2  = tarfile.replace('0.000',min2) in lista
        cond3  = tarfile.replace('0.000',max2) in lista

        if cond1 and cond2 and cond3:

            for i in np.arange(min1,max1+0.001,step):
                slope = '0.000' if round(i,3)==0 else str('%.3f'%i).replace('-','m') if '-' in str(i) else 'p'+str('%.3f'%i)
                os.system('mv '+tarfile.replace('0.000',slope)+' ./compress/')
                done.append(name)

        elif cond1:
            todo.append(name)

    for tarfile in listb:
        
        path = tarfile.split('/')[-1].split('--')
        name = path[0]+'--'+path[1]+'--'+path[2]
        if '0.000' in tarfile:
            done.append(name)
    
    for tarfile in listc:
        
        path   = tarfile.split('/')[-1].split('--')
        name   = path[0] + '--' + path[1] + '--' + path[2].replace('.sh','')
        if name not in done and name not in todo:
            todo.append(name)

    todo = sorted(todo)
    joblist = open('runjobs.sh','w')
    joblist.write('#!/bin/bash\n')
    for system in todo:
        joblist.write('qsub -P gd5 -q normal -l walltime=40:00:00,mem=300MB -l wd '+self.here+'/'+system+'.sh\n')
    joblist.close()

    os.system('rm lista listb listc')
    
#==================================================================================================================
