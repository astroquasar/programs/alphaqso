#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def getsigmarand():
    
#    print '\n\tSigma rand\n'
    
    x,y = [],[]
    
    for i in self.distlist:
        
        strslope = '0.000' if round(i,3)==0 \
                   else str('%.3f'%i).replace('-','m') if '-' in str(i) \
                   else 'p'+str('%.3f'%i)
                   
        pathdir  = self.Crep+self.instrument+'/'+self.selection+'/'+self.test+'/'\
                   +strslope+'/'+self.zsample+'/termout.dat'
        
        if os.path.exists(pathdir)==True and round(i,2) not in self.discard:
            
            output = np.loadtxt(pathdir,dtype='str',delimiter='\n')
            
            for k in range (len(output)):
                
                if 'Modifying error arrays with factor' in output[k]:
                    
                    sigfac = float(output[k].split('=')[1].replace('...','').replace(' ',''))
#                    print '\t\t','{:>10}'.format('%.3f'%i),'{:>10}'.format('%.3f'%sigfac)
                    x.append(round(i,3))
                    y.append(sigfac)
                    break
    
    return x,y
    
#==================================================================================================================
