import alpha

class sanity(object):

    """
    This is a sanity check algorithm for fort.26 models.
    """

    def showhelp(self):
        
        print ""
        print "-------------------------------------------------------------------------"
        print ""
        print "required arguments:"
        print ""
        print "   --selection   Sample to study"
        print ""
        print "optional arguments:"
        print ""
        print "   --chisq       Chi-square of sample"
        print "   --compress    Compress results for easy extraction from supercomputer"
        print "   --instrument  Spectrograph from which systems are selected"
        print "   --mgisotope   Fit for non-terrestrial Mg abundances"
        print "   --original    Extract original results from selected model"
        print ""
        print "examples:"
        print ""
        print "   alpha sanity --instrument uves --chisq 1e-6 --selection vd17a"
        print ""
        print "-------------------------------------------------------------------------"
        print ""
        quit()
        
    def __init__(self):

        if '-h' in sys.argv or '--help' in sys.argv:
            self.showhelp()
        n = 1
        for k in range(len(setup.curlist)):
            model = str(setup.curlist[setup.selection][k])
            system = setup.curlist['system'][k]
            turb_flag = ther_flag = 0 
            if model!='-' and (setup.instrument==None or setup.curlist['instrument'][k]==setup.instrument.upper()):
                distpath = setup.fitdir+system+'/model-'+model
                setup.distpath = distpath+'/runs/'+setup.test+'/'+setup.distortion
                if os.path.exists(setup.distpath):
                    turb_flag = self.readfort26(setup.distpath+'/turbulent/turbulent.26')
                    ther_flag = self.readfort26(setup.distpath+'/thermal/thermal.26')
                    if turb_flag==0 and ther_flag==1:
                        #print n,system,'\t','turb'
                        n+=1
                    if turb_flag==1 and ther_flag==0:
                        #print n,system,'\t','ther'
                        n+=1
                    if turb_flag==1 and ther_flag==1:
                        #print n,system,'\t','both'
                        n+=1
                    if abs(2*(turb_flag-ther_flag)/(turb_flag+ther_flag))<1:
                        print system,ther_flag,turb_flag,abs(2*(turb_flag-ther_flag)/(turb_flag+ther_flag))
            #if turb_flag==ther_flag==0:
            #    print system,'\t','-'

    def readfort26(self,modelpath):
        fort26 = np.loadtxt(modelpath,dtype='str',delimiter='\n')
        header = np.empty((0,6))
        flag = 0
        for i in range (len(fort26)):
            if fort26[i][0:2]=='%%':
                datapath  = fort26[i].split()[1]
                specnum   = fort26[i].split()[2]
                wavestart = fort26[i].split()[3]
                waveend   = fort26[i].split()[4]
                restype   = fort26[i].split()[5].split('=')[0]
                resvalue  = fort26[i].split()[5].split('=')[1]
                header    = np.vstack((header,[datapath,specnum,wavestart,waveend,restype,resvalue]))
            if fort26[i][0]=='!':
                flag = float(fort26[i].split()[3])
                break
        #while i<len(fort26):
        #    if fort26[i][0:2]=='%%' or len(fort26[i].split())==0:
        #        break
        #    elif fort26[i][0]!='!':
        #        vals  = fort26[i].split()
        #        val00 = vals[0]+' '+vals[1] if len(vals[0])==1 else vals[0]
        #        k = 1 if len(vals[0])==1 else 0
        #        val01 = float(vals[k+1][:-2]+re.compile(r'[^\d.-]+').sub('',vals[k+1][-2:]))
        #        val02 = " ".join(re.findall("[a-zA-Z]+",vals[k+1][-2:]))
        #        val03 = float(vals[k+2][:-2]+re.compile(r'[^\d.-]+').sub('',vals[k+2][-2:]))
        #        val04 = float(vals[k+3][:-2]+re.compile(r'[^\d.-]+').sub('',vals[k+3][-2:]))
        #        val05 = " ".join(re.findall("[a-zA-Z]+",vals[k+3][-2:]))
        #        val06 = float(vals[k+4][:-2]+re.compile(r'[^\d.-]+').sub('',vals[k+4][-2:]))
        #        val07 = float(vals[k+5][:-2]+re.compile(r'[^\d.-]+').sub('',vals[k+5][-2:]))
        #        val08 = " ".join(re.findall("[a-zA-Z]+",vals[k+5][-2:]))
        #        val09 = vals[k+6][:-2]+re.compile(r'[^\d.-]+').sub('',vals[k+6][-2:])
        #        val09 = 'nan' if '*' in val09 else float(val09)
        #        val10 = float(vals[k+7][:-2]+re.compile(r'[^\d.-]+').sub('',vals[k+7][-2:]))
        #        val11 = float(vals[k+8][:-2]+re.compile(r'[^\d.-]+').sub('',vals[k+8][-2:]))
        #        val12 = int(float(vals[k+9][:-2]+re.compile(r'[^\d.-]+').sub('',vals[k+9][-2:])))
        #        #if val00 not in ['<>','>>','<<','__'] and (val09=='nan' or val09>10):
        #        #    flag = 1
        #        #    break
        #    i = i + 1
        return flag
