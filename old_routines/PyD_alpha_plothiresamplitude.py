#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def plothiresamplitude(instrument,shape,distlist):

    os.chdir(self.home+'/results/first/')
    
    self.instrument = instrument
    self.shape      = shape
    self.distlist   = distlist
        
    fig = figure(figsize=(12,10))
    plt.subplots_adjust(left=0.07, right=0.93, bottom=0.08, top=0.93, hspace=0.2, wspace=0.25)
        
    #==============================================================================================================
    
    print '\nHIRES all systems combined with amplitude distortions\n'
    
    ax = plt.subplot(331)
    title('HIRES combined $\mathrm{(138}$ $\mathrm{systems)}$\n',fontsize=10)
    x,y,dy = getmag('all','comb')
    ax.errorbar(x,y,yerr=dy,fmt='o',ms=5,markeredgecolor='none',ecolor='purple',alpha=0.8)
    ax.axhline(y=0,ls='dotted',color='black')
    xlabel('Applied Distortion Amplitude')
    ylabel('Amplitude')
    
    ax = plt.subplot(334)
    x,y = getchisq('all','comb')
    scatter(x,y,s=25,edgecolors='none',alpha=0.8)
    ax.axhline(y=0,ls='dotted',color='black')
    xlabel('Applied Distortion Amplitude')
    ylabel('Chi-square')
    
    ax = plt.subplot(337)
    x,y = getsigmarand('all','comb')
    scatter(x,y,s=25,edgecolors='none',alpha=0.8)
    ax.axhline(y=0,ls='dotted',color='black')
    xlabel('Applied Distortion Amplitude')
    ylabel('Sigma-rand factor')

    #==============================================================================================================
    
    print '\nHIRES low redshift systems with amplitude distortions\n'
    
    ax = plt.subplot(332)
    title('HIRES low-z sample $\mathrm{(74}$ $\mathrm{systems)}$\n',fontsize=10)
    x,y,dy = getmag('all','lowz')
    ax.errorbar(x,y,yerr=dy,fmt='o',ms=5,markeredgecolor='none',ecolor='purple',alpha=0.8)
    ax.axhline(y=0,ls='dotted',color='black')
    xlabel('Applied Distortion Amplitude')
    ylabel('Amplitude')

    ax = plt.subplot(335)
    x,y = getchisq('all','lowz')
    scatter(x,y,s=25,edgecolors='none',alpha=0.8)
    ax.axhline(y=0,ls='dotted',color='black')
    xlabel('Applied Distortion Amplitude')
    ylabel('Chi-square')
    
    ax = plt.subplot(338)
    x,y = getsigmarand('all','lowz')
    scatter(x,y,s=25,edgecolors='none',alpha=0.8)
    ax.axhline(y=0,ls='dotted',color='black')
    xlabel('Applied Distortion Amplitude')
    ylabel('Sigma-rand factor')

    #==============================================================================================================
    
    print '\nHIRES high redshift systems with amplitude distortions\n'
    
    ax = plt.subplot(333)
    title('HIRES high-z sample $\mathrm{(64}$ $\mathrm{systems)}$\n',fontsize=10)
    x,y,dy = getmag('all','highz')
    ax.errorbar(x,y,yerr=dy,fmt='o',ms=5,markeredgecolor='none',ecolor='purple',alpha=0.8)
    ax.axhline(y=0,ls='dotted',color='black')
    xlabel('Applied Distortion Amplitude')
    ylabel('Amplitude')
    
    ax = plt.subplot(336)
    x,y = getchisq('all','highz')
    scatter(x,y,s=25,edgecolors='none',alpha=0.8)
    ax.axhline(y=0,ls='dotted',color='black')
    xlabel('Applied Distortion Amplitude')

    ax = plt.subplot(339)
    x,y = getsigmarand('all','highz')
    scatter(x,y,s=25,edgecolors='none',alpha=0.8)
    ax.axhline(y=0,ls='dotted',color='black')
    xlabel('Applied Distortion Amplitude')
    ylabel('Sigma-rand factor')

    savefig('hires_amplitude.pdf')
    clf()

#==================================================================================================================
