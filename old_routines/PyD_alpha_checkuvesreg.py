#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def check_uves_reg():

    print '\nChecking for non-similar coverage between TS and FSR wavelength limits...\n'
    
    fortlist()
    
    modfitreg = 0
    totfitreg = 0
    filelist = np.loadtxt('autolist.dat',dtype='str',comments='!',ndmin=1)
    for i in range (len(filelist)):
        self.distpath = filelist[i]
        self.sample   = filelist[i].split('/')[-5]
        self.qso      = filelist[i].split('/')[-4]
        self.zabs     = filelist[i].split('/')[-3]
        self.selection  = self.sample+'/'+self.qso+'/'+self.zabs
        read_fort = np.loadtxt(self.fitdir+self.selection+'/original/current.13',dtype='str',delimiter='\n')
        flag = 0
        for i in range (len(read_fort)):
            if flag==1 and '*' in read_fort[i]:
                break
            if flag==0 and '*' in read_fort[i]:
                flag = 1
            if flag==1 and '*' not in read_fort[i] and read_fort[i].split()[0][0]!='!':
                left  = float(read_fort[i].split()[2])
                right = float(read_fort[i].split()[3])
                modfitreg = modfitreg if uves_loopover(left,right)=='unchanged' else modfitreg + 1
                totfitreg = totfitreg + 1

    print '\nNumber of fitting regions scanned:',totfitreg
    print 'Number of fitting regions modified:',modfitreg,'\n'

#==================================================================================================================

def uves_loopover(left,right):

    pos = np.where(self.uvesexp['name']==self.qso)[0][0]
    sumshift = sumcount = cover1 = cover2 = cover3 = 0
    for l in range (pos,len(self.uvesexp)):
        if self.uvesexp['name'][l]!=self.qso:
            break
        if self.uvesexp['cent'][l] in self.uvesset['cent']:
            idx  = np.where(self.uvesset['cent']==self.uvesexp['cent'][l])[0][0]
        else:
            wave = np.loadtxt([int(self.uvesset['cent'][k]) for k in range (len(self.uvesset))])
            idx  = abs(wave-int(self.uvesexp['cent'][l])).argmin()
        wbeg1 = 10*float(self.uvesset['FSR_min'][idx])
        cent1 = 10*float(self.uvesset['cent'][idx])
        wend1 = 10*float(self.uvesset['FSR_max'][idx])
        if wbeg1 < left and right < wend1:
            cover1 = cover1 + 1
        wbeg2 = 10*float(self.uvesset['TS_min'][idx])
        cent2 = 10*float(self.uvesset['cent'][idx])
        wend2 = 10*float(self.uvesset['TS_max'][idx])
        if wbeg2 < left and right < wend2:
            cover2 = cover2 + 1
    if cover1==cover2:
        outcome = 'unchanged'
    if cover1!=cover2:        
        outcome = 'changed'
    if cover1!=cover2 or cover1==0:
        print self.selection,'|','%8.2f'%left,'%8.2f'%right,'|','%3.f'%cover1,'%3.f'%cover2

    return outcome
            
#==================================================================================================================

