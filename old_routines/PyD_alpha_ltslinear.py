#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def ltslinear():

    data = np.loadtxt('slopes.dat')
    ntot = len(data) # Number of values

    # Coefficients of the test line
    #
    a = 10.
    b = 0.5

    n = int(ntot*0.6)  # 60% good values
    np.random.seed(913)
    x = np.random.uniform(18.5, 21.5, n)
    y = a + b*x

    sig_int = 0.3 # Intrinsic scatter in y
    y += np.random.normal(0, sig_int, n)

    sigx = 0.1 # Observational error in x
    sigy = 0.2 # Observational error in y
    x += np.random.normal(0, sigx, n)
    y += np.random.normal(0, sigy, n)

    # Outliers produce a background of spurious values
    # intersecting with the true distribution
    #
    nout = int(ntot*0.4)   # 40% outliers
    x1 = np.random.uniform((max(x) + min(x))/2, max(x), nout)
    y1 = np.random.uniform(20, 26, nout)

    # Combines the good values and the outliers in one vector
    #
    x = np.append(x, x1)
    y = np.append(y, y1)

    sigx = np.full_like(x, sigx)  # Adopted error in x
    sigy = np.full_like(x, sigy)  # Adopted error in y

    plt.clf()
    lts_linefit(x, y, sigx, sigy, pivot=np.median(x))
    plt.xlabel('x')
    plt.ylabel('$y, a + b\ (x - x_{0})$')
    plt.show()
    #plt.pause(0.01)

    # Illustrates how to obtain the best-fitting values from the class
    print("The best fitting parameters are:", self.ab)

#==================================================================================================================

def lts_linefit(x0, y, sigx, sigy, clip=2.6, epsy=True, label=None,
              frac=None, pivot=0, plot=True, text=True, corr=True):

    if not (x0.size == y.size == sigx.size == sigy.size):
        raise ValueError('[X, Y, SIGX, SIGY] must have the same size')

    t = clock()

    x = x0 - pivot

    p = 2  # two dimensions
    n = x.size
    h = int((n + p + 1)/2) if frac is None else int(max(round(frac*n), (n + p + 1)/2))

    self.single_fit(x, y, sigx, sigy, h, clip)
    rms = np.std(self.ab[0] + self.ab[1]*x[self.mask] - y[self.mask], ddof=2)

    par = np.append(self.ab, self.sig_int)
    sig_par = np.append(self.sig_ab, self.sig_int_err)
    print('################################# Values and formal errors')
    string = display_errors(par, sig_par, epsy)
    print('Observed rms scatter: %.3g ' % rms)
    if pivot != 0:
        print('y = a + b*(x-pivot) with pivot = %.4g' % pivot)
    print('Spearman r=%.2g and p=%.2g' % stats.spearmanr(x, y))
    print('Pearson r=%.2g and p=%.2g' % stats.pearsonr(x, y))
    print('##########################################################')

    print('seconds %.2f' % (clock() - t))

    if plot:

        plt.errorbar(x0[self.mask], y[self.mask], xerr=sigx[self.mask], yerr=sigy[self.mask],
                     fmt='ob', capthick=0, label=label)
        plt.errorbar(x0[~self.mask], y[~self.mask], xerr=sigx[~self.mask], yerr=sigy[~self.mask],
                     fmt='dg', capthick=0)
        plt.autoscale(False)
        plt.title('Best fit, 1$\sigma$ (68%) and 2.6$\sigma$ (99%)')

        # Extends lines well beyond plot to allow for rescaling.
        # Lines are plotted below the symbols using z-buffer

        xmin, xmax = np.min(x0), np.max(x0)
        dx = xmax - xmin
        xlimits = np.array([xmin - dx, xmax + dx])
        y1 = par[0] + par[1]*(xlimits - pivot)
        plt.plot(xlimits, y1, '-k',
                 xlimits, y1 + rms, '--r',
                 xlimits, y1 - rms, '--r',
                 xlimits, y1 + 2.6*rms, ':r',
                 xlimits, y1 - 2.6*rms, ':r', linewidth=2, zorder=1)

        ax = plt.gca()
        if text:
            string += '$\Delta=%.2g$\n' % rms
            if pivot != 0:
                string += '$(x_0=%.4g)$' % pivot
            ax.text(0.05, 0.95, string, horizontalalignment='left',
                    verticalalignment='top', transform=ax.transAxes)

        if corr:
            txt = '${\\rm Spearman/Pearson}$\n'
            txt += '$r=%.2g\, p=%.2g$\n' % stats.spearmanr(x, y)
            txt += '$r=%.2g\, p=%.2g$\n' % stats.pearsonr(x, y)
            ax.text(0.95, 0.95, txt, horizontalalignment='right',
                    verticalalignment='top', transform=ax.transAxes)

        ax.minorticks_on()
        ax.tick_params(length=10, width=1, which='major')
        ax.tick_params(length=5, width=1, which='minor')

#==================================================================================================================

def single_fit(x, y, sigx, sigy, h, clip):

    if self.find_outliers(0, x, y, sigx, sigy, h, 0, clip) < 0:
        print('No intrinsic scatter or errors overestimated')
        sig_int = 0
        sig_int_err = 0
    else:
        sig1 = 0.
        sig2 = np.std(y)  # Upper bound to intrinsic scatter
        print('Computing sig_int')
        sig_int = optimize.brentq(self.find_outliers, sig1, sig2,
                            args=(x, y, sigx, sigy, h, 0, clip), rtol=1e-3)
        print('Computing sig_int error') # chi2 can always decrease
        sigMax_int = optimize.brentq(self.find_outliers, sig_int, sig2,
                            args=(x, y, sigx, sigy, h, 1, clip), rtol=1e-3)
        sig_int_err = sigMax_int - sig_int

    self.sig_int = sig_int
    self.sig_int_err = sig_int_err

    print('Repeat at best fitting solution')
    self.find_outliers(sig_int, x, y, sigx, sigy, h, 0, clip)

#==================================================================================================================

def find_outliers(sig_int, x, y, sigx, sigy1, h, offs, clip):

    sigy = np.sqrt(sigy1**2 + sig_int**2) # Gaussian intrinsic scatter

    if h == x.size: # No outliers detection

        ab = linefit(x, y, sigy=sigy)  # quick initial guess
        ab, sig_ab, chi_sq = fitting(x, y, sigx, sigy, ab)
        mask = np.ones_like(x, dtype=bool)  # No outliers

    else: # Robust fit and outliers detection

        # Initial estimate using the maximum breakdown of
        # the method of 50% but minimum efficiency
        #
        ab, mask = fast_algorithm(x, y, sigx, sigy, h)

        # inside-out outliers removal
        #
        while True:
            res = residuals(ab, x, y, sigx, sigy)
            sig = np.std(res[mask], ddof=2)
            maskOld = mask
            mask = np.abs(res) < clip*sig
            ab, sig_ab, chi_sq = fitting(x[mask], y[mask], sigx[mask], sigy[mask], ab)
            if np.array_equal(mask, maskOld):
                break

    # To determine 1sigma error on the intrinsic scatter the chi2
    # is decreased by 1sigma=sqrt(2(h-2)) while optimizing (a,b)
    #
    h = mask.sum()
    dchi = np.sqrt(2*(h - 2)) if offs else 0.

    self.ab = ab
    self.sig_ab = sig_ab
    self.mask = mask

    err = (chi_sq + dchi)/(h - 2.) - 1.
    print('sig_int: %10.4f  %10.4f' % (sig_int, err))

    return err

#==================================================================================================================
    
def linefit(x, y, sigy=None, weights=None):
    """
    Fit a line y = a + b*x to a set of points (x, y)
    by minimizing chi2 = np.sum(((y - yfit)/sigy)**2)

    """
    v1 = np.ones_like(x)
    if weights is None:
        if sigy is None:
            sw = v1
        else:
            sw = v1/sigy
    else:
        sw = np.sqrt(weights)

    a = np.column_stack([v1, x])
    ab = np.linalg.lstsq(a*sw[:, None], y*sw)[0]

    return ab

#==================================================================================================================

def display_errors(par, sig_par, epsy):
    """
    Print parameters rounded according to their errors

    """
    prec = np.zeros_like(par)
    w = (sig_par != 0) & (par != 0)
    prec[w] = np.ceil(np.log10(np.abs(par[w]))) - np.floor(np.log10(sig_par[w])) + 1
    prec = prec.clip(0)  # negative precisions not allowed
    dg = list(map(str, prec.astype(int)))

    # print on the terminal and save as string

    txt = ['intercept: ', 'slope: ', 'scatter: ']
    for t, d, p, s in zip(txt, dg, par, sig_par):
        print('%12s' % t, ('%.' + d + 'g') % p, '+/- %.2g' % s)

    txt = ['a=', 'b=', '\\varepsilon_y=']
    if not epsy:
        txt = txt[:-1]
    string = ''
    for t, d, p, s in zip(txt, dg, par, sig_par):
        string += '$' + t + ('%.' + d + 'g') % p + '\\pm%.2g' % s + '$\n'

    return string

#==================================================================================================================

def residuals(ab, x, y, sigx, sigy):
    """
    See equation (6) of Cappellari et al. (2013, MNRAS, 432, 1709)

    """
    res = (ab[0] + ab[1]*x - y) / np.sqrt((ab[1]*sigx)**2 + sigy**2)

    return res

#==================================================================================================================

def fitting(x, y, sigx, sigy, ab):

    ab, pcov, infodict, errmsg, success = optimize.leastsq(
        residuals, ab, args=(x, y, sigx, sigy), full_output=1)

    if pcov is None or np.any(np.diag(pcov) < 0):
        sig_AB = np.full(2, np.inf)
        chi2 = np.inf
    else:
        chi2 = np.sum(infodict['fvec']**2)
        sig_AB = np.sqrt(np.diag(pcov)) # ignore covariance

    return ab, sig_AB, chi2

#==================================================================================================================

def fast_algorithm(x, y, sigx, sigy, h):

    # Robust least trimmed squares regression.
    # Pg. 38 of Rousseeuw & van Driessen (2006)
    # http://dx.doi.org/10.1007/s10618-005-0024-4
    #
    m = 500 # Number of random starting points
    abv = np.empty((m, 2))
    chi2v = np.empty(m)
    for j in range(m): # Draw m random starting points
        w = np.random.choice(x.size, 2, replace=False)
        ab = linefit(x[w], y[w])  # Find a line going trough two random points
        for k in range(3): # Run C-steps up to H_3
            res = residuals(ab, x, y, sigx, sigy)
            good = np.argsort(np.abs(res))[:h] # Fit the h points with smallest errors
            ab, sig_ab, chi_sq = fitting(x[good], y[good], sigx[good], sigy[good], ab)
        abv[j, :] = ab
        chi2v[j] = chi_sq

    # Perform full C-steps only for the 10 best results
    #
    w = np.argsort(chi2v)
    nbest = 10
    chi_sq = np.inf
    for j in range(nbest):
        ab1 = abv[w[j], :]
        while True: # Run C-steps to convergence
            abOld = ab1
            res = residuals(ab1, x, y, sigx, sigy)
            good1 = np.argsort(np.abs(res))[:h] # Fit the h points with smallest errors
            ab1, sig_ab1, chi1_sq = fitting(x[good1], y[good1], sigx[good1], sigy[good1], ab1)
            if np.array_equal(abOld, ab1):
                break
        if chi_sq > chi1_sq:
            ab = ab1  # Save best solution
            good = good1
            chi_sq = chi1_sq

    mask = np.zeros_like(x, dtype=bool)
    mask[good] = True

    return ab, mask

#==================================================================================================================
