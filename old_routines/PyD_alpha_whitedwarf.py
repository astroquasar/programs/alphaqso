#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *
    
#==================================================================================================================

def wdprepare():

    self.distpath = self.here+'/distortion/'+self.slstr

    if os.path.exists(self.distpath)==False:
        os.system('mkdir -p '+self.distpath)

    fort = open(self.here+'/fit/fort.13','r')
    line13 = []
    flag   = 0
    for line in fort:
        flag = 1 if '*' in line else flag
        if len(line.split())==0: break
        elif line[0]!='!' and flag==1: line13.append(line.replace('\n',''))
    fort.close()

    shiftlist = []
    final  = open(self.distpath+'/fort_ini.13','w')
    final.write('   *'+'\n')
    i,star,ncomp = 0,0,0
    for k in range (1,len(line13)):
        star = 1 if '*' in line13[k] else star
        if star==0:
            shift = wdgetshift(float(line13[k].split()[2]),float(line13[k].split()[3]),self.slope)
            shiftlist.append(shift)
            final.write(line13[k]+'\n')
            i=i+1
        else:
            final.write(line13[k].replace('../','')+'\n')
            ncomp = ncomp+1

    for p in range (len(shiftlist)):
        final.write('   >>         1.0000FF  0.000000FF  '+'{0:>10}'.format(shiftlist[p])+'FF   0.000FF    0.00   0.00E+00  '+'{0:>3}'.format(p+1)+'   !'+'{0:>3}'.format(ncomp)+'\n')
        ncomp = ncomp+1

    final.close()
    
#==================================================================================================================

def wdgetshift(left,right,slope):

    def stis_slope(x,slope):
        return -slope*1425. + slope*x

    middle = (left+right)/2
    shift  = stis_slope(middle,slope)
                
    return '{0:.4f}'.format(-float(shift)/1000.)
        
#==================================================================================================================

def wdrunfit():

    os.chdir(self.distpath)

    os.system('ln -s ../../../data')
    os.system('cp ../../fit/atom.dat .')
    os.system('cp ../../fit/vp_setup.dat .')

    os.environ['ATOMDIR']='atom.dat'
    os.environ['VPFSETUP']='vp_setup.dat'

    open('fitcommands','w').write('f\n\n\nfort_ini.13\nn\nn\n')
    os.system('vpfit10.2 < fitcommands')
    if os.path.exists('fort.18')==True:
        createfit13('fort')
        
#==================================================================================================================

def wdplots():

    xmin,xmax = -0.1,1.6
    results   = np.empty((0,6))
    print '\n{0:>5}{1:>10}{2:>10}{3:>15}{4:>10}{5:>10}\n'.format('slope','alpha','error','chisq','chisqnu','df')
    for i in np.arange(xmin,xmax,0.05):#self.wd_slope:
        slope    = i
        slopestr = '0.000' if round(i,3)==0 else str('%.3f'%i).replace('-','m') if '-' in str(i) else 'p'+str('%.3f'%i)
        fortpath = self.here+'/distortion/'+slopestr+'/fort.26'
        if os.path.exists(fortpath)==True:
            fort26 = open(fortpath,'r')
            for line in fort26:
                if line[0]=='!':
                    chisqnu = float(line.split()[3])
                    n       = float(line.split()[4])
                    df      = float(line.split()[5])
                if line[0] not in ['%','!'] and len(line.split()[0])==1:
                    redshift = float(re.compile(r'[^\d.-]+').sub('',line.split()[2]))
                    if 'q' in str(line.split()[8]):
                        alpha = float(line.split()[8].split('q')[0])*10**5
                        error = float(line.split()[9])*10**5
                        k  = n-df
                        chisq = chisqnu*df
                        break
                if line[0] not in ['%','!'] and len(line.split()[0])>1:
                    redshift = float(re.compile(r'[^\d.-]+').sub('',line.split()[1]))
                    if 'q' in str(line.split()[7]):
                        alpha = float(line.split()[7].split('q')[0])*10**5
                        error = float(line.split()[8])*10**5
                        k  = n-df
                        chisq = chisqnu*df
                        break
            fort26.close()
            print '{0:>5}{1:>10}{2:>10}{3:>15}{4:>10}{5:>10}'.format('%.1f'%slope,'%.4f'%alpha,'%.4f'%error,'%.6f'%chisq,'%.6f'%chisqnu,int(df))
            results = np.vstack((results,[slope,alpha,error,chisq,chisqnu,df]))

    print ''
    fig = figure(figsize=(6,5))
    plt.subplots_adjust(left=0.13, right=0.95, bottom=0.1, top=0.95, hspace=0, wspace=0.2)
    
#    ax = plt.subplot(4,1,1,xlim=[1150,1700])
#    title('G191-B2B - Distortion Results\n',fontsize=10)
#    for i in self.wd_slope:
#        x0 = 1425
#        x = np.arange(1150,1700,1)
#        slope = i
#        y = -slope*x0 + slope*x
#        ax.plot(x,y,'grey',lw=0.1,zorder=2,alpha=0.7)
#    ax.axhline(y=0,ls='dotted',color='black')
#    ylabel('Shift (m/s)')
#    xlabel('FUV-MAMA detector wavelength range')

    imin = abs(results[:,0]+0.05).argmin()
    imax = abs(results[:,0]-1.55).argmin()

    ymin = 2335#min(results[imin:imax,3])
    ymax = 2339#max(results[imin:imax,3])
    ax = plt.subplot(2,1,2,xlim=[xmin,xmax],ylim=[ymin,ymax])
    ax.errorbar(results[:,0],results[:,3],fmt='o',ms=4,markeredgecolor='none',ecolor='grey',alpha=0.8,color='black')
    ax.axhline(y=0,ls='dotted',color='black')
    ylabel('$\chi^2$',fontsize=12)
    xlabel('Distortion slope (m/s/$\AA$)',fontsize=12)
    fitparabola2(results[:,0],results[:,3])
    ax.axvline(x=0,color='black')
    ax.axhline(y=results[imin+1,3],color='black')
    ax.yaxis.set_major_locator(plt.FixedLocator([2335,2336,2337,2338,2339]))
    y_formatter = matplotlib.ticker.ScalarFormatter(useOffset=False)
    ax.yaxis.set_major_formatter(y_formatter)
                
    ymin = 4.6#min(results[imin:imax,1]-results[imin:imax,2])
    ymax = 6.4#max(results[imin:imax,1]+results[imin:imax,2])
    ax = plt.subplot(2,1,1,xlim=[xmin,xmax],ylim=[ymin,ymax])
    plt.setp(ax.get_xticklabels(), visible=False)
    ax.errorbar(results[:,0],results[:,1],yerr=results[:,2],fmt='o',ms=4,markeredgecolor='none',ecolor='grey',alpha=0.8,color='black')
    ax.axhline(y=0,ls='dotted',color='black')
    ylabel(r'$\Delta\alpha/\alpha$ $(10^{-5})$',fontsize=12)
    fitlinear(results[:,0],results[:,1],yerr=results[:,2])
    ax.axvline(x=0,color='black')
    ax.axhline(y=results[imin+1,1],color='black')
    ax.yaxis.set_major_locator(plt.FixedLocator([4.8,5.0,5.2,5.4,5.6,5.8,6.0,6.2]))
    y_formatter = matplotlib.ticker.ScalarFormatter(useOffset=False)
    ax.yaxis.set_major_formatter(y_formatter)
                
    savefig(self.here+'/distortion.pdf')
    clf()

#==================================================================================================================
