#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def plotdiscard():

    xmin,xmax = -0.5,0.5
    if '--whitmore' in sys.argv:
        self.discard = [-0.45,-0.39,-0.35,-0.25,0.05,0.12,0.14,0.19,0.31]
    else:
        self.discard = [0.41,0.47,1.15,1.33,1.41]

    chisquare = np.arange(0.001,10,0.001)

    threshold = []
    results = np.empty((0,2))
    for i in chisquare:
        check_chisq('chisq',chisq=i)
        x,yabs,yred = getglobchisq()
        ymin = min(yred)
        ymax = max(yred)
        mean,sigma = 0,1
        def gaus(x,a,x0,sigma):
            return a*np.exp(-(x-x0)**2/(2*sigma**2))
        popt,pcov = curve_fit(gaus,x,yred,p0=[1,mean,sigma])
        x = np.arange(xmin,xmax,0.001)
        imid = abs(gaus(x,*popt)-max(gaus(x,*popt))).argmin()
        xmid = x[imid]
        if len(self.skipabs) not in results[:,1]:
            results = np.vstack((results,[xmid,len(self.skipabs)]))
            threshold.append(i)
            slope = [k for k in self.distlist if round(k,2) not in self.discard]
            model = [gaus(m,*popt) for m in slope]
            dist  = getattr(stats,'norm')
            param = dist.fit(yred)
            print 'Threshold:','%.3f'%i,\
                  '| Discarded:','%3.f'%len(self.skipabs),\
                  '| p-value:','%.6f'%stats.kstest(yred,'norm',param)[1],\
                  '| Best fit:','%.3f'%xmid

    ''' Create plot '''
        
    rc('font', size=2, family='fantasy')
    rc('axes', labelsize=10, linewidth=0.2)
    rc('legend', fontsize=10, handlelength=10)
    rc('xtick', labelsize=7)
    rc('ytick', labelsize=7)
    rc('lines', lw=0.2, mew=0.2)
    rc('grid', linewidth=0.2)

    fig = figure(figsize=(11.69,8.27))
    plt.subplots_adjust(left=0.07, right=0.87, bottom=0.1, top=0.95, hspace=0, wspace=0)

    ax = plt.subplot(1,1,1,xlim=[0,153],ylim=[0,0.16])
    ax.scatter(results[:,1],results[:,0],\
               marker='o',s=40,edgecolors='none',\
               c=threshold,cmap=mpl.cm.rainbow,vmin=0,vmax=0.3)
    for i in range (len(results)):
        ax.text(results[i,1],results[i,0],'  '+str('%.3f'%threshold[i]),rotation='90',ha='center',va='bottom',fontsize=6)
    xlabel('Number of absorption systems discarded from analysis',fontsize=10)
    ylabel('Best global chi-square from maximum likelihood',fontsize=10)

    ax1  = fig.add_axes([0.87,0.1,0.04,0.85])
    cmap = mpl.cm.rainbow
    norm = mpl.colors.Normalize(vmin=0,vmax=0.3)
    cb1  = mpl.colorbar.ColorbarBase(ax1,cmap=cmap,norm=norm)
    cb1.set_label('Chi-square threshold')
    
    savefig('uves-discard.pdf')
    clf()

#==================================================================================================================
