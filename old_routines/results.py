import alpha

class results(object):

    """
    Extract da/a results and compare values between different fitting model or versions
    """ 

    def showhelp(self):
        
        print ""
        print "-------------------------------------------------------------------------"
        print ""
        print "required arguments:"
        print ""
        print "   --selection    Sample to study"
        print "   --instrument   "
        print ""
        print "optional arguments:"
        print ""
        print "   --chisq        Chi-square of sample"
        print "   --constrain    Apply constrain where 1 sigma deviant points are flagged"
        print "   --compress     Compress results for easy extraction from supercomputer"
        print "   --mgisotope    Fit for non-terrestrial Mg abundances"
        print "   --original     Extract original results from selected model"
        print ""
        print "examples:"
        print ""
        print "   alpha results --instrument hires --chisq 1e-6"
        print ""
        print "-------------------------------------------------------------------------"
        print ""
        quit()
        
    def __init__(self):

        if '-h' in sys.argv or '--help' in sys.argv:
            self.showhelp()

        info1 = self.extract()
        if '-' not in info1[:,-3]:
            self.compare_model(info1)
        if setup.compare!=None:
            setup.original  = False
            setup.selection = setup.compare
            info2 = self.extract()
            self.compare_version(info1,info2)

    def extract(self,display=False):
        if setup.display:
            print '\n{0:<50} {1:>10}{2:>15}{3:>15}{4:>15}{5:>15}{6:>15}{7:>15}{8:>15}{9:>15}{10:>15}{11:>15}{12:>15}{13:>5}{14:>5}\n'.format('system','ther_alpha','ther_error','ther_chisq','ther_df','ther_n','turb_alpha','turb_error','turb_chisq','turb_df','turb_n','mom_alpha','mom_error','regs','tied')
        info = np.empty((0,15))
        for k in range(len(setup.curlist)):
            model = str(setup.curlist[setup.selection][k])
            if model!='-' and (setup.instrument==None or setup.curlist['instrument'][k]==setup.instrument.upper()):
                system = setup.curlist['system'][k]
                info   = np.vstack((info,[system]+['-']*14))
                if setup.curlist[model][k]=='MTM' and setup.original:
                    flag = 0
                    for mod in ['thermal','turbulent']:
                        idx  = 1 if mod=='thermal' else 6
                        path = setup.fitdir+system+'/model-'+model+'/model/'+mod+'.18'
                        if os.path.exists(path)==True:
                            fort18 = np.loadtxt(path,dtype=str,delimiter='\n')
                            for i in range(len(fort18)-1,0,-1):
                                if 'statistics for whole fit:' in fort18[i] and flag==0:
                                    info[-1,idx+3] = fort18[i+2].split()[4] #ndf
                                    info[-1,idx+4] = fort18[i+2].split()[3] #npix
                                    flag       += 1
                                if 'dfsc/fsc' in fort18[i] and flag==1:
                                    info[-1,idx+0] = '%.5f'%float(fort18[i].split()[2]) #alpha
                                    info[-1,idx+1] = '%.5f'%float(fort18[i].split()[4]) #error
                                    flag       += 1
                                if 'chi-squared :' in fort18[i] and flag==2:
                                    info[-1,idx+2] = '%.5f'%float(fort18[i].split()[-3].replace(',','')) #chisq
                                    flag       += 1
                else:
                    distpath = setup.fitdir+system+'/model-'+model
                    setup.distpath = distpath+'/model/' if setup.original else distpath+'/runs/'+setup.test+'/'+setup.distortion
                    setup.zabs = setup.curlist['z_abs'][k]
                    if os.path.exists(setup.distpath):
                        getresults(original=setup.original)
                        info[-1,1]  = '%.5f'%setup.ther_alpha
                        info[-1,2]  = '%.5f'%setup.ther_error
                        info[-1,3]  = '%.5f'%setup.ther_chisq
                        info[-1,4]  = '%.0f'%setup.ther_df
                        info[-1,5]  = '%.0f'%setup.ther_n
                        info[-1,6]  = '%.5f'%setup.turb_alpha
                        info[-1,7]  = '%.5f'%setup.turb_error
                        info[-1,8]  = '%.5f'%setup.turb_chisq
                        info[-1,9]  = '%.0f'%setup.turb_df
                        info[-1,10] = '%.0f'%setup.turb_n
                        info[-1,11] = '%.6f'%setup.mom_alpha
                        info[-1,12] = '%.6f'%setup.mom_error
                reg,tied=0,0
                for mod in ['thermal','turbulent']:
                    path = setup.fitdir+system+'/model-'+model+'/model/'+mod+'.13'
                    if os.path.exists(path)==True:
                        fort13 = np.loadtxt(path,dtype=str,delimiter='\n')
                        i,reg,tied,flag = 0,0,0,0
                        while i < len(fort13):
                            if len(fort13[i].split())==0:
                                break
                            if fort13[i].split()[0]=='*':
                                flag += 1
                                i += 1
                            if flag==1:
                                reg += 1
                            if flag==2:
                                if ((len(fort13[i].split()[0])==1 and fort13[i].split()[4][-1].islower()==True) or (len(fort13[i].split()[0])>1 and fort13[i].split()[3][-1].islower()==True)):
                                    tied +=1
                            i += 1
                info[-1,-2] = str(int(reg))
                info[-1,-1] = str(int(tied))
                if setup.display:
                    print '{0:<50} {1:>10}{2:>15}{3:>15}{4:>15}{5:>15}{6:>15}{7:>15}{8:>15}{9:>15}{10:>15}{11:>15}{12:>15}{13:>5}{14:>5}'.format(info[-1,0],info[-1,1],info[-1,2],info[-1,3],info[-1,4],info[-1,5],info[-1,6],info[-1,7],info[-1,8],info[-1,9],info[-1,10],info[-1,11],info[-1,12],str(int(reg)),str(int(tied)))
                if setup.display2:
                    quasar     = info[-1,0].split('/')[0]
                    redshift   = info[-1,0].split('/')[1]
                    sample     = 'D'
                    if setup.curlist['source'][k]=='Churchill':
                        sample = 'A'
                    if setup.curlist['source'][k]=='Prochaska' and setup.curlist['1.3'][k]=='MTM':
                        sample = 'B1' 
                    if setup.curlist['source'][k]=='Prochaska' and setup.curlist['1.2'][k]=='-':
                        sample = 'B2'
                    if setup.curlist['source'][k]=='Sargent':
                        sample = 'C'
                    if info[-1,1]!='-' and info[-1,6]=='-':
                        models = 'ther'
                        alpha  = '$%.3f \pm %.3f$'%(float(info[-1,1]),float(info[-1,2]))
                        chisq  = '%.4f'%(float(info[-1,3])/float(info[-1,4]))
                        ndf    = info[-1,4]
                        print quasar+'\t'+redshift+'\t'+sample+'\t ther \t'+alpha+'\t'+chisq+'\t'+ndf
                    if info[-1,1]=='-' and info[-1,6]!='-':
                        models = 'ther'
                        alpha  = '$%.3f \pm %.3f$'%(float(info[-1,6]),float(info[-1,7]))
                        chisq  = '%.4f'%(float(info[-1,8])/float(info[-1,9]))
                        ndf    = info[-1,9]
                        print quasar+'\t'+redshift+'\t'+sample+'\t turb \t'+alpha+'\t'+chisq+'\t'+ndf
                    if info[-1,1]!='-' and info[-1,6]!='-':
                        ther_alpha = '$%.3f \pm %.3f$'%(float(info[-1,1]),float(info[-1,2]))
                        ther_chisq = '%.4f'%(float(info[-1,3])/float(info[-1,4]))
                        ther_ndf   = info[-1,4]
                        turb_alpha = '$%.3f \pm %.3f$'%(float(info[-1,6]),float(info[-1,7]))
                        turb_chisq = '%.4f'%(float(info[-1,8])/float(info[-1,9]))
                        turb_ndf   = info[-1,9]
                        mom_alpha  = '$%.3f \pm %.3f$'%(float(info[-1,11]),float(info[-1,12]))
                        print (quasar+'\t'+redshift+'\t'+\
                               ther_alpha+'\t'+ther_chisq+'\t'+\
                               turb_alpha+'\t'+turb_chisq+'\t'+mom_alpha+' \\\ ')
        return info
    
    def compare_model(self,info):
            
        instrument   = 'comb' if setup.instrument==None else setup.instrument
        ther_chisqnu = np.array(info[:,3],dtype=float)/np.array(info[:,4],dtype=float)
        turb_chisqnu = np.array(info[:,8],dtype=float)/np.array(info[:,9],dtype=float)
        fig = figure(figsize=(12,6))
        plt.subplots_adjust(left=0.1, right=0.96, bottom=0.12, top=0.95, hspace=0.05, wspace=0)
        ax1 = subplot(111)
        x = np.array(info[:,1],dtype=float)
        ax1.plot(x,x,color='black',lw=1,zorder=2)
        for i in range(len(info)):
            x,xerr = float(info[i,1]),float(info[i,2])
            y,yerr = float(info[i,6]),float(info[i,7])
            if setup.constrain and  abs(y-x)>xerr+yerr:
                ax1.scatter(x,y,alpha=0.5,s=50,c='red',edgecolor='white')
                print info[i,0]
            else:
                color = 'blue' if turb_chisqnu[i]<ther_chisqnu[i] else 'red'
                ax1.scatter(x,y,alpha=0.5,s=50,c='black',edgecolor='white')
        ax1.xaxis.set_minor_locator(MultipleLocator(0.1))
        ax1.yaxis.set_minor_locator(MultipleLocator(0.1))
        xlabel(r'Thermal $\Delta\alpha/\alpha$ $(10^{-5})$',fontsize=20)
        ylabel(r'Turbulent $\Delta\alpha/\alpha$ $(10^{-5})$',fontsize=20)
        savefig(setup.home+'/results/alpha/'+instrument+'/'+setup.test+'-model.pdf')
        clf()
        
    def compare_version(self,info1,info2):
        
        instrument = 'comb' if setup.instrument==None else setup.instrument
        x,xerr,y,yerr = [],[],[],[]
        for i in range(len(info1)):
            value = info1[i,11] if info1[i,11]!='-' else info1[i,1]  if info1[i,1]!='-' else info1[i,6]
            x.append(float(value))
            value = info1[i,12] if info1[i,12]!='-' else info1[i,2]  if info1[i,2]!='-' else info1[i,7]
            xerr.append(float(value))
            value = info2[i,11] if info2[i,11]!='-' else info2[i,1]  if info2[i,1]!='-' else info2[i,6]
            y.append(float(value))
            value = info2[i,12] if info2[i,12]!='-' else info2[i,2]  if info2[i,2]!='-' else info2[i,7]
            yerr.append(float(value))
        c = np.array([info1[i,0].split('/')[1] for i in range(len(info1))],dtype=float)
        fig = figure(figsize=(12,6))
        plt.subplots_adjust(left=0.1, right=0.87, bottom=0.12, top=0.95, hspace=0.05, wspace=0)
        ax1 = subplot(111)        
        lim = 1
        band = np.linspace(min(x),max(x),10)
        ax1.plot(band,band,color='black',lw=1.5,zorder=2)
        for i in range(len(x)):
            if setup.constrain and abs(y[i]-x[i])>xerr[i]+yerr[i]:
                ax1.scatter(x[i],y[i],s=50,c='red',edgecolor='white',zorder=3)
                print info1[i,0]
            else:
                ax1.scatter(x[i],y[i],alpha=0.4,s=100,c=c[i],edgecolor='white',cmap=mpl.cm.cool,vmin=min(c),vmax=max(c),zorder=1)
        ax1.xaxis.set_minor_locator(MultipleLocator(0.1))
        ax1.yaxis.set_minor_locator(MultipleLocator(0.1))
        xlabel(r'Original $\Delta\alpha/\alpha$ $(10^{-5})$',fontsize=20)
        ylabel(r'VPFIT10 $\Delta\alpha/\alpha$ $(10^{-5})$',fontsize=20)
        ax1  = fig.add_axes([0.87,0.12,0.04,0.83])
        cmap = mpl.cm.cool
        norm = mpl.colors.Normalize(vmin=min(c),vmax=max(c))
        cb1  = mpl.colorbar.ColorbarBase(ax1,cmap=cmap,norm=norm)
        cb1.set_label(r'$z_\mathrm{abs}$',fontsize=20)
        savefig(setup.home+'/results/alpha/'+instrument+'/'+setup.test+'-version.pdf')
        clf()
        
