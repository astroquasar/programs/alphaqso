import alpha

def slopes_sim():

    """
    Plot results of slope weighted mean vs. standard deviation used.
    """

    def showhelp(self):

        print ""
        print "-------------------------------------------------------------------------"
        print ""
        print "required arguments:"
        print ""
        print "   --instrument  High-resolution spectrograph used"
        print "                   [uves]  VLT spectrograph"
        print "                   [hires] Keck spectrograph"
        print ""
        print "optional arguments:"
        print ""
        print "   --expind      Whether we use individual slope per exposure or not"
        print "   --selection   Individual or list of absorption systems to study"
        print "   --show        Print out the results for every slope in the terminal"
        print "   --simulation  Type of simulated model to be used"
        print "                   [simple]  One component per system"
        print "                   [complex] Realistic kinematic structure"
        print "   --stdev       Standard deviation for random slope per exposure"
        print "   --whitmore    Look at results from simplistic distortion model"
        print ""
        print "example:"
        print ""
        print "   alpha slopes_sim --instrument uves"
        print ""
        print "-------------------------------------------------------------------------"
        print ""
        quit()

    # Display help message or check if required arguments are given
    if '--help' in sys.argv or '-h' in sys.argv:
        showhelp()
    elif setup.instrument==None:
        print 'ERROR: --instrument is missing...'
        quit()
    os.chdir(setup.home+'/results/slopes/')
    name = setup.instrument
    name = name+'_jw' if '--whitmore' in sys.argv else name
    # Extract results
    os.system('ls > temp.dat')
    temp = np.loadtxt('temp.dat',dtype=str)
    os.system('rm temp.dat')
    data = np.empty((0,3),dtype=float)
    for folder in temp:
        if name in folder and folder[-1]=='T':
            results = np.loadtxt(folder+'/wmclipnr.dat',dtype=str,delimiter='\n')
            for line in results:
                #if 'Clipped weighted mean' in line:
                if 'Wmean (unclipped)' in line:
                    stdev = float(folder.split('_')[-1].replace('T',''))
                    slope = float(line.split()[-3])
                    error = float(line.split()[-1])
                    data  = np.vstack((data,[stdev,slope,error]))
                    
    # Initializing the figure
    rc('font', size=2, family='sans-serif')
    rc('axes', labelsize=10, linewidth=0.2)
    rc('legend', fontsize=10, handlelength=10)
    rc('xtick', labelsize=7)
    rc('ytick', labelsize=7)
    rc('lines', lw=0.2, mew=0.2)
    rc('grid', linewidth=0.4)
    # Plot results
    fig = figure(figsize=(7,4))
    plt.subplots_adjust(left=0.1, right=0.97, bottom=0.1, top=0.98, hspace=0.03, wspace=0)
    ax = plt.subplot(111,xlim=[-0.01,0.21])
    ax.errorbar(data[:,0],data[:,1],yerr=data[:,2],ms=20,lw=3,fmt='o',alpha=0.7,zorder=1)
    ax.errorbar(data[:,0],data[:,1],ms=3,lw=3,fmt='o',zorder=1,color='0.8')
    ax.axhline(-0.123,ls="dashed",zorder=2,lw=1,color='red',alpha=0.4)
    ax.set_xlabel('Standard Deviation')
    ax.set_ylabel(r'Distortion slope clipped weighted mean (m/s/$\mathrm{\AA}$)')
    savefig(name+'_sim.pdf')
    plt.close(fig)
        
    # Initializing the figure
    rc('font', size=2, family='sans-serif')
    rc('axes', labelsize=10, linewidth=0.2)
    rc('legend', fontsize=10, handlelength=10)
    rc('xtick', labelsize=7)
    rc('ytick', labelsize=7)
    rc('lines', lw=0.2, mew=0.2)
    rc('grid', linewidth=0.4)
    # Plot results
    fig = figure(figsize=(7,4))
    plt.subplots_adjust(left=0.1, right=0.97, bottom=0.12, top=0.98, hspace=0.03, wspace=0)
    ax = plt.subplot(111,xlim=[0.0185,0.019],ylim=[0,0.225])
    for i in range(len(data)):
        print data[i,0],data[0,2],data[i,2],data[i,2],np.sqrt(data[i,0]**2+data[0,2]**2)
    x = data[:,2]
    y = np.sqrt(data[:,0]**2+data[0,2]**2)
    out = open('sdlist.dat','w')
    for i in range(len(x)):
        out.write('%.5f  %.5f  0.001\n'%(x[i],y[i]))
    out.close()
    #os.system('straightline3 < sdlist.dat')
    ax.errorbar(x,y,ms=18,lw=3,fmt='o',zorder=1,alpha=0.7)
    ax.errorbar(x,y,ms=3,lw=3,fmt='o',zorder=1,color='0.8')
    def func(func,b):
        return b*x
    pars,cov = curve_fit(func,x,y)
    xfit = np.arange(0,0.05,0.001)
    yfit = pars[0]*xfit
    #yfit = 0.707472444*xfit
    #yfit_min = (0.707472503-0.105001055)*xfit
    #yfit_max = (0.707472503+0.105001055)*xfit
    #print 0.707472444*1.5392995959635771E-002
    #a = (0.707472503-0.105001055)*1.5392995959635771E-002
    #b = (0.707472503+0.105001055)*1.5392995959635771E-002
    #print a,b,b-a
    #plot(xfit,yfit_min,color='black',ls='dotted',lw=1,zorder=2)
    #plot(xfit,yfit,color='black',lw=1,zorder=2)
    #plot(xfit,yfit_max,color='black',ls='dotted',lw=1,zorder=2)
    ax.axvline(1.5392995959635771E-002,ls="dashed",zorder=2,lw=1,color='red',alpha=0.4)
    ax.set_ylabel(r'$\sqrt{\sigma_\mathrm{int}^2 + \sigma_\mathrm{stat}^2}$ (m/s/$\AA$)')
    ax.set_xlabel(r'$\sigma_\mathrm{obs}$ (m/s/$\AA$)')
    savefig(name+'_sim2.pdf')
    plt.close(fig)
        
