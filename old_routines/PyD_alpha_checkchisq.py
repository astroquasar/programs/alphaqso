#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def check_chisq(criteria=None,chisq=0.1,alpha=0.1,error=0.1,model=None,skipabs=None):

    criteria = None if chisq==None else criteria
    self.skipabs = np.empty((0,1)) if skipabs==None else self.skipabs
    range1 = np.arange(0,-0.51,-0.01)
    range2 = np.arange(0,0.51,0.01)
    for looprange in [range1,range2]:
        previous = np.genfromtxt(self.rundir+self.instrument+'/'+self.selection+'/'+self.test+'/0.000/results.dat',names=True,dtype=object,comments='!')
        for i in looprange:
            slope      = '0' if round(i,3)==0 else str(i)
            distortion = '0.000' if float(slope)==0 else str('%.3f'%float(slope)).replace('-','m') if float(slope)<0 else 'p'+str('%.3f'%float(slope))
            path       = self.rundir+self.instrument+'/'+self.selection+'/'+self.test+'/'+distortion+'/results.dat'
            if os.path.exists(path)==True:
                output = np.genfromtxt(path,names=True,dtype=object,comments='!')
                for k in range (len(output)):
                    absorber  = output['sample'][k]+'/'+output['qso'][k]+'/'+output['z_abs'][k]
                    condition = 0==1
                    if criteria=='ndf':
                        cond0  = absorber not in self.skipabs
                        cond1  = previous['ther_df'][k]!=output['ther_df'][k] or previous['turb_df'][k]!=output['turb_df'][k]
                        condition = cond0 and cond1
                    if criteria=='chisq':
                        cond0  = absorber not in self.skipabs
                        if model==None:
                            cond1a = output['ther_chisq'][k] in ['nan','-'] or previous['ther_chisq'][k] in ['nan','-']
                            cond1b = (output['ther_chisq'][k] not in ['nan','-'] and previous['ther_chisq'][k] not in ['nan','-']) \
                                     and (abs(float(output['ther_chisq'][k])-float(previous['ther_chisq'][k]))>chisq)
                            cond2a = output['turb_chisq'][k] in ['nan','-'] or previous['turb_chisq'][k] in ['nan','-']
                            cond2b = (output['turb_chisq'][k] not in ['nan','-'] and previous['turb_chisq'][k] not in ['nan','-']) \
                                     and (abs(float(output['turb_chisq'][k])-float(previous['turb_chisq'][k]))>chisq)
                            condition = cond0 and (cond1a or cond1b or cond2a or cond2b)
                        if model=='ther':
                            cond1a = output['ther_chisq'][k] in ['nan','-'] or previous['ther_chisq'][k] in ['nan','-']
                            cond1b = (output['ther_chisq'][k] not in ['nan','-'] and previous['ther_chisq'][k] not in ['nan','-']) \
                                     and (abs(float(output['ther_chisq'][k])-float(previous['ther_chisq'][k]))>chisq)
                            condition = cond0 and (cond1a or cond1b)
                        if model=='turb':
                            cond2a = output['turb_chisq'][k] in ['nan','-'] or previous['turb_chisq'][k] in ['nan','-']
                            cond2b = (output['turb_chisq'][k] not in ['nan','-'] and previous['turb_chisq'][k] not in ['nan','-']) \
                                     and (abs(float(output['turb_chisq'][k])-float(previous['turb_chisq'][k]))>chisq)
                            condition = cond0 and (cond2a or cond2b)
                    if criteria=='error':
                        cond0  = absorber not in self.skipabs
                        cond1a = output['ther_error'][k] in ['nan','-'] or previous['ther_error'][k] in ['nan','-']
                        cond1b = (output['ther_error'][k] not in ['nan','-'] and previous['ther_error'][k] not in ['nan','-']) \
                                 and (abs(float(output['ther_error'][k])-float(previous['ther_error'][k]))>error)
                        cond2a = output['turb_error'][k] in ['nan','-'] or previous['turb_error'][k] in ['nan','-']
                        cond2b = (output['turb_error'][k] not in ['nan','-'] and previous['turb_error'][k] not in ['nan','-']) \
                                 and (abs(float(output['turb_error'][k])-float(previous['turb_error'][k]))>error)
                        condition = cond0 and (cond1a or cond1b or cond2a or cond2b)
                    if criteria=='chisq&ndf':
                        cond0  = absorber not in self.skipabs
                        cond1a = output['ther_chisq'][k] in ['nan','-'] or previous['ther_chisq'][k] in ['nan','-']
                        cond1b = (output['ther_chisq'][k] not in ['nan','-'] and previous['ther_chisq'][k] not in ['nan','-']) \
                                 and (abs(float(output['ther_chisq'][k])-float(previous['ther_chisq'][k]))>chisq)
                        cond1c = previous['ther_df'][k]!=output['ther_df'][k]
                        cond2a = output['turb_chisq'][k] in ['nan','-'] or previous['turb_chisq'][k] in ['nan','-']
                        cond2b = (output['turb_chisq'][k] not in ['nan','-'] and previous['turb_chisq'][k] not in ['nan','-']) \
                                 and (abs(float(output['turb_chisq'][k])-float(previous['turb_chisq'][k]))>chisq)
                        cond2c = previous['turb_df'][k]!=output['turb_df'][k]
                        condition = cond0 and ((cond1a or cond1b or cond1c) or (cond2a or (cond2b or cond2c)))
                    if condition==True:
                       self.skipabs = np.vstack((self.skipabs,absorber))
                       if '--show' in sys.argv:
                           print 'Jump at',distortion,'for',absorber
                       prev_ther = previous['ther_alpha'][k],previous['ther_error'][k],previous['ther_chisq'][k],previous['ther_df'][k]
                       prev_turb = previous['turb_alpha'][k],previous['turb_error'][k],previous['turb_chisq'][k],previous['turb_df'][k]
                       out_ther  = output['ther_alpha'][k],output['ther_error'][k],output['ther_chisq'][k],output['ther_df'][k]
                       out_turb  = output['turb_alpha'][k],output['turb_error'][k],output['turb_chisq'][k],output['turb_df'][k]
                       if '--show' in sys.argv:
                           print prev_ther,' | ',prev_turb,'\n',out_ther,' | ',out_turb
                previous = np.genfromtxt(self.rundir+self.instrument+'/'+self.selection+'/'+self.test+'/'+distortion+'/results.dat',names=True,dtype=object,comments='!')
            else:
                break

    if '--show' in sys.argv:
        print 'Total number of discarded systems:',len(self.skipabs)
            
#==================================================================================================================
