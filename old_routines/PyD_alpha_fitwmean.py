#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def fitwmean():

    folder = self.instrument
    folder = folder+'_jw'                  if '--whitmore'   in sys.argv else folder
    folder = folder+'_sim_'+self.deviation if '--simulation' in sys.argv else folder
    os.system('mkdir -p '+self.home+'/results/slopes/'+folder)
    os.chdir(self.home+'/results/slopes/'+folder)

    distres = self.distsim if '--simulation' in sys.argv else self.distres
    reflist = np.empty((0,2))
    systems = np.empty((0,3))
    outfile = open('slopes.dat','w')
    for i in range(len(distres)):
        slope  = distres['slope%02i'%(100*self.stdev)][i] if '--simulation' in sys.argv else distres['MoM slope'][i]
        error  = distres['error%02i'%(100*self.stdev)][i] if '--simulation' in sys.argv else distres['MoM error'][i]
        cond1  = slope!='-' and error!='-'
        cond2  = distres['instrument'][i].lower()==self.instrument
        cond3  = distres['system'][i] not in self.degensys+self.badwhit
        cond4a = '--whitmore'   in sys.argv     and distres['whitmore'][i]=='yes'
        cond4b = '--whitmore'   not in sys.argv and distres['whitmore'][i]=='no'
        if cond1 and cond2 and cond3 and (cond4a or cond4b):
            reflist = np.vstack((reflist,['%.4f'%slope,'%.4f'%error]))
            systems = np.vstack((systems,['%.4f'%slope,'%.4f'%error,distres['system'][i]]))
            outfile.write('{0:>9}{1:>9}\n'.format('%.4f'%slope,'%.4f'%error))
    outfile.close()
    
    open('command','w').write('slopes.dat\n')
    os.system('wmclipnr2 < command > wmclipnr.dat')

    i,flag = 0,0
    rejects = np.empty((0,2))
    results = np.loadtxt('wmclipnr.dat',dtype=str,delimiter='\n')
    vals = np.empty((0,4))
    while i<len(results):
        line = results[i].split()
        if 'Chi-squared, sample size, weighted mean, error:' in results[i]:
            flag  = 0
            chisq = float(line[6])
            size  = float(line[7])
            mean  = float(line[8])
            error = float(line[9])
            vals  = np.vstack((vals,[chisq,size,mean,error]))
            for j in range(len(reflist)):
                if len(np.where(np.logical_and(newlist[:,0]==reflist[j,0],newlist[:,1]==reflist[j,1]))[0])==0:
                    rejects = np.vstack((rejects,['%.4f'%float(reflist[j,0]),'%.4f'%float(reflist[j,1])]))
                    reflist = newlist
                    break
        if flag==1:
            newlist = np.vstack((newlist,['%.4f'%(float(line[0])),'%.4f'%(float(line[1]))]))
        if 'SAMPLE AFTER CLIPPING:' in results[i]:
            flag = 1
            newlist = np.empty((0,2))
        i += 1

    for i in range(len(rejects)):
        j = np.where(np.logical_and(systems[:,0]==rejects[i,0],systems[:,1]==rejects[i,1]))[0][0]
        print systems[j,2],rejects[i,0],rejects[i,1]

    os.system('wmclipnr < command > wmclipnr.dat')
    os.system('wmean    < command > wmean.dat')
    os.system('rm command')
        
    fig = figure(figsize=(12,10))
    plt.subplots_adjust(left=0.1, right=0.9, bottom=0.05, top=0.95, hspace=0, wspace=0)
    ax = plt.subplot(2,1,1,xlim=[min(vals[:,1]),max(vals[:,1])],ylim=[100,600])
    ax.scatter(vals[:,1],vals[:,0],alpha=0.6,color='black',s=20)
    ax.yaxis.set_major_locator(plt.FixedLocator([200,300,400,500]))
    setp(ax.get_xticklabels(), visible=False)
    plt.gca().invert_xaxis()
    grid()
    ylabel('Chi-squared',fontsize=10)
    ax = plt.subplot(2,1,2,xlim=[min(vals[:,1]),max(vals[:,1])],ylim=[-0.18,-0.02])
    ax.errorbar(vals[:,1],vals[:,2],yerr=vals[:,3],fmt='o',ms=8,markeredgecolor='none',alpha=0.6,color='red',elinewidth=3)
    ax.yaxis.set_major_locator(plt.FixedLocator([-0.16,-0.14,-0.12,-0.10,-0.08,-0.06,-0.04]))
    plt.gca().invert_xaxis()
    grid()
    ylabel('Weighted mean',fontsize=10)
    xlabel('Sample size',fontsize=10)
    savefig('wmclipnr.pdf')
    clf()

#==================================================================================================================
