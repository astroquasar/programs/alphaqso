#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def rcode():

    if 'hires' in fullpath:
        
        self.Rpath = self.Rrep+self.instrument+'_'+self.arm+'/'+self.distortion
        fullpath   = self.Rpath
        
        if os.path.exists(fullpath)==False:
            os.system('mkdir -p '+fullpath)
            
        save = open(fullpath+'/alpha.csv','w')
        save.write('dalpha,err_tot,phi_rad,theta_rad\n')
    
        values = np.loadtxt(self.Cpath+'/comb/results.dat')
        for i in range (len(values)):
            alpha     = str(values[i,3])
            error     = str(values[i,4])
            phi_rad   = values[i,1]*360/24*np.pi/180.
            theta_rad = np.pi/2-values[i,2]*np.pi/180.
            save.write(alpha+','+error+','+str(phi_rad)+','+str(theta_rad))
            save.write('\n')
    
    if 'uves' in fullpath:
        
        self.Rpath = self.Rrep+self.instrument+'_'+self.arm+'/'+self.distortion
        fullpath   = self.Rpath
        
        if os.path.exists(fullpath)==False:
            os.system('mkdir -p '+fullpath)
            
        save = open(fullpath+'/alpha.csv','w')
        save.write('dalpha,err_tot,phi_rad,theta_rad\n')
        values = np.loadtxt(self.Cpath+'/alphafit_errmod.dat')
        
        for i in range (len(values)):
            alpha     = str(values[i,3])
            error     = str(values[i,4])
            phi_rad   = values[i,1]*360/24*np.pi/180.
            theta_rad = np.pi/2-values[i,2]*np.pi/180.
            save.write(alpha+','+error+','+str(phi_rad)+','+str(theta_rad))
            save.write('\n')
    
#        for i in range (len(self.restable)):
#
#            zem,ra,dec,dist = calc_dist2dip(self.restable[i,1])
#            phi_rad         = ra*np.pi/180.
#            theta_rad       = np.pi/2-dec*np.pi/180.
#            if self.restable[i,10]=='0.000' and self.restable[i,7] not in ['0.000','-']:
#                save.write(self.restable[i,7]+','+self.restable[i,8]+','+str(phi_rad)+','+str(theta_rad))
#                save.write('\n')
#            if self.restable[i,10] not in ['0.000','-'] and self.restable[i,7]=='0.000':
#                save.write(self.restable[i,10]+','+self.restable[i,11]+','+str(phi_rad)+','+str(theta_rad))
#                save.write('\n')
#            if self.restable[i,10] not in ['0.000','-'] and self.restable[i,7] not in ['0.000','-']:
#                save.write(self.restable[i,12]+','+self.restable[i,13]+','+str(phi_rad)+','+str(theta_rad))
#                save.write('\n')
            
    save.close()

    os.chdir(fullpath)
    os.system('cp '+os.getenv('CODE')+'/R/alpha/alpha_analysis_v01.R .')
    os.system('cp '+os.getenv('CODE')+'/R/alpha/alpha_analysis_jags_v01.R .')
    os.system('R < alpha_analysis_v01.R --no-save > termout.dat')
    os.system('rm alpha_analysis_v01.R')
    os.system('rm alpha_analysis_jags_v01.R')
    os.chdir(self.here)
    
#==================================================================================================================

def rcode_plot():

    fig = figure(figsize=(8,11))
    plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95, hspace=0.2, wspace=0)
    
    ax1 = plt.subplot(311,xlim=[-.05,1.4])
    x,y,dy = rcode_getval('uves','slope')
    ax1.errorbar(x, y, yerr=dy,fmt='o',ms=6,markeredgecolor='none',label='UVES')
    ax1.axhline(y=0,ls='dotted',color='black')
    lg = legend(prop={'size':8},loc='upper left',numpoints=1,handlelength=2)
    fr = lg.get_frame().set_alpha(0)
    xlabel('Applied Distortion Slope')
    ylabel('Dipole Amplitude')
    
    ax2 = plt.subplot(312)
    x,y,dy = rcode_getval('hires','slope')
    ax2.errorbar(x, y, yerr=dy,fmt='ro',ms=6,markeredgecolor='none',label='HIRES')
    ax2.axhline(y=0,ls='dotted',color='black')
    lg = legend(prop={'size':8},loc='upper left',numpoints=1,handlelength=2)
    fr = lg.get_frame().set_alpha(0)
    xlabel('Applied Distortion Slope')
    ylabel('Dipole Amplitude')
    
    ax3 = subplot(313,xlim=[-20,370])
    x,y,dy = rcode_getval('hires','amplitude')
    ax3.errorbar(x, y, yerr=dy,fmt='ro',ms=6,markeredgecolor='none',label='HIRES')
    ax3.axhline(y=0,ls='dotted',color='black')
    lg = legend(prop={'size':8},loc='upper left',numpoints=1,handlelength=2)
    fr = lg.get_frame().set_alpha(0)
    xlabel('Applied Distortion Amplitude')
    ylabel('Dipole Amplitude')

    savefig('alpha_R.pdf')
    clf()
    
#==================================================================================================================

def rcode_getval(inst,shape):

    print '\n',inst,'|',shape,'\n'
    distlist = self.distlist if inst=='uves' else self.hires_amp if shape=='amplitude' else self.distlist
    x,y,dy = [],[],[]
    for i in distlist:
        strslope = '0.000' if round(i,3)==0 else str('%.3f'%i).replace('-','m') if '-' in str(i) else 'p'+str('%.3f'%i)
        output = np.loadtxt(self.Rrep+inst+'/'+strslope+'/termout.dat',dtype='str',delimiter='\n')
        for k in range (len(output)):
            if 'Empirical mean and standard deviation for each variable' in output[k]:
                print round(i,2),float(output[k+3].split()[1]),float(output[k+3].split()[2])
                x.append(i)
                y.append(float(output[k+3].split()[1]))
                dy.append(float(output[k+3].split()[2]))

    if len(dy)==0:
        return x,y,[0 for i in range (len(x))]
    else:
        return x,y,dy

#==================================================================================================================
