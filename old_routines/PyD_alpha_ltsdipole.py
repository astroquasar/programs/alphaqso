#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def ltsdipole(multdata,n,npar,nvar,parms,parmfix,scale,maxdata,ltsnumtry):
        
    npbuf    = 3
    gentries = 1000000
    facscale = 0.1
    multdata_eval=multdata;

#    pastdraws=malloc((gentries+4)*sizeof(long));
#    x=dvector(0,n+3);
#    y=dvector(0,n+3);
#    yerr=dvector(0,n+3);
#    yerr_save=dvector(0,n+3);
#    res_sq=dvector(0,n+3);
#    sweights=dvector(0,n+3);
#    initparms=dvector(0,npar);
#    bestparms=dvector(0,npar);
#    bestres=dvector(0,n+3);
#    bestsres=dvector(0,n+3);
#    randarray=dvector(0,n+3);
#  
#    funceval=dvector(0,npar);
#    for(i=1;i<=npar;i++){
#      funceval[i]=0;
#    }
#    
#    index=malloc((n+3)*sizeof(unsigned long));
#    covar=dmatrix(1, npar, 1, npar);
#  
#    for(i=1;i<=n;i++){ # Fill the dummy x array with lookup values
#      x[i]=i;
#      y[i]=multdata->y[i];
#      yerr[i]=multdata->err[i];
#    }    
#  
#    smallesterr=1e30; # Find the smallest error value ;
#    for(i=1;i<=n;i++){
#      if(yerr[i]<smallesterr){
#        smallesterr=yerr[i];
#      }
#    }
#    mfit=0;
#    for(i=1;i<=npar;i++){ # Work out how many things to fit
#      if(parmfix[i]) mfit++;
#      initparms[i]=parms[i];
#    }
#  
#    /* Calculate the LTS size based on the supplied trimming fraction */
#    #lts_k=(n+mfit+1)/2;
#    lts_k=0.85*n;  
#    if(ltsnminusone != 0){
#      lts_k = n-1;
#    }
#    #lts_k=n;
#    #lts_k=n-1;
#    ltsfrac = ((double)lts_k)/((double)n);
#    /* Work out number of standard deviations away from zero such that we have the 
#       appropriate trimming fraction */
#    if(lts_k<n){
#      ltsnorminterval = probit((1.0+ltsfrac)/2.0);
#      #printf("...which corresponds to  +/- %lf standard deviations on normal distribution\n", ltsnorminterval);
#      /* Work out the expected value of chisq_nu for this interval.
#         i.e. calculate int(phi(x)*x^2,x=-ltsnorminterval..ltsnorminterval) */
#      dtemp=ltsnorminterval;
#      targetscale = 1/sqrt(2*math.pi)*(sqrt(2*math.pi)*erf(dtemp/sqrt(2)) - 2*dtemp*exp(-dtemp*dtemp/2));
#    } else { # Can't calculate for n or the series will never converge
#      targetscale=1.0;
#    }
#    
#    
#    #printf("\n");
#    printf("----Starting LTS estimator----\n"); 
#    #printf("Fitting with %d parameters free\n",mfit);
#    #printf("lts_k = %ld\n", lts_k);
#    printf("LTS trimming fraction = %lf\n",ltsfrac);
#    #printf("...which corresponds to  +/- %lf standard deviations on normal distribution\n", ltsnorminterval);
#    #printf("   (probit target = %lf)\n", (1.0+ltsfrac)/2.0);
#    printf("Targeting chisq_nu(LTS) = %lf\n", targetscale);
#    /* First, compute the initial scale estimate based on the 
#       initial parameters */
#    df=n-mfit;
#    best_s=1e30; # Make sure that LTS always runs
#    s=1e30;
#  
#    /* Calculate scale correction factor */
#    /* Calculate LTS estimator, by increasing error bars */
#    if(s>targetscale){
#      breakloop=0;
#      facctr=-1;
#      do{
#        for(i=1;i<=npar;i++){ # Restore parameters
#  	parms[i]=initparms[i];
#        }
#        facctr++;
#        fac=((double)facctr)*facscale*smallesterr; # Modify error arrays 
#        for(i=1;i<=n;i++){ 
#  	dtemp=multdata->err[i];
#  	yerr[i]=sqrt(dtemp*dtemp + fac*fac);
#        }
#        for(p=1;p<=ltsnumtry;p++){
#  	/* Generate lots of LTS points and apply concentration steps to each until convergence */
#  	ii=0;
#  	i=0;
#  	for(i=1;i<=n;i++){
#  	  randarray[i]=genrand_real2();
#  	}
#  	dindexx(n, randarray, index); 
#  	for(i=1;i<=npar+npbuf;i++){
#  	  for(k=1;k<=nvar;k++){ # Extract random point
#  	    randdraw->x[i][k]=multdata->x[index[i]][k];
#  	    randdraw->x_rad[i][k]=multdata->x_rad[index[i]][k];
#  	  }
#  	  randdraw->y[i]=multdata->y[index[i]];
#  	  randdraw->t[i]=multdata->t[index[i]];
#  	  dtemp=multdata->err[index[i]];
#  	  randdraw->err[i]=sqrt(dtemp*dtemp + fac*fac);
#  	}
#  	randdraw->n=npar+npbuf;
#  	randdraw->nvar=nvar;
#  	/* Do fit to the (npar+npbuf) numbers we have */
#  	for(i=1;i<=npar+npbuf;i++){ # 
#  	  y[i]=randdraw->y[i];
#  	  yerr[i]=randdraw->err[i];
#  	}    
#  	
#  	/* Get an initial linear fit that should be relatively free of outliers */
#  	multdata_eval=randdraw;
#  	genlinfit(x, y, yerr, npar+npbuf, parms, parmfix, npar, covar, &chi2, funcs);
#  	multdata_eval=multdata;
#  	for(i=1;i<=n;i++){
#  	  (*funcs)(x[i],funceval,npar); # Evaluate function
#  	  dtemp=0;
#  	  for(j=1;j<=npar;j++){
#  	    dtemp=dtemp +parms[j]*funceval[j];
#  	  }
#  	  dtempa=multdata->err[i];
#  	  dtemp=(multdata->y[i]-dtemp)/sqrt(fac*fac + dtempa*dtempa);
#  	  multdata->res[i]=dtemp; # Store residuals
#  	  res_sq[i] = dtemp*dtemp; # Store squared residuals for sorting
#  	}
#  	dindexx(n, res_sq, index); # Sort on residuals and store
#  	lts_sort->n=n;
#  	lts_sort->nvar=nvar;
#  	for(i=1;i<=n;i++){
#  	  lts_sort->y[i]=multdata->y[index[i]];
#  	  lts_sort->err[i]=multdata->err[index[i]];
#  	  lts_sort->res[i]=multdata->res[index[i]];
#  	  lts_sort->t[i]=multdata->t[index[i]];
#  	  for(j=1;j<=nvar;j++){
#  	    lts_sort->x[i][j]=multdata->x[index[i]][j];
#  	    lts_sort->x_rad[i][j]=multdata->x_rad[index[i]][j];
#  	  }
#  	}
#  	s=0;
#  	for(i=1;i<=lts_k;i++){
#  	  dtemp=lts_sort->res[i];
#  	  s = s + dtemp*dtemp;
#  	}
#  	s=s/df;
#  	s_last=s;
#  	m=0;
#  	breakloop2=0;
#  	
#  	do{ /* Now iterate C-steps */
#  	  m++;
#  	  /* Do least squares fit to trimmed set */
#  	  for(i=1;i<=n;i++){ # Fill the dummy x array with all lookup values
#  	    y[i]=lts_sort->y[i];
#  	    dtemp=lts_sort->err[i];
#  	    yerr[i]=sqrt(dtemp*dtemp + fac*fac);
#  	  }    
#  	  multdata_eval=lts_sort;
#  	  genlinfit(x, y, yerr, lts_k, parms, parmfix, npar, covar, &chi2, funcs);
#  	  multdata_eval=multdata;
#  	  for(i=1;i<=n;i++){
#  	    (*funcs)(x[i],funceval,npar); # Evaluate function
#  	    dtemp=0;
#  	    for(j=1;j<=npar;j++){
#  	      dtemp=dtemp +parms[j]*funceval[j];
#  	    }
#  	    dtempa=multdata->err[i];
#  	    dtemp=(multdata->y[i]-dtemp)/sqrt(fac*fac + dtempa*dtempa);
#  	    multdata->res[i]=dtemp; # Store residuals
#  	    res_sq[i] = dtemp*dtemp; # Store squared residuals for sorting
#  	  }
#  	  dindexx(n, res_sq, index); # Sort on residuals and store
#  	  for(i=1;i<=n;i++){
#  	    lts_sort->y[i]=multdata->y[index[i]];
#  	    lts_sort->err[i]=multdata->err[index[i]];
#  	    lts_sort->res[i]=multdata->res[index[i]];
#  	    lts_sort->t[i]=multdata->t[index[i]];
#  	    for(j=1;j<=nvar;j++){
#  	      lts_sort->x[i][j]=multdata->x[index[i]][j];
#  	      lts_sort->x_rad[i][j]=multdata->x_rad[index[i]][j];
#  	    }
#  	  }
#  	  s=0;
#  	  for(i=1;i<=lts_k;i++){
#  	    dtemp=lts_sort->res[i];
#  	    s = s + dtemp*dtemp;
#  	    #printf("res_sq[%d] = %lf\n", i, dtemp*dtemp);
#  	  }
#  	  s=s/df;
#  	  if(s_last==s){ # Convergence reached
#  	    breakloop2=1;
#  	  } else {# Otherwise go for another C-step
#  	    s_last = s;
#  	  }
#  	}while(breakloop2==0);
#  	/* Check if this iteration was better than the last */
#  	
#  	if(s<best_s){
#  	  for(i=1;i<=npar;i++){
#  	    bestparms[i]=parms[i];
#  	  }
#  	  for(i=1;i<=n;i++){
#  	    bestres[i]=multdata->res[i];
#  	  }
#  	  if(p!=1){
#  	    #printf("...on iteration %ld found a better s (=%lf)\n",p,s);
#  	  }
#  	  best_s=s;
#  	}
#        }
#        s=best_s;
#        for(i=1;i<=npar;i++){
#  	parms[i]=bestparms[i];
#        }
#        fflush(stdout);
#        if(m>1){
#  	printf("fac = %lf     -->   s = %lf\n", fac, s);
#        }
#        if(s<=targetscale || noerrorincrease==1){ # Convergence reached with some fac... terminate
#  	breakloop=1;
#        } # Else increase fac and try again
#      }while(breakloop==0);
#      #printf("\n\n");
#      #printf("   S   = %lf (targeting %lf\n", s, targetscale);
#      #printf("   fac = %lf \n",fac);
#      *scale=fac;
#      for(i=1;i<=npar;i++){
#        parms[i]=bestparms[i];
#      }
#      for(i=1;i<=n;i++){
#        multdata->res[i]=bestres[i];
#        #printf("res[%d] = %lf\n", i, multdata->res[i]);
#      }
#      
#    } else {
#      #printf("S is already lower than the target, so don't need to do anything\n");
#    }
  
    return best_s;

#==================================================================================================================
