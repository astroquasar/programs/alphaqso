#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def compking():

    version = 10          # 10 to use Murphy's systems refitted by JK ; 8 to use original Murphy's values
    
    xdip = 17.3*360/24
    ydip = -61

    xuves_ini,yuves_ini,xhires_ini,yhires_ini = [],[],[],[]
    xuves_end,yuves_end,xhires_end,yhires_end = [],[],[],[]

    for i in range (len(self.publist)):

        inst = self.publist['path'][i].split('_')[0]
        qso  = self.publist['path'][i].split('/')[1]
        idx  = np.where(self.qsolist['name']==qso)[0][0]
        val  = self.qsolist['ra'][idx].split(':')
        ra   = (360/24)*(float(val[0])+float(val[1])/60+float(val[2])/3600)
        val  = self.qsolist['dec'][idx].split(':')
        if (val[0][0]=='-'):
            dec = float(val[0])-float(val[1])/60-float(val[2])/3600
        else:
            dec = float(val[0])+float(val[1])/60+float(val[2])/3600
        if inst=='UVES':
            xuves_ini.append(calc_spheredist(ra,dec,xdip,ydip))
            yuves_ini.append(self.publist['alpha'][i])
            xuves_end.append(calc_spheredist(ra,dec,xdip,ydip))
            yuves_end.append(self.publist['alpha_2003'][i])
        if inst=='HIRES':
            xhires_ini.append(calc_spheredist(ra,dec,xdip,ydip))
            yhires_ini.append(self.publist['alpha'][i])
            xhires_end.append(calc_spheredist(ra,dec,xdip,ydip))
            yhires_end.append(self.publist['alpha_2003'][i])
            
    figure(figsize=(10,5))
    plt.subplots_adjust(left=0.05, right=0.95, bottom=0.1, top=0.95, hspace=0, wspace=0)
    ax = subplot(111,xlim=[0,180],ylim=[-5,5])
    scatter(xuves_ini[:],yuves_ini[:],marker='o',c='blue',s=10,edgecolors='none')
    scatter(xhires_ini[:],yhires_ini[:],marker='s',c='red',s=10,edgecolors='none')
    xlabel(r'$\Theta$, angle from dipole (17.3,-61)')
    ylabel(r'$\Delta\alpha/\alpha$ $(10^{-5})$')
    axhline(y=5,color='black',ls='dotted',lw=.2)
    axhline(y=0,color='black',lw=.2)
    axhline(y=-5,color='black',ls='dotted',lw=.2)
    savefig('alpha_ini.pdf')

    figure(figsize=(10,5))
    plt.subplots_adjust(left=0.05, right=0.95, bottom=0.1, top=0.95, hspace=0, wspace=0)
    ax = subplot(111,xlim=[0,180],ylim=[-5,5])
    scatter(xuves_end[:],yuves_end[:],marker='o',c='blue',s=10,edgecolors='none')
    scatter(xhires_end[:],yhires_end[:],marker='s',c='red',s=10,edgecolors='none')
    xlabel(r'$\Theta$, angle from dipole (17.3,-61)')
    ylabel(r'$\Delta\alpha/\alpha$ $(10^{-5})$')
    axhline(y=5,color='black',ls='dotted',lw=.2)
    axhline(y=0,color='black',lw=.2)
    axhline(y=-5,color='black',ls='dotted',lw=.2)
    savefig('alpha_cor.pdf')
    
#==================================================================================================================
