import alpha,sys,os,numpy,datetime
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.ticker import MultipleLocator

class slopes_real(object):
    '''
    This operation plots the distribution of slopes value from real systems
    against several parameters. 

    **Operations**

    --chisq        Distortion slope vs. minimum absolute chi-square
    --chisq2       Histogram of distortion slope results
    --redshift     Distribution of distortion slope with absorption redshift
                   of the system.

    **Required argument**

    --instrument  High-resolution spectrograph used [uves,hires]

    **Optional arguments**

    --show        Print out the results for every slope in the terminal
    --whitmore    Look at results from simplistic distortion model

    Examples
    --------
    >>> alpha slopes_real --instrument uves --show --whitmore
    '''
    
    def __init__(self):

        if '--chisq'    in sys.argv: self.plot_slopes_chisq()
        if '--chisq2'   in sys.argv: self.plot_slopes_chisq2()
        if '--redshift' in sys.argv: self.plot_slopes_redshift()

    def get_slope_results(self,instrument,whitmore,tbs):
        '''
        Loop through the tabulated distortion results from CoDDAM spreadsheet
        and extract the slope values from all the fort.13 matching both the input
        instrument and the requested distortion model.

        Parameters
        ----------
        instrument : str
          Spectrograph to retrieve data from
        whitmore : bool
          Flag if results from the whitmore model is requested
        tbs : bool
          Flag to save figure
        '''
        qsos    = []
        results = numpy.empty((0,9))    
        data    = numpy.empty((0,18))
        for i in range(len(alpha.distres)):
            qso    = alpha.distres['system'][i].split('/')[0]
            zabs   = alpha.distres['system'][i].split('/')[1]
            model  = 'model-%.1f'%float(alpha.distres['model individual'][i])
            cond0  = alpha.distres['system'][i] not in alpha.degensys
            cond1  = alpha.distres['MoM slope'][i] not in ['-','degenerate','badwhit']# and abs(float(alpha.distres['MoM slope'][i]))<5
            cond2  = alpha.distres['instrument'][i].lower()==instrument
            cond3a = alpha.distres['whitmore'][i]=='yes' and whitmore and \
                     alpha.distres['system'][i] not in alpha.badwhit
            cond3b = alpha.distres['whitmore'][i]=='no'  and whitmore==False
            if cond0 and cond1 and cond2 and (cond3a or cond3b):
                iqso = numpy.where(numpy.logical_and(alpha.distres['name']==qso,
                                                     alpha.distres['Thermal slope']!='-'))[0]
                flag = 0
                disp = []
                cent = []
                fort = numpy.loadtxt(alpha.fitdir+alpha.distres['system'][i]+'/'+model+'/model/thermal.13',
                                     dtype=str,delimiter='\n')
                for line in fort:
                    if '*' in line:
                        flag += 1
                    elif flag==1:
                        wmin = float(line.split()[2])
                        wmax = float(line.split()[3])
                        cent.append((wmin+wmax)/2.)
                        disp.append(2*(wmax-wmin)/(wmin+wmax)*alpha.c)
                # Estimating average observing date among the exposures
                if '--whitmore' in sys.argv:
                    middate = float('nan')
                else:                
                    date  = []
                    idx   = numpy.where(alpha.uvesexp['name']==qso)[0]
                    for j in idx:
                        for k in range(len(alpha.uvesset)):
                            cond1 = alpha.uvesset['cent'][k]==alpha.uvesexp['cent'][j]
                            cond2 = alpha.uvesset['arm'][k]==alpha.uvesexp['arm'][j]
                            cond3 = alpha.uvesset['mode'][k]==alpha.uvesexp['mode'][j]
                            if cond1==cond2==cond3==True:
                                wmin = 10*float(alpha.uvesset['TS_min'][k])
                                wmax = 10*float(alpha.uvesset['TS_max'][k])
                                break
                        for wmid in cent:
                            if wmin < wmid < wmax:
                                obsdate = alpha.uvesexp['dataset'][j].replace('UVES.','').replace('T',' ')
                                date.append(datetime.datetime.strptime(obsdate,'%Y-%m-%d %H:%M:%S.%f'))
                                break                    
                    deltas  = [(date[k]-date[0]).days for k in range(len(date))]
                    middate = (date[0]+datetime.timedelta(days=sum(deltas)/len(deltas))).date()
                therchisq  = float(alpha.distres['ther chisq individual'][i])
                therslope  = float(alpha.distres['Thermal slope'][i])
                thererror  = float(alpha.distres['Thermal error'][i])
                turbchisq  = float(alpha.distres['turb chisq individual'][i])
                turbslope  = float(alpha.distres['Turbulent slope'][i])
                turberror  = float(alpha.distres['Turbulent error'][i])
                momchisq   = float(alpha.distres['MoM chisq individual'][i])
                momslope   = float(alpha.distres['MoM slope'][i])
                momerror   = float(alpha.distres['MoM error'][i])
                ntrans     = float(alpha.distres['ntrans'][i])
                ntied      = float(alpha.distres['tied'][i])
                vdisp      = numpy.average(disp)
                z,ra,dec,d = alpha.dist2dip(qso)
                data       = numpy.vstack((data,[middate,d,float(zabs),vdisp,ra,dec,therchisq,
                                                 therslope,thererror,turbchisq,turbslope,turberror,
                                                 momchisq,momslope,momerror,ntrans,ntied,i]))
                results    = numpy.vstack((results,[therslope,therslope/thererror**2,1/thererror**2,
                                                 turbslope,turbslope/turberror**2,1/turberror**2,
                                                 momslope ,momslope/momerror**2  ,1/momerror**2]))
        return data

    def plot_slopes_chisq(self,instrument=alpha.instrument,whitmore=alpha.whitmore,tbs=alpha.tbs):
        '''
        Distortion slope vs. minimum absolute chi-square
        '''
        data = self.get_slope_results(instrument,whitmore,tbs)
        fig = plt.figure(figsize=(12,6),frameon=False)
        plt.subplots_adjust(left=0.06, right=0.97, bottom=0.07, top=0.92, hspace=0.05, wspace=0.05)
        for i in [0,1,2]:
            xmin,xmax = -5,5
            title = 'Thermal' if i==0 else 'Turbulent' if i==1 else 'Method-of-Moment'
            ax = plt.subplot(1,3,i+1,xlim=[xmin,xmax],ylim=[0,3500])
            ax.errorbar(data[:,7+3*i],data[:,6+3*i],fmt='o',ms=5,markeredgecolor='none',
                        ecolor='grey',alpha=0.4,color='black')
            ax.axvline(x=0,ls='dotted',color='black')
            plt.xlabel('Best distortion slope',fontsize=10)
            if i!=0: plt.setp(ax.get_yticklabels(), visible=False)
            if i==0: plt.ylabel('Absolute chi-square',fontsize=10)
            divider = make_axes_locatable(ax)
            ax2 = divider.append_axes("top",1.2,xlim=[xmin,xmax],ylim=[0,30])
            ax2.hist(data[:,7+3*i],bins=200,histtype='stepfilled',alpha=0.4,lw=0.2)
            plt.setp(ax2.get_xticklabels(), visible=False)
            ax2.yaxis.set_major_locator(plt.FixedLocator(numpy.arange(0,31,10)))
            ax2.set_title(title,color='red',fontsize=10)
            if i!=0: plt.setp(ax2.get_yticklabels(), visible=False)       
        plt.savefig('slopes_chisq.pdf') if tbs else plt.show()
        plt.close(fig)

    def plot_slopes_hist(self,instrument=alpha.instrument,whitmore=alpha.whitmore,tbs=alpha.tbs):
        '''
        Histogram of distortion slope results
        '''
        data = self.get_slope_results(instrument,whitmore,tbs)
        fig = plt.figure(figsize=(10,8),frameon=False)
        plt.subplots_adjust(left=0.06, right=0.97, bottom=0.07, top=0.97, hspace=0.1, wspace=0)
        for i in [0,1,2]:
            title = 'Thermal' if i==0 else 'Turbulent' if i==1 else 'Method-of-Moment'
            ax = plt.subplot(3,1,i+1,xlim=[-10,10],ylim=[0,17.5])
            hist = numpy.array(data[:,7+3*i],dtype=float)
            ax.hist(hist,bins=200,histtype='stepfilled',alpha=0.4,lw=0.2,range=[-10,10])
            ax.text(6.25,13.75,title,color='black',ha='center')
            ax.axvline(-0.123,color='black',ls='dotted',lw=2,alpha=0.5)
            ax.xaxis.set_major_locator(MultipleLocator(2))
            ax.yaxis.set_major_locator(MultipleLocator(5))
            if i!=2: plt.setp(ax.get_xticklabels(), visible=False)
            if i==2: plt.xlabel('Best distortion slope (m/s/$\mathrm{\AA}$)',fontsize=10)
        plt.savefig('slopes_chisq2.pdf') if tbs else plt.show()
        plt.close(fig)
        
    def plot_slopes_redshift(self):
        '''
        Distribution of distortion slope with absorption redshift of the system.
        '''
        print '\n\nSorting by redshift\n'
        bindata = self.slopebin(data,2)
        fig = plt.figure(figsize=(8,10),frameon=False)
        plt.subplots_adjust(left=0.1, right=0.95, bottom=0.05, top=0.95, hspace=0.05, wspace=0.05)
        for i in [0,1,2]:
            title = 'Thermal' if i==0 else 'Turbulent' if i==1 else 'Method-of-Moment'
            ax = plt.subplot(3,1,i+1,ylim=[-1,1])
            ax.errorbar(data[:,2],data[:,7+3*i],fmt='o',ms=5,markeredgecolor='none',ecolor='grey',alpha=0.4,color='black')
            ax.errorbar(bindata[:,2],bindata[:,7+3*i],fmt='o',ms=10,markeredgecolor='none',alpha=0.6,color='blue',elinewidth=3)
            t = text(3.4,0.8,title,color='red',weight='bold',fontsize=10,ha='right')
            t.set_bbox(dict(color='white', alpha=0.5, edgecolor=None))
            ax.axhline(y=0,ls='dotted',color='black')
            ax.axhline(y=-0.04,ls='dashed',color='red',lw=1)
            if i==2: xlabel('Average absorption redshift',fontsize=10)
            ylabel('Best distortion slope',fontsize=10)
            t = text(3.4,0.8,title,color='red',weight='bold',fontsize=10,ha='right')
            t.set_bbox(dict(color='white', alpha=0.5, edgecolor=None))
            ax.yaxis.set_major_locator(plt.FixedLocator(numpy.arange(-0.8,0.9,0.2)))
            if i!=2: plt.setp(ax.get_xticklabels(), visible=False)
        savefig('slopes_redshift.pdf')
        plt.close(fig)
    
        print '\n\nSorting by distance to best fitting model\n'
    
        bindata = self.slopebin(data,1)
        fig = figure(figsize=(8,10),frameon=False)
        plt.subplots_adjust(left=0.1, right=0.95, bottom=0.05, top=0.95, hspace=0.05, wspace=0.05)
        for i in [0,1,2]:
            title = 'Thermal' if i==0 else 'Turbulent' if i==1 else 'Method-of-Moment'
            ax = plt.subplot(3,1,i+1,xlim=[0,180],ylim=[-1,1])
            ax.errorbar(data[:,1],data[:,7+3*i],fmt='o',ms=5,markeredgecolor='none',ecolor='grey',alpha=0.4,color='black')
            ax.errorbar(bindata[:,1],bindata[:,7+3*i],fmt='o',ms=10,markeredgecolor='none',alpha=0.6,color='blue',elinewidth=3)
            ax.axhline(y=0,ls='dotted',color='black')
            if i==2: xlabel(r'$\Theta$, angle from dipole ('+str(alpha.alphara)+','+str(alpha.alphadec)+')',fontsize=10)
            ylabel('Best distortion slope',fontsize=10)
            t = text(170,0.8,title,color='red',weight='bold',fontsize=10,ha='right')
            t.set_bbox(dict(color='white', alpha=0.5, edgecolor=None))
            ax.yaxis.set_major_locator(plt.FixedLocator(numpy.arange(-0.8,0.9,0.2)))
            if i!=2: plt.setp(ax.get_xticklabels(), visible=False)
        savefig('slopes_distance.pdf')
        clf()
    
        print '\n\nSorting by RA\n'
    
        bindata = self.slopebin(data,4)
        fig = plt.figure(figsize=(8,10),frameon=False)
        plt.subplots_adjust(left=0.1, right=0.95, bottom=0.05, top=0.95, hspace=0.05, wspace=0.05)
        for i in [0,1,2]:
            title = 'Thermal' if i==0 else 'Turbulent' if i==1 else 'Method-of-Moment'
            ax = plt.subplot(3,1,i+1,xlim=[0,24],ylim=[-1,1])
            ax.errorbar(data[:,4],data[:,7+3*i],fmt='o',ms=5,markeredgecolor='none',ecolor='grey',alpha=0.4,color='black')
            ax.errorbar(bindata[:,4],bindata[:,7+3*i],fmt='o',ms=10,markeredgecolor='none',alpha=0.6,color='blue',elinewidth=3)
            ax.axhline(y=0,ls='dotted',color='black')
            if i==2: xlabel('Average Right Ascension',fontsize=10)
            ylabel('Best distortion slope',fontsize=10)
            t = text(23,0.8,title,color='red',weight='bold',fontsize=10,ha='right')
            t.set_bbox(dict(color='white', alpha=0.5, edgecolor=None))
            ax.yaxis.set_major_locator(plt.FixedLocator(numpy.arange(-0.8,0.9,0.2)))
            if i!=2: plt.setp(ax.get_xticklabels(), visible=False)
        savefig('slopes_ra.pdf')
        clf()
    
        print '\n\nSorting by DEC\n'
    
        bindata = self.slopebin(data,5)
        fig = plt.figure(figsize=(8,10),frameon=False)
        plt.subplots_adjust(left=0.1, right=0.95, bottom=0.05, top=0.95, hspace=0.05, wspace=0.05)
        for i in [0,1,2]:
            title = 'Thermal' if i==0 else 'Turbulent' if i==1 else 'Method-of-Moment'
            ax = plt.subplot(3,1,i+1,xlim=[-90,90],ylim=[-1,1])
            ax.errorbar(data[:,5],data[:,7+3*i],fmt='o',ms=5,markeredgecolor='none',ecolor='grey',alpha=0.4,color='black')
            ax.errorbar(bindata[:,5],bindata[:,7+3*i],fmt='o',ms=10,markeredgecolor='none',alpha=0.6,color='blue',elinewidth=3)
            ax.axhline(y=0,ls='dotted',color='black')
            if i==2: xlabel('Average Declinaison',fontsize=10)
            ylabel('Best distortion slope',fontsize=10)
            t = text(85,0.8,title,color='red',weight='bold',fontsize=10,ha='right')
            t.set_bbox(dict(color='white', alpha=0.5, edgecolor=None))
            ax.yaxis.set_major_locator(plt.FixedLocator(numpy.arange(-0.8,0.9,0.2)))
            if i!=2: plt.setp(ax.get_xticklabels(), visible=False)
        savefig('slopes_dec.pdf')
        clf()
    
        print '\n\nSorting by dispersion\n'
    
        bindata = self.slopebin(data,4)
        fig = plt.figure(figsize=(12,6),frameon=False)
        plt.subplots_adjust(left=0.06, right=0.95, bottom=0.07, top=0.95, hspace=0.25, wspace=0.25)
        for i in [0,1,2]:
            title = 'Thermal' if i==0 else 'Turbulent' if i==1 else 'Method-of-Moment'
            ax = plt.subplot(1,3,i+1,xlim=[-5,5],ylim=[0,450])
            ax.errorbar(data[:,7+3*i],data[:,3],fmt='o',ms=5,markeredgecolor='none',ecolor='grey',alpha=0.5,color='black')
            ax.set_title(title,color='red',fontsize=12)
            ax.axhline(y=0,ls='dotted',color='black')
            xlabel('Best distortion slope',fontsize=10)
            ylabel('Velocity dispersion',fontsize=10)
        savefig('slopes_dispersion.pdf')
        clf()
    
        if '--whitmore' not in sys.argv:
        
            print '\n\nSorting by average date\n'
        
            bindata = self.slopebin(data,0)
            fig = plt.figure(figsize=(8,10),frameon=False)
            plt.subplots_adjust(left=0.1, right=0.95, bottom=0.05, top=0.95, hspace=0.05, wspace=0.05)
            for i in [0,1,2]:
                title = 'Thermal' if i==0 else 'Turbulent' if i==1 else 'Method-of-Moment'
                ax = plt.subplot(3,1,1+i,xlim=[datetime.date(2000,1,1),datetime.date(2008,1,1)],ylim=[-1,1])
                ax.errorbar(data[:,0],data[:,7+3*i],fmt='o',ms=5,markeredgecolor='none',alpha=0.4,color='black')
                ax.errorbar(bindata[:,0],bindata[:,7+3*i],fmt='o',ms=10,markeredgecolor='none',alpha=0.6,color='blue',elinewidth=3)
                ax.axhline(y=0,ls='dotted',color='black')
                if i==2: xlabel('Average observation date',fontsize=10)
                ylabel('Best distortion slope',fontsize=10)
                t = text(datetime.date(2007,8,1),0.8,title,color='red',weight='bold',fontsize=10,ha='right')
                t.set_bbox(dict(color='white', alpha=0.5, edgecolor=None))
                ax.yaxis.set_major_locator(plt.FixedLocator(numpy.arange(-0.8,0.9,0.2)))
                if i!=2: plt.setp(ax.get_xticklabels(), visible=False)
            savefig('slopes_dates.pdf')
            clf()
            
    def slopebin(self,data,isort):
    
        k = 1
        data = numpy.array(sorted(data,key=lambda col: col[isort]))
        bindata = numpy.empty((0,15))
        for i in range(0,len(data),alpha.binning):
            ilim      = i+alpha.binning if i+alpha.binning<=len(data) else len(data)
            if '--show' in sys.argv:
                print '\n\nbin',k,'\n'
                print 'Date        |  Distance    |  Redshift    |  vdisp     |  RA          |  DEC         |  Chisq     |  Thermal Slope              |  Chisq     |  Turbulent Slope            |  Chisq     |  MoM Slope                  |  Quasar'
                print '----------  |  ----------  |  ----------  |  --------  |  ----------  |  ----------  |  --------  |  -------------------------  |  --------  |  -------------------------  |  --------  |  -------------------------  |  --------------'
                for j in range(i,ilim):
                    print '{:<10}  | '.format(str(data[j,0])),
                    print '{:>10}  | '.format('%.6f'%data[j,1]),
                    print '{:>10}  | '.format('%.6f'%data[j,2]),
                    print  '{:>8}  | '.format('%.2f'%data[j,3]),
                    print '{:>10}  | '.format('%.6f'%data[j,4]),
                    print '{:>10}  | '.format( '%.6f'%data[j,5]),
                    print  '{:>8}  |  {:>10} +/- {:>10}  | '.format('%.2f'%data[j,6],'%.6f'%data[j,7],'%.6f'%data[j,8]),
                    print  '{:>8}  |  {:>10} +/- {:>10}  | '.format('%.2f'%data[j,9],'%.6f'%data[j,10],'%.6f'%data[j,11]),
                    print  '{:>8}  |  {:>10} +/- {:>10}  | '.format('%.2f'%data[j,12],'%.6f'%data[j,13],'%.6f'%data[j,14]),
                    print alpha.distres['name'][data[j,15]]
            middate   = min(data[i:ilim,0]) + (max(data[i:ilim,0]) - min(data[i:ilim,0]))/2
            distance  = numpy.average([data[j,1] for j in range(i,ilim)])
            redshift  = numpy.average([data[j,2] for j in range(i,ilim)])
            vdisp     = numpy.average([data[j,3] for j in range(i,ilim)])
            ra        = numpy.average([data[j,4] for j in range(i,ilim)])
            dec       = numpy.average([data[j,5] for j in range(i,ilim)]) 
            therchisq = sum([data[j,6] for j in range(i,ilim)])
            therslope = sum([data[j,7]/data[j,8]**2 for j in range(i,ilim)]) / sum([1/data[j,8]**2 for j in range(i,ilim)])
            thererror = 1 / numpy.sqrt(sum(1/data[j,8]**2 for j in range(i,ilim)))
            turbchisq = sum([data[j,9] for j in range(i,ilim)])
            turbslope = sum([data[j,10]/data[j,11]**2 for j in range(i,ilim)]) / sum([1/data[j,11]**2 for j in range(i,ilim)])
            turberror = 1 / numpy.sqrt(sum(1/data[j,11]**2 for j in range(i,ilim)))
            momchisq  = sum([data[j,12] for j in range(i,ilim)])
            momslope  = sum([data[j,13]/data[j,14]**2 for j in range(i,ilim)]) / sum([1/data[j,14]**2 for j in range(i,ilim)])
            momerror  = 1 / numpy.sqrt(sum(1/data[j,14]**2 for j in range(i,ilim)))
            if '--show' in sys.argv:
                print '----------  |  ----------  |  ----------  |  --------  |  ----------  |  ----------  |  --------  |  -------------------------  |  --------  |  -------------------------  |  --------  |  -------------------------  |  --------------'
                print '{:<10}  | '.format(str(middate)),
                print '{:>10}  | '.format('%.6f'%distance),
                print '{:>10}  | '.format('%.6f'%redshift),
                print  '{:>8}  | '.format('%.2f'%vdisp),
                print '{:>10}  | '.format('%.6f'%ra),
                print '{:>10}  | '.format('%.6f'%dec),
                print  '{:>8}  |  {:>10} +/- {:>10}  | '.format('%.2f'%therchisq,'%.6f'%therslope,'%.6f'%thererror),
                print  '{:>8}  |  {:>10} +/- {:>10}  | '.format('%.2f'%turbchisq,'%.6f'%turbslope,'%.6f'%turberror),
                print  '{:>8}  |  {:>10} +/- {:>10}  | '.format('%.2f'%momchisq ,'%.6f'%momslope ,'%.6f'%momerror ),
            bindata   = numpy.vstack((bindata,[middate,distance,redshift,vdisp,ra,dec,therchisq,therslope,thererror,turbchisq,turbslope,turberror,momchisq,momslope,momerror]))
            k += 1
        return bindata
    
