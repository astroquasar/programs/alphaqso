#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def sysclean(line,condition):

    conds = True
    if condition=='region':
        if '--clean' in sys.argv and self.selection=='J043037-485523/1.35560/UVES_squader':
            conds = line not in ['AlII_1670','FeII_2260']
    if condition=='component':
        cond1 = line.split()[0] not in ['>>','<<','??','__','<>']
        cond2 = '7.99000' not in line
        cond3 = 'H I' not in line
        conds = cond1 and cond2 and cond3
        if '--clean' in sys.argv and self.selection=='J043037-485523/1.35560/UVES_squader':
            cond4 = line.split()[0]!='AlII'
            cond5 = 'aj' not in line
            cond6 = 'AJ' not in line
            conds = conds and cond4 and cond5 and cond6
    return conds

#==================================================================================================================

def sim_complex():
    
    self.regtotal = self.regboth = self.regblue = self.regred = 0                                                   # Initialize non-used parameters called in getshift function
    
    idxs = []                                                                                                       # Initialize array listing the indexes of all the targeted systems
    for i in range(len(self.distsim)):                                                                              # Loop over the self.distmin array from CoDDAM
        cond0  = self.selection in [self.distsim['system'][i],'published']                                          # Condition 1: the 
        cond1  = self.distsim['system'][i] not in self.outliers
        cond2  = self.distsim['instrument'][i].lower()==self.instrument
        if cond0 and cond1 and cond2:
            idxs.append(i)
            
    for i in idxs:

        colname        = 'mid%02i'%(100*self.stdev)
        colname        = colname+'T' if '--expind' in sys.argv else colname
        self.selection = self.distsim['system'][i]
        self.qso       = self.distsim['system'][i].split('/')[0]
        self.zabs      = self.distsim['system'][i].split('/')[1]
        self.sample    = self.distsim['system'][i].split('/')[2]
        self.distmid   = self.distmid if '--distmid' in sys.argv else float(self.distsim[colname][i])
        self.model     = 'model%02i'%float(self.distsim['model'][i])
        simpath        = self.fitdir+self.selection+'/'+self.model+'/sims/'+self.simtest
        
        print '|- System:',self.selection
        print '|  |- Model-Simulation:',self.model,'-',self.simtest
        print '|  |  |- Distortion-Slope:',self.test,'-',self.distortion
        
        # If default synthetic spectrum not created, do it
        
        if os.path.exists(simpath+'/model/')==True:
            print '|  |  |  |- Spectra already available.'
        if os.path.exists(simpath+'/model/')==False:
            print '|  |  |  |- Reading original fort.13...'
            os.system('mkdir -p '+simpath+'/model/original')
            os.system('mkdir -p '+simpath+'/model/data')
            
            # Read fort.18 and create fort.13 to be used to generate the model

            os.chdir(simpath+'/model/')
            os.system('cp '+self.fitdir+self.selection+'/'+self.model+'/model/header.dat .')
            os.system('cp '+self.fitdir+self.selection+'/'+self.model+'/model/turbulent.18 fort.18')
            os.system('cp '+self.fitdir+self.selection+'/'+self.model+'/model/turbulent.26 fort.26')
            createfit13('fort','sim')
            hdin   = np.loadtxt('header.dat',dtype=str,delimiter='\n')
            fort13 = open('fort_fit.13','r')
            fort13 = [line.strip() for line in fort13]
            os.system('rm header.dat fort*')
            
            file1  = open('original/fort.13','w')
            file2  = open('turbulent.13','w')
            hdout  = open('header.dat','w')
            regs   = np.empty((0,2))
            
            i,flag,ihead,idx = 0,0,0,1
            while i<len(fort13):
                line = fort13[i]
                if len(line.strip())==0:
                    break
                elif '*' in line:
                    file1.write(line+'\n')
                    file2.write(line+'\n')
                    flag += 1
                elif flag==1:
                    if sysclean(hdin[ihead],'region')==True:
                        spec = line.split()[0].split('/')[-1].replace('.fits','')
                        regs = np.vstack((regs,[float(line.split()[2]),float(line.split()[3])]))
                        head = line.replace(line.split()[0],'data/'+line.split()[0].split('/')[-1])
                        file1.write(head+'\n')
                        file2.write(line.replace(line.split()[0],'data/spec%02i.txt'%(idx))+'\n')
                        hdout.write(hdin[ihead]+'\n')
                        idx += 1
                    ihead += 1
                elif flag==2 and sysclean(line,'component')==True:
                    val = line.split()
                    if len(val[0])==1:
                        vals = [val[0]+' '+val[1],val[2],val[3],val[4],val[5],val[6],val[7],val[8]]
                    else:
                        vals = [val[0],val[1],val[2],val[3],val[4],val[5],val[6],val[7]]
                    #if '--noalpha' in sys.argv:
                    comp = np.delete(vals,4,0)
                    file1.write('   {0:<6} {1:>10} {2:>15} {3:>11} {4:>10} {5:>10} {6:>3}\n'.format(*comp))
                    vals[4] = '0.000'+" ".join(re.findall("[a-zA-Z]+",vals[4][-2:]))
                    file2.write('   {0:<6} {1:>10} {2:>15} {3:>11} {4:>11} {5:>10} {6:>10} {7:>3}\n'.format(*vals))
                    #else:
                    #    file1.write('   {0:<6} {1:>10} {2:>15} {3:>11} {4:>11} {5:>10} {6:>10} {7:>3}\n'.format(*vals))
                    #    file2.write('   {0:<6} {1:>10} {2:>15} {3:>11} {4:>11} {5:>10} {6:>10} {7:>3}\n'.format(*vals))
                i += 1
            file1.close()
            file2.close()
            hdout.close()
            
            # Create chunks for original fort.13
            
            os.chdir(simpath+'/model/original/')
            if os.path.exists('data'): os.system('rm data')
            os.system('ln -s ../../../../../../../../spectra/UVES_squader ./data')
            
            opfile = open('fitcommands','w')
            opfile.write('d\n\n\n\n\n')
            for i in range(len(regs)):
                if i==0:
                    opfile.write('\nas\n\n\n\n')
                elif i==len(regs)-1:
                    opfile.write('\n\n\nn\n\n')
                else:
                    opfile.write('\n\n\n\n')
            opfile.close()
            
            os.system('cp '+self.atomdir+' atom.dat')
            opfile = open('vp_setup.dat','w')
            for row in self.vpfsetup:
                #row = 'NOVARS 3' if 'NOVARS' in row and '--noalpha' in sys.argv else row
                row = 'NOVARS 3' if 'NOVARS' in row else row
                opfile.write(row+'\n')
            opfile.close()
            os.environ['ATOMDIR']='./atom.dat'
            os.environ['VPFSETUP']='./vp_setup.dat'
            
            os.system(self.vpcommand+' < fitcommands > termout')
            os.system('rm fitcommands termout')
            os.system('mkdir -p chunks/')
            os.system('mv vpfit_chunk* chunks/')
            
            # Extract data chunks, modify error array and add noise
            
            self.shifts = np.empty((0,2))
            os.chdir(simpath+'/model/')
            sp = np.loadtxt(self.fitdir+'spectra/UVES_squader_ASCII/'+spec+'.dat',comments='!')
            wa = sp[:,0]
            er = sp[:,2]
            for i in range(len(regs)):
                data  = np.loadtxt('./original/chunks/vpfit_chunk%03i.txt'%(i+1),comments='!')
                #print '\n\n',i,'\n\n'
                #print self.shifts
                shift = float(self.getshift(regs[i,0],regs[i,1],self.distmid,mode='create'))
                wmin  = data[0,0]
                wmax  = data[-1,0]
                imin1 = abs(wa-(wmin-2)).argmin()
                imin2 = abs(wa-(wmin-0)).argmin()
                imax1 = abs(wa-(wmax+0)).argmin()
                imax2 = abs(wa-(wmax+2)).argmin()
                wave  = wa[imin1:imax2]
                error = er[imin1:imax2]
                flux  = np.hstack(([1]*(imin2-imin1),data[:,3],[1]*(imax2-imax1)))
                ofile = open('data/spec%02i.txt'%(i+1),'w')
                for j in range(len(wave)):
                    iwave  = wave[j]*(2*self.c+shift)/(2*self.c-shift)
                    ierror = 1./self.snr if '--snr' in sys.argv else error[j]
                    iflux  = flux[j] if ierror<0 else flux[j]+np.random.normal(0,ierror)
                    ofile.write('{0:>21} {1:>25} {2:>30}\n'.format('%.13f'%iwave,'%.16f'%iflux,'%.16E'%ierror))
                ofile.close()
                
            if '--expind' in sys.argv:
                outfile = open('shifts.dat','w')
                for i in range(len(self.shifts)):
                    outfile.write(self.shifts[i,0]+'  '+self.shifts[i,1]+'\n')
                outfile.close()
                
        self.distpath = simpath+'/'+self.test+'/'+self.distortion+'/'
        os.system('mkdir -p '+self.distpath)
        os.chdir(self.distpath)
        
        if self.prepare('turbulent')=='proceed':
            print '|  |  |  |- Processing Turbulent fitting...'
            self.fit('turbulent')
            
        if '--compress' in sys.argv:
            destination = self.here.replace('list','execute')+'/'
            os.chdir(self.fitdir)
            target   = self.selection+'/'+self.model+'/sims/'+self.simtest+'/'+self.test+'/'+self.distortion
            filename = target.replace('/','--')
            os.system('tar -zcvf '+filename+'.tar.gz '+target+'/')
            os.system('rm -rf '+target+'/')
            os.system('mkdir -p '+destination)
            os.system('mv '+filename+'.tar.gz '+destination)
            
#==================================================================================================================
