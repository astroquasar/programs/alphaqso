import alpha

class supercomputer(object):

    """
    Various operations to run jobs on the supercomputer or extract results.
    """
        
    def showhelp(self):
        
        print ""
        print "-------------------------------------------------------------------------"
        print ""
        print "description:"
        print ""
        print "required argument (one of the following):"
        print ""
        print "   --bashlist    Prepare bash scripts for supercomputer run"
        print "   --include     Include extracted contents to local repository"
        print ""
        print "example:"
        print ""
        print "   alpha supercomputer --bashlist"
        print ""
        print "-------------------------------------------------------------------------"
        print ""
        quit()
        
    def __init__(self):
        
        # Display help message or check if required arguments are given
        if '--help' in sys.argv or '-h' in sys.argv:
            self.showhelp()
        elif '--bashlist' in sys.argv:
            self.bashlist()
        elif '--include' in sys.argv:
            self.include()
        else:
            print 'ERROR: Either --bashlist or --include must be called...'
            quit()

    def bashlist(self):
        
        results = setup.distres
        joblist = open('runjobs.sh','w')
        joblist.write('#!/bin/bash\n')
        for i in range(len(results)):
            distsep = float(results['distsep'][i])
            options = '--raijin --previous --compress --simulation complex --expind'
            options = options + ' --selection ' + results['system'][i]
            options = options + ' --model '     + results['model'][i]
            options = options + ' --stdev %.2f'%setup.stdev
            options = options + ' --distmid -0.123'
            options = options + ' --distmin %.3f'%(-0.123-10*distsep)
            options = options + ' --distmax %.3f'%(-0.123+10*distsep)
            options = options + ' --distsep %.3f'%distsep
            options = options + ' --chisq 1E-5'
            name    = results['system'][i].replace('/','--')
            outfile = open(name+'.sh','w')
            outfile.write('#!/bin/bash\n')
            outfile.write('module load python/2.7.5\n')
            outfile.write('module load python/2.7.5-matplotlib\n')
            outfile.write(setup.alphapath+'PyD_alpha run_complex '+options+'\n')
            outfile.close()
            joblist.write('qsub -P gd5 -q normal -l walltime=40:00:00,mem=300MB -l wd '+setup.here+'/'+name+'.sh\n')
        joblist.close()
    
    def include(self):
    
        os.system('tar -zxvf '+setup.tarfile)
        os.chdir(setup.tarfile.replace('.tar.gz',''))
        #os.system('ls *.tar.gz > listabsorbers')
        os.system('find . -name "*.tar.gz" -type f > listabsorbers')
        abslist = np.loadtxt('listabsorbers',dtype='str',ndmin=1)
        for absorber in abslist:
            os.system('tar -zxvf '+absorber)
            target = absorber.replace('.tar.gz','').replace('--','/')
            destin = setup.fitdir+'/'+target
            if os.path.exists(target+'/turbulent/'):
                os.system('mkdir -p '+destin+'/turbulent/')
                os.system('rsync -avzlr --delete '+target+'/turbulent/ '+destin+'/turbulent/')
            if os.path.exists(target+'/thermal/'):
                os.system('mkdir -p '+destin+'/thermal/')
                os.system('rsync -avzlr --delete '+target+'/thermal/ '+destin+'/thermal/')
            if os.path.exists(target+'/turbulent/')==False and os.path.exists(target+'/thermal/')==False:
                os.system('mkdir -p '+destin)
                os.system('rsync -avzlr --delete '+target+'/ '+destin+'/')
            os.system('rm -rf '+setup.tarfile.split('--')[0])
    
    def prepare(self):
    
        for system in setup.curlist['system']:
            os.system('ls '+setup.fitdir+'/'+system+'/ > list')
            filelist = np.loadtxt('list',dtype=str)
            for line in filelist:
                if 'model' in line:
                    pathhead = setup.fitdir+'/'+system+'/'+line+'/model/header.dat'
                    pathther = setup.fitdir+'/'+system+'/'+line+'/model/thermal.13'
                    pathturb = setup.fitdir+'/'+system+'/'+line+'/model/turbulent.13'
                    os.system('mkdir -p ./prepare/'+system+'/'+line+'/model/')
                    if os.path.exists(pathhead): os.system('cp '+pathhead+' ./prepare/'+system+'/'+line+'/model/')
                    if os.path.exists(pathther): os.system('cp '+pathther+' ./prepare/'+system+'/'+line+'/model/')
                    if os.path.exists(pathturb): os.system('cp '+pathturb+' ./prepare/'+system+'/'+line+'/model/')
            if 'UVES_squader' in system:
                os.system('ln -s ../../../spectra/UVES_king ./prepare/'+system+'/data')
            else:
                os.system('mkdir -p ./prepare/'+system+'/data/')
                os.system('cp '+setup.fitdir+'/'+system+'/data/* ./prepare/'+system+'/data/')
                
    def clean(self):
        
        os.system('ls ./execute/*.tar.gz  > lista')
        os.system('ls ./compress/*.tar.gz > listb')
        os.system('ls ./list/*.sh         > listc')
        lista = [] if os.stat('lista').st_size==0 else np.loadtxt('lista',dtype=str)
        listb = [] if os.stat('listb').st_size==0 else np.loadtxt('listb',dtype=str)
        listc = [] if os.stat('listc').st_size==0 else np.loadtxt('listc',dtype=str)[:-1]
        done  = []
        todo  = []
        for tarfile in lista:
            path   = tarfile.split('/')[-1].split('--')
            name   = path[0] + '--' + path[1] + '--' + path[2]
            system = path[0] + '/'  + path[1] + '/'  + path[2]
            i      = np.where(setup.distres['system']==system)[0][0]
            step   = float(setup.distres['step'][i]) if '/jobs2/' in setup.here else 0.01
            min1   = float(setup.distres['min'][i])  if '/jobs2/' in setup.here else -0.2
            max1   = float(setup.distres['max'][i])  if '/jobs2/' in setup.here else 0
            min2   = '0.000' if round(min1,3)==0 else str('%.3f'%min1).replace('-','m') if '-' in str(min1) else 'p'+str('%.3f'%min1)
            max2   = '0.000' if round(max1,3)==0 else str('%.3f'%max1).replace('-','m') if '-' in str(max1) else 'p'+str('%.3f'%max1)
            cond1  = '0.000' in tarfile
            cond2  = tarfile.replace('0.000',min2) in lista
            cond3  = tarfile.replace('0.000',max2) in lista
            if cond1 and cond2 and cond3:
                for i in np.arange(min1,max1+0.001,step):
                    slope = '0.000' if round(i,3)==0 else str('%.3f'%i).replace('-','m') if '-' in str(i) else 'p'+str('%.3f'%i)
                    os.system('mv '+tarfile.replace('0.000',slope)+' ./compress/')
                    done.append(name)
            elif cond1:
                todo.append(name)
        for tarfile in listb:
            path = tarfile.split('/')[-1].split('--')
            name = path[0]+'--'+path[1]+'--'+path[2]
            if '0.000' in tarfile:
                done.append(name)
        for tarfile in listc:
            path   = tarfile.split('/')[-1].split('--')
            name   = path[0] + '--' + path[1] + '--' + path[2].replace('.sh','')
            if name not in done and name not in todo:
                todo.append(name)
        todo = sorted(todo)
        joblist = open('runjobs.sh','w')
        joblist.write('#!/bin/bash\n')
        for system in todo:
            joblist.write('qsub -P gd5 -q normal -l walltime=40:00:00,mem=300MB -l wd '+setup.here+'/'+system+'.sh\n')
        joblist.close()
        os.system('rm lista listb listc')
    
