#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def plotwidth():

    self.path = self.home+'/results/metals/'
    os.chdir(self.path)

    def func(x,a,b):
        return a + b*x
        
    bindata,metals = getmetals()


    ''' ----------------------------------------------------- '''
    ''' Plot Average Equivalent Width vs. Velocity Dispersion '''
    ''' ----------------------------------------------------- '''


    x    = bindata[:,5]
    xerr = bindata[:,6]
    y    = bindata[:,1]
    yerr = bindata[:,2]
    zabs = bindata[:,3]

    xmax = 1.1*max(x)
    ymax = 0.2#1.1*max(y)
    
    fig = figure(figsize=(7,6))
    plt.subplots_adjust(left=0.1, right=0.87, bottom=0.1, top=0.95, hspace=0, wspace=0)

    ax = subplot(111,xlim=[0,xmax],ylim=[0,ymax])
    ax.scatter(x,y,marker='o',s=50,edgecolors='none',zorder=3,\
            c=zabs,cmap=mpl.cm.cool,vmin=min(zabs),vmax=max(zabs))
    errorbar(x,y,yerr=yerr,fmt='o',ms=0,c='0.7',zorder=1)
    xlabel('Average velocity dispersion',fontsize=12)
    ylabel('Average equivalent width',fontsize=12)
    axhline(y=0,ls='dotted',color='black')
    axvline(x=0,ls='dotted',color='black')
    
    xfit = np.arange(0,xmax,0.01)
    coeffs,matcov = curve_fit(func,x,y)
    yfit = func(xfit,coeffs[0],coeffs[1])
    ax.plot(xfit,yfit,color='black',lw=3,ls='dotted',
            label=r'Unweighted fit: $%.6f \pm %.6f$ '%(coeffs[1],np.sqrt(matcov[1][1])))
    
    xfit = np.arange(0,xmax,0.01)
    coeffs,matcov = curve_fit(func,x,y,sigma=yerr)
    yfit = func(xfit,coeffs[0],coeffs[1])
    ax.plot(xfit,yfit,color='black',lw=3,ls='dashed',
            label=r'Weighted fit: $%.6f \pm %.6f$ '%(coeffs[1],np.sqrt(matcov[1][1])))
    
    leg = plt.legend(fancybox=True,loc=2,numpoints=1,handlelength=3,prop={'size':12})
    leg.get_frame().set_linewidth(0.1)
                
    ax1  = fig.add_axes([0.87,0.1,0.04,0.85])
    cmap = mpl.cm.cool
    norm = mpl.colors.Normalize(vmin=min(zabs),vmax=max(zabs))
    cb1  = mpl.colorbar.ColorbarBase(ax1,cmap=cmap,norm=norm)
    cb1.set_label('Absorption redshift',fontsize=12)
    
    savefig('equiwidth_vs_dispersion.pdf')
    clf()

    
    ''' ----------------------------------------------------------- '''
    ''' Plot Average Equivalent Width vs. Number of Tied Components '''
    ''' ----------------------------------------------------------- '''


    x    = bindata[:,0]
    y    = bindata[:,1]
    yerr = bindata[:,2]
    zabs = bindata[:,3]
    
    xmax = 1.1*max(x)
    ymax = 1.1*max(y)
    
    fig = figure(figsize=(7,6))
    plt.subplots_adjust(left=0.1, right=0.87, bottom=0.1, top=0.95, hspace=0, wspace=0)

    ax = subplot(111,xlim=[0,xmax],ylim=[0,ymax])
    ax.scatter(x,y,marker='o',s=50,edgecolors='none',zorder=3,\
            c=zabs,cmap=mpl.cm.cool,vmin=min(zabs),vmax=max(zabs))
    errorbar(x,y,yerr=yerr,fmt='o',ms=0,c='0.7',zorder=1)
    xlabel('Number of tied components',fontsize=12)
    ylabel('Average equivalent width',fontsize=12)
    axhline(y=0,ls='dotted',color='black')
    axvline(x=0,ls='dotted',color='black')
    
    xfit = np.arange(0,xmax,0.01)
    coeffs,matcov = curve_fit(func,x,y)
    yfit = func(xfit,coeffs[0],coeffs[1])
    ax.plot(xfit,yfit,color='black',lw=3,ls='dotted',
            label=r'Unweighted fit: $%.6f \pm %.6f$ '%(coeffs[1],np.sqrt(matcov[1][1])))
    
    xfit = np.arange(0,xmax,0.01)
    coeffs,matcov = curve_fit(func,x,y,sigma=yerr)
    yfit = func(xfit,coeffs[0],coeffs[1])
    ax.plot(xfit,yfit,color='black',lw=3,ls='dashed',
            label=r'Weighted fit: $%.6f \pm %.6f$ '%(coeffs[1],np.sqrt(matcov[1][1])))
    
    leg = plt.legend(fancybox=True,loc=2,numpoints=1,handlelength=3,prop={'size':12})
    leg.get_frame().set_linewidth(0.1)
                
    ax1  = fig.add_axes([0.87,0.1,0.04,0.85])
    cmap = mpl.cm.cool
    norm = mpl.colors.Normalize(vmin=min(zabs),vmax=max(zabs))
    cb1  = mpl.colorbar.ColorbarBase(ax1,cmap=cmap,norm=norm)
    cb1.set_label('Absorption redshift',fontsize=12)
    
    savefig('equiwidth_vs_tied.pdf')
    clf()

    
    ''' ------------------------------------------- '''
    ''' Plot Number of tied components vs. Redshift '''
    ''' ------------------------------------------- '''


    x    = bindata[:,3]
    y    = bindata[:,0]
    c    = bindata[:,4]
        
    xmax = 1.1*max(x)
    ymax = 1.1*max(y)
    
    fig = figure(figsize=(7,6))
    plt.subplots_adjust(left=0.1, right=0.87, bottom=0.1, top=0.95, hspace=0, wspace=0)
    
    ax = subplot(111,xlim=[0,xmax],ylim=[0,ymax])
    ax.scatter(x,y,marker='o',s=50,edgecolors='none',zorder=3,\
               c=c,cmap=mpl.cm.rainbow,vmin=min(c),vmax=max(c))
    xlabel('Absorption redshift',fontsize=12)
    ylabel('Number of tied components',fontsize=12)
    axhline(y=0,ls='dotted',color='black')
    axvline(x=0,ls='dotted',color='black')

    xfit = np.arange(0,xmax,0.01)
    coeffs,matcov = curve_fit(func,x,y)
    yfit = func(xfit,coeffs[0],coeffs[1])
    ax.plot(xfit,yfit,color='black',lw=3,ls='dashed',
            label=r'Unweighted fit: $%.6f \pm %.6f$ '%(coeffs[1],np.sqrt(matcov[1][1])))
    
    leg = plt.legend(fancybox=True,loc=2,numpoints=1,handlelength=3,prop={'size':12})
    leg.get_frame().set_linewidth(0.1)
                
    ax1  = fig.add_axes([0.87,0.1,0.04,0.85])
    cmap = mpl.cm.rainbow
    norm = mpl.colors.Normalize(vmin=min(c),vmax=max(c))
    cb1  = mpl.colorbar.ColorbarBase(ax1,cmap=cmap,norm=norm)
    cb1.set_label('Signal-to-Noise ratio',fontsize=12)
    
    savefig('tiedcomp_vs_redshift.pdf')
    clf()

    
    ''' ------------------------------------------ '''
    ''' Plot Average Equivalent Width vs. Redshift '''
    ''' ------------------------------------------ '''

    
    x    = bindata[:,3]
    y    = bindata[:,1]
    yerr = bindata[:,2]
    c    = bindata[:,4]
        
    xmax = 1.1*max(x)
    ymax = 1.1*max(y)
    
    fig = figure(figsize=(7,6))
    plt.subplots_adjust(left=0.1, right=0.87, bottom=0.1, top=0.95, hspace=0, wspace=0)
    
    ax = subplot(111,xlim=[0,xmax],ylim=[0,ymax])
    ax.scatter(x,y,marker='o',s=50,edgecolors='none',zorder=3,\
               c=c,cmap=mpl.cm.rainbow,vmin=min(c),vmax=max(c))
    errorbar(x,y,yerr=yerr,fmt='o',ms=0,c='0.7',zorder=1)
    xlabel('Absorption redshift',fontsize=12)
    ylabel('Average Equivalent Width',fontsize=12)
    axhline(y=0,ls='dotted',color='black')
    axvline(x=0,ls='dotted',color='black')

    xfit = np.arange(0,xmax,0.01)
    coeffs,matcov = curve_fit(func,x,y)
    yfit = func(xfit,coeffs[0],coeffs[1])
    ax.plot(xfit,yfit,color='black',lw=3,ls='dotted',
            label=r'Unweighted fit: $%.6f \pm %.6f$ '%(coeffs[1],np.sqrt(matcov[1][1])))
    
    xfit = np.arange(0,xmax,0.01)
    coeffs,matcov = curve_fit(func,x,y,sigma=yerr)
    yfit = func(xfit,coeffs[0],coeffs[1])
    ax.plot(xfit,yfit,color='black',lw=3,ls='dashed',
            label=r'Weighted fit: $%.6f \pm %.6f$ '%(coeffs[1],np.sqrt(matcov[1][1])))
    
    leg = plt.legend(fancybox=True,loc=2,numpoints=1,handlelength=3,prop={'size':12})
    leg.get_frame().set_linewidth(0.1)
                
    ax1  = fig.add_axes([0.87,0.1,0.04,0.85])
    cmap = mpl.cm.rainbow
    norm = mpl.colors.Normalize(vmin=min(c),vmax=max(c))
    cb1  = mpl.colorbar.ColorbarBase(ax1,cmap=cmap,norm=norm)
    cb1.set_label('Signal-to-Noise ratio',fontsize=12)
    
    savefig('equiwidth_vs_redshift.pdf')
    clf()


#==================================================================================================================

def plotwidthperion():

    self.path = self.home+'/results/metals/'
    os.chdir(self.path)

    def func(x,a,b):
        return a + b*x
        
    bindata,metals = getmetals()

    ''' ------------------------------------------- '''
    ''' Plot number of tied components vs. redshift '''
    ''' ------------------------------------------- '''

    
    fig = figure(figsize=(8,12))
    plt.subplots_adjust(left=0.07, right=0.93, bottom=0.05, top=0.93, hspace=0.3, wspace=0.1)
    fig.suptitle('Number of tied components vs. Absorption redshift',fontsize=15)

    for i in range(len(metals)):

        ion   = metals[i,0]
        data  = np.array(sorted(metals[i,2],key=lambda col: col[2]))
        bdata = np.empty((0,2))
        for j in range(0,len(data),self.binning):
            jlim  = j+self.binning if j+self.binning<=len(data) else len(data)
            zabs  = np.average([float(data[k,2]) for k in range(j,jlim)])
            tied  = np.average([float(data[k,0]) for k in range(j,jlim)])
            bdata = np.vstack((bdata,[zabs,tied]))
            
        ax = subplot(4,2,i+1,xlim=[0,max(bdata[:,0])],ylim=[0,max(bdata[:,1])])
        ax.scatter(bdata[:,0],bdata[:,1],marker='o',s=40,edgecolors='none',zorder=3,c='black',alpha=0.6)
        axhline(y=0,ls='dotted',color='black')
        axvline(x=0,ls='dotted',color='black')

        if len(bdata)>1:
            xfit = np.arange(0,max(bdata[:,0]),0.01)
            coeffs,matcov = curve_fit(func,bdata[:,0],bdata[:,1])
            yfit = func(xfit,coeffs[0],coeffs[1])
            ax.plot(xfit,yfit,color='red',lw=2,ls='dashed')
            self.linslopeval = coeffs[1]
            self.linslopeerr = np.sqrt(matcov[1][1])
            #print metals[i,0]+r': %.4f +/- %.4f'%(self.linslopeval,self.linslopeerr)

        plt.title(ion,fontsize=12,color='red')

        if (i+1)%2==0:
            ax.yaxis.tick_right()
            ax.yaxis.set_ticks_position('both')
                    
    savefig('tied_vs_redshift_per_ion.pdf')
    clf()

    
    ''' ----------------------------------------------------------- '''
    ''' Plot average equivalent width vs. number of tied components '''
    ''' ----------------------------------------------------------- '''

    
    fig = figure(figsize=(8,12))
    plt.subplots_adjust(left=0.07, right=0.93, bottom=0.05, top=0.93, hspace=0.3, wspace=0.1)
    fig.suptitle('Average Equivalent Width vs. Number of tied components',fontsize=15)

    for i in range(len(metals)):

        ion   = metals[i,0]
        data  = np.array(sorted(metals[i,2],key=lambda col: col[0]))
        bdata = np.empty((0,2))
        for j in range(0,len(data),self.binning):
            jlim  = j+self.binning if j+self.binning<=len(data) else len(data)
            tied  = np.average([float(data[k,0]) for k in range(j,jlim)])
            width = np.average([float(data[k,1]) for k in range(j,jlim)])
            bdata = np.vstack((bdata,[tied,width]))
            
        ax = subplot(4,2,i+1,xlim=[0,max(bdata[:,0])],ylim=[0,max(bdata[:,1])])
        ax.scatter(bdata[:,0],bdata[:,1],marker='o',s=40,edgecolors='none',zorder=3,c='black',alpha=0.6)
        axhline(y=0,ls='dotted',color='black')
        axvline(x=0,ls='dotted',color='black')

        if len(bdata)>1:
            xfit = np.arange(0,max(bdata[:,0]),0.01)
            coeffs,matcov = curve_fit(func,bdata[:,0],bdata[:,1])
            yfit = func(xfit,coeffs[0],coeffs[1])
            ax.plot(xfit,yfit,color='red',lw=2,ls='dashed')

        plt.title(ion,fontsize=12,color='red')

        if (i+1)%2==0:
            ax.yaxis.tick_right()
            ax.yaxis.set_ticks_position('both')
                    
    savefig('equiwidth_vs_tied_per_ion.pdf')
    clf()

    
    ''' ------------------------------------------ '''
    ''' Plot average equivalent width vs. redshift '''
    ''' ------------------------------------------ '''

    
    fig = figure(figsize=(8,12))
    plt.subplots_adjust(left=0.07, right=0.93, bottom=0.05, top=0.93, hspace=0.3, wspace=0.1)
    fig.suptitle('Average Equivalent Width vs. Absorption redshift',fontsize=15)

    for i in range(len(metals)):

        ion   = metals[i,0]
        data  = np.array(sorted(metals[i,2],key=lambda col: col[2]))
        bdata = np.empty((0,2))
        for j in range(0,len(data),self.binning):
            jlim  = j+self.binning if j+self.binning<=len(data) else len(data)
            zabs  = np.average([float(data[k,2]) for k in range(j,jlim)])
            width = np.average([float(data[k,1]) for k in range(j,jlim)])
            bdata = np.vstack((bdata,[zabs,width]))
            
        ax = subplot(4,2,i+1,xlim=[0,max(bdata[:,0])],ylim=[0,max(bdata[:,1])])
        ax.scatter(bdata[:,0],bdata[:,1],marker='o',s=40,edgecolors='none',zorder=3,c='black',alpha=0.6)
        axhline(y=0,ls='dotted',color='black')
        axvline(x=0,ls='dotted',color='black')
        
        if len(bdata)>1:
            xfit = np.arange(0,max(bdata[:,0]),0.01)
            coeffs,matcov = curve_fit(func,bdata[:,0],bdata[:,1])
            yfit = func(xfit,coeffs[0],coeffs[1])
            ax.plot(xfit,yfit,color='red',lw=2,ls='dashed')

        plt.title(ion,fontsize=12,color='red')

        if (i+1)%2==0:
            ax.yaxis.tick_right()
            ax.yaxis.set_ticks_position('both')
                    
    savefig('equiwidth_vs_redshift_per_ion.pdf')
    clf()

#==================================================================================================================
