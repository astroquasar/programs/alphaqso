#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *
      
#==================================================================================================================

def getndf(arm,zsample):

    print '\n\tTotal number of degrees of freedom\n'
    
    x,y,dy = [],[],[]
    
    for i in self.distlist:
        
        strslope = '0.000' if round(i,3)==0 \
                   else str('%.3f'%i).replace('-','m') if '-' in str(i) \
                   else 'p'+str('%.3f'%i)
                   
        pathdir  = self.rundir+'/'+self.instrument+'/'+arm+'/'+strslope+'/results.dat'
        
        if os.path.exists(pathdir)==True:
            
            output  = np.genfromtxt(pathdir,names=True,dtype=object,comments='!')
            chilist = np.empty((0,2))
            
            for k in range (len(output)):
                
                if (sample=='lowz' and float(output['z_abs'][k])<1.8) \
                  or (sample=='highz' and float(output['z_abs'][k])>1.8) \
                  or sample=='comb':
                  
                    condturb1 = output['turb_alpha'][k] not in ['0.000','-'] \
                                or output['turb_error'][k] not in ['0.000','-']
                    condther1 = output['ther_alpha'][k] not in ['0.000','-'] \
                                or output['ther_error'][k] not in ['0.000','-']
                    condturb2 = output['turb_alpha'][k] in ['0.000','-'] \
                                and output['turb_error'][k] in ['0.000','-']
                    condther2 = output['ther_alpha'][k] in ['0.000','-'] \
                                and output['ther_error'][k] in ['0.000','-']
                                
                    if (condturb1 and condther2) \
                      or (condturb1 and condther1 and \
                          float(output['turb_chisq'][k]) < float(output['ther_chisq'][k])):
                      
                        chilist = np.vstack((chilist,[float(output['turb_df'][k]),float(output['turb_chisq'][k])]))
                        
                    if (condther1 and condturb2) \
                      or (condturb1 and condther1 and \
                          float(output['turb_chisq'][k]) > float(output['ther_chisq'][k])):
                      
                        chilist = np.vstack((chilist,[float(output['ther_df'][k]),float(output['ther_chisq'][k])]))
                        
            sigfac = sum(chilist[:,0])#*np.average(chilist[:,1])
            
            print '\t\t','{:>10}'.format('%.3f'%i),'{:>10}'.format('%.3f'%sigfac)
            
            x.append(round(i,3))
            y.append(sigfac)
            dy.append(np.std(chilist[:,1]))
            
    return x,y,dy
    
#==================================================================================================================
