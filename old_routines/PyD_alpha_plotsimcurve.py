#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def plotsimcurve():
    
    os.chdir(self.home+'/results/curves/')                                                                      # Go to specific directory where all the figures will be stored
    
    # Define output file name and which systems to consider
    
    self.name = self.instrument+'/' if '--simulation' not in sys.argv else 'sim2/' if '--expind' in sys.argv else 'sim1/'
    self.name = self.name + self.instrument + '-'
    self.name = self.name + 'sim_'+self.deviation+'_' if '--simulation' in sys.argv   else self.name
    self.name = self.name + 'v10_slope'               if self.instrument=='hires'     else self.name
    self.name = self.name + 'v10_all'                 if self.instrument=='uves'      else self.name
    self.name = self.name + '_jw'                     if '--whitmore' in sys.argv     else self.name
    self.name = self.name + '_prev'                   #if '--previous' in sys.argv     else self.name
    writefile = 'yes'                                 if self.selection=='published'  else 'no'
    
    idxs = []
    for i in range(len(self.distsim)):
        cond1 = self.selection in [self.distsim['system'][i],'published']
        cond2 = self.distsim['instrument'][i].lower()==self.instrument
        cond3 = self.sample==None or self.sample in self.distsim['system'][i]
        if cond1 and cond2 and cond3:
            idxs.append(i)

    # Initializing the figure

    if writefile=='yes':
        output = open('temp.dat','w')
    
    rc('font', size=2, family='sans-serif')
    rc('axes', labelsize=10, linewidth=0.2)
    rc('legend', fontsize=10, handlelength=10)
    rc('xtick', labelsize=7)
    rc('ytick', labelsize=7)
    rc('lines', lw=0.2, mew=0.2)
    rc('grid', linewidth=0.2)
    
    for i in idxs:

        # Extract tabulating distortion information

        suffix          = '%02iT'%(100*self.stdev) if '--expind' in sys.argv and self.stdev!=0 else '%02i'%(100*self.stdev)
        self.selection  = self.distsim['threshold'][i]
        self.selection  = self.distsim['system'][i]
        self.instrument = self.distsim['instrument'][i].lower() 
        self.distmid    = float(self.distsim['mid'+suffix][i])    if '--expind' not in sys.argv else float(self.distsim['mid00'][i])
        self.distmin    = float(self.distsim['min'+suffix][i])    if '--expind' not in sys.argv else float(self.distsim['min00'][i])
        self.distmax    = float(self.distsim['max'+suffix][i])    if '--expind' not in sys.argv else float(self.distsim['max00'][i])
        self.distsep    = float(self.distsim['step'+suffix][i])   if '--expind' not in sys.argv else float(self.distsim['step00'][i])
        self.wminturb   = float(self.distsim['minfit'+suffix][i])
        self.wmaxturb   = float(self.distsim['maxfit'+suffix][i])
        self.model      = 'model%02i'%float(self.distsim['model'][i])
        self.test       = 'v10_chisq%.E_step%.E_prev'%(self.chisq,self.distsep)
        tilt            = '0.000' if round(self.distmid,3)==0 else 'm%.3f'%abs(self.distmid) if self.distmid<0 else 'p%.3f'%abs(self.distmid)
        deviation       = '0.00'  if round(self.stdev,2)==0   else '%.2f'%self.stdev
        deviation       = deviation+'T' if '--expind' in sys.argv else deviation
        simtest         = self.simulation+'_'+tilt
        simtest         = simtest+'_'+deviation if '--expind' in sys.argv else simtest
        simtest         = simtest+'_snr%.E'%snr if '--snr'    in sys.argv else simtest

        print '\n',self.selection

        if self.distmin < self.distmax <= self.distmid:
            self.distlist = np.arange(self.distmax,self.distmin-0.001,-self.distsep)
            self.distplot = sorted(self.distlist)
        elif self.distmax > self.distmin >= self.distmid:
            self.distlist = np.arange(self.distmin,self.distmax+0.001,self.distsep)
            self.distplot = sorted(self.distlist)
        elif self.distmin < self.distmid < self.distmax:
            self.distlist = np.hstack((np.arange(self.distmid,self.distmax+0.001,self.distsep),np.arange(self.distmid,self.distmin-0.001,-self.distsep)))
            self.distplot = sorted(np.delete(self.distlist,0))

        # Extract turbulent results
            
        fitres   = np.empty((0,5))
    
        for j in self.distplot:
            strslope      = '0.000' if round(j,3)==0 else str('%.3f'%j).replace('-','m') if '-' in str(j) else 'p'+str('%.3f'%j)
            self.distpath = self.fitdir+'/'+self.selection+'/'+self.model+'/sims/'+simtest+'/'+self.test+'/'+strslope+'/'

            self.quasar   = self.selection.split('/')[0]
            self.zabs     = self.selection.split('/')[1]
            self.sample   = self.selection.split('/')[2]
            if os.path.exists(self.distpath)==True:
                self.getresults()
                dist          = round(j,3)
                fitres        = np.vstack((fitres,[None]*5))
                fitres[-1,0]  = dist
                fitres[-1,1]  = self.turb_df
                fitres[-1,2]  = self.turb_chisq
                fitres[-1,3]  = self.turb_alpha
                fitres[-1,4]  = self.turb_error
                if '-' not in fitres[-1]:
                    if '--show' in sys.argv:
                        print '{:>7} '.format('%.3f'%dist),
                        print '{:>11}'.format('%i'%self.turb_df),
                        print '{:>11}'.format('%.4f'%self.turb_chisq),
                        print '{:>11}'.format('%.4f'%self.turb_alpha),
                        print '{:>11}'.format('%.4f'%self.turb_error)
                else:
                    fitres = np.delete(fitres,-1,0)

        # Plot turbulent chi-square curve

        fig = figure()
        x     = fitres[:,0]
        y     = fitres[:,2]
        ax    = plt.subplot(111,xlim=[self.distmin,self.distmax])
        ax.errorbar(x,y,fmt='o',ms=6,markeredgecolor='none',ecolor='grey',alpha=0.7,color='black')
        ymin  = 0 if len(y)==0 else min(y)-1 if min(y)==max(y) else min(y)
        ymax  = 1 if len(y)==0 else max(y)+1 if min(y)==max(y) else max(y)
        if self.selection not in self.degensys+self.badwhit and len(fitres)>0:
            self.fitparabola2(x,y,model='Turbulent')
        ylim(ymin,ymax)
        y_formatter = matplotlib.ticker.ScalarFormatter(useOffset=False)
        ax.yaxis.set_major_formatter(y_formatter)
        t1 = ax.text((self.distmin+self.distmax)/2,ymax-0.1*(ymax-ymin),self.selection,color='grey',ha='center',fontsize=6)
        t1.set_bbox(dict(color='white', alpha=0.7, edgecolor=None))
        os.system('mkdir -p '+self.name)
        savefig(self.name + '/' + self.selection.replace('/','_').replace('.','z')+'.pdf')
        plt.close(fig)

        if writefile=='yes':
            output.write('{:<40}\t'.format(self.selection))
            output.write('{0:>10}\t'.format('%.2f'%np.mean(fitres[:,2])))
            slope = 'badwhit' if self.selection in self.badwhit and '--whitmore' in sys.argv else 'degenerate' if self.selection in self.degensys else '%.4f'%self.slope
            error = 'badwhit' if self.selection in self.badwhit and '--whitmore' in sys.argv else 'degenerate' if self.selection in self.degensys else '%.4f'%self.slope_error
            output.write('{0:>10}\t{1:>10}\t\n'.format(slope,error))
        
    if writefile=='yes':
        output.close()
        os.system('mv temp.dat '+self.home+'/results/curves/'+self.name+'.dat')
    
#==================================================================================================================

def plotsimnullhigh():
    
    os.chdir(self.home+'/results/curves/')
    
    # Initializing the figure

    rc('font', size=2, family='sans-serif')
    rc('axes', labelsize=10, linewidth=0.2)
    rc('legend', fontsize=10, handlelength=10)
    rc('xtick', labelsize=7)
    rc('ytick', labelsize=7)
    rc('lines', lw=0.2, mew=0.2)
    rc('grid', linewidth=0.2)

    # Extract tabulating distortion information

    self.selection = 'J043037-485523/1.35560/UVES_squader'
    simpath = 'sims/complex_p0.200_snr1E-03'
    self.model     = 'model01'
    self.distmin   = 0
    self.distmax   = +0.3
    self.distsep   = 0.005
    self.wminturb  = self.distmin
    self.wmaxturb  = self.distmax
    
    print '\n',self.selection

    if self.distmin < self.distmax <= self.distmid:
        self.distlist = np.arange(self.distmax,self.distmin-0.001,-self.distsep)
        self.distplot = sorted(self.distlist)
    elif self.distmax > self.distmin >= self.distmid:
        self.distlist = np.arange(self.distmin,self.distmax+0.001,self.distsep)
        self.distplot = sorted(self.distlist)
    elif self.distmin < self.distmid < self.distmax:
        self.distlist = np.hstack((np.arange(self.distmid,self.distmax+0.001,self.distsep),np.arange(self.distmid,self.distmin-0.001,-self.distsep)))
        self.distplot = sorted(np.delete(self.distlist,0))

    for whitflag in ['','_jw']:
        
        # Extract turbulent results
        
        self.test = 'v10_chisq1E-05_step5E-03_prev'+whitflag
        fitres    = np.empty((0,5))
        
        for j in self.distplot:
            strslope      = '0.000' if round(j,3)==0 else str('%.3f'%j).replace('-','m') if '-' in str(j) else 'p'+str('%.3f'%j)
            self.distpath = self.fitdir+'/'+self.selection+'/'+self.model+'/'+simpath+'/'+self.test+'/'+strslope+'/'
            self.quasar   = self.selection.split('/')[0]
            self.zabs     = self.selection.split('/')[1]
            self.sample   = self.selection.split('/')[2]
            if os.path.exists(self.distpath)==True:
                self.getresults()
                dist          = round(j,3)
                fitres        = np.vstack((fitres,[None]*5))
                fitres[-1,0]  = dist
                fitres[-1,1]  = self.turb_df
                fitres[-1,2]  = self.turb_chisq
                fitres[-1,3]  = self.turb_alpha
                fitres[-1,4]  = self.turb_error
                if '-' not in fitres[-1]:
                    if '--show' in sys.argv:
                        print '{:>7} '.format('%.3f'%dist),
                        print '{:>11}'.format('%i'%self.turb_df),
                        print '{:>11}'.format('%.4f'%self.turb_chisq),
                        print '{:>11}'.format('%.4f'%self.turb_alpha),
                        print '{:>11}'.format('%.4f'%self.turb_error)
                else:
                    fitres = np.delete(fitres,-1,0)
    
        # Plot turbulent chi-square curve
    
        fig = figure(figsize=(5,5))
        plt.subplots_adjust(left=0.15, right=0.97, bottom=0.1, top=0.95, hspace=0, wspace=0.2)
        
        x  = fitres[:,0]
        y  = fitres[:,2]
        ax = plt.subplot(211,xlim=[self.distmin,self.distmax])
        ax.errorbar(x,y,fmt='o',ms=6,markeredgecolor='none',ecolor='grey',alpha=0.7,color='black')
        ymin  = 0 if len(y)==0 else min(y)-1 if min(y)==max(y) else min(y)
        ymax  = 1 if len(y)==0 else max(y)+1 if min(y)==max(y) else max(y)
        self.fitparabola2(x,y,model='Turbulent',show=False)
        ylim(ymin,ymax)
        y_formatter = matplotlib.ticker.ScalarFormatter(useOffset=False)
        ax.yaxis.set_major_formatter(y_formatter)
        ax.get_xaxis().set_visible(False)
        ax.axhline(y=0,ls='dotted',color='black')
        #t1 = ax.text((self.distmin+self.distmax)/2,ymax-0.1*(ymax-ymin),self.selection,color='grey',ha='center',fontsize=6)
        #t1.set_bbox(dict(color='white', alpha=0.7, edgecolor=None)) 
        ax.set_ylabel('$\chi^2$',fontsize=10)
       
        x    = fitres[:,0]
        y    = fitres[:,3]
        yerr = fitres[:,4]
        ax   = plt.subplot(212,xlim=[self.distmin,self.distmax])
        ax.errorbar(x,y,yerr=yerr,fmt='o',ms=6,markeredgecolor='none',ecolor='grey',alpha=0.7,color='black')
        ymin  = -0.15 #0 if len(y)==0 else min(y)-1 if min(y)==max(y) else min(y-yerr)
        ymax  = +0.08 #1 if len(y)==0 else max(y)+1 if min(y)==max(y) else max(y+yerr)
        self.fitlinear(x,y,yerr,model='Turbulent',show=False)
        ylim(ymin,ymax)
        ax.axhline(y=0,ls='dotted',color='black')
        y_formatter = matplotlib.ticker.ScalarFormatter(useOffset=False)
        ax.yaxis.set_major_formatter(y_formatter)
        #t1 = ax.text((self.distmin+self.distmax)/2,ymax-0.1*(ymax-ymin),self.selection,color='grey',ha='center',fontsize=6)
        #t1.set_bbox(dict(color='white', alpha=0.7, edgecolor=None)) 
        ax.set_ylabel('$da/a$ $(10^{-5})$',fontsize=10)
        ax.set_xlabel('Applied distortion slope (in m/s/$\AA$)',fontsize=10)

        os.system('mkdir -p others')
        savefig('others/' + self.selection.replace('/','_').replace('.','z')+whitflag+'.pdf')
        plt.close(fig)
        
#==================================================================================================================
