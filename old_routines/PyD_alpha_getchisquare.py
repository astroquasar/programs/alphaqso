#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def getchisquare(model=None,predicted=None):

    if '--show' in sys.argv:
        print '\n{0:>7}{1:>12}{2:>12}{3:>12}{4:>12}{5:>12}{6:>12}{7:>12}{8:>12}{9:>12}{10:>12}{11:>12}{12:>12}\n'.format('dist','ther_ndf','therchisq','theralpha','thererror','turb_ndf','turbchisq','turbalpha','turberror','mom_ndf','momchisq','momalpha','momerror')

    fitres   = np.empty((0,13))

    for i in self.distplot:
        
        strslope      = '0.000' if round(i,3)==0 else str('%.3f'%i).replace('-','m') if '-' in str(i) else 'p'+str('%.3f'%i)
        self.distpath = 'sims' if '--simulation' in sys.argv else 'runs'
        self.distpath = self.fitdir+'/'+self.selection+'/'+self.model+'/'+self.distpath+'/'+self.test+'/'+strslope+'/'
        self.quasar   = self.selection.split('/')[0]
        self.zabs     = self.selection.split('/')[1]
        self.sample   = self.selection.split('/')[2]
        cond1 = os.path.exists(self.distpath)==True
        cond2 = round(i,2) not in self.discard

        if cond1 and cond2:

            self.getresults()

            dist          = round(i,3)
            fitres        = np.vstack((fitres,[None]*13))
            fitres[-1,0]  = dist
            fitres[-1,1]  = self.ther_df
            fitres[-1,2]  = self.ther_chisq
            fitres[-1,3]  = self.ther_alpha
            fitres[-1,4]  = self.ther_error
            fitres[-1,5]  = self.turb_df
            fitres[-1,6]  = self.turb_chisq
            fitres[-1,7]  = self.turb_alpha
            fitres[-1,8]  = self.turb_error
            fitres[-1,9]  = self.mom_df
            fitres[-1,10] = self.mom_chisq
            fitres[-1,11] = self.mom_alpha
            fitres[-1,12] = self.mom_error

            if '-' not in fitres[-1]:
                
                if '--show' in sys.argv:
                    print '{:>7} '.format('%.3f'%dist),
                    print '{:>10}'.format('%i'%self.ther_df),
                    print '{:>11}'.format('%.4f'%self.ther_chisq),
                    print '{:>11}'.format('%.4f'%self.ther_alpha),
                    print '{:>11}'.format('%.4f'%self.ther_error),
                    print '{:>11}'.format('%i'%self.turb_df),
                    print '{:>11}'.format('%.4f'%self.turb_chisq),
                    print '{:>11}'.format('%.4f'%self.turb_alpha),
                    print '{:>11}'.format('%.4f'%self.turb_error),
                    print '{:>11}'.format('%i'%self.mom_df),
                    print '{:>11}'.format('%.4f'%self.mom_chisq),
                    print '{:>11}'.format('%.4f'%self.mom_alpha),
                    print '{:>11}'.format('%.4f'%self.mom_error)
    
                chisq = self.turb_chisq if model=='turb' else self.ther_chisq if model=='ther' else self.mom_chisq
                ndf   = self.turb_df    if model=='turb' else self.ther_df    if model=='ther' else self.mom_df
                alpha = self.turb_alpha if model=='turb' else self.ther_alpha if model=='ther' else self.mom_alpha
                error = self.turb_error if model=='turb' else self.ther_error if model=='ther' else self.mom_error

            else:

                fitres = np.delete(fitres,-1,0)

    if '--show' in sys.argv:
        print '\n'

    ''' Calculate global chi-square '''
    
    dist,x,yabs,y = [],[],[],[]

    if len(fitres)>0:
        
        dist = [round(i,3) for i in fitres[:,0]]
        yndf = fitres[:,1]
        yabs = fitres[:,2]
        yred = yabs/yndf

        if predicted=='null':
            A = np.vander(dist,3)
            (coeffs, residuals, rank, sing_vals) = np.linalg.lstsq(A,yalp)
            f = np.poly1d(coeffs)
            x = np.arange(-0.5,0.5,0.0001)
            imid = abs(f(x)-min(f(x))).argmin()
            imid = abs(dist-x[imid]).argmin()
            minchisq = yalp[imid]
            y = [np.exp(-minchisq/2)*np.exp(-chisq/2.) for chisq in yalp]
            y = yalp
        else:
            A = np.vander(dist,3)
            (coeffs, residuals, rank, sing_vals) = np.linalg.lstsq(A,yabs)
            f = np.poly1d(coeffs)
            x = np.arange(-0.5,0.5,0.0001)
            imid = abs(f(x)-min(f(x))).argmin()
            imid = abs(dist-x[imid]).argmin()     
            minchisq = yred[imid]
#            self.norm = 10**int(math.log10(np.exp(-minchisq/2)*np.exp(-minchisq/2.)))
            y = [np.exp(-minchisq/2)*np.exp(-chisq/2.) for chisq in yred]
#            y = yred

    ''' Convert parameters as self for easy extraction '''

    self.dist     = dist
    self.yabs     = yabs
    self.fitres   = fitres
                    
#==================================================================================================================
